ALTER TABLE `tbl_proposal`
	ADD COLUMN `jenis_bantuan` ENUM('Uang','Barang') NULL DEFAULT 'Uang' COLLATE 'utf8_general_ci' AFTER `tanggal`,
	ADD COLUMN `usulan_barang` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8_general_ci' AFTER `jenis_bantuan`,
        ADD COLUMN `barang` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8_general_ci' AFTER `nama_file_upload`;
ALTER TABLE `tbl_survey`
	ADD COLUMN `barang` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8_general_ci' AFTER `latitude`;
