CREATE TABLE `management_system` (
	`id` BIGINT NOT NULL AUTO_INCREMENT COLLATE 'utf8_general_ci',
	`option_name` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`option_value` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`created_date` DATETIME NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`created_by` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`updated_date` DATETIME NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`updated_by` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`deleted_date` DATETIME NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `id` (`id`),
	INDEX `option_name` (`option_name`)
)
COLLATE='utf8_general_ci'
;