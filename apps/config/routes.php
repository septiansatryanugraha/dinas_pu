<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */

/*   VERIFIKATOR PAGE  */
$route['login-pengajuan'] = 'Verifikator/Login_verifikator/index';

/*   route modul profile */
$route['profil-verifikator'] = 'Verifikator/Verifikator_front/profil_verifikator';
$route['update-user-verifikator/(:any)'] = 'Verifikator/Verifikator_front/prosesUpdateUser/$1';

/*   route modul dashboard */
$route['beranda'] = 'Verifikator/Verifikator_front/index';
$route['ajax-data_proposal'] = 'Verifikator/Verifikator_front/ajax_list';
$route['load-data-foreign-verifikator'] = 'Verifikator/Verifikator_front/loadDataForeign';

/*   route modul verifikator */
$route['add-proposal'] = 'Verifikator/Verifikator_front/tambah_proposal';
$route['simpan-pengajuan-proposal'] = 'Verifikator/Verifikator_front/prosesAdd';
$route['edit-proposal-verifikator/(:any)'] = 'Verifikator/Verifikator_front/edit/$1';
$route['ubah-pengajuan-proposal/(:any)'] = 'Verifikator/Verifikator_front/prosesUpdateProposal/$1';
$route['delete-pengajuan-proposal-verifikator'] = 'Verifikator/Verifikator_front/prosesDelete';
/* End of VERIFIKATOR PAGE */


/*   ADMIN PAGE  */
$route['default_controller'] = "Default/Auth";
$route['404_override'] = 'Default/Not_found';
$route['login'] = 'Default/Auth';
$route['logout'] = 'Auth/logout';
$route['login-admin'] = 'Default/Auth';

/*   route modul dashboard  */
$route['dashboard'] = 'Dashboard/Dashboard/index';
$route['change-year'] = 'Dashboard/Dashboard/changeYear';
$route['load-data-foreign'] = 'Dashboard/Dashboard/loadDataForeign';

/*   route modul profile */
$route['profile'] = 'Setting/Profile/index';
$route['ubah-password'] = 'Setting/Profile/ubah_password';
$route['update-profile'] = 'Setting/Profile/update';

/*   route modul master kabupaten  */
$route['kabupaten'] = 'Master/Kabupaten/index';
$route['ajax-kabupaten'] = 'Master/Kabupaten/ajax_list';
$route['edit-kabupaten/(:any)'] = 'Master/Kabupaten/edit/$1';
$route['update-kabupaten/(:any)'] = 'Master/Kabupaten/prosesUpdate/$1';
$route['delete-kabupaten'] = 'Master/Kabupaten/prosesDelete';

/*   route modul master kecamatan */
$route['kecamatan'] = 'Master/Kecamatan/index';
$route['ajax-kecamatan'] = 'Master/Kecamatan/ajax_list';
$route['add-kecamatan'] = 'Master/Kecamatan/add';
$route['save-kecamatan'] = 'Master/Kecamatan/prosesAdd';
$route['edit-kecamatan/(:any)'] = 'Master/Kecamatan/edit/$1';
$route['update-kecamatan/(:any)'] = 'Master/Kecamatan/prosesUpdate/$1';
$route['delete-kecamatan'] = 'Master/Kecamatan/prosesDelete';

/*   route modul master desa */
$route['master-desa'] = 'Master/Desa/index';
//$route['load-data-foreign'] = 'Master/Desa/loadDataForeign';
$route['ajax-desa'] = 'Master/Desa/ajax_list';
$route['add-desa'] = 'Master/Desa/add';
$route['save-desa'] = 'Master/Desa/prosesAdd';
$route['edit-desa/(:any)'] = 'Master/Desa/edit/$1';
$route['update-desa/(:any)'] = 'Master/Desa/prosesUpdate/$1';
$route['delete-desa'] = 'Master/Desa/prosesDelete';

/*   route modul master pokmas */
$route['master-pokmas'] = 'Master/Pokmas/index';
$route['ajax-pokmas'] = 'Master/Pokmas/ajax_list';
$route['import-pokmas'] = 'Master/Pokmas/import';
$route['add-pokmas'] = 'Master/Pokmas/add';
$route['save-pokmas'] = 'Master/Pokmas/prosesAdd';
$route['edit-pokmas/(:any)'] = 'Master/Pokmas/edit/$1';
$route['update-pokmas/(:any)'] = 'Master/Pokmas/prosesUpdate/$1';
$route['delete-pokmas'] = 'Master/Pokmas/prosesDelete';
// ==
$route['cetak-pokmas/(:any)'] = 'Master/Pokmas/Cetak/$1';

/*   route modul master verifikator */
$route['verifikator'] = 'Master/Verifikator/index';
$route['ajax-verifikator'] = 'Master/Verifikator/ajax_list';
$route['add-verifikator'] = 'Master/Verifikator/add';
$route['save-verifikator'] = 'Master/Verifikator/prosesAdd';
$route['edit-verifikator/(:any)'] = 'Master/Verifikator/edit/$1';
$route['update-verifikator/(:any)'] = 'Master/Verifikator/prosesUpdate/$1';
$route['delete-verifikator'] = 'Master/Verifikator/prosesDelete';

/*   route modul form pengajuan proposal */
$route['master-proposal-pengajuan'] = 'Master/Master_proposal_pengajuan/index';
$route['ajax-pengajuan-proposal'] = 'Master/Master_proposal_pengajuan/ajax_list';
$route['add-pengajuan-proposal'] = 'Master/Master_proposal_pengajuan/add';
$route['save-pengajuan-proposal'] = 'Master/Master_proposal_pengajuan/prosesAdd';
$route['edit-pengajuan-proposal/(:any)'] = 'Master/Master_proposal_pengajuan/edit/$1';
$route['update-pengajuan-proposal/(:any)'] = 'Master/Master_proposal_pengajuan/prosesUpdate/$1';
$route['delete-pengajuan-proposal'] = 'Master/Master_proposal_pengajuan/prosesDelete';
$route['history-pengajuan-proposal/(:any)'] = 'Master/Master_proposal_pengajuan/history/$1';
$route['cetak-proposal/(:any)'] = 'Master/Master_proposal_pengajuan/cetak_proposal/$1';

/*   route modul master pengajuan proposal */
$route['master-proposal'] = 'Master/Master_proposal/index';
$route['ajax-proposal'] = 'Master/Master_proposal/ajax_list';
$route['edit-proposal/(:any)'] = 'Master/Master_proposal/edit/$1';
$route['update-proposal/(:any)'] = 'Master/Master_proposal/prosesUpdate/$1';
$route['checklist/(:any)'] = 'Master/Master_proposal/checklist/$1';
$route['update-checklist/(:any)'] = 'Master/Master_proposal/prosesChecklist/$1';
$route['print-ba/(:any)'] = 'Master/Master_proposal/cetak_ba/$1';
$route['print-checklist/(:any)'] = 'Master/Master_proposal/cetak_checklist/$1';
$route['print-sketsa-survey/(:any)'] = 'Master/Master_proposal/cetak_sketsa_survey/$1';

/*   route modul master survei  */
$route['master-survei'] = 'Master/Master_survei/index';
$route['ajax-survei'] = 'Master/Master_survei/ajax_list';
$route['edit-survei/(:any)'] = 'Master/Master_survei/Edit/$1';
$route['update-survei/(:any)'] = 'Master/Master_survei/prosesUpdate/$1';
$route['upload-gambar-survei/(:any)'] = 'Master/Master_survei/upload_gambar/$1';
$route['delete-gambar-survei/(:any)'] = 'Master/Master_survei/delete_gambar/$1';
$route['print-survei/(:any)'] = 'Master/Master_survei/cetak_survei/$1';

/*   route modul master proposal rekom  */
$route['master-proposal-rekom'] = 'Master/Master_proposal_rekom/index';
$route['ajax-proposal-rekom'] = 'Master/Master_proposal_rekom/ajax_list';
$route['edit-proposal-rekom/(:any)'] = 'Master/Master_proposal_rekom/Edit/$1';
$route['update-proposal-rekom/(:any)'] = 'Master/Master_proposal_rekom/prosesUpdate/$1';

/*   route modul master proposal penetapan */
$route['master-proposal-penetapan'] = 'Master/Master_proposal_penetapan/index';
$route['ajax-proposal-penetapan'] = 'Master/Master_proposal_penetapan/ajax_list';
$route['edit-proposal-penetapan/(:any)'] = 'Master/Master_proposal_penetapan/edit/$1';
$route['update-proposal-penetapan/(:any)'] = 'Master/Master_proposal_penetapan/prosesUpdate/$1';

/*   route modul master proposal nphd */
$route['master-proposal-nphd'] = 'Master/Master_proposal_nphd/index';
$route['ajax-proposal-nphd'] = 'Master/Master_proposal_nphd/ajax_list';
$route['edit-proposal-nphd/(:any)'] = 'Master/Master_proposal_nphd/edit/$1';
$route['update-proposal-nphd/(:any)'] = 'Master/Master_proposal_nphd/prosesUpdate/$1';
$route['upload-proposal-nphd/(:any)'] = 'Master/Master_proposal_nphd/upload/$1';
$route['process-upload-proposal-nphd/(:any)'] = 'Master/Master_proposal_nphd/prosesUpload/$1';
$route['lpj-proposal-nphd/(:any)'] = 'Master/Master_proposal_nphd/lpj/$1';
$route['process-lpj-proposal-nphd/(:any)'] = 'Master/Master_proposal_nphd/prosesLpj/$1';
$route['cetak-laporan/(:any)'] = 'Master/Master_proposal_nphd/cetak_laporan/$1';
$route['cetak-undangan/(:any)'] = 'Master/Master_proposal_nphd/cetak_undangan/$1';
$route['cetak-pokmas/(:any)'] = 'Master/Master_proposal_nphd/cetak_pokmas/$1';
$route['cetak-nphd/(:any)'] = 'Master/Master_proposal_nphd/cetak_nphd/$1';
$route['cetak-kwitansi/(:any)'] = 'Master/Master_proposal_nphd/cetak_kwitansi/$1';
$route['cetak-pakta/(:any)'] = 'Master/Master_proposal_nphd/cetak_pakta/$1';
$route['cetak-penggunaan-uang/(:any)'] = 'Master/Master_proposal_nphd/cetak_penggunaan_uang/$1';
$route['cetak-ta/(:any)'] = 'Master/Master_proposal_nphd/cetak_ta/$1';
$route['cetak-sptmh/(:any)'] = 'Master/Master_proposal_nphd/cetak_sptmh/$1';
$route['cetak-sp/(:any)'] = 'Master/Master_proposal_nphd/cetak_sp/$1';
$route['cetak-double-account/(:any)'] = 'Master/Master_proposal_nphd/cetak_double_account/$1';
$route['cetak-tanda-terima/(:any)'] = 'Master/Master_proposal_nphd/cetak_tanda_terima/$1';
$route['cetak-spkmp/(:any)'] = 'Master/Master_proposal_nphd/cetak_spkmp/$1';
$route['cetak-bukti-nphd/(:any)'] = 'Master/Master_proposal_nphd/cetak_bukti_nphd/$1';
$route['import-nphd'] = 'Master/Master_proposal_nphd/importData';
$route['import-data-nphd'] = 'Master/Master_proposal_nphd/prosesImportData';
$route['export-nphd'] = 'Master/Master_proposal_nphd/exportData';
$route['ajax-export-data-nphd'] = 'Master/Master_proposal_nphd/ajaxListExportData';
$route['export-data-nphd'] = 'Master/Master_proposal_nphd/prosesExportData';

/*   route modul user  */
$route['user'] = 'Setting/User/index';
$route['ajax-user'] = 'Setting/User/ajax_list';
$route['add-user'] = 'Setting/User/add';
$route['save-user'] = 'Setting/User/prosesAdd';
$route['edit-user/(:any)'] = 'Setting/User/Edit/$1';
$route['update-user/(:any)'] = 'Setting/User/prosesUpdate/$1';
$route['delete-user'] = 'Setting/User/prosesDelete';

/*   route modul menu  */
$route['menu'] = 'Setting/Menu_evo/index';
$route['ajax-menu'] = 'Setting/Menu_evo/menu_ajax';
$route['save-menu'] = 'Setting/Menu_evo/prosesTambah';
$route['save-sort-menu'] = 'Setting/Menu_evo/saveSort';
$route['icon'] = 'Setting/Menu_evo/icon';
$route['edit-menu/(:any)'] = 'Setting/Menu_evo/edit/$1';
$route['update-menu/(:any)'] = 'Setting/Menu_evo/prosesUpdate/$1';
$route['delete-menu'] = 'Setting/Menu_evo/prosesDelete';

/*   route modul grup  */
$route['user-grup'] = 'Setting/Grup/index';
$route['add-grup'] = 'Setting/Grup/add';
$route['save-grup'] = 'Setting/Grup/prosesTambah';
$route['edit-grup/(:any)'] = 'Setting/Grup/edit/$1';
$route['update-grup/(:any)'] = 'Setting/Grup/prosesUpdate/$1';
$route['delete-grup'] = 'Setting/Grup/prosesDelete';

/*    route modul Akses  */
$route['hak-akses/(:any)'] = 'Setting/Akses/hak_akses/$1';
$route['update-hak-akses/(:any)'] = 'Setting/Akses/update_hak_akses/$1';
/* End of ADMIN PAGE */

/* End of file routes.php */
/* Location: ./application/config/routes.php */