<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends AUTH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_sidebar');
    }

    public function index()
    {
        $data['userdata'] = $this->userdata;

        $data['page'] = "profile";
        $data['judul'] = "Profile";
        $data['datagrup'] = $this->M_admin->select_group();
        $data['dataStatus'] = $this->M_admin->select_status();
        parent::loadkonten('v_profil/profile', $data);
    }

    public function update()
    {
        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');
        $this->form_validation->set_rules('last_update_by', 'last_update_by', 'trim|required');
        $id = $this->input->post('id');

        $nama = $this->input->post('nama');
        $foto = $this->input->post('foto');

        if ($this->form_validation->run() == TRUE) {
            $data['nama'] = $nama;

            $config['upload_path'] = './upload/user/';
            $config['allowed_types'] = 'jpg|png';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('foto')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $data_foto = $this->upload->data();
                $data['foto'] = $data_foto['file_name'];
                $this->session->set_userdata($data);
            }

            $result = $this->M_admin->update($data, $id);
            if ($result > 0) {
                $this->session->set_userdata('nama', $nama);
                $out = array('status' => true, 'pesan' => ' Profile berhasil di update');
            } else {
                $out = array('status' => false, 'pesan' => 'Maaf data gagal update profile !');
            }
        } else {
            $out = array('status' => false, 'pesan' => 'Maaf data gagal update profile !');
        }

        echo json_encode($out);
    }

    public function ubah_password()
    {
        $this->form_validation->set_rules('passLama', 'Password Lama', 'trim|required');
        $this->form_validation->set_rules('passBaru', 'Password Baru', 'trim|required');
        $this->form_validation->set_rules('passKonf', 'Password Konfirmasi', 'trim|required');
        $this->form_validation->set_rules('last_update_by', 'last_update_by', 'trim|required');
        $password = $this->input->post('passLama');

        $id = $this->session->userdata('id');
        if ($this->form_validation->run() == TRUE) {
            if (md5($this->input->post('passLama')) == $this->session->userdata('password')) {
                if ($this->input->post('passBaru') != $this->input->post('passKonf')) {
                    $out = array('status' => false, 'pesan' => 'Password Baru dan Konfirmasi Password harus sama !');
                } else {
                    $data = [
                        'password' => md5($this->input->post('passBaru'))
                    ];

                    $result = $this->M_admin->update($data, $id);
                    if ($result > 0) {
                        $this->session->set_userdata('password', $password);
                        $this->updateProfil();
                        $out = array('status' => true, 'pesan' => ' Password berhasil di update');
                    } else {
                        $out = array('status' => false, 'pesan' => 'Maaf gagal update password !');
                    }
                }
            } else {
                $out = array('status' => false, 'pesan' => 'Maaf gagal update password !');
            }
        } else {
            $out = array('status' => false, 'pesan' => 'Maaf gagal update password kosong !');
        }
        echo json_encode($out);
    }
}

/* End of file Profile.php */
/* Location: ./application/controllers/Profile.php */