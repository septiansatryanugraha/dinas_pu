<?php
$this->load->view('_heading/_headerContent');
$foto_user = $this->session->userdata('foto');
$nama_user = $this->session->userdata('nama');

?>

<style type="text/css">
    #slider {
        margin-left: -2%;
    }

    #btn_loading {
        display: none;
    }

    #btn_loading2 {
        display: none;
    }
</style>
<!-- style loading -->
<div class="loading2"></div>
<!-- -->
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url() . (strlen($this->session->userdata('foto')) == 0) ? 'assets/tambahan/gambar/no-image.jpg' : 'upload/user/' . $this->session->userdata('foto'); ?>" alt="User profile picture">
                    <h3 class="profile-username text-center"><?php echo $this->session->userdata('nama') ?></h3>
                    <?php foreach ($datagrup as $data) { ?>
                        <?php if ($data->grup_id == $userdata->grup_id) { ?>
                            <p class="text-muted text-center"><?php echo $data->nama_grup; ?></p>
                            <?php
                        }
                    }

                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#settings" data-toggle="tab">Settings</a></li>
                    <li><a href="#password" data-toggle="tab">Changes Password</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="settings">
                        <form class="form-horizontal" id="form-update" method="POST">
                            <input type="hidden" class="form-control" placeholder="Name" name="id" value="<?php echo $this->session->userdata('id') ?>">
                            <input type="hidden" name="last_update_by" value="<?php echo $this->session->userdata('nama') ?>">
                            <div class="form-group">
                                <label for="inputUsername" class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" placeholder="Username" id="username" name="username" value="<?php echo $this->session->userdata('username'); ?>" disabled/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputNama" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" placeholder="Name" id="nama" name="nama" value="<?php echo $this->session->userdata('nama') ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputNama" class="col-sm-2 control-label">Level</label>
                                <div class="col-sm-6">
                                    <?php
                                    foreach ($datagrup as $data) {
                                        $level = $this->session->userdata('grup_id');
                                        if ($data->grup_id == $level) {

                                            ?>
                                            <input type="text" class="form-control" value="<?php echo $data->nama_grup; ?>" readonly>
                                            <?php
                                        }
                                    }

                                    ?>  
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputNama" class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-6">
                                    <?php
                                    foreach ($dataStatus as $data) {
                                        $status = $this->session->userdata('status');
                                        if ($data->id_status == $status) {

                                            ?>
                                            <p class="form-control-static"><?php echo $data->nama; ?></p>
                                            <?php
                                        }
                                    }

                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputFoto" class="col-sm-2 control-label">Photo</label>
                                <div class="col-sm-5">
                                    <input type="file" class="form-control" placeholder="Foto" name="foto">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div id="buka"> 
                                        <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
                                    </div>
                                    <div id="btn_loading">
                                        <button name="submit" type="submit" class="btn btn-primary btn-flat animated-gradient">&nbsp;Tunggu..</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!---- ini buat update password -->
                    <div class="tab-pane" id="password">
                        <form class="form-horizontal" id="form-update-pass" method="POST">
                            <input type="hidden" class="form-control" placeholder="Name" name="id" value="<?php echo $this->session->userdata('id') ?>">
                            <input type="hidden" name="last_update_by" value="<?php echo $this->session->userdata('nama') ?>">
                            <div class="form-group row">
                                <label for="passLama" class="col-sm-2 control-label">Password Lama</label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" placeholder="Password Lama" name="passLama">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="passBaru" class="col-sm-2 control-label">Password Baru</label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" placeholder="Password Baru" name="passBaru">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="passKonf" class="col-sm-2 control-label">Konfirmasi Password</label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" placeholder="Konfirmasi Password" name="passKonf">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div id="buka2"> 
                                        <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
                                    </div>
                                    <div id="btn_loading2">
                                        <button name="submit" type="submit" class="btn btn-primary btn-flat animated-gradient">&nbsp;Tunggu..</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    //Proses update ajax
    $(document).ready(function () {
        $('#form-update').submit(function (e) {
            $.ajax({
                url: '<?php echo base_url('update-profile'); ?>',
                type: "post",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    setTimeout(location.reload.bind(location), 500);
                    swal("Success", result.pesan, "success");
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
            e.preventDefault();
        })
    });

    //Proses update password ajax
    $(document).ready(function () {
        $('#form-update-pass').submit(function (e) {
            $.ajax({
                url: '<?php echo base_url('ubah-password'); ?>',
                type: "post",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $("#buka2").hide();
                    $("#btn_loading2").show();
                },
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $("#buka2").show();
                    $("#btn_loading2").hide();
                    setTimeout(location.reload.bind(location), 500);
                    swal("Success", result.pesan, "success");
                } else {
                    $("#buka2").show();
                    $("#btn_loading2").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
            e.preventDefault();
        })
    });
</script>