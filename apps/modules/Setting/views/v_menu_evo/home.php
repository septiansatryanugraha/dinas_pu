<?php $this->load->view('_heading/_headerContent') ?>

<style>
    .select-width {width:310px;}
    #btn_loading {
        display: none;
        width:900px;
    }
    #btn_loading2 {
        display: none;
        width:900px;
    }
    .menu-panjang {
        width: 400px;
    }
</style>

<section class="content">
    <div class="row">
        <div class="col-md-5">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <div class="box-header with-border">
                        <h3 class="box-title">Menu Order</h3>
                    </div>
                    <menu id="nestable-menu">
                        <button type="button" class="btn btn-warning" data-action="expand-all"><i class="fa fa-minus"></i> Expand All</button>
                        <button type="button" class="btn btn-primary" data-action="collapse-all"><i class="fa fa-plus"></i> Collapse All</button>
                    </menu>
                    <div class="dd menu-panjang" id="nestable">
                        <?php
                        $ref = [];
                        $items = [];
                        foreach ($dataMenu as $data) {
                            $thisRef = &$ref[$data->id_menu];
                            $thisRef['parent'] = $data->parent;
                            $thisRef['nama_menu'] = $data->nama_menu;
                            $thisRef['link'] = $data->link;
                            $thisRef['id_menu'] = $data->id_menu;
                            $thisRef['icon'] = $data->icon;
                            if ($data->parent == 0) {
                                $items[$data->id_menu] = &$thisRef;
                            } else {
                                $ref[$data->parent]['child'][$data->id_menu] = &$thisRef;
                            }
                        }

                        function get_menu($items, $class = 'dd-list')
                        {
                            $html = "<ol class=\"" . $class . "\" id=\"menu-id\">";
                            foreach ($items as $key => $value) {
                                $html .= '<li class="dd-item dd3-item" data-id="' . $value['id_menu'] . '" >
                    <div class="dd-handle dd3-handle"></div>
                    <div class="dd3-content"><i class="' . $value['icon'] . '"></i>&nbsp;&nbsp; <span id="label_show' . $value['id_menu'] . '">' . $value['nama_menu'] . '</span> 
                        <span class="span-right"><span id="link_show' . $value['id_menu'] . '"></span> &nbsp;&nbsp;' . anchor('edit-menu/' . $value['id_menu'], '<span tooltip="Edit Data"><i class="fa fa-edit"></i></span> ', ' class="ajaxify" ') . ' | 
                          <a class="del-button" id="' . $value['id_menu'] . '"><span tooltip="Delete Data"><i class="fa fa-trash"></i></span></a></span> 
                    </div>';
                                if (array_key_exists('child', $value)) {
                                    $html .= get_menu($value['child'], 'child');
                                }
                                $html .= "</li>";
                            }
                            $html .= "</ol>";

                            return $html;
                        }
                        print get_menu($items);

                        ?>
                    </div>
                </div>
                <div class="box-footer">
                    <div id="buka2">          
                        <button name="simpan" type="submit" id="save" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                    <div id="btn_loading2">
                        <button name="submit" type="submit" class="btn btn-primary btn-flat animated-gradient">&nbsp;Tunggu..</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Menu</h3>
                    </div>
                    <!-- form start -->
                    <form class="form-horizontal" id="form-tambah" method="POST">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama Menu</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="nama_menu" placeholder="nama menu" name="nama_menu" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Icon</label>
                                <div class="col-sm-5 wrap">
                                    <input type="text" class="form-control ikonpicker signup-input text-value" id="icon" placeholder="icon menu" name="icon" aria-describedby="sizing-addon2">
                                    <div class="ex">&times;</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Link Menu</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="link_menu" placeholder="link menu" name="link" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Kode Menu</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="kode_menu" placeholder="kode menu" name="kode_menu" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Jenis Menu</label>
                                <div class="col-sm-5">
                                    <select name="parent" id="type" class="form-control select-menu" aria-describedby="sizing-addon2">
                                        <option></option>
                                        <option value="0">Menu Utama</option>
                                        <option value="sub-menu">Sub Menu</option>
                                    </select> 
                                </div>
                            </div>
                            <div class="menu-show">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Sub Menu</label>
                                    <div class="col-sm-5">
                                        <select name="parent" class="form-control select-kategori-menu select-width">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Urutan</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="urutan_menu" placeholder="urutan" name="urutan" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Menu Available</label>
                                <div class="col-xs-3">
                                    <input type='checkbox' name='menu_file[]' value="view" />&nbsp; View<br>
                                    <input type='checkbox' name='menu_file[]' value="add" />&nbsp; Add<br>
                                    <input type='checkbox' name='menu_file[]' value="edit" />&nbsp; Edit<br>
                                    <input type='checkbox' name='menu_file[]' value="del" />&nbsp; Delete<br>                   
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div id="buka"> 
                                <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                                <input type="hidden" id="nestable-output">
                            </div>
                            <div id="btn_loading">
                                <button name="submit" type="submit" class="btn btn-primary btn-flat animated-gradient">&nbsp;Tunggu..</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="<?php echo base_url(); ?>assets/plugins/menu/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/menu/jquery.nestable.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ex').click(function () {
            $('.signup-input').val("");
        });
    });
</script>
<script>
    $(document).ready(function () {
        var updateOutput = function (e) {
            var list = e.length ? e : $(e.target), output = list.data('output');
            if (window.JSON) {
                output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };

        // activate Nestable for list 1
        $('#nestable').nestable({group: 1}).on('change', updateOutput);

        // output initial serialised data
        updateOutput($('#nestable').data('output', $('#nestable-output')));

        $('#nestable-menu').on('click', function (e) {
            var target = $(e.target), action = target.data('action');
            if (action === 'expand-all') {
                $('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.dd').nestable('collapseAll');
            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('#form-tambah').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            swal({
                title: "Simpan Data?",
                text: "Apakah Anda Yakin?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Simpan",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $.ajax({
                    beforeSend: function () {
                        $("#buka").hide();
                        $("#btn_loading").show();
                    },
                    method: 'POST',
                    url: '<?php echo base_url('save-menu'); ?>',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                }).done(function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        document.getElementById("form-tambah").reset();
                        $("#buka").show();
                        $("#btn_loading").hide();
                        setTimeout(location.reload.bind(location), 700);
                        toastr.success(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                    } else {
                        $("#buka").show();
                        $("#btn_loading").hide();
                        toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                    }
                })
            });
        });

        $('.dd').on('change', function () {
            var dataString = {
                data: $("#nestable-output").val(),
            };
//            console.log(dataString);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('save-sort-menu'); ?>",
                data: dataString,
                cache: false,
                success: function (data) {
                }, error: function (xhr, status, error) {
                    alert(error);
                },
            });
        });

        $("#save").click(function () {
            $("#buka2").hide();
            $("#btn_loading2").show();
            var dataString = {
                data: $("#nestable-output").val(),
            };
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('save-sort-menu'); ?>",
                data: dataString,
                cache: false,
                success: function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        $("#btn_loading2").hide();
                        $("#buka2").show();
                        setTimeout(location.reload.bind(location), 500);
                        toastr.success(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                    } else {
                        $("#btn_loading2").hide();
                        $("#buka2").show();
                        toastr.error(result.pesan, 'Warning', {timeOut: 5000}, toastr.options = {
                            "closeButton": true});
                    }
                }
            });
        });

        $(document).on("click", ".del-button", function () {
            var id = $(this).attr('id');
            swal({
                title: "Deleted Data?",
                text: "Are you sure deleted this data ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Delete",
                confirmButtonColor: '#dc1227',
                customClass: ".sweet-alert button",
                closeOnConfirm: false,
                html: true
            }, function () {
                $(".confirm").attr('disabled', 'disabled');
                $.ajax({
                    method: "POST",
                    url: "<?php echo base_url('delete-menu'); ?>",
                    data: {id: id},
                    success: function (data) {
                        var result = jQuery.parseJSON(data);
                        if (result.status == true) {
                            $("li[data-id='" + id + "']").remove();
                            swal("Success", result.pesan, "success");
                        } else {
                            swal("Success", result.pesan, "success");
                        }
                    }
                });
            });
        });

        $(function () {
            $('.menu-show').hide();
            $('#type').change(function () {
                if ($('#type').val() == 'sub-menu') {
                    $('.menu-show').show();
                } else {
                    $('.menu-show').hide();
                }
            });
        });
    });
</script>
<script type="text/javascript">
    // untuk select2 ajak pilih menu
    $(function () {
        $(".select-menu").select2({
            placeholder: " -- Pilih Jenis Menu -- "
        });
    });

    $(document).ready(function () {
        $('.select-kategori-menu').select2({
            placeholder: 'Pilih Sub Menu',
            allowClear: true,
            ajax: {
                url: "<?php echo base_url('ajax-menu') ?>",
                dataType: "json",
                delay: 250,
                data: function (params) {
                    return {
                        cari: params.term
                    };
                },
                processResults: function (data) {
                    var results = [];
                    $.each(data, function (index, item) {
                        results.push({
                            id: item.id_menu,
                            text: item.nama_menu
                        });
                    });
                    return{
                        results: results
                    };
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ikonpicker').iconpicker();
    });
</script>
<script type='text/javascript'>
    (function () {
        if (window.localStorage) {
            if (!localStorage.getItem('firstLoad')) {
                localStorage['firstLoad'] = true;
                window.location.reload();
            } else
                localStorage.removeItem('firstLoad');
        }
    })();
</script>