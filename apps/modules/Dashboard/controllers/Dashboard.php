<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends AUTH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_total');
        $this->load->model('M_kabupaten');
        $this->load->model('M_kecamatan');
        $this->load->model('M_desa');
        $this->load->model('M_pokmas');
    }

    public function index()
    {
        $data['pokmas'] = $this->M_total->total_pokmas();
        $data['proposal'] = $this->M_total->total_proposal();
        $data['proposal_survei'] = $this->M_total->total_proposal_survei();
        $data['proposal_rekomendasi'] = $this->M_total->total_proposal_rekomendasi();
        $data['proposal_penetapan'] = $this->M_total->total_proposal_penetapan();
        $data['proposal_nphd'] = $this->M_total->total_proposal_nphd();
        $data['graph'] = $this->M_total->get_total_proposal();
        $data['log'] = $this->M_total->log();
        $data['userdata'] = $this->userdata;

        $data['page'] = "home";
        $data['judul'] = "Beranda";
        parent::loadkonten('home', $data);
    }

    public function changeYear()
    {
        $year = $this->input->post('year');
        $_SESSION['year'] = $year;
        echo json_encode($_SESSION);
    }

    public function loadDataForeign()
    {
        $option = "<option></option>";
        $modul = $this->input->post('modul');

        switch ($modul) {
            case "kecamatan":
                $idKabupaten = $this->input->post('id_kabupaten');
                $resultKecamatan = $this->M_kecamatan->select(array('id_kabupaten' => $idKabupaten));
                if ($resultKecamatan != null) {
                    foreach ($resultKecamatan as $k => $val) {
                        $option .= "<option value='{$val->id}'>{$val->kecamatan}</option>";
                    }
                }
                break;
            case "desa":
                $idKecamatan = $this->input->post('id_kecamatan');
                $resultDesa = $this->M_desa->select(array('id_kecamatan' => $idKecamatan));
                if ($resultDesa != null) {
                    foreach ($resultDesa as $k => $val) {
                        $option .= "<option value='{$val->id}'>{$val->desa}</option>";
                    }
                }
                break;
            case "kelompok":
                $idDesa = $this->input->post('id_desa');
                $resultKelompok = $this->M_pokmas->select(array('id_desa' => $idDesa, 'status' => 'Aktif'));
                if ($resultKelompok != null) {
                    foreach ($resultKelompok as $k => $val) {
                        $option .= "<option value='{$val->id_pokmas}' pokmas_nama_ketua_{$val->id_pokmas}='{$val->nama_ketua}' pokmas_alamat_{$val->id_pokmas}='{$val->alamat}'>{$val->nama_kelompok}</option>";
                    }
                }
                break;
        }

        echo $option;
    }
}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */