</div>
<!-- /.content-wrapper -->
<!-- footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Dashboard Admin
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <?php echo date("Y") ?> Dinas PU Sumber Daya Air</strong> All rights reserved.
</footer>
<div class="control-sidebar-bg"></div>
</div>
<!-- js -->
<!-- REQUIRED JS SCRIPTS -->
<!-- FastClick -->
<script src="<?php echo base_url(); ?>admin-lte/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>admin-lte/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>admin-lte/dist/js/demo.js"></script>
<script src="<?php echo base_url(); ?>admin-lte/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url(); ?>admin-lte/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>admin-lte/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url(); ?>admin-lte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- checkbox -->
<script src="<?php echo base_url(); ?>admin-lte/plugins/iCheck/icheck.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url(); ?>admin-lte/dist/js/app.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-summernote/summernote.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/ajax.js"></script>
<script src="<?php echo base_url(); ?>assets/toastr/toastr.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>admin-lte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/icon-picker/fontawesome-iconpicker.js"></script>

<div id="loadingAjak"></div>
<script type="text/javascript">
    $('.sidebar-menu a').click(function () {
        // Remove all class active and display none
        $(".sidebar-menu .active").removeClass("active");
        $(".sidebar-menu .treeview-menu.menu-open").css("display", "none");
        $(".sidebar-menu .treeview-menu.menu-open").removeClass("menu-open");

        // add class active and set display block
        $('.' + this.getAttribute("parent")).addClass('active');
        $('.' + this.getAttribute("child")).addClass('active');
        $('.' + this.getAttribute("child")).closest("ul.treeview-menu").addClass('menu-open');
        $('.' + this.getAttribute("child")).closest("ul.treeview-menu").css("display", "block");
    });
    //klik loading ajax
    $(document).ready(function () {
        $('.klik').click(function () {
            var url = $(this).attr('href');
            $("#loading2").show().html("<img src='<?php base_url(); ?>assets/tambahan/gambar/loaders.gif' height='130'> ");
            $('#loadingAjak').show();
            $.ajax({
                complete: function () {
                    $("#loading2").hide();
                    $('#loadingAjak').hide();
                }
            });
            return true;
        });
    });
    // handle ajax link dengan konten
    var base_url = '<?= base_url(); ?>';
    var ajaxify = [null, null, null];

    $('.content-wrapper').on('click', '.ajaxify', function (e) {
        var ele = $(this);
        function_ajaxify(e, ele);
    });

    $('.sidebar-menu').on('click', ' li > a.ajaxify', function (e) {
        var ele = $(this);
        function_ajaxify(e, ele);
    });

    $('.pull-left').on('click', 'a.ajaxify', function (e) {
        var ele = $(this);
        function_ajaxify(e, ele);
    });
    // loading ajax
    $.ajaxSetup({
        beforeSend: function (xhr) {
            $("#loading2").show().html("<img src='<?php base_url(); ?>assets/tambahan/gambar/loaders.gif' height='130'> ");
            $('#loadingAjak').show();
        },
        complete: function () {
            $("#loading2").hide();
            $('#loadingAjak').hide();
        },
        error: function () {
            $("#loading2").hide();
            $('#loadingAjak').hide();
        }
    });
    // load konten ajax
    var function_ajaxify = function (e, ele) {
        e.preventDefault();
        var url = $(ele).attr("href");
        //var pageContent = $('.page-content');
        var pageContentBody = $('.content-wrapper');
        if (url != ajaxify[2]) {
            ajaxify.push(url);
            history.pushState(null, null, url);
        }
        ajaxify = ajaxify.slice(-3, 5);
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            data: {status_link: 'ajax'},
            dataType: "html",
            success: function (res) {
                if (res.search("content-login") > 0) {
                    window.location = base_url + 'login';
                } else {
                    //hide_loading_bar();
                    pageContentBody.html(res);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: 'error/error_404',
                    data: {url: ajaxify[1], url1: ajaxify[2]},
                    dataType: "html",
                    success: function (res) {
                        if (res == 'out') {
                            window.location = base_url + 'login';
                        } else {
                            //hide_loading_bar();
                            pageContentBody.html(res);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        //hide_loading_bar();
                    }
                });
            }
        });
    }

    $("#filter_year").change(function () {
        $.ajax({
            url: '<?php echo base_url('change-year'); ?>',
            method: 'POST',
            data: {year: $(this).val()},
            success: function (data) {
                var result = jQuery.parseJSON(data);
                setTimeout(location.reload.bind(location), 450);
            },
        });
    });

    $('.format_currency').keyup(function () {
        this.value = formatRupiah(this.value);
    });

    function formatRupiah(bilangan, prefix) {
        var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    function fileValidationPdf(size, labelsize) {
        $('.upload-pdf').change(function () {
            var fileInput = $(this).val();
            if (fileInput != '') {
                var checkfile = fileInput.toLowerCase();
                if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
                    toastr.error('Maaf, Format file harus pdf.', 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                    $(this).val('');
                    return false;
                }
                if (this.files[0].size > size) { // validasi ukuran size file
                    toastr.error('Maximal File ' + labelsize, 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                    $(this).val('');
                    return false;
                }
                return true;
            }
        });
    }

    function fileValidationImage(size, labelsize) {
        $('.upload-image').change(function () {
            var fileInput = $(this).val();
            if (fileInput != '') {
                var checkfile = fileInput.toLowerCase();
                if (!checkfile.match(/(\.jpg|\.jpeg|\.png|\.gif)$/)) { // validasi ekstensi file
                    toastr.error('Maaf, Format file harus .jpeg |.jpg |.png | only.', 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                    $(this).val('');
                    return false;
                }
                if (this.files[0].size > size) { // validasi ukuran size file
                    toastr.error('Maximal File ' + labelsize, 'Warning', {timeOut: 5000}, toastr.options = {
                        "closeButton": true});
                    $(this).val('');
                    return false;
                }
                return true;
            }
        });
    }
</script> 
</html>