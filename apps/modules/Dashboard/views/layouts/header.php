<?php
$photo = "assets/tambahan/gambar/no-image.jpg";
if (strlen($this->session->userdata('foto')) > 0) {
    $photo = "upload/user/" . $this->session->userdata('foto');
}
if (!isset($_SESSION['year'])) {
    $_SESSION['year'] = date('Y');
}
if (strlen($_SESSION['year']) == 0) {
    $_SESSION['year'] = date('Y');
}

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Dashboard | Dinas Pekerjaan Umum Sumber Daya Air </title>
        <link rel="icon" href="<?php echo base_url(); ?>assets/tambahan/gambar/logo-icon.png">
        <!-- meta -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- css --> 
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/tambahan.css">
        <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/media_manager.css">-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/eksternal/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/icon-picker/fontawesome5/css/all.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/icon-picker/fontawesome-iconpicker.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/eksternal/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-summernote/summernote.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/skin-blue.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/all.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/toastr/toastr.css">
        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <style type="text/css">
            .overflow-scroll {
                overflow-x: auto;
            }
            .menubar-icon {
                font-size: 30px;
            }
            .menubar-icon-kecil {
                font-size: 20px;
                color:#28a745;
            }
            .animated-gradient {
                background: linear-gradient(-45deg,#007cba 28%,#006395 0,#006395 72%,#007cba 0);
                width: 10%;
                background-size: 200% auto;
                background-position: 0 100%;
                border-color: #007cba;
                animation: gradient 1s infinite;
                animation-fill-mode: forwards;
                animation-timing-function: linear;
            }
            @keyframes gradient { 
                0%   { background-position: 0 0; }
                100% { background-position: -200% 0; }
            }
            .merah {
                color:red;
            }
            .thumb1 { 
                background: url(blah.jpg) 50% 50% no-repeat; /* 50% 50% centers image in div */
                width: 150px;
                height: 150px;
            }
            .jarak-pop {
                margin-left: -10px;
            }
            .pdf {
                color: red;
            }
            @media only screen and (min-width: 800px) {
                .table-responsive {
                    overflow: hidden;
                }
            }
            .control-sidebar {
                .tab-content {
                    height: calc(100vh - 135px);
                    overflow-y: scroll;
                    overflow-x: hidden;
                }
            }
            @media (min-width: 768px) {
                .control-sidebar {
                    .tab-content {
                        height: calc(100vh - 85px);
                    }
                }
            }
            /* START TOOLTIP STYLES */
            [tooltip] {
                position: relative; /* opinion 1 */
            }
            /* Applies to all tooltips */
            [tooltip]::before,
            [tooltip]::after {
                text-transform: none; /* opinion 2 */
                font-size: .9em; /* opinion 3 */
                line-height: 1;
                user-select: none;
                pointer-events: none;
                position: absolute;
                display: none;
                opacity: 0;
            }
            [tooltip]::before {
                content: '';
                border: 5px solid transparent; /* opinion 4 */
                z-index: 1001; /* absurdity 1 */
            }
            [tooltip]::after {
                content: attr(tooltip); /* magic! */
                /* most of the rest of this is opinion */
                font-family: Helvetica, sans-serif;
                text-align: center;
                /* 
                  Let the content set the size of the tooltips 
                  but this will also keep them from being obnoxious
                */
                min-width: 3em;
                max-width: 21em;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
                padding: 1ch 1.5ch;
                border-radius: .3ch;
                box-shadow: 0 1em 2em -.5em rgba(0, 0, 0, 0.35);
                background: #333;
                color: #fff;
                z-index: 1000; /* absurdity 2 */
            }
            /* Make the tooltips respond to hover */
            [tooltip]:hover::before,
            [tooltip]:hover::after {
                display: block;
            }
            /* don't show empty tooltips */
            [tooltip='']::before,
            [tooltip='']::after {
                display: none !important;
            }
            /* FLOW: UP */
            [tooltip]:not([flow])::before,
            [tooltip][flow^="up"]::before {
                bottom: 150%;
                border-bottom-width: 0;
                border-top-color: #333;
            }
            [tooltip]:not([flow])::after,
            [tooltip][flow^="up"]::after {
                bottom: calc(150% + 5px);
            }
            [tooltip]:not([flow])::before,
            [tooltip]:not([flow])::after,
            [tooltip][flow^="up"]::before,
            [tooltip][flow^="up"]::after {
                left: 50%;
                transform: translate(-50%, -.5em);
            }
            /* KEYFRAMES */
            @keyframes tooltips-vert {
                to {
                    opacity: .9;
                    transform: translate(-50%, 0);
                }
            }
            @keyframes tooltips-horz {
                to {
                    opacity: .9;
                    transform: translate(0, -50%);
                }
            }
            /* FX All The Things */ 
            [tooltip]:not([flow]):hover::before,
            [tooltip]:not([flow]):hover::after,
            [tooltip][flow^="up"]:hover::before,
            [tooltip][flow^="up"]:hover::after,
            [tooltip][flow^="down"]:hover::before,
            [tooltip][flow^="down"]:hover::after {
                animation: tooltips-vert 300ms ease-out forwards;
            }
            [tooltip][flow^="left"]:hover::before,
            [tooltip][flow^="left"]:hover::after,
            [tooltip][flow^="right"]:hover::before,
            [tooltip][flow^="right"]:hover::after {
                animation: tooltips-horz 300ms ease-out forwards;
            }
            .select-session{
                width: 100px;
            }
        </style>
    </head>
    <div id='LoadingDulu'></div>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <!-- header -->
            <header class="main-header">
                <!-- Logo -->
                <a href="<?php echo base_url('dashboard'); ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><small>WEB</small></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>WEB</b> ADMIN</span>
                </a>
                <!-- nav -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu" style="padding-top: 11px; width: 200px;">
                                <span style="color: #fff;">Filter Tahun :</span>
                                <select id="filter_year" class="select-session" name="filter_year">
                                    <?php for ($i = 2010; $i <= 2050; $i++) { ?>
                                        <option value="<?php echo $i; ?>" <?php if ($i == $_SESSION['year']) echo 'selected'; ?> ><?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <!-- User Account Menu -->
                            <li class="dropdown user user-menu">
                                <!-- Menu Toggle Button -->
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <!-- The user image in the navbar-->
                                    <img src="<?php echo base_url() . $photo; ?>" class="user-image" alt="User Image">
                                    <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                    <span class="hidden-xs"><?php echo $this->session->userdata('nama') ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- The user image in the menu -->
                                    <li class="user-header">
                                        <img src="<?php echo base_url() . $photo; ?>" class="img-circle" alt="User Image">
                                        <p>
                                            <?php echo $this->session->userdata('nama') ?>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a class="ajaxify klik " href="<?php echo site_url('profile'); ?>" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a class="ajaxify klik" href="<?php echo site_url('Default/Auth/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <div id ="loading2"></div>
                <section class="sidebar">
                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url() . $photo; ?>" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $this->session->userdata('nama') ?></p>
                            <!-- Status -->
                            <a href="<?php echo base_url(); ?>assets/#"> Web Admin</a>
                        </div>
                    </div>
                    <!-- Sidebar Menu -->
                    <div id="menu">
                        <ul class="sidebar-menu">
                            <li class="header">LIST MENU</li>
                            <!-- menu sidebar list-->
                            <?php
                            $this->load->model('M_sidebar');
                            $menuActiveNow = $this->uri->rsegment(1);
                            $menu = $this->M_sidebar->left_menu();
//                            echo '<pre>';
//                            print_r($menuActiveNow);
//                            echo '</pre>';
//                            echo '<br>';
                            $getMenuActiveNow = $this->M_sidebar->selectByController($menuActiveNow);
                            if ($menu->num_rows() > 0) {
                                foreach ($menu->result() as $row) {
                                    $menu_child = $this->M_sidebar->left_menu_child($row->id_menu);
                                    $access = $this->M_sidebar->access('view', $row->kode_menu);
                                    // jika view 1 (tampilkan menu)
                                    if ($access->menuview == 1) {
                                        if ($menu_child->num_rows() > 0) {
                                            $activeParent = "";
                                            $open = "";
                                            if ($getMenuActiveNow->parent == $row->id_menu) {
                                                $activeParent = "active";
                                                $open = "open";
                                            }
                                            echo "  <li class='treeview ajaxify parent_" . $row->kode_menu . " " . $activeParent . "'><a href='javascript:;'><i class='" . $row->icon . "'></i>&nbsp;&nbsp;
                                                    <span class='title'>" . $row->nama_menu . "</span>
                                                    <span class='pull-right-container " . $open . "'>
                                                    <i class='fa fa-angle-left pull-right'></i></span></a>";
                                            // untuk sub menu dropdownya
                                            echo "<ul class='treeview-menu'>";
                                            if ($menu_child->num_rows() > 0) {
                                                foreach ($menu_child->result() as $obj) {
                                                    $access_child = $this->M_sidebar->access('view', $obj->kode_menu);
                                                    if ($access_child->menuview == 1) {
                                                        $active = ($getMenuActiveNow->id_menu == $obj->id_menu) ? 'active' : '';
                                                        echo "  <li class='child_" . $obj->link . " " . $active . "'>
                                                                    <a class='klik ajaxify' href='" . site_url($obj->link) . "' parent='parent_" . $row->kode_menu . "' child='child_" . $obj->link . "'>
                                                                        <i class='" . $obj->icon . "'></i>&nbsp;&nbsp;
                                                                        <span class='title'>" . $obj->nama_menu . "</span>
                                                                    </a>
                                                                </li>";
                                                    }
                                                }
                                            }
                                            echo"</ul></li>";
                                        } else {
                                            $activeParent = ($menuActiveNow == $row->controller) ? 'active' : '';
                                            echo "  <li class='child_" . $row->link . " " . $activeParent . "'>
                                                        <a class='klik ajaxify' href='" . site_url($row->link) . "' child='child_" . $row->link . "'>
                                                            <i class='" . $row->icon . "'></i>&nbsp;&nbsp;
                                                            <span class='title'>" . $row->nama_menu . "</span>
                                                        </a>
                                                    </li>";
                                        }
                                    }
                                }
                            }

                            ?>
                        </ul>
                    </div>
                    <!-- /.sidebar-menu -->
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <script type="text/javascript">
                    // untuk select2 ajak pilih tipe
                    $(function () {
                        $(".select-session").select2({
                            placeholder: " -- pilih tahun -- "
                        });
                    });
                </script>