<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_total extends CI_Model
{
    private $idUser = null;
    private $year = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->idUser = $this->session->userdata('id');
        $this->idGrupUser = $this->session->userdata('grup_id');
        $this->year = isset($_SESSION['year']) ? $_SESSION['year'] : date('Y');
    }

    public function total_pokmas()
    {
        $this->db->select('*');
        $this->db->from('tbl_pokmas');
        if ($this->idGrupUser > 2) {
            $this->db->where("id_grup", $this->idGrupUser);
        }
        if (strlen($this->year) > 0) {
            $this->db->where("YEAR(created_date)", $this->year);
        }
        $data = $this->db->get();

        return $data->num_rows();
    }

    public function total_proposal()
    {
        $this->db->select('tbl_proposal.*');
        $this->db->from('tbl_proposal');
        $this->db->join('tbl_pokmas', 'tbl_pokmas.id_pokmas = tbl_proposal.id_pokmas', 'left');
        $this->db->where('tbl_proposal.deleted_date IS NULL');
        if ($this->idGrupUser > 2) {
            $this->db->where("tbl_pokmas.id_grup", $this->idGrupUser);
        }
        if (strlen($this->year) > 0) {
            $this->db->where("YEAR(tbl_proposal.created_date)", $this->year);
        }
        $data = $this->db->get();

        return $data->num_rows();
    }

    public function total_proposal_survei()
    {
        $sql = "SELECT tbl_proposal.*
                FROM tbl_proposal 
                LEFT JOIN tbl_pokmas ON tbl_pokmas.id_pokmas = tbl_proposal.id_pokmas
                WHERE tbl_proposal.survei='Selesai Survey'";
        if ($this->idGrupUser > 2) {
            $sql .= " AND tbl_pokmas.id_grup = '{$this->idGrupUser}'";
        }
        if (strlen($this->year) > 0) {
            $sql .= " AND YEAR(tbl_proposal.created_date) = '{$this->year}'";
        }
        $data = $this->db->query($sql);

        return $data->num_rows();
    }

    public function total_proposal_rekomendasi()
    {
        $sql = "SELECT tbl_proposal.*
                FROM tbl_proposal 
                LEFT JOIN tbl_pokmas ON tbl_pokmas.id_pokmas = tbl_proposal.id_pokmas
                WHERE tbl_proposal.status='Rekom Bidang'";
        if ($this->idGrupUser > 2) {
            $sql .= " AND tbl_pokmas.id_grup = '{$this->idGrupUser}'";
        }
        if (strlen($this->year) > 0) {
            $sql .= " AND YEAR(tbl_proposal.created_date) = '{$this->year}'";
        }
        $data = $this->db->query($sql);

        return $data->num_rows();
    }

    public function total_proposal_penetapan()
    {
        $sql = "SELECT tbl_proposal.*
                FROM tbl_proposal 
                LEFT JOIN tbl_pokmas ON tbl_pokmas.id_pokmas = tbl_proposal.id_pokmas
                WHERE tbl_proposal.status='Penetapan'";
        if ($this->idGrupUser > 2) {
            $sql .= " AND tbl_pokmas.id_grup = '{$this->idGrupUser}'";
        }
        if (strlen($this->year) > 0) {
            $sql .= " AND YEAR(tbl_proposal.created_date) = '{$this->year}'";
        }
        $data = $this->db->query($sql);

        return $data->num_rows();
    }

    public function total_proposal_nphd()
    {
        $sql = "SELECT tbl_proposal.*
                FROM tbl_proposal 
                LEFT JOIN tbl_pokmas ON tbl_pokmas.id_pokmas = tbl_proposal.id_pokmas
                WHERE tbl_proposal.status='NPHD'";
        if ($this->idGrupUser > 2) {
            $sql .= " AND tbl_pokmas.id_grup = '{$this->idGrupUser}'";
        }
        if (strlen($this->year) > 0) {
            $sql .= " AND YEAR(tbl_proposal.created_date) = '{$this->year}'";
        }
        $data = $this->db->query($sql);

        return $data->num_rows();
    }

    public function log()
    {
        $sql = "SELECT tbl_log.*
                FROM tbl_log 
                LEFT JOIN tbl_proposal ON tbl_proposal.id_proposal = tbl_log.id_proposal
                LEFT JOIN tbl_pokmas ON tbl_pokmas.id_pokmas = tbl_proposal.id_pokmas
                WHERE tbl_log.status = 'Belum Verifikasi'";
        if ($this->idGrupUser > 2) {
            $sql .= " AND tbl_pokmas.id_grup = '{$this->idGrupUser}'";
        }
        if (strlen($this->year) > 0) {
            $sql .= " AND YEAR(tbl_log.created_date) = '{$this->year}'";
        }
        $sql .= " ORDER BY tbl_log.id_history DESC limit 30";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function get_total_proposal()
    {
        $sql = "SELECT tbl_proposal.tgl_help, COUNT(tbl_proposal.id_proposal) AS id_proposal
                FROM tbl_proposal
                LEFT JOIN tbl_pokmas ON tbl_pokmas.id_pokmas = tbl_proposal.id_pokmas
                WHERE tbl_proposal.tgl_help between DATE_ADD(date(now()), INTERVAL -30 DAY) and date(now())";
        if ($this->idGrupUser > 2) {
            $sql .= " AND tbl_pokmas.id_grup = '{$this->idGrupUser}'";
        }
        if (strlen($this->year) > 0) {
            $sql .= " AND YEAR(tbl_proposal.created_date) = '{$this->year}'";
        }
        $sql .= " GROUP BY tbl_proposal.tgl_help";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }
    // public function get_total_proposal() {
    //     $query = $this->db->query("SELECT tanggal,COUNT(id_proposal) AS id_proposal FROM tbl_proposal WHERE tanggal between DATE_ADD(date(now()), INTERVAL -30 DAY) and date(now()) GROUP BY tanggal");
    //     if ($query->num_rows() > 0) {
    //         foreach ($query->result() as $data) {
    //             $hasil[] = $data;
    //         }
    //         return $hasil;
    //     }
    // }
}
