<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_verifikator extends CI_Model
{
    const __tableName = 'tbl_proposal';
    const __tableName2 = 'tbl_verifikator';
    const __tableName3 = 'tbl_kabupaten';
    const __tableId = 'id_proposal';
    const __tableId2 = 'id';

    private $idUser = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->idUser = $this->session->userdata('id');
    }

    function getProposalVerifikator($isAjaxList = 0)
    {

        $sql = "SELECT " . self::__tableName . ".*,
                tbl_pokmas.nama_kelompok as nama_kelompok, 
                tbl_desa.desa as desa,
                tbl_kecamatan.kecamatan as kecamatan,
                tbl_kabupaten.kabupaten as kabupaten
                FROM " . self::__tableName . "
                LEFT JOIN tbl_pokmas ON tbl_pokmas.id_pokmas = " . self::__tableName . ".id_pokmas
                LEFT JOIN tbl_desa ON tbl_desa.id = tbl_pokmas.id_desa
                LEFT JOIN tbl_kecamatan ON tbl_kecamatan.id = tbl_desa.id_kecamatan
                LEFT JOIN tbl_kabupaten ON tbl_kabupaten.id = tbl_kecamatan.id_kabupaten
                WHERE " . self::__tableName . ".deleted_date IS NULL";

        if ($isAjaxList > 0) {
            $sql .= " AND tbl_proposal.id_verifikator = '{$this->idUser}'";
            $sql .= " ORDER BY " . self::__tableName . ".updated_date DESC";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    function selectStatus()
    {
        $sql = " select * from status_grup WHERE nama in ('Belum Verifikasi','Direvisi','Verifikasi')";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectByVerifikator($id)
    {
        $sql = "SELECT " . self::__tableName2 . ".*, 
                grup.nama_grup as nama_grup
                FROM " . self::__tableName2 . "
                LEFT JOIN grup ON grup.grup_id= " . self::__tableName2 . ".grup_id
                WHERE " . self::__tableName2 . ".deleted_date IS NULL
                AND " . self::__tableName2 . "." . self::__tableId2 . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }
}
