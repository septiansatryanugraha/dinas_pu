<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifikator_front extends MX_Controller
{
    const __tableName = 'tbl_verifikator';
    const __tableName2 = 'tbl_proposal';
    const __tableId = 'id';
    const __tableId2 = 'id_proposal';
    const __title = 'Data Pengajuan ';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_verifikator');
        $this->load->model('M_proposal_pengajuan');
        $this->load->model('M_grup');
        $this->load->model('M_kabupaten');
        $this->load->model('M_kecamatan');
        $this->load->model('M_desa');
        $this->load->model('M_pokmas');
        $this->load->model('M_generate_code');
        $this->load->model('M_utilities');
    }

    public function loadDataForeign()
    {
        $option = "<option></option>";
        $modul = $this->input->post('modul');

        switch ($modul) {
            case "desa":
                $idKecamatan = $this->input->post('id_kecamatan');
                $resultDesa = $this->M_desa->select(array('id_kecamatan' => $idKecamatan));
                if ($resultDesa != null) {
                    foreach ($resultDesa as $k => $val) {
                        $option .= "<option value='{$val->id}'>{$val->desa}</option>";
                    }
                }
                break;
            case "kelompok":
                $idDesa = $this->input->post('id_desa');
                $resultKelompok = $this->M_pokmas->select(array('id_desa' => $idDesa, 'status' => 'Aktif'));
                if ($resultKelompok != null) {
                    foreach ($resultKelompok as $k => $val) {
                        $option .= "<option value='{$val->id_pokmas}' pokmas_nama_ketua_{$val->id_pokmas}='{$val->nama_ketua}' pokmas_alamat_{$val->id_pokmas}='{$val->alamat}'>{$val->nama_kelompok}</option>";
                    }
                }
                break;
        }

        echo $option;
    }

    public function profil_verifikator()
    {
        if ($this->lib->login() == "") {
            redirect('login-pengajuan');
        } else {
            $data = array(
                'title' => "Profil Verifikator",
                'judul' => "Profil Verifikator",
            );

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_verifikator->selectByVerifikator($id_user);
            $data['grup'] = $this->M_grup->select(array(), array('PSDA', 'Irigasi', 'Binfat', 'SWP', 'Sekretariat'));

            $this->load->view('v_dashboard/header', $data);
            $this->load->view('v_dashboard/profil_verifikator', $data);
            $this->load->view('v_dashboard/footer', $data);
        }
    }

    public function prosesUpdateUser($id)
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $nama = trim($this->input->post("nama"));
        $ketua = trim($this->input->post("ketua"));
        $alamat = trim($this->input->post("alamat"));
        $telp = trim($this->input->post("telp"));
        $password = trim($this->input->post("password"));

        $this->db->trans_begin();
        if ($errCode == 0) {
            $checkValid = $this->Mdl_verifikator->selectByVerifikator($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($nama) == 0) {
                $errCode++;
                $errMessage = "Nama wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = array(
                    'nama' => $nama,
                    'ketua' => $ketua,
                    'alamat' => $alamat,
                    'telp ' => $telp,
                    'updated_date' => $date,
                    'updated_by' => $username,
                );
                if (strlen($password) > 0) {
                    $data = array_merge($data, array(
                        'password ' => base64_encode($password),
                    ));
                }
                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->session->set_userdata(array(
                'nama' => $nama,
                'ketua' => $ketua,
            ));
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }

    public function index()
    {
        if ($this->lib->login() == "") {
            redirect('login-pengajuan');
        } else {
            $data = array(
                'title' => "Dashboard",
                'judul' => "Dashboard",
            );

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_verifikator->selectByVerifikator($id_user);

            $this->load->view('v_dashboard/header', $data);
            $this->load->view('v_dashboard/dashboard', $data);
            $this->load->view('v_dashboard/footer', $data);
        }
    }

    public function ajax_list()
    {
        $id_user = $this->session->userdata('id');

        $list = $this->Mdl_verifikator->getProposalVerifikator(1);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {

            $status = '<small class="label pull-center bg-blue">Belum Verifikasi</small>';
            if ($brand->status == 'Rekomendasi') {
                $status = '<small class="label pull-center bg-green">Telah Direkomendasi</small>';
            } else if ($brand->status == 'Verifikasi') {
                $status = '<small class="label pull-center bg-green">Verifikasi Admin</small>';
            } else if ($brand->status == 'Rekom Bidang') {
                $status = '<small class="label pull-center bg-green">Rekom Bidang</small>';
            } else if ($brand->status == 'Direvisi') {
                $status = '<small class="label pull-center bg-red">Direvisi</small>';
            }

            if ($brand->created_date == $brand->updated_date) {
                $proses = "Belum ada yang memproses";
            } else {
                $proses = $brand->updated_by;
            }

            $StatusSurvey = "";
            if ($brand->survei == "Survey") {
                $StatusSurvey = "<li><b>Sedang dilakukan Survey Lapangan<b></li>";
            } else if ($brand->survei == "Selesai Survey") {
                $StatusSurvey = "<li><b>Survei selesai di lakukan<b></li>";
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->kode_proposal;
            $row[] = $brand->nama_kelompok . '<br><br> No DPA : <b>' . $brand->no_dpa . '';
            $row[] = $brand->kabupaten;
            $row[] = $status . '<br> Proposal di Proses oleh : <b>' . $proses . '</b><br>' . $StatusSurvey;

            $buttonEdit = anchor('edit-proposal-verifikator/' . $brand->id_proposal, ' <span tooltip="Edit Data"><span class="btn-icon-only icon-ok" ></span>', ' class="btn btn-sm btn-success"');


            $buttonDel = '<button class="btn btn-sm btn-danger hapus-proposal-verifikator" data-id=' . "'" . $brand->id_proposal . "'" . '><span tooltip="Hapus Data"><i class="btn-icon-only icon-trash"></button>';

            $row[] = $buttonEdit . '  ' . $buttonDel;
            $data[] = $row;
        }


        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function tambah_proposal()
    {
        if ($this->lib->login() == "") {
            redirect('login-pengajuan');
        } else {
            $data = array(
                'title' => "Tambah Pengajuan Proposal",
                'judul' => "Tambah Pengajuan Proposal",
            );

            $id_user = $this->session->userdata('id');
            $idKabupaten = $this->session->userdata('id_kabupaten');

            $data['user'] = $this->Mdl_verifikator->selectByVerifikator($id_user);
            $data['kecamatan'] = $this->M_kecamatan->select(array('id_kabupaten' => $idKabupaten));

            $this->load->view('v_dashboard/header', $data);
            $this->load->view('v_dashboard/tambah_proposal', $data);
            $this->load->view('v_dashboard/footer', $data);
        }
    }

    public function prosesAdd()
    {
        $username = $this->session->userdata('username');
        $idKabupaten = $this->session->userdata('id_kabupaten');
        $IdUser = $this->session->userdata('id');
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";
        $config = array();
        $params = array();

        $idKecamatan = trim($this->input->post("id_kecamatan"));
        $idDesa = trim($this->input->post("id_desa"));
        $idPokmas = trim($this->input->post("id_pokmas"));
        $noDpa = trim($this->input->post("no_dpa"));
        $noSurat = trim($this->input->post("no_surat"));
        $tanggal = trim($this->input->post("tanggal"));
        $perihal = trim($this->input->post("perihal"));
        $usulanNilaiAnggaran = trim($this->input->post("usulan_nilai_anggaran"));
        $nilaiAnggaran = trim($this->input->post("nilai_anggaran"));
        $fileUpload = $_FILES['file_upload'];

        $this->db->trans_begin();
        $code = $this->M_generate_code->getNextProposal();

        if ($errCode == 0) {
            $checkKabupaten = $this->M_kabupaten->selectById($idKabupaten);
            if ($checkKabupaten == null) {
                $errCode++;
                $errMessage = "Kabupaten tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKecamatan) == 0) {
                $errCode++;
                $errMessage = "Kecamatan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKecamatan = $this->M_kecamatan->selectById($idKecamatan);
            if ($checkKecamatan == null) {
                $errCode++;
                $errMessage = "Kecamatan tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkKecamatan->id_kabupaten != $idKabupaten) {
                $errCode++;
                $errMessage = "Kecamatan tidak sesuai dengan kabupaten.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idDesa) == 0) {
                $errCode++;
                $errMessage = "Kelurahan / Desa wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkDesa = $this->M_desa->selectById($idDesa);
            if ($checkDesa == null) {
                $errCode++;
                $errMessage = "Desa tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkDesa->id_kecamatan != $idKecamatan) {
                $errCode++;
                $errMessage = "Desa tidak sesuai dengan Kecamatan.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idPokmas) == 0) {
                $errCode++;
                $errMessage = "Kelompok wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkPokmas = $this->M_pokmas->selectById($idPokmas);
            if ($checkPokmas == null) {
                $errCode++;
                $errMessage = "Kelompok tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkPokmas->id_desa != $idDesa) {
                $errCode++;
                $errMessage = "Kelompok tidak sesuai dengan Kelurahan / Desa.";
            }
        }

        if ($errCode == 0) {
            if (strlen($noDpa) == 0) {
                $errCode++;
                $errMessage = "Nomor DPA wajib di isi.";
            }
        }

        if ($errCode == 0) {
            if (strlen($noSurat) == 0) {
                $errCode++;
                $errMessage = "Nomor Surat Proposal wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tanggal) == 0) {
                $errCode++;
                $errMessage = "Tabggal Proposal wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($perihal) == 0) {
                $errCode++;
                $errMessage = "Perihal Proposal wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if ($fileUpload['size'] == 0) {
                $errCode++;
                $errMessage = "Berkas File Pengajuan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if ($fileUpload['type'] != 'application/pdf') {
                $errCode++;
                $errMessage = "Berkas File Pengajuan wajib format PDF.";
            }
        }
        if ($errCode == 0) {
            if (strlen($usulanNilaiAnggaran) == 0) {
                $errCode++;
                $errMessage = "Usulan Anggaran Proposal wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($nilaiAnggaran) == 0) {
                $errCode++;
                $errMessage = "Usulan Anggaran Setelah Evaluasi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $config['cacheable'] = true;
                $config['cachedir'] = './upload/dir/';
                $config['errorlog'] = './upload/log/';
                $config['imagedir'] = './upload/qrcode/';
                $config['quality'] = true;
                $config['size'] = '1024';
                $config['black'] = array(224, 255, 255);
                $config['white'] = array(70, 130, 180);
                $this->ciqrcode->initialize($config);
                $qrcodeImage = $code . '.png';

                $params['data'] = $code;
                $params['level'] = 'H';
                $params['size'] = 10;
                $params['savename'] = FCPATH . $config['imagedir'] . $qrcodeImage;
                $this->ciqrcode->generate($params);

                $newName = time() . $fileUpload['name'];
                $nmfile = "file_" . $newName;
                $config['upload_path'] = "./upload/file_proposal/";
                $config['allowed_types'] = 'pdf';
                $config['max_size'] = '8048'; //maksimum besar file 8M
                $config['file_name'] = $nmfile;
                $config['overwrite'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload("file_upload")) {
                    $imageData = $this->upload->data();
                    $path['link'] = "upload/file_proposal/";

                    $data = array(
                        "qrcode" => $qrcodeImage,
                        'kode_proposal' => $code,
                        'id_pokmas' => $idPokmas,
                        'id_verifikator' => $IdUser,
                        'no_dpa' => $noDpa,
                        'no_surat' => $noSurat,
                        'tanggal' => date('Y-m-d', strtotime($tanggal)),
                        'perihal' => $perihal,
                        'usulan_nilai_anggaran' => $usulanNilaiAnggaran,
                        'nilai_anggaran' => $nilaiAnggaran,
                        'nama_file_upload' => $imageData['file_name'],
                        'tipe' => 'pengajuan permohonan rekomendasi',
                        'status' => 'Belum Verifikasi',
                        'file_upload' => 'upload/file_proposal/' . $imageData['file_name'],
                        'tgl_help' => $date2,
                        'created_date' => $date,
                        'created_by' => $username,
                        'updated_date' => $date,
                        'updated_by' => $username,
                    );
                    $this->db->insert(self::__tableName2, $data);
                    $idProposal = $this->db->insert_id();

                    $data2 = array(
                        'id_proposal' => $idProposal,
                        'status' => 'Belum Verifikasi',
                        'keterangan' => 'user <b>' . $username . '</b> melakukan proses pengajuan proposal ke System dengan atas nama Pokmas <b>' . $checkPokmas->nama_kelompok . '</b>',
                        'created_date' => $date,
                        'created_by' => $username,
                        'updated_date' => $date,
                        'updated_by' => $username,
                    );
                    $result = $this->db->insert('tbl_log', $data2);

                    $data3 = array(
                        'id_proposal' => $idProposal,
                        'jumlah_anggaran' => $nilaiAnggaran,
                        'tgl_survei' => $date2,
                        'created_date' => $date,
                        'created_by' => $username,
                        'updated_date' => $date,
                        'updated_by' => $username,
                    );
                    $result = $this->db->insert('tbl_survey', $data3);

                    $data4 = array(
                        'ref_table' => 'tbl_proposal',
                        'ref_id' => $idProposal,
                        'nama_file_upload' => $imageData['file_name'],
                        'file_upload' => 'upload/file_proposal/' . $imageData['file_name'],
                        'tanggal' => $date2,
                        'status' => 'Pengajuan File',
                        'created_date' => $date,
                        'created_by' => $username,
                        'updated_date' => $date,
                        'updated_by' => $username,
                    );
                    $result = $this->db->insert('tbl_history_upload', $data4);

                    $data5 = array(
                        'id_proposal' => $idProposal,
                        'created_date' => $date,
                        'created_by' => $username,
                        'updated_date' => $date,
                        'updated_by' => $username,
                    );
                    $result = $this->db->insert('tbl_checklis', $data5);
                }
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }

    public function edit($id)
    {
        if ($this->lib->login() == "") {
            redirect('login-pengajuan');
        } else {
            $data = array(
                'title' => "Edit Pengajuan Proposal",
                'judul' => "Edit Pengajuan Proposal",
            );

            $id_user = $this->session->userdata('id');
            $idKabupaten = $this->session->userdata('kabupaten');
            $data['user'] = $this->Mdl_verifikator->selectByVerifikator($id_user);

            $resultData = $this->M_proposal_pengajuan->selectById($id);
            if ($resultData != null) {
                $data['resultData'] = $resultData;
                $data['kecamatan'] = $this->M_kecamatan->select(array('id_kabupaten' => $idKabupaten));
                $data['desa'] = $this->M_desa->select(array('id_kecamatan' => $resultData->id_kecamatan));
                $data['pokmas'] = $this->M_pokmas->select(array('id_desa' => $resultData->id_desa));
                $data['historiFile'] = $this->M_utilities->historiFile('tbl_proposal', $id, 'Pengajuan File');
                $data['historiFileBeritaAcara'] = $this->M_utilities->historiFile('tbl_proposal', $id, 'berita acara');
                $data['historiFileChecklist'] = $this->M_utilities->historiFile('tbl_proposal', $id, 'checklist');
                $data['historiFileHasilSurvei'] = $this->M_utilities->historiFile('tbl_proposal', $id, 'hasil survei');
                $data['historiSketsaSurvei'] = $this->M_utilities->historiFile('tbl_proposal', $id, 'sketsa survei');

                $this->load->view('v_dashboard/header', $data);
                $this->load->view('v_dashboard/ubah_proposal', $data);
                $this->load->view('v_dashboard/footer', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url('beranda') . "';</script>";
            }
        }
    }

    public function prosesUpdateProposal($id)
    {
        $username = $this->session->userdata('username');
        $IdUser = $this->session->userdata('id');
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";
        $config = array();
        $params = array();

        $idKabupaten = trim($this->input->post("id_kabupaten"));
        $idKecamatan = trim($this->input->post("id_kecamatan"));
        $idDesa = trim($this->input->post("id_desa"));
        $idPokmas = trim($this->input->post("id_pokmas"));
        $noDpa = trim($this->input->post("no_dpa"));
        $noSurat = trim($this->input->post("no_surat"));
        $tanggal = trim($this->input->post("tanggal"));
        $perihal = trim($this->input->post("perihal"));
        $usulanNilaiAnggaran = trim($this->input->post("usulan_nilai_anggaran"));
        $nilaiAnggaran = trim($this->input->post("nilai_anggaran"));
        $fileUpload = $_FILES['file_upload'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_proposal_pengajuan->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKabupaten) == 0) {
                $errCode++;
                $errMessage = "Kota / Kabupaten wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKabupaten = $this->M_kabupaten->selectById($idKabupaten);
            if ($checkKabupaten == null) {
                $errCode++;
                $errMessage = "Kabupaten tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKecamatan) == 0) {
                $errCode++;
                $errMessage = "Kecamatan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKecamatan = $this->M_kecamatan->selectById($idKecamatan);
            if ($checkKecamatan == null) {
                $errCode++;
                $errMessage = "Kecamatan tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkKecamatan->id_kabupaten != $idKabupaten) {
                $errCode++;
                $errMessage = "Kecamatan tidak sesuai dengan kabupaten.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idDesa) == 0) {
                $errCode++;
                $errMessage = "Kelurahan / Desa wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkDesa = $this->M_desa->selectById($idDesa);
            if ($checkDesa == null) {
                $errCode++;
                $errMessage = "Desa tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkDesa->id_kecamatan != $idKecamatan) {
                $errCode++;
                $errMessage = "Desa tidak sesuai dengan Kecamatan.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idPokmas) == 0) {
                $errCode++;
                $errMessage = "Kelompok wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkPokmas = $this->M_pokmas->selectById($idPokmas);
            if ($checkPokmas == null) {
                $errCode++;
                $errMessage = "Kelompok tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkPokmas->id_desa != $idDesa) {
                $errCode++;
                $errMessage = "Kelompok tidak sesuai dengan Kelurahan / Desa.";
            }
        }

        if ($errCode == 0) {
            if (strlen($noDpa) == 0) {
                $errCode++;
                $errMessage = "Nomor DPA wajib di isi.";
            }
        }

        if ($errCode == 0) {
            if (strlen($noSurat) == 0) {
                $errCode++;
                $errMessage = "Nomor Surat Proposal wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tanggal) == 0) {
                $errCode++;
                $errMessage = "Tabggal Proposal wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($perihal) == 0) {
                $errCode++;
                $errMessage = "Perihal Proposal wajib di isi.";
            }
        }
        if ($fileUpload['size'] > 0) {
            if ($errCode == 0) {
                if ($fileUpload['type'] != 'application/pdf') {
                    $errCode++;
                    $errMessage = "Berkas File Pengajuan wajib format PDF.";
                }
            }
        }
        if ($errCode == 0) {
            if (strlen($usulanNilaiAnggaran) == 0) {
                $errCode++;
                $errMessage = "Usulan Anggaran Proposal wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($nilaiAnggaran) == 0) {
                $errCode++;
                $errMessage = "Usulan Anggaran Setelah Evaluasi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $config['cacheable'] = true;
                $config['cachedir'] = './upload/dir/';
                $config['errorlog'] = './upload/log/';
                $config['imagedir'] = './upload/qrcode/';
                $config['quality'] = true;
                $config['size'] = '1024';
                $config['black'] = array(224, 255, 255);
                $config['white'] = array(70, 130, 180);
                $this->ciqrcode->initialize($config);
                $qrcodeImage = $checkValid->kode_proposal . '.png';

                $params['data'] = $checkValid->kode_proposal;
                $params['level'] = 'H';
                $params['size'] = 10;
                $params['savename'] = FCPATH . $config['imagedir'] . $qrcodeImage;
                $this->ciqrcode->generate($params);

                $data = array(
                    'id_pokmas' => $idPokmas,
                    'no_dpa' => $noDpa,
                    'no_surat' => $noSurat,
                    'tanggal' => date('Y-m-d', strtotime($tanggal)),
                    'perihal' => $perihal,
                    'usulan_nilai_anggaran' => $usulanNilaiAnggaran,
                    'nilai_anggaran' => $nilaiAnggaran,
                    'updated_date' => $date,
                    'updated_by' => $username,
                );

                if ($fileUpload['size'] > 0) {
                    $newName = time() . $fileUpload['name'];
                    $nmfile = "file_" . $newName;
                    $config['upload_path'] = "./upload/file_proposal/";
                    $config['allowed_types'] = 'pdf';
                    $config['max_size'] = '8048'; //maksimum besar file 8M
                    $config['file_name'] = $nmfile;
                    $config['overwrite'] = TRUE;
                    $this->load->library('upload', $config);
                    $imageData = $this->upload->data();

                    if ($this->upload->do_upload("file_upload")) {
                        $data = array_merge($data, array(
                            'nama_file_upload' => $imageData['file_name'],
                            'file_upload' => 'upload/file_proposal/' . $imageData['file_name'],
                            'tgl_help' => $date2
                        ));

                        $data2 = array(
                            'ref_table' => 'tbl_proposal',
                            'ref_id' => $id,
                            'nama_file_upload' => $imageData['file_name'],
                            'file_upload' => 'upload/file_proposal/' . $imageData['file_name'],
                            'tanggal' => $date2,
                            'status' => 'Pengajuan File Revisi',
                            'created_date' => $date,
                            'created_by' => $username,
                            'updated_date' => $date,
                            'updated_by' => $username,
                        );
                        $result = $this->db->insert('tbl_history_upload', $data2);
                    }
                }
                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $this->db->trans_begin();

        $id = $_POST[self::__tableId2];

        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }

        if ($errCode == 0) {
            $checkValid = $this->M_proposal_pengajuan->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            $checkForeign = $this->M_proposal_pengajuan->checkForeign($id);
            if ($checkForeign) {
                $errCode++;
                $errMessage = self::__title . " terdapat pada modul lain.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->load->helper("file");

                $files = $this->db->get_where(self::__tableName2, array('id_proposal' => $id));
                if ($files->num_rows() > 0) {
                    $hasil = $files->row();
                    $judul = $hasil->nama_file_upload;
                    //hapus unlink file
                    $path = 'upload/file_proposal/' . $judul;
                    unlink($path);
                }

                if ($files->num_rows() > 0) {
                    $hasil = $files->row();
                    $judul = $hasil->qrcode;
                    //hapus unlink file
                    $path = 'upload/qrcode/' . $judul;
                    unlink($path);
                }

                $this->db->update(self::__tableName2, array('deleted_date' => date('Y-m-d H:i:s')), array(self::__tableId2 => $id));
                $this->db->update('tbl_log', array('deleted_date' => date('Y-m-d H:i:s')), array('id_proposal' => $id));
                $this->db->update('tbl_survey', array('deleted_date' => date('Y-m-d H:i:s')), array('id_proposal' => $id));
                $this->db->update('tbl_history_upload', array('deleted_date' => date('Y-m-d H:i:s')), array('ref_id' => $id));
                $this->db->update('tbl_checklis', array('deleted_date' => date('Y-m-d H:i:s')), array('id_proposal' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di hapus');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }
}
