<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_verifikator extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('mdl_login');
    }

    public function index()
    {

        if ($this->lib->login() != "") {
            redirect('Login_verifikator/logout');
        } else {

            $this->load->view('header', $data);
            $this->load->view('login', $data);
            $this->load->view('footer', $data);
        }
    }

    public function login_user()
    {

        $this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean');

        if ($this->form_validation->run() == TRUE) {
            $username = $this->input->post('username');
            $password = base64_encode($this->input->post('password'));
            $check = $this->mdl_login->login(array('username' => $username), array('password' => $password));

            if ($check == TRUE) {
                foreach ($check as $user) {
                    if ($user->status == "Non aktif") {
                        $json['pesan'] = "Maaf akun belum aktif.";
                    } else {
                        $this->session->set_userdata(array(
                            'id' => $user->id,
                            'id_kabupaten' => $user->id_kabupaten,
                            'username' => $user->username,
                            'nama' => $user->nama,
                            'ketua' => $user->ketua,
                        ));
                        $URL_home = base_url('beranda');
                        $json['status'] = true;
                        $json['url_home'] = $URL_home;
                        $json['pesan'] = "Success Login.";
                    }
                }
            } else {
                $json['pesan'] = "Username / Password salah";
            }
        } else {
            $json['pesan'] = "Maaf tidak bisa login, silahkan cek user dan password anda";
        }

        echo json_encode($json);
    }

    function logout()
    {
        if ($this->lib->login() != "") {
            $this->lib->logout();
            redirect('login-pengajuan');
        } else {
            redirect('beranda');
        }
    }
}
