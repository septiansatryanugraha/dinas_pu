<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Dinas PU SDA Jawa Timur</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/verifikator/publik/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/verifikator/publik/css/bootstrap-responsive.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/verifikator/publik/css/font-awesome.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/verifikator/publik/css/pages/signin.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/verifikator/publik/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/toastr/toastr.css">
  <script type="text/javascript" src="<?php echo base_url().'assets/publik//verifikator/js/jquery-2.2.3.min.js'?>"></script> 
</head>   
<body>

  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container"><center><img src="<?php echo base_url(); ?>assets/tambahan/gambar/logo-white.png" width="250px"></center>
      </div>
    </div>
  </div>
