<style type="text/css">
  #btn_loading { display: none; }

  .field-icon {
    float: left;
    margin-left: 90%;
    margin-top: -25px;
    font-size: 17px;
    position: relative;
    z-index: 999;
    color: #ccc;
  }
</style>

<div class="account-container">
  <div class="content clearfix">
    <form action="<?php echo base_url('Verifikator/Login_verifikator/login_user'); ?>" method="post" id="FormLogin">

      <h1><center>Login Verifikator</center></h1>

      <div class="login-fields">
        <p><center>Please provide your details</center></p>
        
        <div class="field">
          <label for="username">Username</label>
          <input type="text" id="username" name="username" id="username" placeholder="Username" class="login username-field" />
        </div> 
        
        <div class="field">
          <label for="password">Password</label>
          <input type="password" id="password-field" name="password"  placeholder="Password" class="login password-field"/><span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
        </div> 

      </div> 

      <div class="login-actions">
        <br>
        <div id='btn_loading'></div>
        <div id="hilang">

          <button type="submit" id="btnSignUp" class="button btn btn-success btn-large pull-right">
            <i class="fa fa-lock" aria-hidden="true"></i> &nbsp;Login
          </button>
        </div> 

        <div id="buka">
          <button type="submit" id="btnSignUp" class="button btn btn-success btn-large pull-right">
            <i class="fa fa-unlock" aria-hidden="true"></i> &nbsp;Login
          </button>
        </div>
      </div> 
      
    </form>
  </div> 
</div> 

<script>


 function formValidation(oEvent) {
  oEvent = oEvent || window.event;
  var txtField = oEvent.target || oEvent.srcElement;
  var t1ck = true;
  var t1ck_user = true;
  var t1ck_pass = true;
  var msg = " ";
  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

  if (document.getElementById("username").value.length < 4) {
    t1ck = false;
    msg = msg + "Your name should be minimun 3 char length";
  }
  if (document.getElementById("username").value.length < 1) {
    t1ck_user = false;
    msg = msg + "Your name should be minimun 3 char length";
  }
  if (document.getElementById("password-field").value.length < 2) {
    t1ck = false;
    msg = msg + "Your name should be minimun 3 char length";
  }
  if (document.getElementById("password-field").value.length < 1) {
    t1ck_pass = false;
    msg = msg + "Your name should be minimun 3 char length";
  }

  if (t1ck) {
    document.getElementById("btnSignUp").disabled = false;
  } else {
    document.getElementById("btnSignUp").disabled = true;
  }
  if (t1ck_user) {
    document.getElementById("bantuan1").style.display = "block";
  } else {
    document.getElementById("bantuan1").style.display = "none";
  }
  if (t1ck_pass) {
    document.getElementById("bantuan2").style.display = "block";
  } else {
    document.getElementById("bantuan2").style.display = "none";
  }
}

window.onload = function () {
  var btnSignUp = document.getElementById("btnSignUp");
  var username = document.getElementById("username");
  var password = document.getElementById("password-field");

  var t1ck = false;
  document.getElementById("btnSignUp").disabled = true;
  username.onkeyup = formValidation;
  password.onkeyup = formValidation;
}


    // untuk show hide password
    $(".toggle-password").click(function() {

      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });
  </script>

  <script>
    $(function(){
      $("#buka").hide();
      $('#FormLogin').submit(function(e){
        e.preventDefault();
        $.ajax({
          beforeSend: function () {
            $("#buka").hide();
            $("#hilang").hide();
            $("#btn_loading").html("<button class='button btn btn-success btn-large pull-right' disabled><i class='fa fa-refresh fa-spin'></i> &nbsp;Wait..</button>");
            $("#btn_loading").show();
          },
          url: $(this).attr('action'),
          type: "POST",
          cache: false,
          data: $(this).serialize(),
          dataType:'json',
          success: function(json){
            if(json.status == true){ 
              $("#btn_loading").hide();
              $("#buka").show();
              toastr.success(json.pesan,'Success', {timeOut: 5000},toastr.options = {
                "closeButton": true}); 
              window.location = json.url_home; 
            } else {
              $("#btn_loading").hide();
              $("#hilang").show();
              toastr.error(json.pesan,'Warning', {timeOut: 5000},toastr.options = {
                "closeButton": true});
            }
          }
        });
      });
    });
  </script>
