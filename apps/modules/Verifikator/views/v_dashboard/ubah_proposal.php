
<style>
    #loadingImg {
        margin-top:5%; 
        margin-left:25%; 
        position: fixed;  
        z-index: 9999; 
    }
    #slider {
        margin-left: 1%;
    }
    #btn_loading {
        display: none;
    }
    #qrcode {
        margin-left: 30%;
        margin-bottom: 20px;
    }
</style>


<div class="container"><br/>

    <div class="row">
        <div class="col-md-8">

            <div class="widget widget-table action-table">
                <div class="widget-header"> <i class="icon-file"></i>
                    <h3>Form Pengajuan Proposal</h3>
                </div>

                <div class="widget-content">
                    <form enctype="multipart/form-data" id="form-ubah" method="POST">
                        <div class="box-body">
                            <div id="loadingImg">
                                <img src="<?php echo base_url() . 'assets/' ?>tambahan/gambar/loaders.gif">
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Kecamatan</label>
                                    <select name="id_kecamatan" class="form-control select-kecamatan" id="id_kecamatan">
                                        <option></option>
                                        <?php foreach ($kecamatan as $data) { ?>
                                            <option value="<?php echo $data->id; ?>" <?php echo ($data->id == $resultData->id_kecamatan) ? "selected" : ""; ?>>
                                                <?php echo $data->kecamatan; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Kelurahan / Desa</label>
                                    <select name="id_desa" class="form-control select-desa" id="id_desa">
                                        <option></option>
                                        <?php foreach ($desa as $data) { ?>
                                            <option value="<?php echo $data->id; ?>" <?php echo ($data->id == $resultData->id_desa) ? "selected" : ""; ?>>
                                                <?php echo $data->desa; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Pokmas</label>
                                    <select name="id_pokmas" class="form-control select-pokmas" id="id_pokmas">
                                        <option></option>
                                        <?php foreach ($pokmas as $data) { ?>
                                            <option value="<?php echo $data->id_pokmas; ?>" <?php echo ($data->id_pokmas == $resultData->id_pokmas) ? "selected" : ""; ?>>
                                                <?php echo $data->nama_kelompok; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Ketua Pokmas</label>
                                    <input type="text" name="ketua" class="form-control" id="ketua" value="<?php echo $resultData->nama_ketua; ?>" readonly="readOnly">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">No urut Pokmas di DPA</label>
                                    <input type="text" id="no_dpa" name="no_dpa" class="form-control" value="<?php echo $resultData->no_dpa; ?>">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">No Surat Proposal</label>
                                    <input type="text" id="no_surat" name="no_surat" class="form-control" value="<?php echo $resultData->no_surat; ?>" >
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">tanggal Proposal</label>
                                    <input type="text" name="tanggal" id="tanggal" class="form-control datepicker" value="<?php echo date('d-m-Y', strtotime($resultData->tanggal)); ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-7">
                                    <label for="exampleInputEmail1">Perihal Proposal</label>
                                    <textarea class="form-control" id="perihal" name="perihal"><?php echo $resultData->perihal; ?></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-9">
                                    <label for="exampleInputEmail1">Berkas File</label>
                                    <input type="file" name="file_upload" id="file_upload"  onchange="return validBerkas()"/>
                                    <a href="<?php echo base_url() . $resultData->file_upload ?>" target="blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-file-pdf" aria-hidden="true"></i> <?php echo $resultData->nama_file_upload; ?></font></b></a>
                                </div>
                                <div class="form-group col-md-2" style="margin-top: 20px;">
                                    <small class="label pull-center bg-red">format .pdf</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-9">
                                <label for="inputEmail3">Catatan</label>
                                <textarea class="form-control"  disabled=""><?php echo $resultData->catatan; ?></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-7">
                                <label for="exampleInputEmail1">Nilai Usulan Proposal</label>
                                <input type="text" name="usulan_nilai_anggaran" class="form-control" id="currency1" value="<?php echo $resultData->usulan_nilai_anggaran; ?>">
                            </div>
                        </div>
                        <div class="row">
                            <br>
                            <div class="form-group col-md-7">
                                <label for="exampleInputEmail1">Pagu DPA</label>
                                <input type="text" name="nilai_anggaran" class="form-control" id="currency2" aria-describedby="sizing-addon2" value="<?php echo $resultData->nilai_anggaran; ?>">
                            </div>
                        </div>
                        <div class="box-footer">
                            <div id="buka"> 
                                <button name="simpan" id="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                            <div id="btn_loading">
                                <button name="simpan" id="simpan" type="submit" class="btn btn-primary btn-flat animated-gradient"><i class="fa fa-save"></i> Tunggu..</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div> 
        </div>
        <div class="col-md-4">
            <div class="widget widget-table action-table">
                <div class="widget-header"> <i class="icon-qrcode"></i>
                    <h3>Data Qrcode</h3>
                </div>
                <div class="widget-content">
                    <div id="qrcode">
                        <?php if ($resultData->qrcode != '') { ?>
                            <img class="img-thumbnail" src='../upload/qrcode/<?php echo $resultData->qrcode; ?>' width="150px">
                        <?php } else { ?>
                            <img class="img-thumbnail" src="<?php echo base_url(); ?>/assets/tambahan/gambar/tidak-ada.png" width="200px" />
                        <?php } ?>
                    </div>
                    <center><b>Kode Proposal : <p><?php echo $resultData->kode_proposal; ?></p></b></center>    
                </div>
            </div>
            <div class="widget widget-table action-table">
                <div class="widget-header"> <i class="icon-file"></i>
                    <h3>Pengajuan File</h3>
                </div>
                <div class="widget-content">
                    <?php if (count($historiFile) > 0) { ?>
                        <div class="box-body box-profile">
                            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="50%">
                                <tr>  
                                    <th>History File Upload</th>
                                </tr>
                                <?php foreach ($historiFile as $data) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() . $data->file_upload ?>" target="blank"><b><font face="verdana" size="2" color="red"><i class="fa fa-download" aria-hidden="true"></i> <?php echo $data->nama_file_upload; ?></font></b></a><br>tanggal upload : <?php echo date_indo(date('Y-m-d', strtotime($data->tanggal))) ?><br><small class="label pull-center bg-green"><?php echo $data->status ?></small></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } else { ?>
                        <div class="box-body box-profile">
                            Belum ada file Pengajuan
                        </div> 
                    <?php } ?> 
                </div>
            </div>
            <div class="widget widget-table action-table">
                <div class="widget-header"> <i class="icon-file"></i>
                    <h3>Berita Acara</h3>
                </div>
                <div class="widget-content">
                    <?php if (count($historiFileBeritaAcara) > 0) { ?>
                        <div class="box-body box-profile">
                            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="50%">
                                <tr>  
                                    <th>History File Upload</th>
                                </tr>
                                <?php foreach ($historiFileBeritaAcara as $data) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() . $data->file_upload ?>" target="blank"><b><font face="verdana" size="2" color="red"><i class="fa fa-download" aria-hidden="true"></i> <?php echo $data->nama_file_upload; ?></font></b></a><br>tanggal upload : <?php echo date_indo(date('Y-m-d', strtotime($data->tanggal))) ?><br><small class="label pull-center bg-green"><?php echo $data->status ?></small></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } else { ?>
                        <div class="box-body box-profile">
                            Belum ada file Berita Acara
                        </div> 
                    <?php } ?>
                </div>
            </div>
            <div class="widget widget-table action-table">
                <div class="widget-header"> <i class="icon-file"></i>
                    <h3>Checklist Survei</h3>
                </div>
                <div class="widget-content">
                    <?php if (count($historiFileChecklist) > 0) { ?>
                        <div class="box-body box-profile">
                            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="50%">
                                <tr>  
                                    <th>History File Upload</th>
                                </tr>
                                <?php foreach ($historiFileChecklist as $data) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() . $data->file_upload ?>" target="blank"><b><font face="verdana" size="2" color="red"><i class="fa fa-download" aria-hidden="true"></i> <?php echo $data->nama_file_upload; ?></font></b></a><br>tanggal upload : <?php echo date_indo(date('Y-m-d', strtotime($data->tanggal))) ?><br><small class="label pull-center bg-green"><?php echo $data->status ?></small></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } else { ?>
                        <div class="box-body box-profile">
                            Belum ada file Checklist Survei
                        </div> 
                    <?php } ?>
                </div>
            </div>
            <div class="widget widget-table action-table">
                <div class="widget-header"> <i class="icon-file"></i>
                    <h3>Hasil Survei</h3>
                </div>
                <div class="widget-content">
                    <?php if (count($historiFileHasilSurvei) > 0) { ?>
                        <div class="box-body box-profile">
                            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="50%">
                                <tr>  
                                    <th>History File Upload</th>
                                </tr>
                                <?php foreach ($historiFileHasilSurvei as $data) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() . $data->file_upload ?>" target="blank"><b><font face="verdana" size="2" color="red"><i class="fa fa-download" aria-hidden="true"></i> <?php echo $data->nama_file_upload; ?></font></b></a><br>tanggal upload : <?php echo date_indo(date('Y-m-d', strtotime($data->tanggal))) ?><br><small class="label pull-center bg-green"><?php echo $data->status ?></small></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } else { ?>
                        <div class="box-body box-profile">
                            Belum ada file Hasil Survei
                        </div> 
                    <?php } ?>
                </div>
            </div>
            <div class="widget widget-table action-table">
                <div class="widget-header"> <i class="icon-file"></i>
                    <h3>Sketsa Survei</h3>
                </div>
                <div class="widget-content">
                    <?php if (count($historiSketsaSurvei) > 0) { ?>
                        <div class="box-body box-profile">
                            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="50%">
                                <tr>  
                                    <th>History File Upload</th>
                                </tr>
                                <?php foreach ($historiSketsaSurvei as $data) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() . $data->file_upload ?>" target="blank"><b><font face="verdana" size="2" color="red"><i class="fa fa-download" aria-hidden="true"></i> <?php echo $data->nama_file_upload; ?></font></b></a><br>tanggal upload : <?php echo date_indo(date('Y-m-d', strtotime($data->tanggal))) ?><br><small class="label pull-center bg-green"><?php echo $data->status ?></small></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } else { ?>
                        <div class="box-body box-profile">
                            Belum ada file Sketsa Survei
                        </div> 
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-ubah').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url('ubah-pengajuan-proposal') . '/' . $resultData->id_proposal; ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-ubah").reset();
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?php echo base_url("beranda"); ?>'", 450);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    $(function () {
        $(".select-kecamatan").select2({
            placeholder: " -- Pilih Kecamatan -- "
        });
        $(".select-desa").select2({
            placeholder: " -- Pilih Kelurahan / Desa -- "
        });
        $(".select-pokmas").select2({
            placeholder: " -- Pilih Kelompok Masyarakat -- "
        });

        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        })

        $("#loadingImg").hide();
        $("#loadingImg2").hide();

        $.ajaxSetup({
            type: "POST",
            url: "<?php echo base_url('load-data-foreign-verifikator') ?>",
            cache: false,
        });

        $("#id_kecamatan").change(function () {
            var value = $(this).val();
            console.log(value);
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg").fadeIn();
                    },
                    data: {modul: 'desa', id_kecamatan: value},
                    success: function (respond) {
                        $("#id_desa").html(respond);
                        $("#id_pokmas").html('<option></option>');
                        $("#ketua").val("");
                        $("#loadingImg").fadeOut();
                        console.log(respond);
                    }
                })
            }
        });

        $("#id_desa").change(function () {
            var value = $(this).val();
            console.log(value);
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg").fadeIn();
                    },
                    data: {modul: 'kelompok', id_desa: value},
                    success: function (respond) {
                        $("#id_pokmas").html(respond);
                        $("#ketua").val("");
                        $("#loadingImg").fadeOut();
                        console.log(respond);
                    }
                })
            }
        });

        $("#id_pokmas").change(function () {
            var value = $(this).val();
            console.log(value);
            var nama_ketua = this.options[this.selectedIndex].getAttribute("pokmas_nama_ketua_" + value)
            console.log(nama_ketua);
            $("#ketua").val(nama_ketua);
        });

        if (window.localStorage) {
            if (!localStorage.getItem('firstLoad')) {
                localStorage['firstLoad'] = true;
                window.location.reload();
            } else {
                localStorage.removeItem('firstLoad');
            }
        }
    });

    function validBerkas() {
        var fileInput = document.getElementById("file_upload").value;
        if (fileInput != '') {
            var checkfile = fileInput.toLowerCase();
            // validasi ekstensi file
            if (!checkfile.match(/(\.pdf)$/)) {
                // swal("Peringatan", "File harus format .docx", "warning");
                toastr.error('File harus format .pdf', 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                document.getElementById("file_upload").value = '';
                return false;
            }
            var ukuran = document.getElementById("file_upload");
            // validasi ukuran size file
            if (ukuran.files[0].size > 8007200) {
                // swal("Peringatan", "File harus maksimal 1MB", "warning");
                toastr.error('File harus maksimal 8 MB', 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                ukuran.value = '';
                return false;
            }
            return true;
        }
    }

    /* Str Replacement untuk ke indo */
    var rupiah = document.getElementById('currency1');
    rupiah.addEventListener('keyup', function (e) {
        rupiah.value = formatRupiah(this.value);
    });

    var rupiah2 = document.getElementById('currency2');
    rupiah2.addEventListener('keyup', function (e) {
        rupiah2.value = formatRupiah2(this.value);
    });

    /* Fungsi */
    function formatRupiah(bilangan, prefix) {
        var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    /* Fungsi */
    function formatRupiah2(bilangan, prefix) {
        var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
</script>