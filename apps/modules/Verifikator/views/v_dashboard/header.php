<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <title>Dinas PU SDA Jawa Timur</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">

  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
  rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/verifikator/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/verifikator/publik/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/verifikator/publik/css/bootstrap-responsive.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/eksternal/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/verifikator/publik/css/font-awesome.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/verifikator/publik/css/pages/dashboard.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/verifikator/publik/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/verifikator/dataTable/css/datatable.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/verifikator/dataTable/css/datatable2.min.css"> 
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/toastr/toastr.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
  <script type="text/javascript" src="<?php echo base_url().'assets/publik//verifikator/js/jquery-2.2.3.min.js'?>"></script> 

</head>   

<!DOCTYPE html>
<html>

<body>
  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">

     <div class="container"><a class="brand" href="index.html"><img src="<?php echo base_url(); ?>assets/tambahan/gambar/logo-white.png" width="150px"> </a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
            class="icon-user"></i> <?php echo $user->nama ?></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo base_url('profil-verifikator'); ?>">Profile</a></li>
              <li><a href="<?php echo base_url('Verifikator/Login_verifikator/logout'); ?>">Logout</a></li>
            </ul>
          </li>
        </ul>

      </div>
      <!--/.nav-collapse --> 
    </div>

  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">


        <li class="<?php echo $this->uri->segment(1) == 'beranda' ? 'active': '' ?>" ><a href="<?php echo site_url();?>beranda.html" ><i class="icon-dashboard"></i><span>Beranda</span></a></li>

        <li class="<?php echo $this->uri->segment(1) == 'profil-verifikator' ? 'active': '' ?>" ><a href="<?php echo site_url();?>profil-verifikator.html" ><i class="icon-user"></i><span>Profil</span></a></li>

        <li><a href="<?php echo site_url();?>Verifikator/Login_verifikator/logout" onClick="return confirm('Ingin Keluar?')"><i class="fa fa-sign-out"></i><span>Keluar</span></a></li>

      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>