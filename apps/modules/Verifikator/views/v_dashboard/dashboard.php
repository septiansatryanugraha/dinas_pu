
<style type="text/css">
	.gap {
		margin-top: 20px;
	}

	.gap2 {
		margin-top: 60px;
	}
</style>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dataTable/css/datatable.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dataTable/css/datatable2.min.css"> 

<div class="container"><br/>

  <div class="row">
    <div class="col-md-12">

       <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-user"></i>
            <h3>Data Pengajuan Proposal</h3>
        </div>

        <div class="widget-content">

            <a class="klik ajaxify" href="<?php echo site_url('add-proposal'); ?>"><button class="btn btn-success" ><i class="fa fa-plus"></i> Add Data</button></a><br><br>

            <table id="tabel_ajax" class="table table-striped table-bordered" style="width:100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Proposal</th>
                  <th>Nama Kelompok</th>
                  <th>Kabupaten</th>
                  <th>Status</th>
                  <th class="td-actions">action</th>
              </tr>
          </thead>
          <tbody>
          </tbody>
      </table>
  </div> 
</div>
</div>
</div>
</div>


<script type="text/javascript">


    var save_method; //for save method string
    var table;

    $(document).ready(function () {
        reloadTable();
    });

    function reloadTable() {
        table = $('#tabel_ajax').DataTable({
            "processing": true, //Feature control the processing indicator.
            "scrollX": true,
            "aLengthMenu": [[50, 75, 100, 150, -1], [50, 75, 100, 150, "All"]],
            "pageLength": 50,
            "order": [], //Initial no order.
            oLanguage: {
                "sProcessing": "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "Data tidak ada di server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('ajax-data_proposal') ?>",
                "type": "POST",
            },
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
                ],
            });
    }


    function reload_table()
    {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    $(document).on("click", ".hapus-proposal-verifikator", function () {
        var id_proposal = $(this).attr("data-id");
        console.log(id_proposal);

        swal({
            title: "Hapus Data?",
            text: "Yakin anda akan menghapus data ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Hapus",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        },
        function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: "POST",
                url: "<?php echo base_url('delete-pengajuan-proposal-verifikator'); ?>",
                data: "id_proposal=" + id_proposal,
                success: function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        $("tr[data-id='" + id_proposal + "']").fadeOut("fast", function () {
                            $(this).remove();
                        });
                        swal("Success", result.pesan, "success");
                    } else {
                        swal("Warning", result.pesan, "warning");
                    }
                    reload_table();
                }
            });
        });
    });

</script>