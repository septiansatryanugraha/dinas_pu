<style type="text/css">
    .gap {
        margin-top: 20px;
    }

    .gap2 {
        margin-top: 60px;
    }

    .field-icon {
        float: left;
        margin-left: 95%;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }
</style>

<div class="container"><br/>
    <div class="row">
        <div class="col-md-8">
            <div class="widget widget-table action-table">
                <div class="widget-header"> <i class="icon-user"></i>
                    <h3>Profil Verifikator</h3>
                </div>
                <div class="widget-content">
                    <form class="form-horizontal" id="form-ubah" method="POST">
                        <div class="control-group">                     
                            <label class="control-label" for="firstname">Nama TIM</label>
                            <div class="controls">
                                <input type="text" class="form-control" name="nama" id="nama" value="<?php echo $user->nama ?>">
                            </div>        
                        </div>
                        <div class="control-group">                     
                            <label class="control-label" for="firstname">Nama Ketua TIM</label>
                            <div class="controls">
                                <input type="text" class="form-control" name="ketua" id="ketua" value="<?php echo $user->ketua ?>">
                            </div>        
                        </div>
                        <div class="control-group">                     
                            <label class="control-label" for="firstname">Nomor Telp</label>
                            <div class="controls">
                                <input type="text" class="form-control" name="telp" placeholder="Telepon" id="telp" aria-describedby="sizing-addon2" value="<?php echo $user->telp; ?>" onkeypress="return hanyaAngka(event)" />
                            </div>        
                        </div> 
                        <div class="control-group">                     
                            <label class="control-label" for="firstname">Alamat</label>
                            <div class="controls">
                                <textarea class="form-control" name="alamat"><?php echo $user->alamat ?></textarea>
                            </div>        
                        </div>
                        <div class="control-group">                     
                            <label class="control-label" for="firstname">Username</label>
                            <div class="controls">
                                <input type="text" class="form-control" placeholder="username" id="username" aria-describedby="sizing-addon2" value="<?php echo $user->username; ?>" disabled>
                            </div>        
                        </div>
                        <div class="control-group">                     
                            <label class="control-label" for="firstname">Password</label>
                            <div class="controls">
                                <input type="password" class="form-control" placeholder="password" id="password-field" name="password" aria-describedby="sizing-addon2"><span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                            </div>        
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save</button> 
                            <button class="btn">Cancel</button>
                        </div> 
                    </form>
                </div> 
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-ubah').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url("update-user-verifikator") . '/' . $user->id; ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-ubah").reset();
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?php echo base_url("profil-verifikator"); ?>'", 450);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    $(function () {
        $(".select-grup").select2({
            placeholder: " -- Pilih Sub Bidang -- "
        });

        $(".select-kabupaten").select2({
            placeholder: " -- Pilih Kabupaten -- "
        });
    });

    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    // untuk show hide password
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>