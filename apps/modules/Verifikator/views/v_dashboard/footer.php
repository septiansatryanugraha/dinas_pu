<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12"> &copy;  <?php echo date('Y'); ?> Dinas PU SDA Jawa Timur </div>
        <!-- /span12 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /footer-inner --> 
</div>
<!-- /footer --> 


<script type="text/javascript" src="<?php echo base_url().'assets/publik/verifikator/dataTable/js/dataTables.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/publik/verifikator/datatable/js/dataTables2.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/publik/verifikator/js/bootstrap.min.js'?>"></script>
<script src="<?php echo base_url(); ?>admin-lte/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url(); ?>admin-lte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/ajax.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url(); ?>assets/toastr/toastr.js"></script>


