<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_proposal_nphd extends AUTH_Controller
{
    const __tableName = 'tbl_proposal';
    const __tableId = 'id_proposal';
    const __tableIdname = 'name';
    const __folder = 'v_proposal_nphd/';
    const __kode_menu = 'master-proposal-nphd';
    const __title = 'Proposal NPHD ';
    const __model = 'M_proposal_nphd';
    const __getLimitDpaYear = 2;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_generate_code');
        $this->load->model('M_utilities');
        $this->load->model('M_sidebar');
        $this->load->model('M_pokmas');
        $this->load->model('M_survei');
        $this->load->model('M_grup');
    }

    public function index()
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('view', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            $data['grupId'] = $this->session->userdata('grup_id');
            $data['status'] = $this->M_proposal_nphd->selectStatus();
            parent::loadkonten(self::__folder . 'home', $data);
        }
    }

    public function ajax_list()
    {
        $status = $this->input->post('status');
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = array(
            'status' => $status,
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $list = $this->M_proposal_nphd->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $proses = "Belum ada yang memproses";
            $status = '<small class="label pull-center bg-green">Penetapan Proposal</small>';
            if ($brand->status == 'NPHD') {
                $status = '<small class="label pull-center bg-green">Proposal NPHD</small>';
                $proses = $brand->updated_by;
            }

            $StatusSurvey = "";
            if ($brand->survei == "Survey") {
                $StatusSurvey = "<li><b>Sedang dilakukan Survey Lapangan<b></li>";
            } else if ($brand->survei == "Selesai Survey") {
                $StatusSurvey = "<li><b>Survei selesai di lakukan<b></li>";
            }

            $statusFile = "";
            $statusFileBerkas = '<small class="label pull-center bg-red">Berkas belum lengkap</small>';
            $statusFileLpj = $statusFileBerkas;
            $nphd = isset($brand->nphd) ? ($brand->nphd != null) ? json_decode($brand->nphd) : null : null;
            if ($nphd != null) {
                // BERKAS
                if (strlen($nphd->file_undangan) == 0) {
                    $statusFileBerkas = '<small class="label pull-center bg-red">Berkas belum lengkap</small>';
                } else if (strlen($nphd->file_pokmas) == 0) {
                    $statusFileBerkas = '<small class="label pull-center bg-red">Berkas belum lengkap</small>';
                } else if (strlen($nphd->file_nphd) == 0) {
                    $statusFileBerkas = '<small class="label pull-center bg-red">Berkas belum lengkap</small>';
                } else if (strlen($nphd->file_kwitansi) == 0) {
                    $statusFileBerkas = '<small class="label pull-center bg-red">Berkas belum lengkap</small>';
                } else if (strlen($nphd->file_pakta) == 0) {
                    $statusFileBerkas = '<small class="label pull-center bg-red">Berkas belum lengkap</small>';
                } else if (strlen($nphd->file_penggunaan_uang) == 0) {
                    $statusFileBerkas = '<small class="label pull-center bg-red">Berkas belum lengkap</small>';
                } else if (strlen($nphd->file_ta) == 0) {
                    $statusFileBerkas = '<small class="label pull-center bg-red">Berkas belum lengkap</small>';
                } else {
                    $statusFileBerkas = '<small class="label pull-center bg-blue">Berkas sudah lengkap</small>';
                }
                // LPJ
                if (strlen($nphd->file_lpj) == 0) {
                    $statusFileLpj = '<small class="label pull-center bg-red">Berkas belum lengkap</small>';
                } else if (strlen($nphd->file_sp2d) == 0) {
                    $statusFileLpj = '<small class="label pull-center bg-red">Berkas belum lengkap</small>';
                } else {
                    $statusFileLpj = '<small class="label pull-center bg-blue">Berkas sudah lengkap</small>';
                }
            }
            $statusFile = " <table>
                                <tr>
                                    <td style='width: 40%; vertical-align: top;'>File Berkas</td>
                                    <td style='width: 5%; vertical-align: top;'>:</td>
                                    <td style='vertical-align: top;'><b>" . $statusFileBerkas . "<b/></td>
                                </tr>
                                <tr>
                                    <td style='vertical-align: top;'>File LPJ</td>
                                    <td style='vertical-align: top;'>:</td>
                                    <td style='vertical-align: top;'><b>" . $statusFileLpj . "</b></td>
                                </tr>
                            </table>";

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->kode_proposal . '</b><br> Jenis Hibah : <b>' . $brand->jenis_hibah . '</b>' . '<br><br>' . $statusFile;
            $row[] = $brand->nama_kelompok . '<br><br>Grup : <b>' . $brand->nama_grup . '</b>';
            $row[] = $brand->kabupaten;
            $row[] = $status . '<br> Proposal di Proses oleh : <b>' . $proses . '</b>';
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->created_date, 'by' => $brand->created_by));
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->updated_date, 'by' => $brand->updated_by));

            //add html for action
            $action = "-";
            if ($accessEdit->menuview > 0) {
                $action = " <div class='btn-group'>";
                $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
                $action .= "    <ul class='dropdown-menu align-left pull-right'>";
                $action .= "        <li><a href='" . base_url('edit-proposal-nphd') . "/" . $brand->id_proposal . "'><i class='fa fa-edit'></i> Ubah</a></li>";
                if ($brand->status == 'NPHD') {
                    $action .= "    <li><a href='" . base_url('cetak-laporan') . "/" . $brand->id_proposal . "' target='__blank'><i class='fa fa-print'></i> Print Laporan</a></li>";
//                    $action .= "    <li><a href='" . base_url('cetak-undangan') . "/" . $brand->id_proposal . "' target='__blank'><i class='fa fa-print'></i> Print Und ke POKMAS</a></li>";
//                    $action .= "    <li><a href='" . base_url('cetak-pokmas') . "/" . $brand->id_proposal . "' target='__blank'><i class='fa fa-print'></i> Print Surat POKMAS</a></li>";
//                    $action .= "    <li><a href='" . base_url('cetak-nphd') . "/" . $brand->id_proposal . "' target='__blank'><i class='fa fa-print'></i> Print NPHD</a></li>";
//                    $action .= "    <li><a href='" . base_url('cetak-kwitansi') . "/" . $brand->id_proposal . "' target='__blank'><i class='fa fa-print'></i> Print Kwitansi</a></li>";
//                    $action .= "    <li><a href='" . base_url('cetak-pakta') . "/" . $brand->id_proposal . "' target='__blank'><i class='fa fa-print'></i> Print PAKTA</a></li>";
//                    $action .= "    <li><a href='" . base_url('cetak-penggunaan-uang') . "/" . $brand->id_proposal . "' target='__blank'><i class='fa fa-print'></i> Print Penggunaan Uang</a></li>";
//                    $action .= "    <li><a href='" . base_url('cetak-ta') . "/" . $brand->id_proposal . "' target='__blank'><i class='fa fa-print'></i> Print TA Sebelumnya</a></li>";
//                    $action .= "    <li><a href='" . base_url('cetak-sp') . "/" . $brand->id_proposal . "' target='__blank'><i class='fa fa-print'></i> Print SP</a></li>";
//                    $action .= "    <li><a href='" . base_url('cetak-sptmh') . "/" . $brand->id_proposal . "' target='__blank'><i class='fa fa-print'></i> Print SP Terus Menerus</a></li>";
//                    $action .= "    <li><a href='" . base_url('cetak-double-account') . "/" . $brand->id_proposal . "' target='__blank'><i class='fa fa-print'></i> Print Double Account</a></li>";
//                    $action .= "    <li><a href='" . base_url('cetak-tanda-terima') . "/" . $brand->id_proposal . "' target='__blank'><i class='fa fa-print'></i> Print Tanda Terima</a></li>";
//                    $action .= "    <li><a href='" . base_url('cetak-spkmp') . "/" . $brand->id_proposal . "' target='__blank'><i class='fa fa-print'></i> Print SPKMP</a></li>";
//                    $action .= "    <li><a href='" . base_url('cetak-bukti-nphd') . "/" . $brand->id_proposal . "' target='__blank'><i class='fa fa-print'></i> Print Hasil NPHD</a></li>";
                    $action .= "    <li><a href='" . base_url('lpj-proposal-nphd') . "/" . $brand->id_proposal . "'><i class='fa fa-edit'></i> LPJ</a></li>";
                    $action .= "    <li><a href='" . base_url('upload-proposal-nphd') . "/" . $brand->id_proposal . "'><i class='fa fa-upload'></i> Unggah Berkas</a></li>";
                }
                $action .= "    </ul>";
                $action .= "</div>";
            }
            $row[] = $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Ubah " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_proposal_nphd->selectById($id);
            if ($resultData != null) {
                $data['resultData'] = $resultData;
                $resultNphd = array();
                if (strlen($resultData->nphd) > 0) {
                    $resultNphd = json_decode($resultData->nphd, TRUE);
                }
                $data['resultNphd'] = $resultNphd;
                $data['terbilang'] = isset($resultData->nilai_anggaran) ? parent::terbilang(str_replace('.', '', $resultData->nilai_anggaran)) . ' Rupiah' : "";
                $data['pokmas'] = $this->M_pokmas->selectById($resultData->id_pokmas);
                $data['status'] = $this->M_proposal_nphd->selectStatus();
                $data['historiFile'] = $this->M_utilities->historiFile('tbl_proposal', $id, 'Pengajuan File');
                $data['historiFileBeritaAcara'] = $this->M_utilities->historiFile('tbl_proposal', $id, 'berita acara');
                $data['historiFileChecklist'] = $this->M_utilities->historiFile('tbl_proposal', $id, 'checklist');
                $data['historiFileHasilSurvei'] = $this->M_utilities->historiFile('tbl_proposal', $id, 'hasil survei');
                $data['historiSketsaSurvei'] = $this->M_utilities->historiFile('tbl_proposal', $id, 'sketsa survei');
                $data['menuName'] = self::__kode_menu;
                $data['idCustomer'] = $id;
                parent::loadkonten(self::__folder . 'update', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $arrFileType = array(
            'img_pokmas_1',
            'img_pokmas_2',
            'img_rek',
            'img_ktp',
        );

        $arrFileArchive = array(
            'undangan',
            'pokmas',
            'nphd',
            'kwitansi',
            'pakta',
            'penggunaan_uang',
            'ta',
            'sp',
            'double_account',
            'tanda_terima',
            'spkmp',
        );

        $skGub = $this->input->post('sk_gub');
        $tglSkGub = $this->input->post('tgl_sk_gub');
        $namaKetua = $this->input->post('nama_ketua');
        $noKtp = $this->input->post('no_ktp');
        $alamatRumah = $this->input->post('alamat_rumah');
        $alamatPokmas = $this->input->post('alamat_pokmas');
        $kodePos = $this->input->post('kode_pos');
        $telp = $this->input->post('telp');
        $waktuPelaksanaan = $this->input->post('waktu_pelaksanaan');
        $namaRek = $this->input->post('nama_rek');
        $cabang = $this->input->post('cabang');
        $noRek = $this->input->post('no_rek');
        $noNphd = $this->input->post('no_nphd');
        $tglNphd = $this->input->post('tgl_nphd');
        $noSuratKePokmas = $this->input->post('no_surat_ke_pokmas');
        $tglSurat = $this->input->post('tgl_surat');
        $tglSuratPokmas = $this->input->post('tgl_surat_pokmas');
        $PerihalProposal = $this->input->post('perihal');
        $NoSurat = $this->input->post('no_surat');
        $status = $this->input->post('status');
        $peringatan = $this->input->post('peringatan');
        $fileUpload = $_FILES['file_upload'];
        $arrFileUpload = parent::rotate($fileUpload);

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_proposal_nphd->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($skGub) == 0) {
                $errCode++;
                $errMessage = "Surat Gubernur wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglSkGub) == 0) {
                $errCode++;
                $errMessage = "Tanggal Surat Gubernur wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($namaKetua) == 0) {
                $errCode++;
                $errMessage = "Nama Ketua Pokmas wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noKtp) == 0) {
                $errCode++;
                $errMessage = "Nomor KTP wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($alamatRumah) == 0) {
                $errCode++;
                $errMessage = "Alamat Rumah wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($alamatPokmas) == 0) {
                $errCode++;
                $errMessage = "Alamat POKMAS wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($kodePos) == 0) {
                $errCode++;
                $errMessage = "Kode Pos wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($telp) == 0) {
                $errCode++;
                $errMessage = "No Telepon/Handphone wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($waktuPelaksanaan) == 0) {
                $errCode++;
                $errMessage = "Waktu Pelaksanaan wajib di isi.";
            }
        }
        // if ($errCode == 0) {
        //     if (strlen($namaRek) == 0) {
        //         $errCode++;
        //         $errMessage = "Atas Nama Pemilik Rekening Bank wajib di isi.";
        //     }
        // }
        // if ($errCode == 0) {
        //     if (strlen($cabang) == 0) {
        //         $errCode++;
        //         $errMessage = "Cabang Bank Jatim wajib di isi.";
        //     }
        // }
        if ($errCode == 0) {
            if (strlen($noRek) > 0) {
                if (strlen($noRek) > 11) {
                    $errCode++;
                    $errMessage = "Nomor Rekening Bank maksimal 11 digit.";
                }
            }
        }
        if ($errCode == 0) {
            if (strlen($noNphd) == 0) {
                $errCode++;
                $errMessage = "Nomor NPHD wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglNphd) == 0) {
                $errCode++;
                $errMessage = "Tanggal NPHD wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noSuratKePokmas) == 0) {
                $errCode++;
                $errMessage = "Tanggal NPHD wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglSurat) == 0) {
                $errCode++;
                $errMessage = "Tanggal Surat wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tglSuratPokmas) == 0) {
                $errCode++;
                $errMessage = "Tanggal Surat POKMAS wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if ($checkValid->status == 'NPHD') {
                $status = 'NPHD';
            }
            if (strlen($status) == 0) {
                $errCode++;
                $errMessage = "Status wajib di isi.";
            }
        }
        foreach ($arrFileUpload as $key => $value) {
            if ($value['size'] > 0) {
                if ($value['type'] != 'image/jpg' && $value['type'] != 'image/jpeg' && $value['type'] != 'image/png') {
                    $errCode++;
                    $errMessage = "Berkas wajib format jpg | jpeg | png.";
                    break;
                }
            }
        }
        if ($errCode == 0) {
            try {
                $dataNphd = array(
                    'sk_gub' => $skGub,
                    'tgl_sk_gub' => $tglSkGub,
                    'nama_ketua' => $namaKetua,
                    'no_ktp' => $noKtp,
                    'alamat_rumah' => $alamatRumah,
                    'alamat_pokmas' => $alamatPokmas,
                    'kode_pos' => $kodePos,
                    'telp' => $telp,
                    'waktu_pelaksanaan' => $waktuPelaksanaan,
                    'nama_rek' => $namaRek,
                    'cabang' => $cabang,
                    'no_rek' => $noRek,
                    'no_nphd' => $noNphd,
                    'tgl_nphd' => $tglNphd,
                    'no_surat_ke_pokmas' => $noSuratKePokmas,
                    'tgl_surat' => $tglSurat,
                    'tgl_surat_pokmas' => $tglSuratPokmas,
                    'peringatan' => $peringatan,
                );

                $resultNphd = array();
                if (strlen($checkValid->nphd) > 0) {
                    $resultNphd = json_decode($checkValid->nphd, TRUE);
                }
                foreach ($arrFileType as $key => $value) {
                    $dataNphd = array_merge($dataNphd, array(
                        'nama_file_' . $value => isset($resultNphd['nama_file_' . $value]) ? $resultNphd['nama_file_' . $value] : null,
                        'file_' . $value => isset($resultNphd['file_' . $value]) ? $resultNphd['file_' . $value] : null,
                    ));
                }

                foreach ($arrFileArchive as $key => $value) {
                    $dataNphd = array_merge($dataNphd, array(
                        'nama_file_' . $value => isset($resultNphd['nama_file_' . $value]) ? $resultNphd['nama_file_' . $value] : null,
                        'file_' . $value => isset($resultNphd['file_' . $value]) ? $resultNphd['file_' . $value] : null,
                    ));
                }

                $filesCount = count($fileUpload['name']);
                if ($filesCount == count($arrFileType)) {
                    for ($i = 0; $i < $filesCount; $i++) {
                        if ($fileUpload['size'][$i] > 0) {
                            $_FILES['file_uploads']['name'] = $fileUpload['name'][$i];
                            $_FILES['file_uploads']['type'] = $fileUpload['type'][$i];
                            $_FILES['file_uploads']['tmp_name'] = $fileUpload['tmp_name'][$i];
                            $_FILES['file_uploads']['error'] = $fileUpload['error'][$i];
                            $_FILES['file_uploads']['size'] = $fileUpload['size'][$i];

                            $folder = parent::createFolder($arrFileType[$i]);
                            $uploadPath = "upload/" . $arrFileType[$i] . "/";

                            $config['path'] = $uploadPath;
                            $config['upload_path'] = "./" . $uploadPath;
                            $config['allowed_types'] = 'jpg|jpeg|png';
                            $config['max_size'] = '8048'; //maksimum besar file 8M
                            $config['file_name'] = str_replace(" ", "_", "file_" . time() . "_" . $fileUpload['name'][$i]);
                            $config['overwrite'] = TRUE;

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);

                            if ($this->upload->do_upload('file_uploads')) {
                                $imageData = $this->upload->data();

                                $data2 = array(
                                    'ref_table' => 'tbl_proposal',
                                    'ref_id' => $id,
                                    'nama_file_upload' => $imageData['file_name'],
                                    'file_upload' => $uploadPath . '' . $imageData['file_name'],
                                    // 'tanggal' => $date2,
                                    'status' => trim(str_replace('file', '', str_replace('_', ' ', $arrFileType[$i]))),
                                    'created_date' => $date,
                                    'created_by' => $username,
                                    'updated_date' => $date,
                                    'updated_by' => $username,
                                );
                                $result = $this->db->insert('tbl_history_upload', $data2);

                                $dataNphd['nama_file_' . $arrFileType[$i]] = $data2['nama_file_upload'];
                                $dataNphd['file_' . $arrFileType[$i]] = $data2['file_upload'];
                            }
                        }
                    }
                }

                $data = array(
                    'nphd' => json_encode($dataNphd),
                    'status' => $status,
                    'perihal' => $PerihalProposal,
                    'no_surat' => $NoSurat,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );


                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $id));

                if ($checkValid->status != 'NPHD') {
                    $data3 = array(
                        'id_proposal' => $id,
                        'status' => $status,
                        'keterangan' => 'user <b>' . $username . '</b> Sedang melakukan proses ' . $status . ' proposal ke Dinas Jawa Timur',
                        'created_date' => $date,
                        'created_by' => $username,
                        'updated_date' => $date,
                        'updated_by' => $username,
                    );
                    $result = $this->db->insert('tbl_log', $data3);
                }
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = true;
            $out['pesan'] = 'Proposal NPHD berhasil di update.';
        } else {
            $this->db->trans_rollback();
            $out['status'] = false;
            $out['pesan'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function Upload($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Upload Berkas File " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_proposal_nphd->selectById($id);
            if ($resultData != null) {
                if ($resultData->status == 'NPHD') {
                    $data['resultData'] = $resultData;
                    $resultNphd = array();
                    if (strlen($resultData->nphd) > 0) {
                        $resultNphd = json_decode($resultData->nphd, TRUE);
                    }
                    $data['resultNphd'] = $resultNphd;
                    $data['pokmas'] = $this->M_pokmas->selectById($resultData->id_pokmas);
                    $data['status'] = $this->M_proposal_nphd->selectStatus();
                    $data['menuName'] = self::__kode_menu;
                    $data['idCustomer'] = $id;
                    parent::loadkonten(self::__folder . 'upload', $data);
                } else {
                    echo "<script>alert('" . self::__title . " belum di proses NPHD.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
                }
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpload($id)
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $arrFileArchive = array(
            'undangan',
            'nphd',
            'pokmas',
            'kwitansi',
            'pakta',
            'penggunaan_uang',
            'ta',
        );

        $fileUpload = $_FILES['file_upload'];
        $arrFileUpload = parent::rotate($fileUpload);

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_proposal_nphd->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkValid->status != 'NPHD') {
                $errCode++;
                $errMessage = "Status proposal blum ter-NPHD.";
            }
        }
        foreach ($arrFileUpload as $key => $value) {
            if ($value['size'] > 0) {
                if ($value['type'] != 'application/pdf') {
                    $errCode++;
                    $errMessage = "Berkas wajib format pdf.";
                    break;
                }
            }
        }
        if ($errCode == 0) {
            try {
                $resultNphd = json_decode($checkValid->nphd, TRUE);

                foreach ($arrFileArchive as $key => $value) {
                    $resultNphd = array_merge($resultNphd, array(
                        'nama_file_' . $value => isset($resultNphd['nama_file_' . $value]) ? $resultNphd['nama_file_' . $value] : null,
                        'file_' . $value => isset($resultNphd['file_' . $value]) ? $resultNphd['file_' . $value] : null,
                    ));
                }

                $filesCount = count($fileUpload['name']);
                if ($filesCount == count($arrFileArchive)) {
                    for ($i = 0; $i < $filesCount; $i++) {
                        if ($fileUpload['size'][$i] > 0) {
                            $_FILES['file_uploads']['name'] = $fileUpload['name'][$i];
                            $_FILES['file_uploads']['type'] = $fileUpload['type'][$i];
                            $_FILES['file_uploads']['tmp_name'] = $fileUpload['tmp_name'][$i];
                            $_FILES['file_uploads']['error'] = $fileUpload['error'][$i];
                            $_FILES['file_uploads']['size'] = $fileUpload['size'][$i];

                            $folder = parent::createFolder('file_berkas_' . $arrFileArchive[$i]);
                            $uploadPath = "upload/file_berkas_" . $arrFileArchive[$i] . "/";

                            $config['path'] = $uploadPath;
                            $config['upload_path'] = "./" . $uploadPath;
                            $config['allowed_types'] = 'pdf';
                            $config['max_size'] = '8048'; //maksimum besar file 8M
                            $config['file_name'] = str_replace(" ", "_", "file_" . time() . "_" . $fileUpload['name'][$i]);
                            $config['overwrite'] = TRUE;

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);

                            if ($this->upload->do_upload('file_uploads')) {
                                $imageData = $this->upload->data();

                                $data2 = array(
                                    'ref_table' => 'tbl_proposal',
                                    'ref_id' => $id,
                                    'nama_file_upload' => $imageData['file_name'],
                                    'file_upload' => $uploadPath . '' . $imageData['file_name'],
                                    // 'tanggal' => $date2,
                                    'status' => trim(str_replace('file', '', str_replace('_', ' ', $arrFileArchive[$i]))),
                                    'created_date' => $date,
                                    'created_by' => $username,
                                    'updated_date' => $date,
                                    'updated_by' => $username,
                                );
                                $result = $this->db->insert('tbl_history_upload', $data2);

                                $resultNphd['nama_file_' . $arrFileArchive[$i]] = $data2['nama_file_upload'];
                                $resultNphd['file_' . $arrFileArchive[$i]] = $data2['file_upload'];
                            }
                        }
                    }
                }

                $data = array(
                    'nphd' => json_encode($resultNphd),
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = true;
            $out['pesan'] = 'Upload berkas NPHD berhasil.';
        } else {
            $this->db->trans_rollback();
            $out['status'] = false;
            $out['pesan'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function lpj($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "LPJ dan SP2D " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_proposal_nphd->selectById($id);
            if ($resultData != null) {
                if ($resultData->status == 'NPHD') {
                    $data['resultData'] = $resultData;
                    $resultNphd = array();
                    if (strlen($resultData->nphd) > 0) {
                        $resultNphd = json_decode($resultData->nphd, TRUE);
                    }
                    $data['resultNphd'] = $resultNphd;
                    $data['pokmas'] = $this->M_pokmas->selectById($resultData->id_pokmas);
                    $data['status'] = $this->M_proposal_nphd->selectStatus();
                    $data['menuName'] = self::__kode_menu;
                    $data['idCustomer'] = $id;
                    parent::loadkonten(self::__folder . 'lpj', $data);
                } else {
                    echo "<script>alert('" . self::__title . " belum di proses NPHD.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
                }
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesLpj($id)
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $noLpj = $this->input->post('no_lpj');
        $tglLpj = $this->input->post('tgl_lpj');
        $tglLpjDiterima = $this->input->post('tgl_lpj_diterima');
        $nominalLpj = $this->input->post('nominal_lpj');
        $statusPenerimaan = $this->input->post('status_penerimaan');
        $skSp2d = $this->input->post('sk_sp2d');
        $tglSp2d = $this->input->post('tgl_sp2d');
        $nominalSp2d = $this->input->post('nominal_sp2d');

        $arrFileArchive = array(
            'lpj',
            'sp2d',
        );

        $fileUpload = $_FILES['file_upload'];
        $arrFileUpload = parent::rotate($fileUpload);

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_proposal_nphd->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkValid->status != 'NPHD') {
                $errCode++;
                $errMessage = "Status proposal blum ter-NPHD.";
            }
        }
        foreach ($arrFileUpload as $key => $value) {
            if ($value['size'] > 0) {
                if ($value['type'] != 'application/pdf') {
                    $errCode++;
                    $errMessage = "Berkas wajib format pdf.";
                    break;
                }
            }
        }
        if ($errCode == 0) {
            try {
                $resultNphd = json_decode($checkValid->nphd, TRUE);

                $resultNphd = array_merge($resultNphd, [
                    'no_lpj' => $noLpj,
                    'tgl_lpj' => $tglLpj,
                    'tgl_lpj_diterima' => $tglLpjDiterima,
                    'nominal_lpj' => $nominalLpj,
                    'status_penerimaan' => $statusPenerimaan,
                    'sk_sp2d' => $skSp2d,
                    'tgl_sp2d' => $tglSp2d,
                    'nominal_sp2d' => $nominalSp2d,
                ]);

                foreach ($arrFileArchive as $key => $value) {
                    $resultNphd = array_merge($resultNphd, [
                        'nama_file_' . $value => isset($resultNphd['nama_file_' . $value]) ? $resultNphd['nama_file_' . $value] : null,
                        'file_' . $value => isset($resultNphd['file_' . $value]) ? $resultNphd['file_' . $value] : null,
                    ]);
                }

                $filesCount = count($fileUpload['name']);
                if ($filesCount == count($arrFileArchive)) {
                    for ($i = 0; $i < $filesCount; $i++) {
                        if ($fileUpload['size'][$i] > 0) {
                            $_FILES['file_uploads']['name'] = $fileUpload['name'][$i];
                            $_FILES['file_uploads']['type'] = $fileUpload['type'][$i];
                            $_FILES['file_uploads']['tmp_name'] = $fileUpload['tmp_name'][$i];
                            $_FILES['file_uploads']['error'] = $fileUpload['error'][$i];
                            $_FILES['file_uploads']['size'] = $fileUpload['size'][$i];

                            $folder = parent::createFolder('file_berkas_' . $arrFileArchive[$i]);
                            $uploadPath = "upload/file_berkas_" . $arrFileArchive[$i] . "/";

                            $config['path'] = $uploadPath;
                            $config['upload_path'] = "./" . $uploadPath;
                            $config['allowed_types'] = 'pdf';
                            $config['max_size'] = '8048'; //maksimum besar file 8M
                            $config['file_name'] = str_replace(" ", "_", "file_" . time() . "_" . $fileUpload['name'][$i]);
                            $config['overwrite'] = TRUE;

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);

                            if ($this->upload->do_upload('file_uploads')) {
                                $imageData = $this->upload->data();

                                $data2 = array(
                                    'ref_table' => 'tbl_proposal',
                                    'ref_id' => $id,
                                    'nama_file_upload' => $imageData['file_name'],
                                    'file_upload' => $uploadPath . '' . $imageData['file_name'],
                                    // 'tanggal' => $date2,
                                    'status' => trim(str_replace('file', '', str_replace('_', ' ', $arrFileArchive[$i]))),
                                    'created_date' => $date,
                                    'created_by' => $username,
                                    'updated_date' => $date,
                                    'updated_by' => $username,
                                );
                                $result = $this->db->insert('tbl_history_upload', $data2);

                                $resultNphd['nama_file_' . $arrFileArchive[$i]] = $data2['nama_file_upload'];
                                $resultNphd['file_' . $arrFileArchive[$i]] = $data2['file_upload'];
                            }
                        }
                    }
                }

                $data = [
                    'nphd' => json_encode($resultNphd),
                    'updated_by' => $username,
                    'updated_date' => $date,
                ];
                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = true;
            $out['pesan'] = 'LPJ berhasil.';
        } else {
            $this->db->trans_rollback();
            $out['status'] = false;
            $out['pesan'] = $errMessage;
        }

        echo json_encode($out);
    }

    // <editor-fold defaultstate="collapsed" desc="Cetak Laporan">

    public function cetak_undangan($id)
    {
        $this->cetak('undangan', $id);
    }

    public function cetak_pokmas($id)
    {
        $this->cetak('pokmas', $id);
    }

    public function cetak_nphd($id)
    {
        $this->cetak('nphd', $id);
    }

    public function cetak_kwitansi($id)
    {
        $this->cetak('kwitansi', $id);
    }

    public function cetak_pakta($id)
    {
        $this->cetak('pakta', $id);
    }

    public function cetak_penggunaan_uang($id)
    {
        $this->cetak('penggunaan_uang', $id);
    }

    public function cetak_ta($id)
    {
        $this->cetak('ta', $id);
    }

    public function cetak_sptmh($id)
    {
        $this->cetak('sptmh', $id);
    }

    public function cetak_sp($id)
    {
        $this->cetak('sp', $id);
    }

    public function cetak_double_account($id)
    {
        $this->cetak('double_account', $id);
    }

    public function cetak_tanda_terima($id)
    {
        $this->cetak('tanda_terima', $id);
    }

    public function cetak_spkmp($id)
    {
        $this->cetak('spkmp', $id);
    }

    public function cetak_bukti_nphd($id)
    {
        $this->cetak('bukti_nphd', $id);
    }

    public function cetak_laporan($id)
    {
        $this->cetak('all', $id);
    }

    public function cetak($type, $id)
    {
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Cetak " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_proposal_nphd->selectById($id);
            if ($resultData != null) {
                if ($resultData->status == 'NPHD') {
                    $data['resultData'] = $resultData;
                    $resultNphd = array();
                    if (strlen($resultData->nphd) > 0) {
                        $resultNphd = json_decode($resultData->nphd, TRUE);
                    }

                    $nilaiAnggaran = isset($resultData->nilai_anggaran) ? $resultData->nilai_anggaran : 0;
                    $nilaiAnggaran = str_replace('.', '', $nilaiAnggaran);
                    $nilaiAnggaran = str_replace(',', '', $nilaiAnggaran);

                    $data['resultNphd'] = $resultNphd;
                    $data['terbilang'] = ucwords(strtolower(parent::terbilang($nilaiAnggaran))) . ' Rupiah';
                    $data['tglProposal'] = isset($resultData->tanggal) ? parent::tgl_indo(date('Y-m-d', strtotime($resultData->tanggal))) : "";
                    $data['tglSurat'] = isset($resultNphd['tgl_surat']) ? parent::tgl_indo(date('Y-m-d', strtotime($resultNphd['tgl_surat']))) : "";
                    $data['tglSuratPokmas'] = isset($resultNphd['tgl_surat_pokmas']) ? parent::tgl_indo(date('Y-m-d', strtotime($resultNphd['tgl_surat_pokmas']))) : "";
                    $data['tglSkGub'] = isset($resultNphd['tgl_sk_gub']) ? parent::tgl_indo(date('Y-m-d', strtotime($resultNphd['tgl_sk_gub']))) : "";
                    $data['daySkGub'] = isset($resultNphd['tgl_sk_gub']) ? parent::getHari(date('l', strtotime($resultNphd['tgl_sk_gub']))) : "";
                    $data['dateSkGub'] = isset($resultNphd['tgl_sk_gub']) ? parent::terbilang(parent::tgl_indo(date('Y-m-d', strtotime($resultNphd['tgl_surat_pokmas'])))) : "";
                    $data['monthSkGub'] = isset($resultNphd['tgl_sk_gub']) ? parent::bln_aja(date('Y-m-d', strtotime($resultNphd['tgl_surat_pokmas']))) : "";
                    $data['yearSkGub'] = isset($resultNphd['tgl_sk_gub']) ? parent::terbilang(date('Y-m-d', strtotime($resultNphd['tgl_surat_pokmas']))) : "";
                    $data['tglNphd'] = isset($resultNphd['tgl_nphd']) ? parent::tgl_indo2(date('Y-m-d', strtotime($resultNphd['tgl_nphd']))) : "";
                    $data['dayNphd'] = isset($resultNphd['tgl_nphd']) ? parent::getHari(date('l', strtotime($resultNphd['tgl_nphd']))) : "";
                    $data['dateNphd'] = isset($resultNphd['tgl_nphd']) ? parent::terbilang(parent::tgl_indo(date('Y-m-d', strtotime($resultNphd['tgl_nphd'])))) : "";
                    $data['monthNphd'] = isset($resultNphd['tgl_nphd']) ? parent::bln_aja(date('Y-m-d', strtotime($resultNphd['tgl_nphd']))) : "";
                    $data['yearNphd'] = isset($resultNphd['tgl_nphd']) ? parent::terbilang(date('Y-m-d', strtotime($resultNphd['tgl_nphd']))) : "";
                    $data['tglNow'] = parent::tgl_indo(date('Y-m-d'));
                    $data['dayNow'] = parent::getHari(date('l'));
                    $data['dateNow'] = parent::terbilang(parent::tgl_indo(date('Y-m-d')));
                    $data['monthNow'] = parent::bln_aja(date('Y-m-d'));
                    $data['yearNow'] = parent::terbilang(date('Y-m-d'));
                    $data['pokmas'] = $this->M_pokmas->selectById($resultData->id_pokmas);
                    $data['page'] = self::__title;
                    $data['judul'] = self::__title;
                    $data['getManagementSystem'] = $this->M_utilities->getManagementSystem($resultData->id_grup);

//                    echo '<pre>';
//                    print_r($data);
//                    echo '</pre>';
//                    echo '<br>';
//                    die();

                    require_once(APPPATH . 'libraries/mpdf_v8.0.3-master/vendor/autoload.php');
                    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);
                    if ($type == 'all' || $type == 'undangan') {
//                        $this->load->view(self::__folder . 'cetak_undangan', $data);
                        $mpdf->AddPage("P", "", "", "", "", "10", "10", "4", "3", "", "", "", "", "", "", "", "", "", "", "", "A4");
                        $mpdf->WriteHTML($this->load->view(self::__folder . 'pdf/cetak_undangan', $data, true));
                        $filename = 'undangan';
                    }
                    if ($type == 'all' || $type == 'pokmas') {
//                        $this->load->view(self::__folder . 'cetak_pokmas', $data);
                        $mpdf->AddPage("P", "", "", "", "", "10", "10", "10", "10", "", "", "", "", "", "", "", "", "", "", "", "A4");
                        $mpdf->WriteHTML($this->load->view(self::__folder . 'pdf/cetak_pokmas', $data, true));
                        $filename = 'pokmas';
                    }
                    if ($type == 'all' || $type == 'kwitansi') {
//                        $this->load->view(self::__folder . 'cetak_kwitansi', $data);
                        $mpdf->AddPage("P", "", "", "", "", "10", "10", "10", "10", "", "", "", "", "", "", "", "", "", "", "", "A4");
                        $mpdf->WriteHTML($this->load->view(self::__folder . 'pdf/cetak_kwitansi', $data, true));
                        $filename = 'kwitansi';
                    }
                    if ($type == 'all' || $type == 'pakta') {
//                        $this->load->view(self::__folder . 'cetak_pakta', $data);
                        $mpdf->AddPage("P", "", "", "", "", "10", "10", "10", "10", "", "", "", "", "", "", "", "", "", "", "", "A4");
                        $mpdf->WriteHTML($this->load->view(self::__folder . 'pdf/cetak_pakta', $data, true));
                        $filename = 'pakta';
                    }
                    if ($type == 'all' || $type == 'penggunaan_uang') {
//                        $this->load->view(self::__folder . 'cetak_penggunaan_uang', $data);
                        $mpdf->AddPage("P", "", "", "", "", "10", "10", "10", "10", "", "", "", "", "", "", "", "", "", "", "", "A4");
                        $mpdf->WriteHTML($this->load->view(self::__folder . 'pdf/cetak_penggunaan_uang', $data, true));
                        $filename = 'penggunaan-uang';
                    }
                    if ($type == 'all' || $type == 'ta') {
//                        $this->load->view(self::__folder . 'cetak_ta', $data);
                        $mpdf->AddPage("P", "", "", "", "", "10", "10", "10", "10", "", "", "", "", "", "", "", "", "", "", "", "A4");
                        $mpdf->WriteHTML($this->load->view(self::__folder . 'pdf/cetak_ta', $data, true));
                        $filename = 'ta-sebelumnya';
                    }
                    if ($type == 'all' || $type == 'sp') {
//                        $this->load->view(self::__folder . 'cetak_sp', $data);
                        $mpdf->AddPage("P", "", "", "", "", "10", "10", "10", "10", "", "", "", "", "", "", "", "", "", "", "", "A4");
                        $mpdf->WriteHTML($this->load->view(self::__folder . 'pdf/cetak_sp', $data, true));
                        $filename = 'pernyataan';
                    }
                    if ($type == 'all' || $type == 'sptmh') {
//                        $this->load->view(self::__folder . 'cetak_sptmh', $data);
                        $mpdf->AddPage("P", "", "", "", "", "10", "10", "10", "10", "", "", "", "", "", "", "", "", "", "", "", "A4");
                        $mpdf->WriteHTML($this->load->view(self::__folder . 'pdf/cetak_sptmh', $data, true));
                        $filename = 'sptmh';
                    }
                    if ($type == 'all' || $type == 'double_account') {
//                        $this->load->view(self::__folder . 'cetak_double_account', $data);
                        $mpdf->AddPage("P", "", "", "", "", "10", "10", "10", "10", "", "", "", "", "", "", "", "", "", "", "", "A4");
                        $mpdf->WriteHTML($this->load->view(self::__folder . 'pdf/cetak_double_account', $data, true));
                        $filename = 'double-account';
                    }
                    if ($type == 'all' || $type == 'tanda_terima') {
//                        $this->load->view(self::__folder . 'cetak_tanda_terima', $data);
                        $mpdf->AddPage("P", "", "", "", "", "10", "10", "10", "10", "", "", "", "", "", "", "", "", "", "", "", "A4");
                        $mpdf->WriteHTML($this->load->view(self::__folder . 'pdf/cetak_tanda_terima', $data, true));
                        $filename = 'tanda-terima';
                    }
                    if ($type == 'all' || $type == 'spkmp') {
//                        $this->load->view(self::__folder . 'cetak_spkmp', $data);
                        $mpdf->AddPage("P", "", "", "", "", "10", "10", "10", "10", "", "", "", "", "", "", "", "", "", "", "", "A4");
                        $mpdf->WriteHTML($this->load->view(self::__folder . 'pdf/cetak_spkmp', $data, true));
                        $filename = 'spkmp';
                    }
                    if ($type == 'all' || $type == 'nphd') {
//                        $this->load->view(self::__folder . 'cetak_nphd', $data);
                        $mpdf->AddPage("P", "", "", "", "", "10", "10", "15", "15", "", "", "", "", "", "", "", "", "", "", "", "A4");
                        $mpdf->WriteHTML($this->load->view(self::__folder . 'pdf/cetak_nphd', $data, true));
                        $filename = 'nphd';
                    }
                    if ($type == 'all' || $type == 'bukti_nphd') {
//                        $this->load->view(self::__folder . 'cetak_bukti_nphd', $data);
                        $mpdf->AddPage("P", "", "", "", "", "10", "10", "10", "10", "", "", "", "", "", "", "", "", "", "", "", "A4");
                        $mpdf->WriteHTML($this->load->view(self::__folder . 'pdf/cetak_bukti_nphd', $data, true));
                        $filename = 'bukti-nphd';
                    }
                    if ($type == 'all') {
                        $filename = 'laporan-nphd';
                    }
                    $mpdf->Output($filename . '.pdf', 'I');
                } else {
                    echo "<script>alert('" . self::__title . " belum di proses NPHD.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
                }
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    // </editor-fold>

    public function importData()
    {
        if ($this->session->userdata('grup_id') == 1) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            $data['userdata'] = $this->userdata;
            $data['page'] = "Import Data";
            $data['judul'] = "Import Data";
            if ($access->menuview == 0) {
                parent::loadkonten('Dashboard/layouts/no_akses', $data);
            } else {
                $data['menuName'] = self::__kode_menu;
                $data['selectGrup'] = $this->M_grup->select(['is_bidang' => 1]);
                parent::loadkonten('' . self::__folder . 'import', $data);
            }
        } else {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        }
    }

    public function prosesImportData()
    {
        $username = 'System';
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');
        $error = false;
        $out = array('status' => true, 'pesan' => ' No Data');

        $this->form_validation->set_rules('excel', 'File', 'trim|required');
        $idGrup = trim($this->input->post("id_grup"));
        if ($_FILES['excel']['name'] != '' && $this->session->userdata('grup_id') == 1 && strlen($idGrup) > 0) {
            $config['upload_path'] = './assets/excel/';
            $config['allowed_types'] = 'xls|xlsx';

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('excel')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $dataUpload = $this->upload->data();

                error_reporting(E_ALL);
                date_default_timezone_set('Asia/Jakarta');

                include './assets/phpexcel/Classes/PHPExcel/IOFactory.php';
                $inputFileName = './assets/excel/' . $dataUpload['file_name'];
                $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                $jml = 0;
                if (!$error) {
                    foreach ($sheetData as $key => $row) {
                        if ($key > 1) {
                            $errCode = 0;
                            $errMessage = "";
                            $this->db->trans_begin();
                            if ($errCode == 0) {
                                $namaPokmas = trim($row['A']);
                                $kodePosPokmas = trim($row['B']);
                                $telpKetumPokmas = trim($row['C']);
                                $namaKetuaPokmas = trim($row['D']);
                                $ktpKetumPokmas = trim($row['E']);
                                $alamatKetumPokmas = trim($row['F']);
                                $noUrutDpa = trim($row['G']);
                                $noSuratProposal = trim($row['H']);
                                $tglProposal = trim($row['I']);
                                $perihalProposal = trim($row['J']);
                                $nominal = trim($row['K']);
                                $nominalPaguDpa = trim($row['L']);
                                $noBa = trim($row['M']);
                                $catatanEvaluasi = trim($row['N']);
                                $jenisKegiatanSurvei = trim($row['O']);
                                $tanggalSurvei = trim($row['P']);
                                $alamatSurvei = trim($row['Q']);
                                $catatanSurvei = trim($row['R']);
                                $noBiro = trim($row['S']);
                                $masukDpa = trim($row['T']);
                                $skGub = trim($row['U']);
                                $tglSkGub = trim($row['V']);
                                $waktuPelaksanaan = trim($row['W']);
                                $pemilikRek = trim($row['X']);
                                $bankJatimCab = trim($row['Y']);
                                $noRek = trim($row['Z']);
                                $noNphd = trim($row['AA']);
                                $tglNphd = trim($row['AB']);
                                $noSuratKePokmas = trim($row['AC']);
                                $tglSurat = trim($row['AD']);
                                $tglSuratPokmas = trim($row['AE']);

                                $desa = trim($row['AG']);
                                $kecamatan = trim($row['AH']);
                                $kabupaten = trim($row['AI']);

                                try {
                                    if (strlen($namaPokmas) > 0) {
                                        $idDesa = null;
                                        if (strlen($kabupaten) > 0 && strlen($kecamatan) > 0 && strlen($desa) > 0) {
                                            // Check Kecamatan
                                            $idKecamatan = null;
                                            $customKabupaten = $kabupaten;
                                            $customKabupaten = str_replace('KABUPATEN', '', $customKabupaten);
                                            $customKabupaten = str_replace('Kabupaten', '', $customKabupaten);
                                            $customKabupaten = str_replace('kabupaten', '', $customKabupaten);
                                            $customKabupaten = str_replace('KOTA', '', $customKabupaten);
                                            $customKabupaten = str_replace('Kota', '', $customKabupaten);
                                            $customKabupaten = str_replace('kota', '', $customKabupaten);
                                            $customKabupaten = trim($customKabupaten);
                                            $qKecamatan = " SELECT tbl_kecamatan.*
                                                            FROM tbl_kecamatan 
                                                            LEFT JOIN tbl_kabupaten ON tbl_kabupaten.id = tbl_kecamatan.id_kabupaten
                                                            WHERE tbl_kecamatan.deleted_date IS NULL 
                                                            AND tbl_kabupaten.kabupaten like '%{$customKabupaten}%' 
                                                            AND tbl_kecamatan.kecamatan = '{$kecamatan}'";
                                            $getKecamatan = $this->db->query($qKecamatan)->row();
                                            if ($getKecamatan != null) {
                                                $idKecamatan = $getKecamatan->id;
                                            } else {
                                                $qKabupaten = "SELECT * FROM tbl_kabupaten WHERE deleted_date IS NULL AND id_provinsi = 1 AND kabupaten = '{$kabupaten}'";
                                                $getKabupaten = $this->db->query($qKabupaten)->row();
                                                if ($getKabupaten == null) {
                                                    $dataKabupaten = array(
                                                        'id_provinsi' => 1,
                                                        'kabupaten' => trim(strtoupper($kabupaten)),
                                                        'created_date' => $date,
                                                        'created_by' => $username,
                                                        'updated_date' => $date,
                                                        'updated_by' => $username,
                                                    );
//                                                    echo '<pre>';
//                                                    print_r($dataKabupaten);
//                                                    echo '</pre>';
//                                                    echo '<br>';
                                                    $this->db->insert('tbl_kabupaten', $dataKabupaten);
                                                    $idKabupaten = $this->db->insert_id();
                                                } else {
                                                    $idKabupaten = $getKabupaten->id;
                                                }
                                                if (strlen($idKabupaten) > 0) {
                                                    $dataKecamatan = array(
                                                        'id_kabupaten' => $idKabupaten,
                                                        'kecamatan' => trim(strtoupper($kecamatan)),
                                                        'created_date' => $date,
                                                        'created_by' => $username,
                                                        'updated_date' => $date,
                                                        'updated_by' => $username,
                                                    );
//                                                    echo '<pre>';
//                                                    print_r($dataKecamatan);
//                                                    echo '</pre>';
//                                                    echo '<br>';
                                                    $this->db->insert('tbl_kecamatan', $dataKecamatan);
                                                    $idKecamatan = $this->db->insert_id();
                                                }
                                            }

                                            if (strlen($idKecamatan) > 0) {
                                                // Check Desa
                                                $qDesa = 'SELECT * FROM tbl_desa WHERE deleted_date IS NULL AND id_kecamatan = "' . $idKecamatan . '" AND desa = "' . $desa . '"';
                                                $getDesa = $this->db->query($qDesa)->row();
                                                if ($getDesa != null) {
                                                    $idDesa = $getDesa->id;
//                                                    echo '<pre>';
//                                                    print_r($idDesa);
//                                                    echo '</pre>';
//                                                    echo '<br>';
                                                } else {
                                                    $dataDesa = array(
                                                        'id_kecamatan' => $idKecamatan,
                                                        'desa' => trim(strtoupper($desa)),
                                                        'created_date' => $date,
                                                        'created_by' => $username,
                                                        'updated_date' => $date,
                                                        'updated_by' => $username,
                                                    );
//                                                    echo '<pre>';
//                                                    print_r($dataDesa);
//                                                    echo '</pre>';
//                                                    echo '<br>';
                                                    $this->db->insert('tbl_desa', $dataDesa);
                                                    $idDesa = $this->db->insert_id();
                                                }

                                                if (strlen($idDesa) > 0) {
                                                    $checkData = $this->M_pokmas->selectByRandom(['nama_kelompok' => $namaPokmas, 'id_grup' => $idGrup, 'id_desa' => $idDesa]);
                                                    if ($checkData == null) {
                                                        $dataPokmas = array(
                                                            "kode_pokmas" => $this->M_generate_code->getNextPokmas(),
                                                            "id_user" => $this->session->userdata('id'),
                                                            "id_grup" => $idGrup,
                                                            'nama_kelompok' => $namaPokmas,
                                                            'nama_ketua' => $namaKetuaPokmas,
                                                            'id_desa' => $idDesa,
                                                            'alamat' => $alamatKetumPokmas,
                                                            'status' => 'Aktif',
                                                            'tanggal' => date('Y-m-d'),
                                                            'created_date' => $date,
                                                            'created_by' => $username,
                                                            'updated_date' => $date,
                                                            'updated_by' => $username,
                                                        );
//                                                        echo '<pre>';
//                                                        print_r($dataPokmas);
//                                                        echo '</pre>';
//                                                        echo '<br>';
                                                        $this->db->insert('tbl_pokmas', $dataPokmas);
                                                    }
                                                }
                                            } else {
                                                $jml++;
                                            }
                                        }

                                        $resultData = $this->M_pokmas->selectByRandom(['nama_kelompok' => $namaPokmas, 'id_grup' => $idGrup, 'id_desa' => $idDesa]);
                                        if ($resultData != null) {
                                            // <editor-fold defaultstate="collapsed" desc="Form Pengajuan">
                                            $code = $this->M_generate_code->getNextProposal();

                                            $config['cacheable'] = true;
                                            $config['cachedir'] = './upload/dir/';
                                            $config['errorlog'] = './upload/log/';
                                            $config['imagedir'] = './upload/qrcode/';
                                            $config['quality'] = true;
                                            $config['size'] = '1024';
                                            $config['black'] = array(224, 255, 255);
                                            $config['white'] = array(70, 130, 180);
                                            $this->ciqrcode->initialize($config);
                                            $qrcodeImage = $code . '.png';

                                            $params['data'] = $code;
                                            $params['level'] = 'H';
                                            $params['size'] = 10;
                                            $params['savename'] = FCPATH . $config['imagedir'] . $qrcodeImage;
                                            $this->ciqrcode->generate($params);

                                            $data = array(
                                                "qrcode" => $qrcodeImage,
                                                'kode_proposal' => $code,
                                                'id_pokmas' => $resultData->id_pokmas,
                                                'no_dpa' => $noUrutDpa,
                                                'no_surat' => $noSuratProposal,
                                                'perihal' => $perihalProposal,
                                                'jenis_bantuan' => 'Uang',
                                                'usulan_nilai_anggaran' => str_replace(',', '.', $nominal),
                                                'nilai_anggaran' => str_replace(',', '.', $nominalPaguDpa),
                                                'tipe' => 'pengajuan permohonan rekomendasi',
                                                'status' => 'Belum Verifikasi',
                                                'tgl_help' => $date2,
                                                'created_date' => $date,
                                                'created_by' => $username,
                                                'updated_date' => $date,
                                                'updated_by' => $username,
                                            );
                                            $tglProposal = $this->reformatDate($tglProposal);
                                            if ($tglProposal != '1970-01-01') {
                                                $data['tanggal'] = $tglProposal;
                                            }
                                            $this->db->insert('tbl_proposal', $data);
                                            $idProposal = $this->db->insert_id();

                                            $data2 = array(
                                                'id_proposal' => $idProposal,
                                                'status' => 'Belum Verifikasi',
                                                'keterangan' => 'user <b>' . $username . '</b> melakukan proses pengajuan proposal ke System dengan atas nama Pokmas <b>' . $resultData->nama_kelompok . '</b>',
                                                'created_date' => $date,
                                                'created_by' => $username,
                                                'updated_date' => $date,
                                                'updated_by' => $username,
                                            );
                                            $this->db->insert('tbl_log', $data2);

                                            $data3 = array(
                                                'id_proposal' => $idProposal,
                                                'jumlah_anggaran' => str_replace(',', '.', $nominalPaguDpa),
                                                'tgl_survei' => $date2,
                                                'created_date' => $date,
                                                'created_by' => $username,
                                                'updated_date' => $date,
                                                'updated_by' => $username,
                                            );
                                            $this->db->insert('tbl_survey', $data3);

                                            $data4 = array(
                                                'id_proposal' => $idProposal,
                                                'created_date' => $date,
                                                'created_by' => $username,
                                                'updated_date' => $date,
                                                'updated_by' => $username,
                                            );
                                            $this->db->insert('tbl_checklis', $data4);
                                            // </editor-fold>
                                            // <editor-fold defaultstate="collapsed" desc="Evaluasi Proposal">
                                            $data5 = array(
                                                'no_ba' => $noBa,
                                                'status' => 'Verifikasi',
                                                'survei' => 'Survey',
                                                'catatan' => $catatanEvaluasi,
                                                'updated_date' => date('Y-m-d H:i:s'),
                                                'updated_by' => $username,
                                            );
                                            $this->db->update('tbl_proposal', $data5, array('id_proposal' => $idProposal));

                                            $data6 = array(
                                                'id_proposal' => $idProposal,
                                                'status' => $data5['status'],
                                                'keterangan' => 'user <b>' . $username . '</b> Sedang melakukan proses ' . $data5['status'] . ' terhadap Proposal yang di ajukan oleh Admin Help Desk Bagian atas nama Pokmas <b>' . $resultData->nama_kelompok . '</b>',
                                                'created_date' => $date,
                                                'created_by' => $username,
                                                'updated_date' => $date,
                                                'updated_by' => $username,
                                            );

                                            $this->db->insert('tbl_log', $data6);
                                            // </editor-fold>
                                            // <editor-fold defaultstate="collapsed" desc="Survey Lapangan">
                                            $data7 = array(
                                                'jenis_kegiatan' => $jenisKegiatanSurvei,
                                                'alamat' => $alamatSurvei,
                                                'catatan' => '<p>' . $catatanSurvei . '</p>',
                                                'jumlah_anggaran' => str_replace(',', '.', $nominalPaguDpa),
                                                'tgl_survei' => null,
                                                'survei' => 'Selesai Survey',
                                                'updated_date' => $date,
                                                'updated_by' => $username,
                                            );
                                            $tanggalSurvei = $this->reformatDate($tanggalSurvei);
                                            if ($tanggalSurvei != '1970-01-01') {
                                                $data7['tgl_survei'] = $tanggalSurvei;
                                            }
                                            $this->db->update('tbl_survey', $data7, array('id_proposal' => $idProposal));

                                            $data8 = array(
                                                'survei' => $data7['survei'],
                                                'updated_date' => date('Y-m-d H:i:s'),
                                                'updated_by' => $username,
                                            );
                                            $this->db->update('tbl_proposal', $data8, array('id_proposal' => $idProposal));

                                            $data9 = array(
                                                'id_proposal' => $idProposal,
                                                'status' => $data7['survei'],
                                                'keterangan' => 'Tim Survey DINAS PU SUMBER DAYA AIR PROVINSI JAWA TIMUR sudah melakukan survei terhadap pengajuan proposal dengan kode nomor ' . $data['kode_proposal'] . ' atas nama Pokmas <b>' . $resultData->nama_kelompok . '</b>',
                                                'created_date' => $date,
                                                'created_by' => $username,
                                                'updated_date' => $date,
                                                'updated_by' => $username,
                                            );
                                            $this->db->insert('tbl_log', $data9);
                                            // </editor-fold>
                                            // <editor-fold defaultstate="collapsed" desc="Proposal Rekomendasi">
                                            $data10 = array(
                                                'status' => 'Rekom Bidang',
                                                'updated_date' => $date,
                                                'updated_by' => $username,
                                            );
                                            $this->db->update('tbl_proposal', $data10, array('id_proposal' => $idProposal));

                                            $data11 = array(
                                                'id_proposal' => $idProposal,
                                                'status' => $data10['status'],
                                                'keterangan' => 'user <b>' . $username . '</b> Sedang melakukan proses ' . $data10['status'] . ' terhadap Proposal yang di ajukan oleh Admin Help Desk Bagian atas nama Pokmas <b>' . $resultData->nama_kelompok . '</b>',
                                                'created_date' => $date,
                                                'created_by' => 'System',
                                                'updated_date' => $date,
                                                'updated_by' => 'System',
                                            );
                                            $this->db->insert('tbl_log', $data11);
                                            // </editor-fold>
                                            // <editor-fold defaultstate="collapsed" desc="Proposal Penetapan">
                                            $data12 = array(
                                                'no_biro' => $noBiro,
                                                'status' => 'Penetapan',
                                                'helper' => 1,
                                                'updated_by' => $username,
                                                'updated_date' => date('Y-m-d H:i:s'),
                                            );
                                            if (strlen($masukDpa) > 0 && $masukDpa == 'YA') {
                                                $data12 = array_merge($data12, array(
                                                    'status_dpa' => 1,
                                                ));

                                                $limitDate = date('Y-m-d', strtotime(isset($data['tanggal']) ? $data['tanggal'] : $date2 . "+" . self::__getLimitDpaYear . " years"));
                                                $dataHistoryPengajuan = array(
                                                    'id_pokmas' => $resultData->id_pokmas,
                                                    'date' => $limitDate,
                                                    'description' => "Pengajuan proposal pada tahun " . date('Y', strtotime(isset($data['tanggal']) ? $data['tanggal'] : $date2)),
                                                    'created_by' => $username,
                                                    'created_date' => $date,
                                                    'updated_by' => $username,
                                                    'updated_date' => $date,
                                                );
                                                $this->db->insert('tbl_history_pengajuan', $dataHistoryPengajuan);
                                            }
                                            $this->db->update('tbl_proposal', $data12, array('id_proposal' => $idProposal));

                                            $data13 = array(
                                                'id_proposal' => $idProposal,
                                                'status' => $data12['status'],
                                                'keterangan' => 'user <b>' . $username . '</b> Sedang melakukan proses ' . $data12['status'] . ' proposal ke Dinas Jawa Timur',
                                                'created_date' => $date,
                                                'created_by' => $username,
                                                'updated_date' => $date,
                                                'updated_by' => $username,
                                            );
                                            $this->db->insert('tbl_log', $data13);
                                            // </editor-fold>
                                            // <editor-fold defaultstate="collapsed" desc="Proposal NPHD">
                                            $dataNphd = array(
                                                'sk_gub' => $skGub,
                                                'tgl_sk_gub' => null,
                                                'nama_ketua' => $resultData->nama_ketua,
                                                'no_ktp' => $ktpKetumPokmas,
                                                'alamat_rumah' => $alamatKetumPokmas,
                                                'alamat_pokmas' => $alamatKetumPokmas,
                                                'kode_pos' => $kodePosPokmas,
                                                'telp' => $telpKetumPokmas,
                                                'waktu_pelaksanaan' => $waktuPelaksanaan,
                                                'nama_rek' => $pemilikRek,
                                                'cabang' => $bankJatimCab,
                                                'no_rek' => $noRek,
                                                'no_nphd' => $noNphd,
                                                'tgl_nphd' => null,
                                                'no_surat_ke_pokmas' => $noSuratKePokmas,
                                                'tgl_surat' => null,
                                                'tgl_surat_pokmas' => null,
                                            );
                                            $tglSkGub = $this->reformatDate($tglSkGub);
                                            if ($tglSkGub != '1970-01-01') {
                                                $dataNphd['tgl_sk_gub'] = $tglSkGub;
                                            }
                                            $tglNphd = $this->reformatDate($tglNphd);
                                            if ($tglNphd != '1970-01-01') {
                                                $dataNphd['tgl_nphd'] = $tglNphd;
                                            }
                                            $tglSurat = $this->reformatDate($tglSurat);
                                            if ($tglSurat != '1970-01-01') {
                                                $dataNphd['tgl_surat'] = $tglSurat;
                                            }
                                            $tglSuratPokmas = $this->reformatDate($tglSuratPokmas);
                                            if ($tglSuratPokmas != '1970-01-01') {
                                                $dataNphd['tgl_surat_pokmas'] = $tglSuratPokmas;
                                            }

                                            $data14 = array(
                                                'nphd' => json_encode($dataNphd),
                                                'status' => 'NPHD',
                                                'updated_by' => $username,
                                                'updated_date' => date('Y-m-d H:i:s'),
                                            );
                                            $this->db->update('tbl_proposal', $data14, array('id_proposal' => $idProposal));

                                            $data15 = array(
                                                'id_proposal' => $idProposal,
                                                'status' => $data14['status'],
                                                'keterangan' => 'user <b>' . $username . '</b> Sedang melakukan proses ' . $data14['status'] . ' proposal ke Dinas Jawa Timur',
                                                'created_date' => $date,
                                                'created_by' => $username,
                                                'updated_date' => $date,
                                                'updated_by' => $username,
                                            );
                                            $this->db->insert('tbl_log', $data15);
                                            // </editor-fold>
                                        }
//                                    } else {
//                                        echo '<pre>';
//                                        print_r($key);
//                                        echo '</pre>';
//                                        echo '<br>';
//                                        echo '<pre>';
//                                        print_r($row);
//                                        echo '</pre>';
//                                        echo '<br>';
                                    }
                                } catch (Exception $ex) {
                                    $errCode++;
                                    $errMessage = $ex->getMessage();
                                }
                            }
                            if ($errCode == 0) {
                                if ($this->db->trans_status() === FALSE) {
                                    $errCode++;
                                    $errMessage = "Error saving databse.";
                                }
                            }
                            if ($errCode == 0) {
                                $this->db->trans_commit();
                            } else {
                                $this->db->trans_rollback();
                            }
                        }
                    }
                }
//                echo '<pre>';
//                print_r($jml);
//                echo '</pre>';
//                echo '<br>';
//                die();

                unlink('./assets/excel/' . $dataUpload['file_name']);
                if (!$error) {
                    $out = array('status' => true, 'pesan' => ' Data berhasil di import');
                } else {
                    $out = array('status' => false, 'pesan' => 'Maaf data gagal di import !');
                }
            }
        }

        echo json_encode($out);
    }

    public function exportData()
    {
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = "Export Excel";
        $data['judul'] = "Export Excel";
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['menuName'] = self::__kode_menu;
            parent::loadkonten('' . self::__folder . 'export', $data);
        }
    }

    public function ajaxListExportData()
    {
        $statusPenerimaan = $this->input->post('status_penerimaan');
        $startYear = $this->input->post('start_year');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = array(
            'status_penerimaan' => $statusPenerimaan,
            'start_year' => $startYear,
            'tanggal_akhir' => $tanggalAkhir,
        );

        $results = $this->M_proposal_nphd->getDataLpj(1, $filter);

        $data = [];
        $no = $_POST['start'];
        if (!empty($results)) {
            foreach ($results as $brand) {
                $nphd = json_decode($brand['nphd'], TRUE);
                $no++;
                $row = [];
                $row[] = $no;
                $row[] = $brand['kode_proposal'];
                $row[] = $brand['nama_kelompok'];
                $row[] = $brand['kabupaten'];
                $row[] = $brand['kecamatan'];
                $row[] = $brand['desa'];
                $row[] = $nphd['status_penerimaan'];

                $data[] = $row;
            }
        }

        $output = [
            "draw" => $_POST['draw'],
            "data" => $data,
        ];
        //output to json format
        echo json_encode($output);
    }

    public function prosesExportData()
    {
        $statusPenerimaan = $this->input->post('status_penerimaan');
        $startYear = $this->input->post('start_year');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = array(
            'status_penerimaan' => $statusPenerimaan,
            'start_year' => $startYear,
            'tanggal_akhir' => $tanggalAkhir,
        );

        $results = $this->M_proposal_nphd->getDataLpj(1, $filter);
        if (!empty($results)) {
            $data = array_merge($filter, [
                'tanggal_akhir_indo' => parent::tgl_indo(date('Y-m-d', strtotime($tanggalAkhir))),
                'results' => $results,
            ]);

            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=daftar-penerima-bantuan-" . date('YmdHis') . ".xls");
            header("Pragma: no-cache");
            header("Expires: 0");

            $this->load->view(self::__folder . 'excel/cetak_penerima_batuan', $data);
        } else {
            echo "<script>alert('Data kosong.'); window.location = '" . base_url('export-nphd') . "';</script>";
        }
    }

    public function reformatDate($date)
    {
        $expDate1 = explode('-', $date);
        if (strlen($expDate1[0]) != 4) {
            if (isset($expDate1[2])) {
                if (strlen($expDate1[1]) != 3) {
                    if (strlen($expDate1[2]) == 2) {
                        $date = $expDate1[2] . '-' . $expDate1[0] . '-' . $expDate1[1];
                    }
                    if (strlen($expDate1[2]) == 4) {
                        $date = $expDate1[0] . '-' . $expDate1[1] . '-' . $expDate1[2];
                    }
                }
            }
            $expDate2 = explode('/', $date);
            if (isset($expDate2[2])) {
                if (strlen($expDate2[2]) == 4) {
                    $date = $expDate2[0] . '-' . $expDate2[1] . '-' . $expDate2[2];
                }
            }
            if (count($expDate1) == 1 && strlen($expDate1[0]) > 0 && count($expDate2) == 1 && strlen($expDate2[0]) > 0) {
                $date = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($date));
            }
        }
        $dateFormatted = date('Y-m-d', strtotime($date));

        return $dateFormatted;
    }
}
