<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pokmas extends AUTH_Controller
{
    const __tableName = 'tbl_pokmas';
    const __tableId = 'id_pokmas';
    const __folder = 'v_pokmas/';
    const __kode_menu = 'master-pokmas';
    const __title = 'Pokmas';
    const __model = 'M_pokmas';
    const __getLimitDpaYear = 2;

    private $idUser = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_generate_code');
        $this->load->model('M_kabupaten');
        $this->load->model('M_kecamatan');
        $this->load->model('M_desa');
        $this->load->model('M_status');
        $this->load->model('M_sidebar');
        $this->load->model('M_utilities');
        $this->load->model('M_grup');
        $this->idUser = $this->session->userdata('id');
    }

    public function index()
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            $data['grupId'] = $this->session->userdata('grup_id');
            parent::loadkonten(self::__folder . '/home', $data);
        }
    }

    public function ajax_list()
    {
        $status = $this->input->post('status');
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = array(
            'status' => $status,
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_pokmas->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {

            $status = '<small class="label pull-center bg-yellow">Non aktif</small">';
            if ($brand->status == 'Aktif') {
                $status = '<small class="label pull-center bg-green">Aktif</small">';
            }

            $tipe_petani = '<small class="label pull-center bg-yellow">Baru</small">';
            if ($brand->tipe_petani == 'Lama') {
                $tipe_petani = '<small class="label pull-center bg-blue">Data Lama</small">';
            }

            $statusProposal = '<small class="label pull-center bg-yellow">Bisa Melakukan Pengajuan</small">';
            $getKelompokDpa = $this->M_utilities->getKelompokDpa($brand->id_pokmas);
            if ($getKelompokDpa != null) {
                if (date('Y') < date('Y', strtotime($getKelompokDpa->date))) {
                    $statusProposal = '<small class="label pull-center bg-red">Telah Melakukan Pengajuan Tahun ' . date("Y", strtotime($getKelompokDpa->created_date)) . '</small">';
                }
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = '<li>' . $brand->nama_kelompok . '<li>Kode : <b>' . $brand->kode_pokmas . '</b><li>Grup : <b>' . $brand->nama_grup . '</b><li>Status :  ' . $status . '';
            $row[] = $brand->nama_ketua;
            $row[] = $brand->kabupaten;
            $row[] = $brand->kecamatan;
            $row[] = $brand->desa;
            $row[] = $statusProposal;
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->created_date, 'by' => $brand->created_by));
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->updated_date, 'by' => $brand->updated_by));

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-pokmas') . "/" . $brand->id_pokmas . "'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-pokmas' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_pokmas . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Tambah " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['menuName'] = self::__kode_menu;
            if ($this->session->userdata('grup_id') <= 2) {
                $data['selectGrup'] = $this->M_grup->select(['is_bidang' => 1]);
            }
            $data['kabupaten'] = $this->M_kabupaten->select();
            parent::loadkonten('' . self::__folder . 'tambah', $data);
        }
    }

    public function prosesAdd()
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $namaKelompok = trim($this->input->post("nama_kelompok"));
        $namaKetua = trim($this->input->post("nama_ketua"));
        $noKtp = trim($this->input->post("no_ktp"));
        $idKabupaten = trim($this->input->post("id_kabupaten"));
        $idKecamatan = trim($this->input->post("id_kecamatan"));
        $idDesa = trim($this->input->post("id_desa"));
        $alamat = trim($this->input->post("alamat"));
        $longitude = trim($this->input->post("longitude"));
        $latitude = trim($this->input->post("latitude"));
        if ($this->session->userdata('grup_id') > 2) {
            $idGrup = $this->session->userdata('grup_id');
        } else {
            $idGrup = trim($this->input->post("id_grup"));
        }

        $this->db->trans_begin();
        $code = $this->M_generate_code->getNextPokmas();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('add', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idGrup) == 0) {
                $errCode++;
                $errMessage = "Grup wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($namaKelompok) == 0) {
                $errCode++;
                $errMessage = "Nama Pokmas wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($namaKetua) == 0) {
                $errCode++;
                $errMessage = "Ketua Pokmas wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noKtp) == 0) {
                $errCode++;
                $errMessage = "No KTP wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noKtp) > 16) {
                $errCode++;
                $errMessage = "No KTP maksimal 16 digit.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKabupaten) == 0) {
                $errCode++;
                $errMessage = "Kabupaten wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKabupaten = $this->M_kabupaten->selectById($idKabupaten);
            if ($checkKabupaten == null) {
                $errCode++;
                $errMessage = "Kabupaten tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKecamatan) == 0) {
                $errCode++;
                $errMessage = "Kecamatan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKecamatan = $this->M_kecamatan->selectById($idKecamatan);
            if ($checkKecamatan == null) {
                $errCode++;
                $errMessage = "Kecamatan tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkKecamatan->id_kabupaten != $idKabupaten) {
                $errCode++;
                $errMessage = "Kecamatan tidak sesuai dengan kabupaten.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idDesa) == 0) {
                $errCode++;
                $errMessage = "Desa wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkDesa = $this->M_desa->selectById($idDesa);
            if ($checkDesa == null) {
                $errCode++;
                $errMessage = "Desa tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkDesa->id_kecamatan != $idKecamatan) {
                $errCode++;
                $errMessage = "Desa tidak sesuai dengan Kecamatan.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = array(
                    "kode_pokmas" => $code,
                    'id_grup' => $idGrup,
                    'id_user' => $this->idUser,
                    'nama_kelompok' => $namaKelompok,
                    'nama_ketua' => $namaKetua,
                    'no_ktp' => $noKtp,
                    'id_desa' => $idDesa,
                    'alamat' => $alamat,
                    // 'longitude' => $longitude,
                    // 'latitude' => $latitude,
                    'status' => 'Non aktif',
                    'tanggal' => date('Y-m-d'),
                    'created_date' => $date,
                    'created_by' => $username,
                    'updated_date' => $date,
                    'updated_by' => $username,
                );
                $this->db->insert(self::__tableName, $data);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Ubah " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_pokmas->selectById($id);
            if ($resultData != null) {
                $desa = $this->M_desa->selectById($resultData->id_desa);
                $kecamatan = $this->M_kecamatan->selectById($desa->id_kecamatan);
                $data['resultData'] = $resultData;
                if ($this->session->userdata('grup_id') <= 2) {
                    $data['selectGrup'] = $this->M_grup->select(['is_bidang' => 1]);
                }
                $data['id_kecamatan'] = $desa->id_kecamatan;
                $data['id_kabupaten'] = $kecamatan->id_kabupaten;
                $data['kabupaten'] = $this->M_kabupaten->select();
                $data['kecamatan'] = $this->M_kecamatan->select(array('id_kabupaten' => $kecamatan->id_kabupaten));
                $data['desa'] = $this->M_desa->select(array('id_kecamatan' => $desa->id_kecamatan));
                $data['datastatus'] = $this->M_status->select('pokmas');
                $data['menuName'] = self::__kode_menu;
                $data['idCustomer'] = $id;
                parent::loadkonten('' . self::__folder . 'update', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $namaKelompok = trim($this->input->post("nama_kelompok"));
        $namaKetua = trim($this->input->post("nama_ketua"));
        $noKtp = trim($this->input->post("no_ktp"));
        $idKabupaten = trim($this->input->post("id_kabupaten"));
        $idKecamatan = trim($this->input->post("id_kecamatan"));
        $idDesa = trim($this->input->post("id_desa"));
        $alamat = trim($this->input->post("alamat"));
        $longitude = trim($this->input->post("longitude"));
        $latitude = trim($this->input->post("latitude"));
        $keterangan = trim($this->input->post("keterangan"));
        $status = trim($this->input->post("status"));
        if ($this->session->userdata('grup_id') > 2) {
            $idGrup = $this->session->userdata('grup_id');
        } else {
            $idGrup = trim($this->input->post("id_grup"));
        }

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_pokmas->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idGrup) == 0) {
                $errCode++;
                $errMessage = "Grup wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($namaKelompok) == 0) {
                $errCode++;
                $errMessage = "Nama Pokmas wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($namaKetua) == 0) {
                $errCode++;
                $errMessage = "Ketua Pokmas wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noKtp) == 0) {
                $errCode++;
                $errMessage = "No KTP wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noKtp) > 16) {
                $errCode++;
                $errMessage = "No KTP maksimal 16 digit.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKabupaten) == 0) {
                $errCode++;
                $errMessage = "Kabupaten wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKabupaten = $this->M_kabupaten->selectById($idKabupaten);
            if ($checkKabupaten == null) {
                $errCode++;
                $errMessage = "Kabupaten tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKecamatan) == 0) {
                $errCode++;
                $errMessage = "Kecamatan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKecamatan = $this->M_kecamatan->selectById($idKecamatan);
            if ($checkKecamatan == null) {
                $errCode++;
                $errMessage = "Kecamatan tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkKecamatan->id_kabupaten != $idKabupaten) {
                $errCode++;
                $errMessage = "Kecamatan tidak sesuai dengan kabupaten.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idDesa) == 0) {
                $errCode++;
                $errMessage = "Desa wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkDesa = $this->M_desa->selectById($idDesa);
            if ($checkDesa == null) {
                $errCode++;
                $errMessage = "Desa tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkDesa->id_kecamatan != $idKecamatan) {
                $errCode++;
                $errMessage = "Desa tidak sesuai dengan Kecamatan.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = array(
                    'id_grup' => $idGrup,
                    'nama_kelompok' => $namaKelompok,
                    'nama_ketua' => $namaKetua,
                    'no_ktp' => $noKtp,
                    'id_desa' => $idDesa,
                    'alamat' => $alamat,
                    // 'longitude' => $longitude,
                    // 'latitude' => $latitude,
                    'keterangan' => $keterangan,
                    'status' => $status,
                    'tanggal' => date('Y-m-d'),
                    'updated_date' => $date,
                    'updated_by' => $username,
                );
                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $id = $_POST[self::__tableId];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_pokmas->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            $checkForeign = $this->M_pokmas->checkForeign($id);
            if ($checkForeign) {
                $errCode++;
                $errMessage = self::__title . " terdapat pada modul lain.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->db->update(self::__tableName, array('deleted_date' => date('Y-m-d H:i:s')), array(self::__tableId => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di hapus');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }

    public function import()
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');
        $error = false;

        $this->form_validation->set_rules('excel', 'File', 'trim|required');
        if ($_FILES['excel']['name'] != '' && $this->session->userdata('grup_id') == 1) {
            $config['upload_path'] = './assets/excel/';
            $config['allowed_types'] = 'xls|xlsx';

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('excel')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $dataUpload = $this->upload->data();

                error_reporting(E_ALL);
                date_default_timezone_set('Asia/Jakarta');

                include './assets/phpexcel/Classes/PHPExcel/IOFactory.php';
                $inputFileName = './assets/excel/' . $dataUpload['file_name'];
                $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                $data = array();
                if (!$error) {
                    foreach ($sheetData as $key => $row) {
                        if ($key > 1) {
                            $kabupaten = trim($row['A']);
                            $kecamatan = trim($row['B']);
                            $desa = trim($row['C']);
                            $namaKelompok = trim($row['D']);
                            $namaKetua = trim($row['E']);
                            $alamat = trim($row['F']);
                            $status = trim($row['G']);
                            $idUser = trim($row['H']);
                            if (strlen($idUser) == 0) {
                                $idUser = $this->idUser;
                            }
                            if (strlen($kabupaten) > 0 && strlen($kecamatan) > 0 && strlen($desa) > 0 && strlen($namaKelompok) > 0 && strlen($namaKetua) > 0 && strlen($alamat) > 0 && strlen($status) > 0) {
                                $checkFiles = $this->M_pokmas->checkFiles($namaKelompok);
                                if ($checkFiles == false) {
                                    $code = $this->M_generate_code->getNextPokmas();
                                    $qDesa = "SELECT * FROM tbl_desa WHERE deleted_date IS NULL AND desa like '%{$desa}%'";
                                    $getDesa = $this->db->query($qDesa)->row();
                                    if ($getDesa == null) {
                                        $qKecamatan = "SELECT * FROM tbl_kecamatan WHERE deleted_date IS NULL AND kecamatan like '%{$kecamatan}%'";
                                        $getKecamatan = $this->db->query($qKecamatan)->row();
                                        if ($getKecamatan == null) {
                                            $qKabupaten = "SELECT * FROM tbl_kabupaten WHERE deleted_date IS NULL AND id_provinsi = 1 AND kabupaten like '%{$kabupaten}%'";
                                            $getKabupaten = $this->db->query($qKabupaten)->row();
                                            if ($getKabupaten == null) {
                                                $dataKabupaten = array(
                                                    'id_provinsi' => 1,
                                                    'kabupaten' => trim(strtoupper($kabupaten)),
                                                    'created_date' => $date,
                                                    'created_by' => $username,
                                                    'updated_date' => $date,
                                                    'updated_by' => $username,
                                                );
                                                $this->db->insert('tbl_kabupaten', $dataKabupaten);
                                                $idKabupaten = $this->db->insert_id();
                                            } else {
                                                $idKabupaten = $getKabupaten->id;
                                            }
                                            $dataKecamatan = array(
                                                'id_kabupaten' => $idKabupaten,
                                                'kecamatan' => trim(strtoupper($kecamatan)),
                                                'created_date' => $date,
                                                'created_by' => $username,
                                                'updated_date' => $date,
                                                'updated_by' => $username,
                                            );
                                            $this->db->insert('tbl_kecamatan', $dataKecamatan);
                                            $idKecamatan = $this->db->insert_id();
                                        } else {
                                            $idKecamatan = $getKecamatan->id;
                                        }
                                        $dataDesa = array(
                                            'id_kecamatan' => $idKecamatan,
                                            'desa' => trim(strtoupper($desa)),
                                            'created_date' => $date,
                                            'created_by' => $username,
                                            'updated_date' => $date,
                                            'updated_by' => $username,
                                        );
                                        $this->db->insert('tbl_desa', $dataDesa);
                                        $idDesa = $this->db->insert_id();
                                    } else {
                                        $idDesa = $getDesa->id;
                                    }

                                    array_push($data, array(
                                        "kode_pokmas" => $code,
                                        "id_user" => $idUser,
                                        'nama_kelompok' => $namaKelompok,
                                        'nama_ketua' => $namaKetua,
                                        'id_desa' => $idDesa,
                                        'alamat' => $alamat,
                                        'status' => $status,
                                        'tanggal' => date('Y-m-d'),
                                        'created_date' => $date,
                                        'created_by' => $username,
                                        'updated_date' => $date,
                                        'updated_by' => $username,
                                    ));
                                }
                            }
                        }
                    }
                }

                unlink('./assets/excel/' . $dataUpload['file_name']);
                if (!$error) {
                    if (empty($data)) {
                        $out = array('status' => 'awas', 'pesan' => 'Maaf, Data sudah ada dalam sistem silahkan cek kembali data import terakhir !');
                        $error = true;
                    }
                }
                if (!$error) {
                    $result = $this->db->insert_batch('tbl_pokmas', $data);

                    if ($result > 0) {
                        $out = array('status' => true, 'pesan' => ' Data berhasil di import');
                    } else {
                        $out = array('status' => false, 'pesan' => 'Maaf data gagal di import !');
                    }
                }
            }

            echo json_encode($out);
        }
    }
}
