<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_proposal_pengajuan extends AUTH_Controller
{
    const __tableName = 'tbl_proposal';
    const __tableId = 'id_proposal';
    const __tableIdname = 'name';
    const __folder = 'v_proposal_pengajuan/';
    const __kode_menu = 'master-proposal-pengajuan';
    const __title = 'Form Pengajuan ';
    const __model = 'M_proposal_pengajuan';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_generate_code');
        $this->load->model('M_kabupaten');
        $this->load->model('M_kecamatan');
        $this->load->model('M_desa');
        $this->load->model('M_pokmas');
        $this->load->model('M_sidebar');
        $this->load->model('M_utilities');
        $this->load->library('ciqrcode');
    }

    public function index()
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('view', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            parent::loadkonten('' . self::__folder . 'home', $data);
        }
    }

    public function ajax_list()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = array(
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        );

        $accessView = $this->M_sidebar->access('view', self::__kode_menu);
        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_proposal_pengajuan->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $status = '<small class="label pull-center bg-blue">Belum Verifikasi</small>';
            if ($brand->status == 'Rekomendasi') {
                $status = '<small class="label pull-center bg-green">Telah Direkomendasi</small>';
            } else if ($brand->status == 'Verifikasi') {
                $status = '<small class="label pull-center bg-green">Verifikasi Admin</small>';
            } else if ($brand->status == 'Rekom Bidang') {
                $status = '<small class="label pull-center bg-green">Rekom Bidang</small>';
            } else if ($brand->status == 'Direvisi') {
                $status = '<small class="label pull-center bg-red">Direvisi</small>';
            }

            if ($brand->created_date == $brand->updated_date) {
                $proses = "Belum ada yang memproses";
            } else {
                $proses = $brand->updated_by;
            }

            $StatusSurvey = "";
            if ($brand->survei == "Survey") {
                $StatusSurvey = "<li><b>Sedang dilakukan Survey Lapangan<b></li>";
            } else if ($brand->survei == "Selesai Survey") {
                $StatusSurvey = "<li><b>Survei selesai di lakukan<b></li>";
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->nama_kelompok . '<br><br>Grup : <b>' . $brand->nama_grup . '</b><br> No DPA : <b>' . $brand->no_dpa . '</b>';
            $row[] = $brand->perihal . '<br><br> Kode Proposal : <b>' . $brand->kode_proposal . '</b><br> Jenis Hibah : <b>' . $brand->jenis_hibah . '</b>';
            $row[] = $brand->kabupaten;
            $row[] = $status . '<br> Proposal di Proses oleh : <b>' . $proses . '</b><br>' . $StatusSurvey;
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->created_date, 'by' => $brand->created_by));
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->updated_date, 'by' => $brand->updated_by));

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($brand->status != 'Rekomendasi') {
                if ($accessEdit->menuview > 0) {
                    $action .= "    <li><a href='" . base_url('edit-pengajuan-proposal') . "/" . $brand->id_proposal . "'><i class='fa fa-edit'></i> Ubah</a></li>";
                }
            }
            if ($accessView->menuview > 0) {
                $action .= "    <li><a href='" . base_url('cetak-proposal') . "/" . $brand->id_proposal . "' target='__blank'><i class='fa fa-print'></i> Print Data</a></li>";
            }
            if ($accessView->menuview > 0) {
                $action .= "    <li><a href='" . base_url('history-pengajuan-proposal') . "/" . $brand->id_proposal . "'><i class='fa fa-eye'></i> Histori Pengajuan</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-proposal-pengajuan' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id_proposal . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            if ($this->session->userdata('is_bidang') == 0) {
                $data['jenisHibah'] = ['Reguler', 'Pokir'];
            }
            $data['menuName'] = self::__kode_menu;
            $data['kabupaten'] = $this->M_kabupaten->select();
            parent::loadkonten('' . self::__folder . 'tambah', $data);
        }
    }

    public function prosesAdd()
    {
        $username = $this->session->userdata('username');
        $IdUser = $this->session->userdata('id');
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";
        $config = array();
        $params = array();

        $idKabupaten = trim($this->input->post("id_kabupaten"));
        $idKecamatan = trim($this->input->post("id_kecamatan"));
        $idDesa = trim($this->input->post("id_desa"));
        $idPokmas = trim($this->input->post("id_pokmas"));
        $noDpa = trim($this->input->post("no_dpa"));
        $noSurat = trim($this->input->post("no_surat"));
        $tanggal = trim($this->input->post("tanggal"));
        $perihal = trim($this->input->post("perihal"));
        $NoBiro = $this->input->post('no_biro');
        $tanggalDisposisi = trim($this->input->post("tanggal_disposisi"));
        $jenisBantuan = trim($this->input->post("jenis_bantuan"));
        $jenisHibah = trim($this->input->post("jenis_hibah"));
        $usulanBarang = trim($this->input->post("usulan_barang"));
        $usulanNilaiAnggaran = trim($this->input->post("usulan_nilai_anggaran"));
        $barang = trim($this->input->post("barang"));
        $nilaiAnggaran = trim($this->input->post("nilai_anggaran"));
        $fileUpload = $_FILES['file_upload'];

        $this->db->trans_begin();
        $code = $this->M_generate_code->getNextProposal();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('add', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKabupaten) == 0) {
                $errCode++;
                $errMessage = "Kota / Kabupaten wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKabupaten = $this->M_kabupaten->selectById($idKabupaten);
            if ($checkKabupaten == null) {
                $errCode++;
                $errMessage = "Kabupaten tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKecamatan) == 0) {
                $errCode++;
                $errMessage = "Kecamatan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKecamatan = $this->M_kecamatan->selectById($idKecamatan);
            if ($checkKecamatan == null) {
                $errCode++;
                $errMessage = "Kecamatan tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkKecamatan->id_kabupaten != $idKabupaten) {
                $errCode++;
                $errMessage = "Kecamatan tidak sesuai dengan kabupaten.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idDesa) == 0) {
                $errCode++;
                $errMessage = "Kelurahan / Desa wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkDesa = $this->M_desa->selectById($idDesa);
            if ($checkDesa == null) {
                $errCode++;
                $errMessage = "Desa tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkDesa->id_kecamatan != $idKecamatan) {
                $errCode++;
                $errMessage = "Desa tidak sesuai dengan Kecamatan.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idPokmas) == 0) {
                $errCode++;
                $errMessage = "Kelompok wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkPokmas = $this->M_pokmas->selectById($idPokmas);
            if ($checkPokmas == null) {
                $errCode++;
                $errMessage = "Kelompok tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkPokmas->id_desa != $idDesa) {
                $errCode++;
                $errMessage = "Kelompok tidak sesuai dengan Kelurahan / Desa.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noDpa) == 0) {
                $errCode++;
                $errMessage = "Nomor DPA wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noSurat) == 0) {
                $errCode++;
                $errMessage = "Nomor Surat Proposal wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tanggal) == 0) {
                $errCode++;
                $errMessage = "Tabggal Proposal wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($perihal) == 0) {
                $errCode++;
                $errMessage = "Perihal Proposal wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($NoBiro) == 0) {
                $errCode++;
                $errMessage = "No Biro wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tanggalDisposisi) == 0) {
                $errCode++;
                $errMessage = "Tanggal Disposisi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if ($fileUpload['size'] == 0) {
                $errCode++;
                $errMessage = "Berkas File Pengajuan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if ($fileUpload['type'] != 'application/pdf') {
                $errCode++;
                $errMessage = "Berkas File Pengajuan wajib format PDF.";
            }
        }
        if ($errCode == 0) {
            if (strlen($jenisBantuan) == 0) {
                $errCode++;
                $errMessage = "Jenis Bantuan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if ($jenisBantuan == "Barang") {
                $usulanNilaiAnggaran = null;
                $nilaiAnggaran = null;
                if ($errCode == 0) {
                    if (strlen($usulanBarang) == 0) {
                        $errCode++;
                        $errMessage = "Usulan Barang wajib di isi.";
                    }
                }
                if ($errCode == 0) {
                    if (strlen($barang) == 0) {
                        $errCode++;
                        $errMessage = "Usulan Barang Setelah Evaluasi wajib di isi.";
                    }
                }
            }
        }
        if ($errCode == 0) {
            if ($jenisBantuan == "Uang") {
                $usulanBarang = null;
                $barang = null;
                if ($errCode == 0) {
                    if (strlen($usulanNilaiAnggaran) == 0) {
                        $errCode++;
                        $errMessage = "Usulan Anggaran Proposal wajib di isi.";
                    }
                }
                if ($errCode == 0) {
                    if (strlen($nilaiAnggaran) == 0) {
                        $errCode++;
                        $errMessage = "Usulan Anggaran Setelah Evaluasi wajib di isi.";
                    }
                }
            }
        }
        if ($errCode == 0) {
            if ($this->session->userdata('is_bidang') > 0) {
                $jenisHibah = "Pokir";
                if ($this->session->userdata('grup_name') == "PSDA") {
                    $jenisHibah = "Reguler";
                }
            }
            if (strlen($jenisHibah) == 0) {
                $errCode++;
                $errMessage = "Jenis Bantuan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $config['cacheable'] = true;
                $config['cachedir'] = './upload/dir/';
                $config['errorlog'] = './upload/log/';
                $config['imagedir'] = './upload/qrcode/';
                $config['quality'] = true;
                $config['size'] = '1024';
                $config['black'] = array(224, 255, 255);
                $config['white'] = array(70, 130, 180);
                $this->ciqrcode->initialize($config);
                $qrcodeImage = $code . '.png';

                $params['data'] = $code;
                $params['level'] = 'H';
                $params['size'] = 10;
                $params['savename'] = FCPATH . $config['imagedir'] . $qrcodeImage;
                $this->ciqrcode->generate($params);

                $newName = time() . $fileUpload['name'];
                $nmfile = "file_" . $newName;
                $config['upload_path'] = "./upload/file_proposal/";
                $config['allowed_types'] = 'pdf';
                $config['max_size'] = '8048'; //maksimum besar file 8M
                $config['file_name'] = $nmfile;
                $config['overwrite'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload("file_upload")) {
                    $imageData = $this->upload->data();
                    $path['link'] = "upload/file_proposal/";

                    $data = array(
                        "qrcode" => $qrcodeImage,
                        'kode_proposal' => $code,
                        'id_pokmas' => $idPokmas,
                        'no_dpa' => $noDpa,
                        'no_surat' => $noSurat,
                        'tanggal' => date('Y-m-d', strtotime($tanggal)),
                        'perihal' => $perihal,
                        'no_biro' => $NoBiro,
                        'tanggal_disposisi' => date('Y-m-d', strtotime($tanggalDisposisi)),
                        'jenis_bantuan' => $jenisBantuan,
                        'jenis_hibah' => $jenisHibah,
                        'usulan_barang' => $usulanBarang,
                        'usulan_nilai_anggaran' => $usulanNilaiAnggaran,
                        'barang' => $barang,
                        'nilai_anggaran' => $nilaiAnggaran,
                        'nama_file_upload' => $imageData['file_name'],
                        'tipe' => 'pengajuan permohonan rekomendasi',
                        'status' => 'Belum Verifikasi',
                        'file_upload' => 'upload/file_proposal/' . $imageData['file_name'],
                        'tgl_help' => $date2,
                        'created_date' => $date,
                        'created_by' => $username,
                        'updated_date' => $date,
                        'updated_by' => $username,
                    );
                    $this->db->insert(self::__tableName, $data);
                    $idProposal = $this->db->insert_id();

                    $data2 = array(
                        'id_proposal' => $idProposal,
                        'status' => 'Belum Verifikasi',
                        'keterangan' => 'user <b>' . $username . '</b> melakukan proses pengajuan proposal ke System dengan atas nama Pokmas <b>' . $checkPokmas->nama_kelompok . '</b>',
                        'created_date' => $date,
                        'created_by' => $username,
                        'updated_date' => $date,
                        'updated_by' => $username,
                    );
                    $result = $this->db->insert('tbl_log', $data2);

                    $data3 = array(
                        'id_proposal' => $idProposal,
                        'jumlah_anggaran' => $nilaiAnggaran,
                        'tgl_survei' => $date2,
                        'created_date' => $date,
                        'created_by' => $username,
                        'updated_date' => $date,
                        'updated_by' => $username,
                    );
                    $result = $this->db->insert('tbl_survey', $data3);

                    $data4 = array(
                        'ref_table' => 'tbl_proposal',
                        'ref_id' => $idProposal,
                        'nama_file_upload' => $imageData['file_name'],
                        'file_upload' => 'upload/file_proposal/' . $imageData['file_name'],
                        // 'tanggal' => $date2,
                        'status' => 'Pengajuan File',
                        'created_date' => $date,
                        'created_by' => $username,
                        'updated_date' => $date,
                        'updated_by' => $username,
                    );
                    $result = $this->db->insert('tbl_history_upload', $data4);

                    $data5 = array(
                        'id_proposal' => $idProposal,
                        'created_date' => $date,
                        'created_by' => $username,
                        'updated_date' => $date,
                        'updated_by' => $username,
                    );
                    $result = $this->db->insert('tbl_checklis', $data5);
                }
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_proposal_pengajuan->selectById($id);
            if ($resultData != null) {
                $data['resultData'] = $resultData;
                $data['kabupaten'] = $this->M_kabupaten->select();
                $data['kecamatan'] = $this->M_kecamatan->select(array('id_kabupaten' => $resultData->id_kabupaten));
                $data['desa'] = $this->M_desa->select(array('id_kecamatan' => $resultData->id_kecamatan));
                $data['pokmas'] = $this->M_pokmas->select(array('id_desa' => $resultData->id_desa));
                $data['menuName'] = self::__kode_menu;
                $data['idCustomer'] = $id;
                parent::loadkonten('' . self::__folder . 'update', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $IdUser = $this->session->userdata('id');
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";
        $config = array();

        $idKabupaten = trim($this->input->post("id_kabupaten"));
        $idKecamatan = trim($this->input->post("id_kecamatan"));
        $idDesa = trim($this->input->post("id_desa"));
        $idPokmas = trim($this->input->post("id_pokmas"));
        $noDpa = trim($this->input->post("no_dpa"));
        $noSurat = trim($this->input->post("no_surat"));
        $tanggal = trim($this->input->post("tanggal"));
        $perihal = trim($this->input->post("perihal"));
        $NoBiro = $this->input->post('no_biro');
        $tanggalDisposisi = trim($this->input->post("tanggal_disposisi"));
        $jenisBantuan = trim($this->input->post("jenis_bantuan"));
        $usulanBarang = trim($this->input->post("usulan_barang"));
        $usulanNilaiAnggaran = trim($this->input->post("usulan_nilai_anggaran"));
        $barang = trim($this->input->post("barang"));
        $nilaiAnggaran = trim($this->input->post("nilai_anggaran"));
        $fileUpload = $_FILES['file_upload'];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_proposal_pengajuan->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKabupaten) == 0) {
                $errCode++;
                $errMessage = "Kota / Kabupaten wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKabupaten = $this->M_kabupaten->selectById($idKabupaten);
            if ($checkKabupaten == null) {
                $errCode++;
                $errMessage = "Kabupaten tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKecamatan) == 0) {
                $errCode++;
                $errMessage = "Kecamatan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKecamatan = $this->M_kecamatan->selectById($idKecamatan);
            if ($checkKecamatan == null) {
                $errCode++;
                $errMessage = "Kecamatan tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkKecamatan->id_kabupaten != $idKabupaten) {
                $errCode++;
                $errMessage = "Kecamatan tidak sesuai dengan kabupaten.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idDesa) == 0) {
                $errCode++;
                $errMessage = "Kelurahan / Desa wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkDesa = $this->M_desa->selectById($idDesa);
            if ($checkDesa == null) {
                $errCode++;
                $errMessage = "Desa tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkDesa->id_kecamatan != $idKecamatan) {
                $errCode++;
                $errMessage = "Desa tidak sesuai dengan Kecamatan.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idPokmas) == 0) {
                $errCode++;
                $errMessage = "Kelompok wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkPokmas = $this->M_pokmas->selectById($idPokmas);
            if ($checkPokmas == null) {
                $errCode++;
                $errMessage = "Kelompok tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkPokmas->id_desa != $idDesa) {
                $errCode++;
                $errMessage = "Kelompok tidak sesuai dengan Kelurahan / Desa.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noDpa) == 0) {
                $errCode++;
                $errMessage = "Nomor DPA wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($noSurat) == 0) {
                $errCode++;
                $errMessage = "Nomor Surat Proposal wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tanggal) == 0) {
                $errCode++;
                $errMessage = "Tabggal Proposal wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($perihal) == 0) {
                $errCode++;
                $errMessage = "Perihal Proposal wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($NoBiro) == 0) {
                $errCode++;
                $errMessage = "No Biro wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($tanggalDisposisi) == 0) {
                $errCode++;
                $errMessage = "Tanggal Disposisi wajib di isi.";
            }
        }
        if ($fileUpload['size'] > 0) {
            if ($errCode == 0) {
                if ($fileUpload['type'] != 'application/pdf') {
                    $errCode++;
                    $errMessage = "Berkas File Pengajuan wajib format PDF.";
                }
            }
        }
        if ($errCode == 0) {
            if (strlen($jenisBantuan) == 0) {
                $errCode++;
                $errMessage = "Jenis Bantuan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if ($jenisBantuan == "Barang") {
                $usulanNilaiAnggaran = null;
                $nilaiAnggaran = null;
                if ($errCode == 0) {
                    if (strlen($usulanBarang) == 0) {
                        $errCode++;
                        $errMessage = "Usulan Barang wajib di isi.";
                    }
                }
                if ($errCode == 0) {
                    if (strlen($barang) == 0) {
                        $errCode++;
                        $errMessage = "Usulan Barang Setelah Evaluasi wajib di isi.";
                    }
                }
            }
        }
        if ($errCode == 0) {
            if ($jenisBantuan == "Uang") {
                $usulanBarang = null;
                $barang = null;
                if ($errCode == 0) {
                    if (strlen($usulanNilaiAnggaran) == 0) {
                        $errCode++;
                        $errMessage = "Usulan Anggaran Proposal wajib di isi.";
                    }
                }
                if ($errCode == 0) {
                    if (strlen($nilaiAnggaran) == 0) {
                        $errCode++;
                        $errMessage = "Usulan Anggaran Setelah Evaluasi wajib di isi.";
                    }
                }
            }
        }
        if ($errCode == 0) {
            try {
                $config['cacheable'] = true;
                $config['cachedir'] = './upload/dir/';
                $config['errorlog'] = './upload/log/';
                $config['imagedir'] = './upload/qrcode/';
                $config['quality'] = true;
                $config['size'] = '1024';
                $config['black'] = array(224, 255, 255);
                $config['white'] = array(70, 130, 180);
                $this->ciqrcode->initialize($config);
                $qrcodeImage = $checkValid->kode_proposal . '.png';

                $params['data'] = $checkValid->kode_proposal;
                $params['level'] = 'H';
                $params['size'] = 10;
                $params['savename'] = FCPATH . $config['imagedir'] . $qrcodeImage;
                $this->ciqrcode->generate($params);

                $data = array(
                    'id_pokmas' => $idPokmas,
                    'no_dpa' => $noDpa,
                    'no_surat' => $noSurat,
                    'tanggal' => date('Y-m-d', strtotime($tanggal)),
                    'perihal' => $perihal,
                    'no_biro' => $NoBiro,
                    'tanggal_disposisi' => date('Y-m-d', strtotime($tanggalDisposisi)),
                    'jenis_bantuan' => $jenisBantuan,
                    'usulan_barang' => $usulanBarang,
                    'usulan_nilai_anggaran' => $usulanNilaiAnggaran,
                    'barang' => $barang,
                    'nilai_anggaran' => $nilaiAnggaran,
                    'updated_date' => $date,
                    'updated_by' => $username,
                );

                if ($fileUpload['size'] > 0) {
                    $newName = time() . $fileUpload['name'];
                    $nmfile = "file_" . $newName;
                    $config['upload_path'] = "./upload/file_proposal/";
                    $config['allowed_types'] = 'pdf';
                    $config['max_size'] = '8048'; //maksimum besar file 8M
                    $config['file_name'] = $nmfile;
                    $config['overwrite'] = TRUE;
                    $this->load->library('upload', $config);
                    $imageData = $this->upload->data();

                    if ($this->upload->do_upload("file_upload")) {
                        $data = array_merge($data, array(
                            'nama_file_upload' => $imageData['file_name'],
                            'file_upload' => 'upload/file_proposal/' . $imageData['file_name'],
                            'tgl_help' => $date2
                        ));

                        $data2 = array(
                            'ref_table' => 'tbl_proposal',
                            'ref_id' => $id,
                            'nama_file_upload' => $imageData['file_name'],
                            'file_upload' => 'upload/file_proposal/' . $imageData['file_name'],
                            // 'tanggal' => $date2,
                            'status' => 'Pengajuan File Revisi',
                            'created_date' => $date,
                            'created_by' => $username,
                            'updated_date' => $date,
                            'updated_by' => $username,
                        );
                        $result = $this->db->insert('tbl_history_upload', $data2);
                    }
                }
                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }

    public function cetak_proposal($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Ubah " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_proposal_pengajuan->selectById($id);
            if ($resultData != null) {
                $data['resultData'] = $resultData;
                $data['page'] = self::__title;
                $data['judul'] = self::__title;
                $this->load->view('' . self::__folder . 'cetak_proposal', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function history($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Ubah " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_proposal_pengajuan->selectById($id);
            if ($resultData != null) {
                $data['page'] = self::__title;
                $data['judul'] = self::__title;
                $where = array(self::__tableId => $id);
                $data['histori'] = $this->M_proposal_pengajuan->select_histori($id);
                parent::loadkonten('' . self::__folder . 'detail_histori', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesDelete()
    {
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $id = $_POST[self::__tableId];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_proposal_pengajuan->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            $checkForeign = $this->M_proposal_pengajuan->checkForeign($id);
            if ($checkForeign) {
                $errCode++;
                $errMessage = self::__title . " terdapat pada modul lain.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->load->helper("file");

                $files = $this->db->get_where(self::__tableName, array('id_proposal' => $id));
                if ($files->num_rows() > 0) {
                    $hasil = $files->row();
                    $judul = $hasil->nama_file_upload;
                    //hapus unlink file
                    $path = 'upload/file_proposal/' . $judul;
                    unlink($path);
                }

                if ($files->num_rows() > 0) {
                    $hasil = $files->row();
                    $judul = $hasil->qrcode;
                    //hapus unlink file
                    $path = 'upload/qrcode/' . $judul;
                    unlink($path);
                }

                $this->db->update(self::__tableName, array('deleted_date' => date('Y-m-d H:i:s')), array(self::__tableId => $id));
                $this->db->update('tbl_log', array('deleted_date' => date('Y-m-d H:i:s')), array('id_proposal' => $id));
                $this->db->update('tbl_survey', array('deleted_date' => date('Y-m-d H:i:s')), array('id_proposal' => $id));
                $this->db->update('tbl_history_upload', array('deleted_date' => date('Y-m-d H:i:s')), array('ref_id' => $id));
                $this->db->update('tbl_checklis', array('deleted_date' => date('Y-m-d H:i:s')), array('id_proposal' => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di hapus');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }
}
