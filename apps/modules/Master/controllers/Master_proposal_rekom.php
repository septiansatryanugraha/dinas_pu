<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_proposal_rekom extends AUTH_Controller
{
    const __tableName = 'tbl_proposal';
    const __tableId = 'id_proposal';
    const __tableIdname = 'name';
    const __folder = 'v_proposal_rekom/';
    const __kode_menu = 'master-proposal-rekom';
    const __title = 'Proposal Rekomendasi ';
    const __model = 'M_proposal_rekom';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
        $this->load->model('M_kabupaten');
        $this->load->model('M_kecamatan');
        $this->load->model('M_desa');
        $this->load->model('M_pokmas');
        $this->load->model('M_status');
        $this->load->model('M_utilities');
        $this->load->model('M_proposal');
    }

    public function index()
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('view', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            $data['status'] = $this->M_proposal_rekom->selectStatus();
            parent::loadkonten(self::__folder . 'home', $data);
        }
    }

    public function ajax_list()
    {
        $status = $this->input->post('status');
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = array(
            'status' => $status,
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $list = $this->M_proposal_rekom->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $status = '<small class="label pull-center bg-blue">Tidak Direkomendasi</small>';
            if ($brand->status == 'Rekom Bidang') {
                $status = '<small class="label pull-center bg-green">Rekom Bidang</small>';
            } else if ($brand->status == 'Verifikasi') {
                $status = '<small class="label pull-center bg-green">Verifikasi Admin</small>';
            }

            if ($brand->created_date == $brand->updated_date) {
                $proses = "Belum ada yang memproses";
            } else {
                $proses = $brand->updated_by;
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->kode_proposal . '</b><br> Jenis Hibah : <b>' . $brand->jenis_hibah . '</b>';
            $row[] = $brand->nama_kelompok . '<br><br>Grup : <b>' . $brand->nama_grup . '</b><br> No DPA : <b>' . $brand->no_dpa . '</b>';
            $row[] = $brand->kabupaten;
            $row[] = $status . '<br> Proposal di Proses oleh : <b>' . $proses . '</b>';
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->created_date, 'by' => $brand->created_by));
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->updated_date, 'by' => $brand->updated_by));

            //add html for action
            $action = '-';
            if ($brand->survei == 'Selesai Survey' && $brand->status != 'Rekom Bidang') {
                if ($accessEdit->menuview > 0) {
                    $action = " <div class='btn-group'>";
                    $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
                    $action .= "    <ul class='dropdown-menu align-left pull-right'>";
                    $action .= "    <li><a href='" . base_url('edit-proposal-rekom') . "/" . $brand->id_proposal . "'><i class='fa fa-edit'></i> Ubah Data</a></li>";
                    $action .= "    </ul>";
                    $action .= "</div>";
                }
            }
            $row[] = $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Ubah " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_proposal_rekom->selectById($id);
            if ($resultData != null) {
                $data['resultData'] = $resultData;
                $data['detail'] = $this->M_proposal_rekom->getDataDetail($id);
                $data['status'] = $this->M_proposal_rekom->selectStatus();
                $data['menuName'] = self::__kode_menu;
                parent::loadkonten('' . self::__folder . 'update', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";
        $config = array();

        $status = $this->input->post('status');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_proposal_rekom->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            $getKelompokDpa = $this->M_utilities->getKelompokDpa($checkValid->id_pokmas);
            if ($getKelompokDpa != null) {
                if (date('Y', strtotime($date2)) < date('Y', strtotime($getKelompokDpa->date))) {
                    $errCode++;
                    $errMessage = "Gagal approve proposal karena data pokmas telah mengajukan proposal pada tahun " . date('Y', strtotime($getKelompokDpa->created_date)) . " .";
                }
            }
        }
        if ($errCode == 0) {
            if (strlen($status) == 0) {
                $errCode++;
                $errMessage = "Status wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = array(
                    'status' => $status,
                    'updated_date' => $date,
                    'updated_by' => $username,
                );
                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $id));

                $pokmas = $this->M_pokmas->selectById($checkValid->id_pokmas);
                $keteranganLog = "";
                if ($status == "Rekom Bidang") {
                    $keteranganLog = 'user <b>' . $username . '</b> Sedang melakukan proses ' . $status . ' terhadap Proposal yang di ajukan oleh Admin Help Desk Bagian atas nama Pokmas <b>' . $pokmas->nama_kelompok . '</b>';
                } else if ($status == "Tidak Rekom") {
                    $keteranganLog = 'user <b>' . $username . '</b> tidak memberikan Rekomendasi terhadap Proposal yang di ajukan oleh Admin Help Desk Bagian atas nama Pokmas <b>' . $pokmas->nama_kelompok . '</b>';
                }

                $data2 = array(
                    'id_proposal' => $id,
                    'status' => $status,
                    'keterangan' => $keteranganLog,
                    'created_date' => $date,
                    'created_by' => 'System',
                    'updated_date' => $date,
                    'updated_by' => 'System',
                );

                $result = $this->db->insert('tbl_log', $data2);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }

    public function download()
    {
        if ($this->input->post('images')) {
            $images = $this->input->post('images');
            foreach ($images as $image) {
                $this->zip->read_file($image);
            }
            $this->zip->download('' . time() . '.zip');
        }
    }
}
