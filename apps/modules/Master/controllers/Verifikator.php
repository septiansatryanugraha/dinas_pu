<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifikator extends AUTH_Controller
{
    const __tableName = 'tbl_verifikator';
    const __tableId = 'id';
    const __folder = 'v_verifikator/';
    const __kode_menu = 'verifikator';
    const __title = 'Verifikator';
    const __model = 'M_verifikator';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_grup');
        $this->load->model('M_kabupaten');
        $this->load->model('M_sidebar');
    }

    public function index()
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            $data['grup'] = $this->M_grup->select(array(), array('PSDA', 'Irigasi', 'Binfat', 'SWP', 'Sekretariat'));
            parent::loadkonten(self::__folder . '/home', $data);
        }
    }

    public function ajax_list()
    {
        $grupId = $this->input->post('grup_id');

        $filter = array(
            'grup_id' => $grupId,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);

        $list = $this->M_verifikator->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->nama_grup;
            $row[] = $brand->nama;
            $row[] = $brand->alamat;
            $row[] = $brand->telp;
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->created_date, 'by' => $brand->created_by));
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->updated_date, 'by' => $brand->updated_by));

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($accessEdit->menuview > 0) {
                $action .= "    <li><a class='ajaxify' href='" . base_url('edit-verifikator') . "/" . $brand->id . "'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-verifikator' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Tambah " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['menuName'] = self::__kode_menu;
            $data['grup'] = $this->M_grup->select(array(), array('PSDA', 'Irigasi', 'Binfat', 'SWP', 'Sekretariat'));
            $data['kabupaten'] = $this->M_kabupaten->select();
            parent::loadkonten('' . self::__folder . 'tambah', $data);
        }
    }

    public function prosesAdd()
    {
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $grupId = trim($this->input->post("grup_id"));
        $idKabupaten = trim($this->input->post("id_kabupaten"));
        $nama = trim($this->input->post("nama"));
        $ketua = trim($this->input->post("ketua"));
        $alamat = trim($this->input->post("alamat"));
        $telp = trim($this->input->post("telp"));
        $username = trim($this->input->post("username"));
        $password = trim($this->input->post("password"));

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('add', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($grupId) == 0) {
                $errCode++;
                $errMessage = "Sub Bidang wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkGrup = $this->M_grup->selectById($grupId);
            if ($checkGrup == null) {
                $errCode++;
                $errMessage = "Sub Bidang tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($nama) == 0) {
                $errCode++;
                $errMessage = "Nama wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKabupaten) == 0) {
                $errCode++;
                $errMessage = "kabupaten wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKabupaten = $this->M_kabupaten->selectById($idKabupaten);
            if ($checkKabupaten == null) {
                $errCode++;
                $errMessage = "Kabupaten tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($username) == 0) {
                $errCode++;
                $errMessage = "username wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkUsername = $this->M_verifikator->selectByUsername($username);
            if ($checkUsername != null) {
                $errCode++;
                $errMessage = "Username sudah digunakan.";
            }
        }
        if ($password == 0) {
            if (strlen($password) == 0) {
                $errCode++;
                $errMessage = "password wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = array(
                    'grup_id' => $grupId,
                    'id_kabupaten' => $idKabupaten,
                    'nama' => $nama,
                    'ketua' => $ketua,
                    'alamat' => $alamat,
                    'telp ' => $telp,
                    'username' => $username,
                    'password ' => base64_encode($password),
                    'created_date' => $date,
                    'created_by' => $this->session->userdata('username'),
                    'updated_date' => $date,
                    'updated_by' => $this->session->userdata('username'),
                );
                $this->db->insert(self::__tableName, $data);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Ubah " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_verifikator->selectById($id);
            if ($resultData != null) {
                $data['resultData'] = $resultData;
                $data['grup'] = $this->M_grup->select(array(), array('PSDA', 'Irigasi', 'Binfat', 'SWP', 'Sekretariat'));
                $data['kabupaten'] = $this->M_kabupaten->select();
                $data['menuName'] = self::__kode_menu;
                parent::loadkonten('' . self::__folder . 'update', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $grupId = trim($this->input->post("grup_id"));
        $idKabupaten = trim($this->input->post("id_kabupaten"));
        $nama = trim($this->input->post("nama"));
        $ketua = trim($this->input->post("ketua"));
        $alamat = trim($this->input->post("alamat"));
        $telp = trim($this->input->post("telp"));
        $username = trim($this->input->post("username"));
        $password = trim($this->input->post("password"));

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_verifikator->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($grupId) == 0) {
                $errCode++;
                $errMessage = "Sub Bidang wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKabupaten = $this->M_grup->selectById($grupId);
            if ($checkKabupaten == null) {
                $errCode++;
                $errMessage = "Sub Bidang tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($nama) == 0) {
                $errCode++;
                $errMessage = "Nama wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKabupaten) == 0) {
                $errCode++;
                $errMessage = "kabupaten wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKabupaten = $this->M_kabupaten->selectById($idKabupaten);
            if ($checkKabupaten == null) {
                $errCode++;
                $errMessage = "Kabupaten tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($username) == 0) {
                $errCode++;
                $errMessage = "username wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkUsername = $this->M_verifikator->selectByUsername($username, $id);
            if ($checkUsername != null) {
                $errCode++;
                $errMessage = "Username sudah digunakan.";
            }
        }
        if ($password == 0) {
            if (strlen($password) == 0) {
                $errCode++;
                $errMessage = "password wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = array(
                    'grup_id' => $grupId,
                    'id_kabupaten' => $idKabupaten,
                    'nama' => $nama,
                    'ketua' => $namaKetua,
                    'alamat' => $alamat,
                    'telp ' => $telp,
                    'username' => $username,
                    'password ' => base64_encode($password),
                    'created_date' => $date,
                    'created_by' => $this->session->userdata('username'),
                    'updated_date' => $date,
                    'updated_by' => $this->session->userdata('username'),
                );
                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $id = $_POST[self::__tableId];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_verifikator->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            $checkForeign = $this->M_verifikator->checkForeign($id);
            if ($checkForeign) {
                $errCode++;
                $errMessage = self::__title . " terdapat pada modul lain.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->db->update(self::__tableName, array('deleted_date' => date('Y-m-d H:i:s')), array(self::__tableId => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di hapus');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }
}
