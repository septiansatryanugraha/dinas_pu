<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_survei extends AUTH_Controller
{
    const __tableName = 'tbl_survey';
    const __tableId = 'id_survey';
    const __tableIdname = 'name';
    const __folder = 'v_survei/';
    const __kode_menu = 'master-survei';
    const __title = 'Survei Lapangan ';
    const __model = 'M_survei';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
        $this->load->model('M_kabupaten');
        $this->load->model('M_kecamatan');
        $this->load->model('M_desa');
        $this->load->model('M_pokmas');
        $this->load->model('M_status');
        $this->load->model('M_proposal');
        $this->load->model('M_utilities');
    }

    public function index()
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('view', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            $data['survei'] = $this->M_survei->selectStatusSurvey();
            parent::loadkonten(self::__folder . 'home', $data);
        }
    }

    public function ajax_list()
    {
        $survei = $this->input->post('survei');
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = array(
            'statusFile' => true,
            'survei' => $survei,
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $list = $this->M_survei->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $qChecklist = "SELECT * FROM tbl_checklis WHERE id_proposal = '{$brand->id_proposal}'";
            $dataChecklist = $this->db->query($qChecklist)->row();

            $status = '<small class="label pull-center bg-blue">Belum Verifikasi</small>';
            if ($brand->status == 'Verifikasi') {
                $status = '<small class="label pull-center bg-green">Verifikasi Admin</small>';
            } else if ($brand->status == 'Direvisi') {
                $status = '<small class="label pull-center bg-red">Direvisi</small>';
            } else if ($brand->status == 'Rekom Bidang') {
                $status = '<small class="label pull-center bg-green">Rekom Bidang</small>';
            } else if ($brand->status == 'Tidak Rekom') {
                $status = '<small class="label pull-center bg-green">Rekom Bidang</small>';
            } else if ($brand->status == 'Penetapan') {
                $status = '<small class="label pull-center bg-green">Penetapan Proposal</small>';
            } else if ($brand->status == 'NPHD') {
                $status = '<small class="label pull-center bg-green">Proposal NPHD</small>';
            }

            if ($brand->created_date == $brand->updated_date) {
                $proses = "Belum ada yang memproses";
            } else {
                $proses = $brand->updated_by;
            }

            $StatusSurvey = "";
            if ($brand->survei == "Survey") {
                $StatusSurvey = "<li><b>Sedang dilakukan Survey Lapangan<b></li>";
            } else if ($brand->survei == "Selesai Survey") {
                $StatusSurvey = "<li><b>Survei selesai di lakukan<b></li>";
            }

            $statusFile = " <table>
                                <tr>
                                    <td style='width: 50%; vertical-align: top;'>File Hasil Survei</td>
                                    <td style='width: 5%; vertical-align: top;'>:</td>
                                    <td style='vertical-align: top;'><b>" . $brand->status_hasil_survei . "<b/></td>
                                </tr>
                                <tr>
                                    <td style='vertical-align: top;'>File Sketsa Survei</td>
                                    <td style='vertical-align: top;'>:</td>
                                    <td style='vertical-align: top;'><b>" . $brand->status_sketsa_survei . "</b></td>
                                </tr>
                                <tr>
                                    <td style='vertical-align: top;'>File Berita Acara</td>
                                    <td style='vertical-align: top;'>:</td>
                                    <td style='vertical-align: top;'><b>" . $brand->status_berita_acara . "</b></td>
                                </tr>
                                <tr>
                                    <td style='vertical-align: top;'>File Checklist Survei</td>
                                    <td style='vertical-align: top;'>:</td>
                                    <td style='vertical-align: top;'><b>" . $brand->status_checklist . "</b></td>
                                </tr>
                            </table>";

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->kode_proposal . '</b><br> Jenis Hibah : <b>' . $brand->jenis_hibah . '</b>' . '<br><br>' . $statusFile;
            $row[] = $brand->nama_kelompok . '<br><br>Grup : <b>' . $brand->nama_grup . '</b><br> No DPA : <b>' . $brand->no_dpa . '</b>';
            $row[] = $brand->kabupaten;
            $row[] = $status . '<br> Proposal di Proses oleh : <b>' . $proses . '</b><br>' . $StatusSurvey;
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->created_date, 'by' => $brand->created_by));
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->updated_date, 'by' => $brand->updated_by));

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
//            if ($brand->status == 'Survey') {
            if ($accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-survei') . "/" . $brand->id_proposal . "'><i class='fa fa-square'></i> Ubah Survei</a></li>";
            }
//            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Ubah " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_survei->selectById($id);
            if ($resultData != null) {
                $data['resultData'] = $resultData;
                $data['survei'] = $this->M_survei->selectStatusSurvey();
                $data['gambar_survei'] = $this->M_survei->selectGbrSurvey($id);
                $data['historiFileHasilSurvei'] = $this->M_utilities->historiFile('tbl_proposal', $id, 'hasil survei');
                $data['historiSketsaSurvei'] = $this->M_utilities->historiFile('tbl_proposal', $id, 'sketsa survei');
                $data['historiFileBeritaAcara'] = $this->M_utilities->historiFile('tbl_proposal', $id, 'berita acara');
                $data['historiFileChecklist'] = $this->M_utilities->historiFile('tbl_proposal', $id, 'checklist');
                $data['menuName'] = self::__kode_menu;
                parent::loadkonten('' . self::__folder . 'update', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";
        $arrFileType = array(
            'file_hasil_survei',
            'file_sketsa_survei',
            'file_berita_acara',
            'file_checklist',
        );

        $jenisKegiatan = trim($this->input->post('jenis_kegiatan'));
        $alamat = trim($this->input->post('alamat'));
        $longitude = trim($this->input->post('longitude'));
        $latitude = trim($this->input->post('latitude'));
        $catatan = trim($this->input->post('catatan'));
        $noBa = trim($this->input->post('no_ba'));
        $hari = trim($this->input->post('hari'));
        $tanggal = trim($this->input->post('tanggal'));
        $bulan = trim($this->input->post('bulan'));
        $tahun = trim($this->input->post('tahun'));
        $lamaPelaksanaan = trim($this->input->post('lama_pelaksanaan'));
        $barang = trim($this->input->post('barang'));
        $jumlahAnggaran = trim($this->input->post('jumlah_anggaran'));
        $StatusSurvey = trim($this->input->post('survei'));
        $tglSurvei = trim($this->input->post('tgl_survei'));
        $fileUpload = $_FILES['file_upload'];
        $arrFileUpload = parent::rotate($_FILES['file_upload']);

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_survei->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            $checkProposal = $this->M_proposal->selectById($checkValid->id_proposal);
            if ($checkProposal == null) {
                $errCode++;
                $errMessage = "Proposal tidak valid.";
            }
        }
        if ($errCode == 0) {
            $checkPokmas = $this->M_pokmas->selectById($checkProposal->id_pokmas);
            if ($checkPokmas == null) {
                $errCode++;
                $errMessage = "Pokmas tidak valid.";
            }
        }
        if ($errCode == 0) {
            foreach ($arrFileUpload as $key => $value) {
                if ($value['size'] > 0) {
                    if ($value['type'] != 'application/pdf') {
                        $errCode++;
                        $errMessage = "Berkas File wajib format PDF.";
                        break;
                    }
                }
            }
        }
        if ($errCode == 0) {
            if ($checkValid->jenis_bantuan == "Barang") {
                $jumlahAnggaran = null;
                if ($errCode == 0) {
                    if (strlen($barang) == 0) {
                        $errCode++;
                        $errMessage = "Usulan Barang Setelah Evaluasi wajib di isi.";
                    }
                }
            }
        }
        if ($errCode == 0) {
            if ($checkValid->jenis_bantuan == "Uang") {
                $barang = null;
                if ($errCode == 0) {
                    if (strlen($jumlahAnggaran) == 0) {
                        $errCode++;
                        $errMessage = "Usulan Anggaran Setelah Evaluasi wajib di isi.";
                    }
                }
            }
        }
        // if ($errCode == 0) {
        //     if (strlen($longitude) > 0 && strlen($latitude) > 0) {
        //         if ($longitude == $checkPokmas->longitude && $latitude == $checkPokmas->latitude) {
        //             $errCode++;
        //             $errMessage = "Lokasi Pekerjaan tidak boleh sama dengan lokasi pokmas.";
        //         }
        //     }
        // }
        if ($errCode == 0) {
            try {
                if (strlen($tglSurvei) > 0) {
                    $tglSurvei = date('Y-m-d', strtotime($tglSurvei));
                } else {
                    $tglSurvei = null;
                }
                $data = array(
                    'jenis_kegiatan' => $jenisKegiatan,
                    'alamat' => $alamat,
                    'longitude' => $longitude,
                    'latitude' => $latitude,
                    'catatan' => $catatan,
                    'barang' => $barang,
                    'jumlah_anggaran' => $jumlahAnggaran,
                    'survei' => $StatusSurvey,
                    'tgl_survei' => $tglSurvei,
                    'updated_date' => $date,
                    'updated_by' => $username,
                );
                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $id));

                $filesCount = count($fileUpload['name']);
                if ($filesCount == count($arrFileType)) {
                    for ($i = 0; $i < $filesCount; $i++) {
                        if ($fileUpload['size'][$i] > 0) {
                            $_FILES['file_uploads']['name'] = $fileUpload['name'][$i];
                            $_FILES['file_uploads']['type'] = $fileUpload['type'][$i];
                            $_FILES['file_uploads']['tmp_name'] = $fileUpload['tmp_name'][$i];
                            $_FILES['file_uploads']['error'] = $fileUpload['error'][$i];
                            $_FILES['file_uploads']['size'] = $fileUpload['size'][$i];

                            $folder = parent::createFolder($arrFileType[$i]);
                            $uploadPath = "upload/" . $arrFileType[$i] . "/";

                            $config['path'] = $uploadPath;
                            $config['upload_path'] = "./" . $uploadPath;
                            $config['allowed_types'] = 'pdf';
                            $config['max_size'] = '8048'; //maksimum besar file 8M
                            $config['file_name'] = str_replace(" ", "_", "file_" . time() . "_" . $fileUpload['name'][$i]);
                            $config['overwrite'] = TRUE;

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);

                            if ($this->upload->do_upload('file_uploads')) {
                                $imageData = $this->upload->data();

                                $data2 = array(
                                    'ref_table' => 'tbl_proposal',
                                    'ref_id' => $id,
                                    'nama_file_upload' => $imageData['file_name'],
                                    'file_upload' => $uploadPath . '' . $imageData['file_name'],
                                    'tanggal' => $date2,
                                    'status' => trim(str_replace('file', '', str_replace('_', ' ', $arrFileType[$i]))),
                                    'created_date' => $date,
                                    'created_by' => $username,
                                    'updated_date' => $date,
                                    'updated_by' => $username,
                                );
                                $result = $this->db->insert('tbl_history_upload', $data2);
                            }
                        }
                    }
                }

                if ($StatusSurvey == 'Selesai Survey') {
                    $data3 = array(
                        'survei' => $StatusSurvey,
                        'updated_date' => $date,
                        'updated_by' => $username,
                    );
                    $result = $this->db->update('tbl_proposal', $data3, array('id_proposal' => $checkValid->id_proposal));

                    $pokmas = $this->M_pokmas->selectById($checkProposal->id_pokmas);
                    $data4 = array(
                        'id_proposal' => $checkValid->id_proposal,
                        'status' => $StatusSurvey,
                        'keterangan' => 'Tim Survey DINAS PU SUMBER DAYA AIR PROVINSI JAWA TIMUR sudah melakukan survei terhadap pengajuan proposal dengan kode nomor ' . $pokmas->kode_proposal . ' atas nama Pokmas <b>' . $pokmas->nama_kelompok . '</b>',
                        'created_date' => $date,
                        'created_by' => 'System',
                        'updated_date' => $date,
                        'updated_by' => 'System',
                    );
                    $result = $this->db->insert('tbl_log', $data4);
                }
            } catch (Exception $exc) {
                $errCode++;
                $errMessage = $exc->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di update');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }

    public function upload_gambar($id)
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_survei->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            $checkProposal = $this->M_proposal->selectById($checkValid->id_proposal);
            if ($checkProposal == null) {
                $errCode++;
                $errMessage = "Proposal tidak valid.";
            }
        }
        if ($errCode == 0) {
            $filesCount = count($_FILES['gambar']['name']);
            for ($i = 0; $i < $filesCount; $i++) {
                $_FILES['upload_File']['name'] = $_FILES['gambar']['name'][$i];
                $_FILES['upload_File']['type'] = $_FILES['gambar']['type'][$i];
                $_FILES['upload_File']['tmp_name'] = $_FILES['gambar']['tmp_name'][$i];
                $_FILES['upload_File']['error'] = $_FILES['gambar']['error'][$i];
                $_FILES['upload_File']['size'] = $_FILES['gambar']['size'][$i];
                $uploadPath = 'upload/survei/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['overwrite'] = TRUE;
                $config['remove_spaces'] = true;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('upload_File')) {
                    $image_data = $this->upload->data();

                    //Compress Image
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = './upload/survei/' . $image_data['file_name'];
                    $config['create_thumb'] = FALSE;
                    $config['maintain_ratio'] = FALSE;
                    $config['quality'] = '70%';
                    $config['width'] = 500;
                    $config['height'] = 300;
                    $config['new_image'] = './upload/survei/' . $image_data['file_name'];
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();

                    $path['link'] = "upload/survei/";
                    $uploadData[$i]['gambar'] = $path['link'] . '' . $image_data['file_name'];
                    $uploadData[$i]['id_survey'] = $id;
                    $uploadData[$i]['created_date'] = $date;
                    $uploadData[$i]['created_by'] = $username;
                    $uploadData[$i]['updated_date'] = $date;
                    $uploadData[$i]['updated_by'] = $username;
                }
            }
            $result = $this->M_survei->insert($uploadData);
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($result > 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di upload');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => 'Maaf data gagal di upload ! ' . $errMessage);
        }

        echo json_encode($out);
    }

    public function cetak_survei($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Cetak " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_survei->selectById($id);
            if ($resultData != null) {
                $data['resultData'] = $resultData;
                $data['gambar_survei'] = $this->M_survei->selectGbrSurvey($id);
                $this->load->view('' . self::__folder . 'cetak_survey', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function download()
    {
        if ($this->input->post('images')) {
            $images = $this->input->post('images');
            foreach ($images as $image) {
                $this->zip->read_file($image);
            }
            $this->zip->download('' . time() . '.zip');
        }
    }

    public function delete_gambar($id)
    {
        $this->load->helper("file");
        $id = array('id_gambar' => $id);
        $files = $this->db->get_where('tbl_gambar_survey', $id);
        if ($files->num_rows() > 0) {
            $hasil = $files->row();
            $judul = $hasil->gambar;
            //hapus unlink foto
            if (file_exists($file = $judul)) {
                unlink($file);
            } else {
                
            }
        }
        $this->M_survei->hapus2('tbl_gambar_survey', $id);
        echo '<script language="javascript">alert("Gambar berhasil dihapus"); history.back();</script>';
    }
}
