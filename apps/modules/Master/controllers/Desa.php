<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Desa extends AUTH_Controller
{
    const __tableName = 'tbl_desa';
    const __tableId = 'id';
    const __folder = 'v_desa/';
    const __kode_menu = 'master-desa';
    const __title = 'Kelurahan / Desa';
    const __model = 'M_desa';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_kabupaten');
        $this->load->model('M_kecamatan');
        $this->load->model('M_sidebar');
    }

    public function index()
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            $data['kab'] = $this->M_kabupaten->select();
            parent::loadkonten(self::__folder . 'home', $data);
        }
    }

    public function ajax_list()
    {
        $idKabupaten = $this->input->post('id_kabupaten');
        $idKecamatan = $this->input->post('id_kecamatan');

        $filter = array(
            'id_kecamatan' => $idKecamatan,
            'id_kabupaten' => $idKabupaten,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);

        $list = $this->M_desa->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->kabupaten;
            $row[] = $brand->kecamatan;
            $row[] = $brand->desa;
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->created_date, 'by' => $brand->created_by));
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->updated_date, 'by' => $brand->updated_by));

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-desa') . "/" . $brand->id . "'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-desa' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Tambah " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['menuName'] = self::__kode_menu;
            $data['kabupaten'] = $this->M_kabupaten->select();
            parent::loadkonten('' . self::__folder . 'tambah', $data);
        }
    }

    public function prosesAdd()
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $idKabupaten = trim($this->input->post("id_kabupaten"));
        $idKecamatan = trim($this->input->post("id_kecamatan"));
        $desa = trim($this->input->post("desa"));

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('add', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKabupaten) == 0) {
                $errCode++;
                $errMessage = "Kabupaten wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKabupaten = $this->M_kabupaten->selectById($idKabupaten);
            if ($checkKabupaten == null) {
                $errCode++;
                $errMessage = "Kabupaten tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKecamatan) == 0) {
                $errCode++;
                $errMessage = "Kecamatan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKecamatan = $this->M_kecamatan->selectById($idKecamatan);
            if ($checkKecamatan == null) {
                $errCode++;
                $errMessage = "Kecamatan tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkKecamatan->id_kabupaten != $idKabupaten) {
                $errCode++;
                $errMessage = "Kecamatan tidak sesuai dengan kabupaten.";
            }
        }
        if ($errCode == 0) {
            if (strlen($desa) == 0) {
                $errCode++;
                $errMessage = self::__title . " wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkExist = $this->M_desa->selectByExist(array('id_kecamatan' => $idKecamatan, 'desa' => $desa));
            if ($checkExist != null) {
                $errCode++;
                $errMessage = self::__title . " sudah ada di list.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = array(
                    'id_kecamatan' => $idKecamatan,
                    'desa' => $desa,
                    'created_date' => $date,
                    'created_by' => $username,
                    'updated_date' => $date,
                    'updated_by' => $username,
                );
                $this->db->insert(self::__tableName, $data);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Ubah " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_desa->selectById($id);
            if ($resultData != null) {
                $kecamatan = $this->M_kecamatan->selectById($resultData->id_kecamatan);
                $data['resultData'] = $resultData;
                $data['id_kabupaten'] = $kecamatan->id_kabupaten;
                $data['kabupaten'] = $this->M_kabupaten->select();
                $data['kecamatan'] = $this->M_kecamatan->select(array('id_kabupaten' => $kecamatan->id_kabupaten));
                $data['menuName'] = self::__kode_menu;
                $data['idCustomer'] = $id;
                parent::loadkonten('' . self::__folder . 'update', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $idKabupaten = trim($this->input->post("id_kabupaten"));
        $idKecamatan = trim($this->input->post("id_kecamatan"));
        $desa = trim($this->input->post("desa"));

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_desa->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKabupaten) == 0) {
                $errCode++;
                $errMessage = "Kabupaten wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKabupaten = $this->M_kabupaten->selectById($idKabupaten);
            if ($checkKabupaten == null) {
                $errCode++;
                $errMessage = "Kabupaten tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKecamatan) == 0) {
                $errCode++;
                $errMessage = "Kecamatan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKecamatan = $this->M_kecamatan->selectById($idKecamatan);
            if ($checkKecamatan == null) {
                $errCode++;
                $errMessage = "Kecamatan tidak valid.";
            }
        }
        if ($errCode == 0) {
            if ($checkKecamatan->id_kabupaten != $idKabupaten) {
                $errCode++;
                $errMessage = "Kecamatan tidak sesuai dengan kabupaten.";
            }
        }
        if ($errCode == 0) {
            if (strlen($desa) == 0) {
                $errCode++;
                $errMessage = self::__title . " wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkExist = $this->M_desa->selectByExist(array('id_kecamatan' => $idKecamatan, 'desa' => $desa), $id);
            if ($checkExist != null) {
                $errCode++;
                $errMessage = self::__title . " sudah ada di list.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = array(
                    'id_kecamatan' => $idKecamatan,
                    'desa' => $desa,
                    'updated_date' => $date,
                    'updated_by' => $username,
                );
                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $id = $_POST[self::__tableId];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_desa->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            $checkForeign = $this->M_desa->checkForeign($id);
            if ($checkForeign) {
                $errCode++;
                $errMessage = self::__title . " terdapat pada modul lain.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->db->update(self::__tableName, array('deleted_date' => date('Y-m-d H:i:s')), array(self::__tableId => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di hapus');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }
}
