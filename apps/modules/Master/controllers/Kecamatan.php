<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan extends AUTH_Controller
{
    const __tableName = 'tbl_kecamatan';
    const __tableId = 'id';
    const __folder = 'v_kecamatan/';
    const __kode_menu = 'kecamatan';
    const __title = 'Kecamatan';
    const __model = 'M_kecamatan';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_kabupaten');
        $this->load->model('M_sidebar');
    }

    public function index()
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            $data['kab'] = $this->M_kabupaten->select();
            parent::loadkonten(self::__folder . '/home', $data);
        }
    }

    public function ajax_list()
    {
        $idKabupaten = $this->input->post('id_kabupaten');

        $filter = array(
            'id_kabupaten' => $idKabupaten,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);

        $list = $this->M_kecamatan->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->kabupaten;
            $row[] = $brand->kecamatan;
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->created_date, 'by' => $brand->created_by));
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->updated_date, 'by' => $brand->updated_by));

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-kecamatan') . "/" . $brand->id . "'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessDel->menuview > 0) {
                $action .= "    <li><a href='#' class='hapus-kecamatan' data-toggle='tooltip' data-placement='top' data-id='" . $brand->id . "'><i class='glyphicon glyphicon-trash'></i> Hapus</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Tambah " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $data['menuName'] = self::__kode_menu;
            $data['kab'] = $this->M_kabupaten->select();
            parent::loadkonten('' . self::__folder . 'tambah', $data);
        }
    }

    public function prosesAdd()
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $idKabupaten = trim($this->input->post("id_kabupaten"));
        $kecamatan = trim($this->input->post("kecamatan"));

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('add', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKabupaten) == 0) {
                $errCode++;
                $errMessage = "Kabupaten wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKabupaten = $this->M_kabupaten->selectById($idKabupaten);
            if ($checkKabupaten == null) {
                $errCode++;
                $errMessage = "Kabupaten tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($kecamatan) == 0) {
                $errCode++;
                $errMessage = self::__title . " wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkExist = $this->M_kecamatan->selectByExist(array('id_kabupaten' => $idKabupaten, 'kecamatan' => $kecamatan));
            if ($checkExist != null) {
                $errCode++;
                $errMessage = self::__title . " sudah ada di list.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = array(
                    'id_kabupaten' => $idKabupaten,
                    'kecamatan' => $kecamatan,
                    'created_date' => $date,
                    'created_by' => $username,
                    'updated_date' => $date,
                    'updated_by' => $username,
                );
                $this->db->insert(self::__tableName, $data);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Ubah " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_kecamatan->selectById($id);
            if ($resultData != null) {
                $data['resultData'] = $resultData;
                $data['kab'] = $this->M_kabupaten->select();
                $data['menuName'] = self::__kode_menu;
                $data['idCustomer'] = $id;
                parent::loadkonten('' . self::__folder . 'update', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');

        $errCode = 0;
        $errMessage = "";

        $idKabupaten = trim($this->input->post("id_kabupaten"));
        $kecamatan = trim($this->input->post("kecamatan"));

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_kecamatan->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($idKabupaten) == 0) {
                $errCode++;
                $errMessage = "Kabupaten wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkKabupaten = $this->M_kabupaten->selectById($idKabupaten);
            if ($checkKabupaten == null) {
                $errCode++;
                $errMessage = "Kabupaten tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($kecamatan) == 0) {
                $errCode++;
                $errMessage = self::__title . " wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $checkExist = $this->M_kecamatan->selectByExist(array('id_kabupaten' => $idKabupaten, 'kecamatan' => $kecamatan), $id);
            if ($checkExist != null) {
                $errCode++;
                $errMessage = self::__title . " sudah ada di list.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = array(
                    'id_kabupaten' => $idKabupaten,
                    'kecamatan' => $kecamatan,
                    'updated_date' => $date,
                    'updated_by' => $username,
                );
                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }

    public function prosesDelete()
    {
        $date = date('Y-m-d H:i:s');
        $errCode = 0;
        $errMessage = "";

        $id = $_POST[self::__tableId];

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('del', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            if (strlen($id) == 0) {
                $errCode++;
                $errMessage = "ID does not exist.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_kecamatan->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            $checkForeign = $this->M_kecamatan->checkForeign($id);
            if ($checkForeign) {
                $errCode++;
                $errMessage = self::__title . " terdapat pada modul lain.";
            }
        }
        if ($errCode == 0) {
            try {
                $this->db->update(self::__tableName, array('deleted_date' => date('Y-m-d H:i:s')), array(self::__tableId => $id));
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di hapus');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }
}
