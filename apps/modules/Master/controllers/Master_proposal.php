<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_proposal extends AUTH_Controller
{
    const __tableName = 'tbl_proposal';
    const __tableId = 'id_proposal';
    const __tableIdname = 'name';
    const __folder = 'v_proposal/';
    const __kode_menu = 'master-proposal';
    const __title = 'Evaluasi Proposal ';
    const __model = 'M_proposal';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
        $this->load->model('M_kabupaten');
        $this->load->model('M_kecamatan');
        $this->load->model('M_desa');
        $this->load->model('M_pokmas');
        $this->load->model('M_status');
        $this->load->model('M_utilities');
    }

    public function index()
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('view', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            $data['status'] = $this->M_proposal->selectStatus();
            parent::loadkonten('' . self::__folder . 'home', $data);
        }
    }

    public function ajax_list()
    {
        $status = $this->input->post('status');
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = array(
            'status' => $status,
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $list = $this->M_proposal->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $status = '<small class="label pull-center bg-blue">Belum Verifikasi</small>';
            if ($brand->status == 'Rekomendasi') {
                $status = '<small class="label pull-center bg-green">Telah Direkomendasi</small>';
            } else if ($brand->status == 'Verifikasi') {
                $status = '<small class="label pull-center bg-green">Verifikasi Admin</small>';
            } else if ($brand->status == 'Rekom Bidang') {
                $status = '<small class="label pull-center bg-green">Rekom Bidang</small>';
            } else if ($brand->status == 'Direvisi') {
                $status = '<small class="label pull-center bg-red">Direvisi</small>';
            }

            if ($brand->created_date == $brand->updated_date) {
                $proses = "Belum ada yang memproses";
            } else {
                $proses = $brand->updated_by;
            }

            $StatusSurvey = "";
            if ($brand->survei == "Survey") {
                $StatusSurvey = "<li><b>Sedang dilakukan Survey Lapangan<b></li>";
            } else if ($brand->survei == "Selesai Survey") {
                $StatusSurvey = "<li><b>Survei selesai di lakukan<b></li>";
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->kode_proposal . '</b><br> Jenis Hibah : <b>' . $brand->jenis_hibah . '</b>';
            $row[] = $brand->nama_kelompok . '<br><br>Grup : <b>' . $brand->nama_grup . '</b><br> No DPA : <b>' . $brand->no_dpa . '</b>';
            $row[] = $brand->kabupaten;
            $row[] = $status . '<br> Proposal di Proses oleh : <b>' . $proses . '</b><br>' . $StatusSurvey;
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->created_date, 'by' => $brand->created_by));
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->updated_date, 'by' => $brand->updated_by));

            //add html for action
            $action = " <div class='btn-group'>";
            $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
            $action .= "    <ul class='dropdown-menu align-left pull-right'>";
            if ($accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('edit-proposal') . "/" . $brand->id_proposal . "'><i class='fa fa-edit'></i> Ubah</a></li>";
            }
            if ($accessEdit->menuview > 0) {
                $action .= "    <li><a href='" . base_url('checklist') . "/" . $brand->id_proposal . "'><i class='fa fa-check'></i> Checklist</a></li>";
            }
            $action .= "    </ul>";
            $action .= "</div>";
            $row[] = $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Ubah " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_proposal->selectById($id);
            if ($resultData != null) {
                $pokmas = $this->M_pokmas->selectById($resultData->id_pokmas);
                $desa = $this->M_desa->selectById($pokmas->id_desa);
                $kecamatan = $this->M_kecamatan->selectById($desa->id_kecamatan);
                $data['resultData'] = $resultData;
                $data['status'] = $this->M_proposal->selectStatus();
                $data['survei'] = $this->M_proposal->selectStatusSurvey();
                $data['historiFileHasilSurvei'] = $this->M_utilities->historiFile('tbl_proposal', $id, 'hasil survei');
                $data['historiSketsaSurvei'] = $this->M_utilities->historiFile('tbl_proposal', $id, 'sketsa survei');
                $data['historiFileBeritaAcara'] = $this->M_utilities->historiFile('tbl_proposal', $id, 'berita acara');
                $data['historiFileChecklist'] = $this->M_utilities->historiFile('tbl_proposal', $id, 'checklist');
                $data['menuName'] = self::__kode_menu;
                $data['idCustomer'] = $id;
                parent::loadkonten('' . self::__folder . 'update', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";
        $arrFileType = array(
            'file_hasil_survei',
            'file_sketsa_survei',
            'file_berita_acara',
            'file_checklist',
        );

        $status = $this->input->post('status');
        $IsSurvey = $this->input->post('status_survei');
        $Catatan = trim($this->input->post('catatan'));
        $noBa = trim($this->input->post('no_ba'));
        $fileUpload = $_FILES['file_upload'];
        $arrFileUpload = parent::rotate($_FILES['file_upload']);

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_proposal->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            $getKelompokDpa = $this->M_utilities->getKelompokDpa($checkValid->id_pokmas);
            if ($getKelompokDpa != null) {
                if (date('Y', strtotime($date2)) < date('Y', strtotime($getKelompokDpa->date))) {
                    $errCode++;
                    $errMessage = "Gagal approve proposal karena data pokmas telah mengajukan proposal pada tahun " . date('Y', strtotime($getKelompokDpa->created_date)) . " .";
                }
            }
        }
        if ($errCode == 0) {
            if (strlen($noBa) == 0) {
                $errCode++;
                $errMessage = "Berita Acara wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($status) == 0) {
                $errCode++;
                $errMessage = "Status wajib di isi.";
            }
        }
        if ($errCode == 0) {
            foreach ($arrFileUpload as $key => $value) {
                if ($value['size'] > 0) {
                    if ($value['type'] != 'application/pdf') {
                        $errCode++;
                        $errMessage = "Berkas File wajib format PDF.";
                        break;
                    }
                }
            }
        }
        if ($errCode == 0) {
            try {
                $data = array(
                    'no_ba' => $noBa,
                    'status' => $status,
                    'survei' => $IsSurvey,
                    'catatan' => $Catatan,
                    'updated_date' => $date,
                    'updated_by' => $username,
                );
                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $id));

                $filesCount = count($fileUpload['name']);
                if ($filesCount == count($arrFileType)) {
                    for ($i = 0; $i < $filesCount; $i++) {
                        if ($fileUpload['size'][$i] > 0) {
                            $_FILES['file_uploads']['name'] = $fileUpload['name'][$i];
                            $_FILES['file_uploads']['type'] = $fileUpload['type'][$i];
                            $_FILES['file_uploads']['tmp_name'] = $fileUpload['tmp_name'][$i];
                            $_FILES['file_uploads']['error'] = $fileUpload['error'][$i];
                            $_FILES['file_uploads']['size'] = $fileUpload['size'][$i];

                            $folder = parent::createFolder($arrFileType[$i]);
                            $uploadPath = "upload/" . $arrFileType[$i] . "/";

                            $config['path'] = $uploadPath;
                            $config['upload_path'] = "./" . $uploadPath;
                            $config['allowed_types'] = 'pdf';
                            $config['max_size'] = '8048'; //maksimum besar file 8M
                            $config['file_name'] = str_replace(" ", "_", "file_" . time() . "_" . $fileUpload['name'][$i]);
                            $config['overwrite'] = TRUE;

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);

                            if ($this->upload->do_upload('file_uploads')) {
                                $imageData = $this->upload->data();

                                $data2 = array(
                                    'ref_table' => 'tbl_proposal',
                                    'ref_id' => $id,
                                    'nama_file_upload' => $imageData['file_name'],
                                    'file_upload' => $uploadPath . '' . $imageData['file_name'],
                                    // 'tanggal' => $date2,
                                    'status' => trim(str_replace('file', '', str_replace('_', ' ', $arrFileType[$i]))),
                                    'created_date' => $date,
                                    'created_by' => $username,
                                    'updated_date' => $date,
                                    'updated_by' => $username,
                                );
                                $result = $this->db->insert('tbl_history_upload', $data2);
                            }
                        }
                    }
                }

                $statusBefore = $checkValid->status;
                if ($statusBefore != $status) {
                    $pokmas = $this->M_pokmas->selectById($checkValid->id_pokmas);
                    $keteranganLog = 'user <b>' . $username . '</b> Sedang mengembalikan status (Belum Verifikasi) Proposal yang di ajukan oleh Admin Help Desk Bagian atas nama Pokmas <b>' . $pokmas->nama_kelompok . '</b>';
                    if ($status == "Verifikasi") {
                        $keteranganLog = 'user <b>' . $username . '</b> Sedang melakukan proses ' . $status . ' terhadap Proposal yang di ajukan oleh Admin Help Desk Bagian atas nama Pokmas <b>' . $pokmas->nama_kelompok . '</b>';
                    } else if ($status == "Direvisi") {
                        $keteranganLog = 'user <b>' . $username . '</b> Sedang melakukan proses Pengembalian terhadap Proposal yang di ajukan oleh Admin Help Desk Bagian atas nama Pokmas <b>' . $pokmas->nama_kelompok . '</b> ada bebrapa syarat yang belum terpenuhi';
                    }

                    $data3 = array(
                        'id_proposal' => $id,
                        'status' => $status,
                        'keterangan' => $keteranganLog,
                        'created_date' => $date,
                        'created_by' => $username,
                        'updated_date' => $date,
                        'updated_by' => $username,
                    );

                    $result = $this->db->insert('tbl_log', $data3);
                }
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => $errMessage);
        }

        echo json_encode($out);
    }

    public function checklist($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Checklist " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_proposal->selectById($id);
            if ($resultData != null) {
                $checklist = $this->M_proposal->selectByIdCheckList($id);
                $data['res'] = $this->M_proposal->selectByIdCheckList($id);
                $arrInput = json_decode($checklist->checklist, TRUE);
                $data['resultData'] = $resultData;
                $data['arrInput'] = $arrInput;
                parent::loadkonten('' . self::__folder . 'checklist', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function prosesChecklist($id)
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";
        $post = $this->input->post();
        $CatatanLain_lain = $this->input->post('catatan');

        $arrInput = array();
        for ($i = 1; $i <= 21; $i++) {
            $check = $this->input->post('check' . $i);
            $catatan = $this->input->post('catatan' . $i);
            $arrInput['check'][$i] = isset($check) ? $check : 'false';
            $arrInput['catatan'][$i] = isset($catatan) ? $catatan : '';
        }

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_proposal->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            $data = array(
                'checklist' => json_encode($arrInput),
                'catatan' => $CatatanLain_lain,
                'updated_date' => $date,
                'updated_by' => $username,
            );
            $result = $this->db->update('tbl_checklis', $data, array('id_proposal' => $id));
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($result > 0) {
            $this->db->trans_commit();
            $out = array('status' => true, 'pesan' => ' Data berhasil di update');
        } else {
            $this->db->trans_rollback();
            $out = array('status' => false, 'pesan' => 'Maaf data gagal di update !');
        }

        echo json_encode($out);
    }

    public function cetak_ba($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Cetak Berita Acara " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_proposal->selectById($id);
            if ($resultData != null) {
                $data['resultData'] = $resultData;
                $this->load->view('' . self::__folder . 'cetak_ba', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function cetak_checklist($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Cetak Checklist " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_proposal->selectById($id);
            if ($resultData != null) {
                $checklist = $this->M_proposal->selectByIdCheckList($id);
                $arrInput = json_decode($checklist->checklist, TRUE);
                $data['res'] = $this->M_proposal->selectByIdCheckList($id);
                $data['resultData'] = $resultData;
                $data['arrInput'] = $arrInput;
                $data['gambar_survei'] = $this->M_proposal->selectGbrSurvey($id);
                $this->load->view('' . self::__folder . 'cetak_checklist', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function cetak_sketsa_survey($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Cetak Sketsa Survey " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_proposal->selectById($id);
            if ($resultData != null) {
                $data['resultData'] = $resultData;
                $this->load->view('' . self::__folder . 'cetak_sketsa_survey', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function download()
    {
        if ($this->input->post('images')) {
            $images = $this->input->post('images');
            foreach ($images as $image) {
                $this->zip->read_file($image);
            }
            $this->zip->download('' . time() . '.zip');
        }
    }
}
