<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_proposal_penetapan extends AUTH_Controller
{
    const __tableName = 'tbl_proposal';
    const __tableId = 'id_proposal';
    const __tableIdname = 'name';
    const __folder = 'v_proposal_penetapan/';
    const __kode_menu = 'master-proposal-penetapan';
    const __title = 'Proposal Penetapan ';
    const __model = 'M_proposal_penetapan';
    const __getLimitDpaYear = 2;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_utilities');
        $this->load->model('M_sidebar');
    }

    public function index()
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('view', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $accessAdd = $this->M_sidebar->access('view', self::__kode_menu);
            $data['accessAdd'] = $accessAdd->menuview;
            $data['status'] = $this->M_proposal_penetapan->selectStatus();
            parent::loadkonten('' . self::__folder . 'home', $data);
        }
    }

    public function ajax_list()
    {
        $status = $this->input->post('status');
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');
        $allDate = $this->input->post('all_date');

        $filter = array(
            'status' => $status,
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
            'all_date' => $allDate,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $list = $this->M_proposal_penetapan->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $status = '<small class="label pull-center bg-blue">Tidak Direkomendasi</small>';
            if ($brand->status == 'Rekom Bidang') {
                $status = '<small class="label pull-center bg-green">Rekom Bidang</small>';
            } else if ($brand->status == 'Penetapan') {
                $status = '<small class="label pull-center bg-green">Penetapan Proposal</small>';
            }

            if ($brand->created_date == $brand->updated_date) {
                $proses = "Belum ada yang memproses";
            } else {
                $proses = $brand->updated_by;
            }

            $StatusSurvey = "";
            if ($brand->survei == "Survey") {
                $StatusSurvey = "<li><b>Sedang dilakukan Survey Lapangan<b></li>";
            } else if ($brand->survei == "Selesai Survey") {
                $StatusSurvey = "<li><b>Survei selesai di lakukan<b></li>";
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->kode_proposal . '</b><br> Jenis Hibah : <b>' . $brand->jenis_hibah . '</b>';
            $row[] = $brand->nama_kelompok . '<br><br>Grup : <b>' . $brand->nama_grup . '</b>';
            $row[] = $brand->kabupaten;
            $row[] = $status . '<br> Proposal di Proses oleh : <b>' . $proses . '</b>';
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->created_date, 'by' => $brand->created_by));
            $row[] = parent::loadCreatedUpdatedContent(array('datetime' => $brand->updated_date, 'by' => $brand->updated_by));

            //add html for action
            $action = "-";
            if ($brand->status == 'Rekom Bidang') {
                $action = " <div class='btn-group'>";
                $action .= "    <a class='dropdown-toggle' data-toggle='dropdown' href='#' aria-expanded='false'><button class='btn-edit'>Action<span class='caret'></span></button></a>";
                $action .= "    <ul class='dropdown-menu align-left pull-right'>";
                if ($accessEdit->menuview > 0) {
                    $action .= "    <li><a href='" . base_url('edit-proposal-penetapan') . "/" . $brand->id_proposal . "'><i class='fa fa-edit'></i> Ubah</a></li>";
                }
                $action .= "    </ul>";
                $action .= "</div>";
            }
            $row[] = $action;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = "Ubah " . self::__title;
        if ($access->menuview == 0) {
            parent::loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $resultData = $this->M_proposal_penetapan->selectById($id);
            if ($resultData != null) {
                $data['resultData'] = $resultData;
                $data['status'] = $this->M_proposal_penetapan->selectStatus();
                $data['menuName'] = self::__kode_menu;
                $data['idCustomer'] = $id;
                parent::loadkonten('' . self::__folder . 'update', $data);
            } else {
                echo "<script>alert('" . self::__title . " tidak tersedia.'); window.location = '" . base_url(self::__kode_menu) . "';</script>";
            }
        }
    }

    public function download()
    {
        if ($this->input->post('images')) {
            $images = $this->input->post('images');
            foreach ($images as $image) {
                $this->zip->read_file($image);
            }
            $this->zip->download('' . time() . '.zip');
        }
    }

    public function prosesUpdate($id)
    {
        $username = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $errCode = 0;
        $errMessage = "";

        $status = $this->input->post('status');
        $statusDpa = $this->input->post('status_dpa');

        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You don't have access.";
            }
        }
        if ($errCode == 0) {
            $checkValid = $this->M_proposal_penetapan->selectById($id);
            if ($checkValid == null) {
                $errCode++;
                $errMessage = self::__title . " tidak valid.";
            }
        }
        if ($errCode == 0) {
            if (strlen($status) == 0) {
                $errCode++;
                $errMessage = "Status wajib di isi.";
            }
        }
        if ($errCode == 0) {
            $getKelompokDpa = $this->M_utilities->getKelompokDpa($checkValid->id_pokmas);
            if ($getKelompokDpa != null) {
                if (date('Y', strtotime($date2)) < date('Y', strtotime($getKelompokDpa->date))) {
                    $errCode++;
                    $errMessage = "Gagal approve proposal karena data pokmas telah mengajukan proposal pada tahun " . date('Y', strtotime($getKelompokDpa->created_date)) . " .";
                }
            }
        }
        if ($errCode == 0) {
            if (strlen($status) == 0) {
                $errCode++;
                $errMessage = "Status wajib di isi.";
            }
        }
        if ($errCode == 0) {
            try {
                $data = array(
                    'status' => $status,
                    'helper' => 1,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                if (strlen($statusDpa) > 0) {
                    $data = array_merge($data, array(
                        'status_dpa' => $statusDpa,
                    ));

                    $limitDate = date('Y-m-d', strtotime($date . "+" . self::__getLimitDpaYear . " years"));
                    $dataHistoryPengajuan = array(
                        'id_pokmas' => $checkValid->id_pokmas,
                        'date' => $limitDate,
                        'description' => "Pengajuan proposal pada tahun " . date('Y', strtotime($date)),
                        'created_by' => $username,
                        'created_date' => $date,
                        'updated_by' => $username,
                        'updated_date' => $date,
                    );
                    $this->db->insert('tbl_history_pengajuan', $dataHistoryPengajuan);
                }
                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $id));

                $data2 = array(
                    'id_proposal' => $id,
                    'status' => $status,
                    'keterangan' => 'user <b>' . $username . '</b> Sedang melakukan proses ' . $status . ' proposal ke Dinas Jawa Timur',
                    'created_date' => $date,
                    'created_by' => $username,
                    'updated_date' => $date,
                    'updated_by' => $username,
                );
                $result = $this->db->insert('tbl_log', $data2);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }

        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = true;
            $out['pesan'] = 'Pengajuan berhasil di update.';
        } else {
            $this->db->trans_rollback();
            $out['status'] = false;
            $out['pesan'] = $errMessage;
        }

        echo json_encode($out);
    }
}
