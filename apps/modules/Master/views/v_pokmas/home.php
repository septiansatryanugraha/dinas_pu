<?php $this->load->view('_heading/_headerContent') ?>

<section class="content">
    <div class="box">
        <div class="box-header">
            <div class="form-group ">
                <div class="col-md-4" style="margin-left: 0px; margin-bottom: 10px;">
                    <?php if ($accessAdd > 0) { ?>
                        <a class="klik ajaxify" href="<?php echo site_url('add-pokmas'); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-plus-sign"></i> Add Data</button></a>
                        <?php if ($grupId == 1) { ?>
                            <!--<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Import Excel</button>-->
                        <?php } ?>
                    <?php } ?>
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="search-form" style="">
                <div class="form-group ">
                    <label class="control-label">Filter</label>
                </div>
                <div class="form-group ">
                    <label class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-3">
                        <select name="status" class="form-control selek-status" id="status" aria-describedby="sizing-addon2">
                            <option value="all">Semua Status</option>
                            <option value="Aktif">Aktif</option>
                            <option value="Non aktif">Nonaktif</option>
                        </select>
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div class="form-group ">
                    <label class="col-sm-2 control-label">Tanggal Last Update</label>
                    <div class="col-sm-6">
                        <input type="text" name="tanggal_awal" id="tanggal_awal" class="form-control datepicker" style="width: 30%; display: inline-block;" value="<?php echo date('01-m-Y'); ?>" readonly="">
                        <span>s/d</span>
                        <input type="text" name="tanggal_akhir" id="tanggal_akhir" class="form-control datepicker" style="width: 30%; display: inline-block;" value="<?php echo date('t-m-Y'); ?>" readonly="">
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div class="form-group">  
                    <label class="col-sm-2 control-label"></label>       
                    <div class="col-sm-6">
                        <label class="checkbox-inline">
                            <input type="checkbox" id="all_date" name="all_date" checked/>Semua Tanggal
                        </label>
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div class="box-footer">
                    <button name="button_filter" id="button_filter" style="margin-top: 13px" type="button" class="btn btn-success btn-flat"><i class="fa fa-refresh"></i> Tampilkan</button>
                </div>
                <div class="box-footer"><br></div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <div class="overflow-scroll">
                    <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Kelompok</th>
                                <th>Ketua</th>
                                <th>Kota / Kabupaten</th>
                                <th>Kecamatan</th>
                                <th>Kelurahan / Desa</th>
                                <th>Status</th>
                                <th style="width: 100px;">Dibuat</th>
                                <th style="width: 100px;">Diubah</th>
                                <th style="width: 50px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<?php if ($grupId == 1) { ?>
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Tutup"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Import Data</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="form-import" method="post" enctype="multipart/form-data" role="form">
                        <div class="modal-body">
                            <div class="input-group form-group">
                                <span class="input-group-addon" id="sizing-addon2">
                                    <i class="glyphicon glyphicon-file"></i>
                                </span>
                                <input type="file" class="form-control" name="excel" id="excel" aria-describedby="sizing-addon2">
                            </div>    
                        </div>
                        <div class="modal-footer">
                            <div id='btn_loading'>
                                <button type='button' class='btn btn-success' disabled><i class='fa fa-spinner fa-spin'></i> &nbsp;Tunggu..</button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"> Batal</button> 
                            </div>
                            <div id="buka">
                                <button class="btn btn-success" type="submit" name="button"><i class="fa fa-upload" aria-hidden="true"></i>&nbsp; Upload</button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"> Batal</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<script type="text/javascript">
    //untuk load data table ajax  
    var save_method; //for save method string
    var table;

    $(document).ready(function () {
        reloadTable();
    });

    function reloadTable() {
        var status = $("#status").val();
        var tanggal_awal = $("#tanggal_awal").val();
        var tanggal_akhir = $("#tanggal_akhir").val();
        var all_date = 0;
        if (document.getElementById("all_date").checked == true) {
            all_date = 1;
        }

        table = $('#table').DataTable({
            "processing": true, //Feature control the processing indicator.
            "aLengthMenu": [[75, 100, 150, -1], [75, 100, 150, "All"]],
            "bSort": false,
            "pageLength": 75,
            "order": [], //Initial no order.
            oLanguage: {
                "sProcessing": "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "Data tidak ada di server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('ajax-pokmas') ?>",
                "type": "POST",
                data: {status: status, tanggal_awal: tanggal_awal, tanggal_akhir: tanggal_akhir, all_date: all_date},
            },
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
            ],
            "initComplete": function (settings, json) {
                $('.row').css('margin-right', '0px');
                $('.row').css('margin-left', '0px');
            },
        });
    }

    $('#search-button').click(function () {
        $('.search-form').toggle();
        return false;
    });

    $("#button_filter").click(function () {
        table.destroy();
        reloadTable();
    });

    // untuk select2 ajak pilih department
    $(function () {
        $(".selek-status").select2({
            placeholder: " -- Pilih Status -- "
        });
    });


    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    $(document).on("click", ".hapus-pokmas", function () {
        var id_pokmas = $(this).attr("data-id");

        swal({
            title: "Hapus Data?",
            text: "Yakin anda akan menghapus data ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Hapus",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: "POST",
                url: "<?php echo base_url('delete-pokmas'); ?>",
                data: "id_pokmas=" + id_pokmas,
                success: function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        $("tr[data-id='" + id_pokmas + "']").fadeOut("fast", function () {
                            $(this).remove();
                        });
                        hapus_berhasil();
                        setTimeout("window.location='<?php echo base_url("master-pokmas"); ?>'", 450);
                    } else {
                        swal("Warning", result.pesan, "warning");
                    }
                    // hapus_berhasil();
                    reload_table();
                }
            });
        });
    });

    $('#form-import').submit(function (e) {
        var error = 0;
        var message = "";

        var data = $(this).serialize();

        var excel = $("#excel").val();
        var excel = excel.trim();

        if (error == 0) {
            if (excel.length == 0) {
                error++;
                message = "File wajib di isi.";
            }
        }

        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url('import-pokmas'); ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-import").reset();
                    $("#buka").show();
                    $("#btn_loading").hide();
                    $("#myModal").hide();
                    $("#myModal").modal('hide');
                    swal("Success", result.pesan, "success");
                    reload_table();

                } else if (result.status == 'awas') {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    $("#myModal").hide();
                    $("#myModal").modal('hide');
                    swal("Warning", result.pesan, "warning");

                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    $("#myModal").hide();
                    $("#myModal").modal('hide');
                    swal("Warning", result.pesan, "warning");

                }
            })
            e.preventDefault();
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });

    // untuk datepicker
    $(function () {
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
    });
</script>