<?php $this->load->view('_heading/_headerContent') ?>

<style>
    #loadingImg {
        margin-left: 17%;
    }
    #loadingImg2 {
        margin-left: 17%;
    }
    .font-data {
        font-family: Futura,Trebuchet MS,Arial,sans-serif; 
        color:red;
        font-size: 13px;
    }
    #slider {
        margin-left: 6%;
    }
    #qrcode {
        margin-left: 30%;
        margin-bottom: 20px;
    }
    #btn_loading {
        display: none;
        width: 140%;
    }
</style>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom" id="newContain">
                    <form class="form-horizontal" id="form-ubah" method="POST">
                        <input type="hidden"  name="kode_pokmas" value="<?php echo $brand->kode_pokmas; ?>">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Kode Pokmas</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->kode_pokmas; ?></p>
                                </div>
                            </div>
                            <?php if (isset($selectGrup)) { ?>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Grup</label>
                                    <div class="col-sm-3">
                                        <select name="id_grup" class="form-control select-grup" id="id_grup">
                                            <option></option>
                                            <?php foreach ($selectGrup as $data) { ?>
                                                <option value="<?php echo $data->grup_id; ?>" <?php echo ($data->grup_id == $resultData->id_grup) ? "selected" : ""; ?>>
                                                    <?php echo $data->nama_grup; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Nama Pokmas</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="nama_kelompok" placeholder="Nama Pokmas" id="nama_kelompok" aria-describedby="sizing-addon2" value="<?php echo $resultData->nama_kelompok; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Ketua Pokmas</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="nama_ketua" placeholder="Ketua Pokmas" id="nama_ketua" aria-describedby="sizing-addon2" value="<?php echo $resultData->nama_ketua; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">No KTP</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="no_ktp" id="no_ktp" placeholder="Nomor KTP" maxlength="16" value="<?php echo $resultData->no_ktp; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Kota / Kabupaten</label>
                                <div class="col-sm-3">
                                    <select name="id_kabupaten" class="form-control select-kabupaten" id="id_kabupaten">
                                        <option></option>
                                        <?php foreach ($kabupaten as $data) { ?>
                                            <option value="<?php echo $data->id; ?>" <?php echo ($data->id == $id_kabupaten) ? "selected" : ""; ?>>
                                                <?php echo $data->kabupaten; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div id="loadingImg">
                                <img src="<?php echo base_url() . 'assets/' ?>tambahan/gambar/loading-bubble.gif">
                                <p class="font-loading">Proccessing Data</p>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Kecamatan</label>
                                <div class="col-sm-3">
                                    <select name="id_kecamatan" class="form-control select-kecamatan" id="id_kecamatan">
                                        <option></option>
                                        <?php foreach ($kecamatan as $data) { ?>
                                            <option value="<?php echo $data->id; ?>" <?php echo ($data->id == $id_kecamatan) ? "selected" : ""; ?>>
                                                <?php echo $data->kecamatan; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div id="loadingImg2">
                                <img src="<?php echo base_url() . 'assets/' ?>tambahan/gambar/loading-bubble.gif">
                                <p class="font-loading">Proccessing Data</p>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Kelurahan / Desa</label>
                                <div class="col-sm-3">
                                    <select name="id_desa" class="form-control select-desa" id="id_desa">
                                        <option></option>
                                        <?php foreach ($desa as $data) { ?>
                                            <option value="<?php echo $data->id; ?>" <?php echo ($data->id == $resultData->id_desa) ? "selected" : ""; ?>>
                                                <?php echo $data->desa; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Alamat</label>
                                <div class="col-sm-4">
                                    <textarea class="form-control" name="alamat" id="alamat"><?php echo $resultData->alamat; ?></textarea>
                                </div>
                            </div> 
                            <!-- <div class="form-group">
                                <label class="col-sm-2 control-label">Koordinat</label>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="latitude" id="latitude" placeholder="latitude" value="<?php echo $resultData->latitude; ?>">
                                        <font color="red"><a href="https://www.latlong.net/" target="blank">cek latitude & longitude</a></font>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="longitude" id="longitude" placeholder="longitude" value="<?php echo $resultData->longitude; ?>">
                                    </div>
                                </div>    
                            </div> -->
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Catatan</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" name="keterangan" id="keterangan"><?php echo $resultData->keterangan; ?></textarea>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-3">
                                    <select name="status" id="status" class="form-control selek-status" aria-describedby="sizing-addon2">
                                        <?php foreach ($datastatus as $status) { ?>
                                            <option value="<?php echo $status->nama; ?>" <?php echo ($status->nama == $resultData->status) ? "selected" : ""; ?>>
                                                <?php echo $status->nama; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div id="buka"> 
                                <button name="simpan" id="simpan" type="button" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                                <a class="klik ajaxify" href="<?php echo site_url('master-pokmas'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                            </div>

                            <div id="btn_loading">
                                <button name="simpan" id="simpan" type="button" class="btn btn-primary btn-flat animated-gradient">Tunggu..</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>  


<script type="text/javascript">
    //Proses Controller logic ajax
    $("#simpan").click(function () {
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            var data = $("#newContain>form").serialize();
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url("update-pokmas") . '/' . $resultData->id_pokmas; ?>',
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-ubah").reset();
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?php echo base_url("master-pokmas"); ?>'", 450);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    $(function () {
        $(".select-kabupaten").select2({
            placeholder: " -- Pilih Kota / Kabupaten -- "
        });
        $(".select-kecamatan").select2({
            placeholder: " -- Pilih Kecamatan -- "
        });
        $(".select-desa").select2({
            placeholder: " -- Pilih Kelurahan / Desa -- "
        });
        $(".select-grup").select2({
            placeholder: " -- Pilih Grup -- "
        });
    });

    $(function () {
        $("#loadingImg").hide();
        $("#loadingImg2").hide();

        $.ajaxSetup({
            type: "POST",
            url: "<?php echo base_url('load-data-foreign') ?>",
            cache: false,
        });

        $("#id_kabupaten").change(function () {
            var value = $(this).val();
            console.log(value);
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg").fadeIn();
                    },
                    data: {modul: 'kecamatan', id_kabupaten: value},
                    success: function (respond) {
                        $("#id_kecamatan").html(respond);
                        $("#loadingImg").fadeOut();
                        console.log(respond);
                    }
                })
            }
        });

        $("#id_kecamatan").change(function () {
            var value = $(this).val();
            console.log(value);
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg").fadeIn();
                    },
                    data: {modul: 'desa', id_kecamatan: value},
                    success: function (respond) {
                        $("#id_desa").html(respond);
                        $("#loadingImg").fadeOut();
                        console.log(respond);
                    }
                })
            }
        });
    });
</script>






