<html>
    <style>
        body {
            /*background: rgb(204,204,204);*/ 
            font-family: "Arial";
        }
        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            /* box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);*/
        }
        page[size="A4"] {  
            width: 21cm;
            height: 29.7cm; 
        }
        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
            }
        }
        /*------ konten ketiga------*/
        #kontenketiga {
            margin-top: 20px;
            margin-left: -30%;
        }
        #kontenketiga li {
            list-style-type: none;
            float: left;
            width: 40%;
            text-align: center;
            line-height: 1.2em;
            margin-left: 10%;
        }
        #kontenketiga:after {
            display: block;
            content: "";
            clear: both;
        }
        td {
            padding-left: 10px;
        }
        .thumbnail {
            margin: 5px;
            border : 3px grey solid;
        }
        .jarak-gambar {
            padding-left: 10px;
        }
        .kop {
            margin-left: 50px;
            margin-top: 10px;
        }	
        .qrcode {
            margin-left: 220px;
            margin-top: 30px;
        }
        #box1 {
            width:656px;
            margin-left:70px;
        }
        .margin-judul-1 {
            padding-top:10px;
            font-size: 16px;
            margin-left:35%;
        }
        .margin-judul-2 {
            margin-top:10px;
            font-size: 16px;
            margin-left:42%;
        }
        .margin-judul-4 {
            margin-top:30px;
            font-size: 14px;
            margin-left:5%;
        }
        .margin-judul-3 {
            margin-top:10px;
            font-size: 16px
        }
        .frame_potong {
            min-height: 150px;
            max-width: 580px;
            border: 2px solid rgba(0, 0, 0, 0.3);
            background: #f2f2f2;
            padding: 20px 20px;
        }
        .container {
            max-width: 88%;
            margin: 0 auto;
            padding-left: 0%;
            padding-top: 1%;
        }
        .no_sertifikat {
            padding-top:20px;
            font-size: 12px
        }
        .text1 {
            margin-top:25px;
            font-size: 14px
        }
        .text2 {
            margin-top:-30px;
            margin-left: 470px;
            font-size: 14px;
            margin-bottom:40px;
        }
        .text3 {
            margin-top:-17px;
            margin-left: 600px;
            font-size: 14px
        }
        .text4 {
            margin-top:10px;
            font-size: 14px;
            line-height:1.5em;
        }
        .text5 {
            font-size: 14px;
            margin-top: -10px;
        }
        .text6 {
            font-size: 15px;
            margin-top: -20px;
            margin-left: 210px;
            position: absolute;
        }
        .text6-tgl {
            font-size: 14px;
            margin-top: 7px;
            margin-left: 50%;
            position: absolute;
        }
        .kotak {
            width: 4.3em;
            height: 6.3em;
            margin-top: 60px;
            margin-left:250px;
            position: absolute;
            z-index: 7;
            border: solid 1px black;
        }
        .kotak p {
            padding-top: 30px;
            padding-left: 15px;
        }
        .gap1 {
            margin-top: 30px;
        }
        .spasi {
            margin-top: 40px;
        }
        .margin-nama {
            margin-top:27px;
            padding-left:325px;
            text-transform: uppercase;
            font-size:18px;
        }
        .margin-tgl {
            margin-top:10px;
            padding-left:325px;
            text-transform: uppercase;
            font-size:18px;
        }
        .margin-kanan-judul-bawah {
            margin-top:0px;
            padding-left:0px;
            font-size:14px; 
        }
        .margin-kanan-judul-bawah2 {
            padding-left:0px;
            font-size:14px; 
        }
        .margin-kanan-judul-bawah3 {
            margin-top:10px;
            margin-left:-180px;
            font-size:14px; 
        }
        .margin-kanan-judul-bawah4 {
            padding-left:0px;
            font-size:14px; 
        }
        .margin-kiri-judul-bawah {
            margin-top:50px;
            padding-left:0px;
            font-size:14px; 
            position: absolute;
        }
        .margin-kiri-judul-bawah2 {
            margin-top:120px;
            padding-left:0px;
            font-size:14px; 
        }
        .margin-kiri-judul-bawah3 {
            margin-top:10px;
            padding-left:0px;
            font-size:14px; 
            position: absolute;
        }
        .margin-kiri-judul-bawah4 {
            margin-top:70px;
            padding-left:0px;
            font-size:14px; 
            position: absolute;
        }
        .margin-tengah-judul-bawah {
            margin-top:80px;
            padding-left:0px;
            font-size:14px; 
            position: absolute;
        }
        .margin-tengah-judul-bawah2 {
            margin-top:115px;
            padding-left:0px;
            font-size:14px; 
            position: absolute;
        }
        .margin-tengah-judul-bawah3 {
            margin-top:100px;
            padding-left:0px;
            font-size:14px; 
        }
        .margin-tengah-judul-bawah4 {
            margin-top:100px;
            padding-left:0px;
            font-size:14px; 
            position: absolute;
        }
        .margin-judul-kapol {
            margin-top:30px;
            padding-left:420px;
            font-size:14px; 
        }
        .margin-judul-kapol2 {
            margin-top:5px;
            padding-left:440px;
            font-size:14px; 
        }
        .margin-nama-kapol {
            margin-top:70px;
            padding-left:550px;
            font-size:14px; 
            text-decoration: underline;
        }
        .margin-nama-kapol2 {
            margin-top:5px;
            padding-left:510px;
            font-size:14px; 
        }
        .table-ok {
            font-family: "Arial";
            font-size: 14px;
        }
        .table-data {
            font-family: "Arial";
            color: #444;
            border-collapse: collapse;
            width: 100%;
            border: 2px solid #f2f5f7;
            font-size: 13px;
        }
        .table-data, th, td {
            margin-left: 0px;
            border: 1px solid black;
            padding: 5px 5px 5px 20px;
            margin-left: 0px;
            text-align: left;
        }
        button {
            width: 150px;
            height: 30px;
        }
        @media print {
            .samping {display:none;}
        }
        .button {
            width: 100%;
            height: 50px;
            margin-top: -250px;
        }
        .left {
            float: left;
            display: block;
        }
        .right{
            margin-top: 30px;
            float: right;
            margin-right: 40%;
            display: block;
        }
        .box .judul-kiri {
            width:70%;
            height:40px;
            border :1px solid black;
            float:left;
            margin-bottom: 2px;
            font-size: 13px;
        }
        .judul-kiri .tulisan {
            margin-top: 10px;
        }
        .judul-kanan .tulisan {
            margin-top: 9px;
        }
        .box .judul-kanan {
            width:25%;
            height:40px;
            margin-left: 10px;
            padding: -30px;
            border :1px solid black;
            float:left;
            margin-bottom: 2px;
            font-size: 12px;
        }
        .box .kiri {
            width:70%;
            height:500px;
            margin-bottom: 2px;
            border :1px solid black;
            float:left;
        }
        .box .kanan {
            width:25%;
            height:150px;
            margin-left: 10px;
            padding: -30px;
            border :1px solid black;
            float:left;
            margin-bottom: 2px;
        }
    </style>
    <page size="A4">
        <div>
            <img class="kop" src="<?php echo site_url(); ?>assets/tambahan/gambar/kop-pu_sda.png" width="680px">
            <br><br>
            <div class="container">
                <table class="table-ok">
                    <tr>
                        <td style="vertical-align: top;">NAMA POKMAS</td>
                        <td style="vertical-align: top;">:</td>
                        <td style="vertical-align: top;"><?php echo $resultData->nama_kelompok; ?></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">Dsn/Ds/Kel/Kec/Kab/Kota</td>
                        <td style="vertical-align: top;">:</td>
                        <td style="vertical-align: top;"><?php echo $resultData->desa; ?> / <?php echo $resultData->kecamatan; ?> / <?php echo $resultData->kabupaten; ?></td>
                    </tr>
                    <?php if ($resultData->jenis_bantuan == 'Barang') { ?>
                        <tr>
                            <td style="vertical-align: top;">BARANG</td>
                            <td style="vertical-align: top;">:</td>
                            <td style="vertical-align: top;"><?php echo nl2br($resultData->barang); ?></td>
                        </tr>
                    <?php } ?>
                    <?php if ($resultData->jenis_bantuan == 'Uang') { ?>
                        <tr>
                            <td style="vertical-align: top;">NILAI ANGGARAN</td>
                            <td style="vertical-align: top;">:</td>
                            <td style="vertical-align: top;">Rp. <?php echo $resultData->jumlah_anggaran; ?></td>
                        </tr>
                    <?php } ?>
                </table>
                <div class="box">
                    <div class="judul-kiri"><center><div class="tulisan">SKETSA GAMBAR</div></center></div>
                    <div class="judul-kanan"><center><div class="tulisan">Ketua <?php echo $resultData->nama_kelompok; ?></div></center></div>
                    <div class="kiri"></div>
                    <div class="kanan"></div>
                    <div class="judul-kanan"><center><div class="tulisan">Kepala Desa/Kelurahan</center></div>
                    <div class="judul-kanan"><center><div class="tulisan"><?php echo $resultData->kecamatan; ?></center></div>
                    <div class="kanan"></div>
                    <div class="gap1"></div>
                </div>
            </div>
        </div>
    </page>
    <div class="button">
        <ul class="right">
            <button class="samping" onClick="window.print();">Cetak Data</button>
            <button class="samping" onclick="window.top.close();">Kembali</button>
        </ul>
    </div>
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
</html>