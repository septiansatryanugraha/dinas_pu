<html>
    <style>
        body {
            /*background: rgb(204,204,204);*/ 
            font-family: "Arial";
        }
        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            /* box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);*/
        }
        page[size="A4"] {  
            width: 21cm;
            height: 29.7cm; 
        }
        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
            }
        }
        /*------ konten ketiga------*/
        #kontenketiga {
            margin-top: 20px;
            margin-left: -10%;
        }
        #kontenketiga li {
            list-style-type: none;
            float: left;
            width: 32%;
            text-align: center;
            line-height: 1.2em;
        }
        #kontenketiga:after {
            display: block;
            content: "";
            clear: both;
        }
        td {
            padding-left: 10px;
        }
        .thumbnail {
            margin: 5px;
            border : 3px grey solid;
        }
        .jarak-gambar {
            padding-left: 10px;
        }
        .kop {
            margin-left: 50px;
            margin-top: 10px;
        }	
        .qrcode {
            margin-left: 220px;
            margin-top: 30px;
        }
        #box1 {
            width:656px;
            margin-left:70px;
        }
        .margin-judul-1 {
            padding-top:10px;
            font-size: 16px;
            margin-left:38%;
        }
        .margin-judul-2 {
            margin-top:10px;
            font-size: 16px;
            margin-left:42%;
        }
        .margin-judul-4 {
            margin-top:30px;
            font-size: 14px;
            margin-left:5%;
        }
        .margin-judul-3 {
            margin-top:10px;
            font-size: 16px
        }
        .frame_potong {
            min-height: 150px;
            max-width: 580px;
            border: 2px solid rgba(0, 0, 0, 0.3);
            background: #f2f2f2;
            padding: 20px 20px;
        }
        .container {
            max-width: 88%;
            margin: 0 auto;
            padding-left: 0%;
            padding-top: 1%;
        }
        .no_sertifikat {
            padding-top:20px;
            font-size: 12px
        }
        .text1 {
            margin-top:25px;
            font-size: 14px
        }
        .text2 {
            margin-top:-30px;
            margin-left: 470px;
            font-size: 14px;
            margin-bottom:40px;
        }
        .text3 {
            margin-top:-17px;
            margin-left: 600px;
            font-size: 14px
        }
        .text4 {
            margin-top:10px;
            font-size: 14px;
            line-height:1.5em;
        }
        .text5 {
            font-size: 14px;
            margin-top: -10px;
        }
        .text6 {
            font-size: 15px;
            margin-top: -20px;
            margin-left: 210px;
            position: absolute;
        }
        .text6-tgl {
            font-size: 14px;
            margin-top: 7px;
            margin-left: 50%;
            position: absolute;
        }
        .kotak {
            width: 4.3em;
            height: 6.3em;
            margin-top: 60px;
            margin-left:250px;
            position: absolute;
            z-index: 7;
            border: solid 1px black;
        }
        .kotak p {
            padding-top: 30px;
            padding-left: 15px;
        }
        .gap1 {
            margin-top: 30px;
        }
        .spasi {
            margin-top: 40px;
        }
        .margin-nama {
            margin-top:27px;
            padding-left:325px;
            text-transform: uppercase;
            font-size:18px;
        }
        .margin-tgl {
            margin-top:10px;
            padding-left:325px;
            text-transform: uppercase;
            font-size:18px;
        }
        .margin-kanan-judul-bawah {
            margin-top:0px;
            padding-left:0px;
            font-size:14px; 
        }
        .margin-kanan-judul-bawah2 {
            padding-left:0px;
            font-size:14px; 
        }
        .margin-kanan-judul-bawah3 {
            margin-top:10px;
            margin-left:-180px;
            font-size:14px; 
        }
        .margin-kanan-judul-bawah4 {
            padding-left:0px;
            font-size:14px; 
        }
        .margin-kiri-judul-bawah {
            margin-top:50px;
            padding-left:0px;
            font-size:14px; 
            position: absolute;
        }
        .margin-kiri-judul-bawah2 {
            margin-top:120px;
            padding-left:0px;
            font-size:14px; 
        }
        .margin-kiri-judul-bawah3 {
            margin-top:10px;
            padding-left:0px;
            font-size:14px; 
            position: absolute;
        }
        .margin-kiri-judul-bawah4 {
            margin-top:70px;
            padding-left:0px;
            font-size:14px; 
            position: absolute;
        }
        .margin-tengah-judul-bawah {
            margin-top:80px;
            padding-left:0px;
            font-size:14px; 
            position: absolute;
        }
        .margin-tengah-judul-bawah2 {
            margin-top:115px;
            padding-left:0px;
            font-size:14px; 
            position: absolute;
        }
        .margin-tengah-judul-bawah3 {
            margin-top:100px;
            padding-left:0px;
            font-size:14px; 
        }
        .margin-tengah-judul-bawah4 {
            margin-top:100px;
            padding-left:0px;
            font-size:14px; 
            position: absolute;
        }
        .margin-judul-kapol {
            margin-top:30px;
            padding-left:420px;
            font-size:14px; 
        }
        .margin-judul-kapol2 {
            margin-top:5px;
            padding-left:440px;
            font-size:14px; 
        }
        .margin-nama-kapol {
            margin-top:70px;
            padding-left:550px;
            font-size:14px; 
            text-decoration: underline;
        }
        .margin-nama-kapol2 {
            margin-top:5px;
            padding-left:510px;
            font-size:14px; 
        }
        .table-ok {
            font-family: "Arial";
            font-size: 14px;
        }
        .table-ok, th, td {
            padding: 5px 0px 0px 50px;
            margin-left: -100px;
            text-align: left;
        }
        button {
            width: 150px;
            height: 30px;
        }
        @media print {
            .samping {display:none;}
        }
        .button {
            width: 100%;
            height: 50px;
        }
        .left {
            float: left;
            display: block;
        }
        .right {
            margin-top: -50px;
            float: right;
            margin-right: 35%;
            display: block;
        }
    </style>
    <page size="A4">
        <div>
            <b>
                <img class="kop" src="<?php echo site_url(); ?>assets/tambahan/gambar/kop-pu_sda.png" width="680px">
                <div class="margin-judul-1"><b>BERITA ACARA VERIFIKASI</b></div>
            </b>
        </div>
        <br><br>
        <div class="container">
            <!-- <div class="text1">Berdasarkan permohonan pemilik kendaraan berrmotor sebagai berikut : </div> -->
            <p align="justify" class="text5" style="line-height: 1.5em;">Pada hari ini........................, Tanggal........................, Bulan ........................, Tahun ........................ bertempat di Desa. <?php echo $resultData->desa; ?> Kec. <?php echo $resultData->kecamatan; ?> Kota / Kab. <?php echo $resultData->kabupaten; ?> telah diadakan verifikasi terhadap permohonan bantuan tanggal :  </p>
            <table class="table-ok">
                <tr>
                    <td>Nomor Berita Acara</td><td>:</td><td><?php echo $resultData->no_ba; ?></td>
                </tr>
                <tr>
                    <td><b>Yang di ajukan oleh</b></td>
                </tr>
                <tr>
                    <td>Nama Kelompok</td><td>:</td><td><?php echo $resultData->nama_kelompok; ?></td>
                </tr>
                <tr>
                    <td>Nama Ketua</td><td>:</td><td><?php echo $resultData->nama_ketua; ?></td>
                </tr>
                <tr>
                    <td><b>Alamat</b></td><td></td>
                </tr>
                <tr>
                    <td>Desa</td><td>:</td><td><?php echo $resultData->desa; ?></td>
                </tr>
                <tr>
                    <td>Kecamatan</td><td>:</td><td><?php echo $resultData->kecamatan; ?></td>
                </tr>
                <tr>
                    <td>Kota / Kabupaten</td><td>:</td><td><?php echo $resultData->kabupaten; ?></td>
                </tr>
                <tr>
                    <td>Telp Pokmas</td><td>:</td><td></td>
                </tr>
                <tr>
                    <td><b>Telp / HP</b></td><td></td>
                </tr>
                <tr>
                    <td>Ketua</td><td>:</td><td></td>
                </tr>
                <tr>
                    <td>Lain-lain</td><td>:</td><td></td>
                </tr>
                <tr>
                    <td>Kegiatan yang di usulkan</td><td>:</td><td><?php echo $resultData->jenis_kegiatan; ?></td>
                </tr>
            </table>
            <div class="gap1"></div>
            <p align="justify" class="text5">Evaluasi dan verifikasi terhadap permohonan tersebut adalah sebagai berikut :</p>
            <p align="justify" class="text5">I. Kelengkapan proposal sesuai dengan check list terlampir, jika ada kekurangan/revisi harus segera dipenuhi.</p>
            <p align="justify" class="text5">II. Kegiatan dilaksanakan di Desa. <?php echo $resultData->desa; ?> Kec. <?php echo $resultData->kecamatan; ?> Kota / Kab. <?php echo $resultData->kabupaten; ?></p>
            <p align="justify" class="text5">III. Dana yang telah ada Rp........................</p>
            <p align="justify" class="text5">IV. Apabila ada kekurangan/revisi maka harus segera dilengkapi dengan waktu maksimal 2 (dua) minggu setelah ditandatanganinya Berita Acara ini.</p>
            <div class="gap1"></div>
            <p align="justify" class="text5">Demikian Berita Acara ini dibuat untuk dipergunakan sebagai bahan pertimbangan.</p>
            <div class="gap1"></div><br>
            <section id="kontenketiga">
                <ul>
                    <li>
                    <center>Ketua Pokmas</center>		
                    <center><?php echo $resultData->nama_kelompok; ?></center>
                    <center><div class="margin-kiri-judul-bawah2"><?php echo $resultData->nama_ketua; ?></div></center>
                    </li>
                    <li>
                    <center>Mengetahui	</center>
                    <center>Kepala Desa/Lurah</center>
                    <center><?php echo $resultData->desa; ?></center>
                    <center><div class="margin-tengah-judul-bawah3">..........................................</div></center>
                    </li>
                    <li>
                    <center>TIM EVALUASI</center>		
                    <div class="margin-kanan-judul-bawah4">DINAS PU SUMBER DAYA AIR</div>
                    <div class="margin-kanan-judul-bawah2">PROVINSI JAWA TIMUR</div><br>
                    <div class="margin-kanan-judul-bawah3">1.</div>
                    <div class="margin-kanan-judul-bawah3">2.</div>
                    <div class="margin-kanan-judul-bawah3">3.</div>
                    <div class="margin-kanan-judul-bawah3">4.</div>
                    </li>
                </ul>
            </section>
        </div>
    </page>
    <div class="button">
        <ul class="right">
            <button class="samping" onClick="window.print();">Cetak Data</button>
            <button class="samping" onclick="window.top.close();">Kembali</button>
        </ul>
    </div>
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
</html>