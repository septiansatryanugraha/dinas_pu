<html>
    <style>
        body {
            /*background: rgb(204,204,204);*/ 
            font-family: "Arial";
        }
        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            /* box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);*/
        }
        page[size="A4"] {  
            width: 21cm;
            height: 29.7cm; 
        }
        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
            }
        }
        td {
            padding-left: 10px;
        }
        .thumbnail{
            margin: 5px;
            border : 3px grey solid;
        }
        .jarak-gambar{
            padding-left: 10px;
        }
        .kop {
            margin-left: 50px;
            margin-top: 10px;
        }	
        .qrcode {
            margin-left: 220px;
            margin-top: 30px;
        }
        #box1{
            width:656px;
            margin-left:70px;
        }
        .margin-judul-1 {
            padding-top:10px;
            font-size: 16px;
            margin-left:38%;
        }
        .margin-judul-2 {
            margin-top:10px;
            font-size: 16px;
            margin-left:42%;
        }
        .margin-judul-4 {
            margin-top:30px;
            font-size: 14px;
            margin-left:5%;
        }
        .margin-judul-3 {
            margin-top:10px;
            font-size: 16px
        }
        .frame_potong {
            min-height: 150px;
            max-width: 580px;
            border: 2px solid rgba(0, 0, 0, 0.3);
            background: #f2f2f2;
            padding: 20px 20px;
        }
        .container {
            max-width: 88%;
            margin: 0 auto;
            padding-left: 0%;
            padding-top: 1%;
        }
        .no_sertifikat {
            padding-top:20px;
            font-size: 12px
        }
        .text1 {
            margin-top:25px;
            font-size: 14px
        }
        .text2 {
            margin-top:-30px;
            margin-left: 470px;
            font-size: 14px;
            margin-bottom:40px;
        }
        .text3 {
            margin-top:-17px;
            margin-left: 600px;
            font-size: 14px
        }
        .text4 {
            margin-top:10px;
            font-size: 14px;
            line-height:1.5em;
        }
        .text5 {
            font-size: 14px;
            margin-top: -10px;
        }
        .text6 {
            font-size: 15px;
            margin-top: -20px;
            margin-left: 210px;
            position: absolute;
        }
        .text6-tgl {
            font-size: 14px;
            margin-top: 7px;
            margin-left: 50%;
            position: absolute;
        }
        .kotak {
            width: 4.3em;
            height: 6.3em;
            margin-top: 60px;
            margin-left:250px;
            position: absolute;
            z-index: 7;
            border: solid 1px black;
        }
        .kotak p {
            padding-top: 30px;
            padding-left: 15px;
        }
        .gap1 {
            margin-top: 30px;
        }
        .spasi {
            margin-top: 40px;
        }
        .margin-nama {
            margin-top:27px;
            padding-left:325px;
            text-transform: uppercase;
            font-size:18px;
        }
        .margin-tgl {
            margin-top:10px;
            padding-left:325px;
            text-transform: uppercase;
            font-size:18px;
        }
        .margin-kanan-judul-bawah {
            margin-top:0px;
            padding-left:0px;
            font-size:14px; 
        }
        .margin-kanan-judul-bawah2 {
            padding-left:0px;
            font-size:14px; 
        }
        .margin-kanan-judul-bawah3 {
            margin-top:10px;
            margin-left:-180px;
            font-size:14px; 
        }
        .margin-kanan-judul-bawah4 {
            padding-left:0px;
            font-size:14px; 
        }
        .margin-kiri-judul-bawah {
            margin-top:50px;
            padding-left:0px;
            font-size:14px; 
            position: absolute;
        }
        .margin-kiri-judul-bawah2 {
            margin-top:120px;
            padding-left:0px;
            font-size:14px; 
        }
        .margin-kiri-judul-bawah3 {
            margin-top:10px;
            padding-left:0px;
            font-size:14px; 
            position: absolute;
        }
        .margin-kiri-judul-bawah4 {
            margin-top:70px;
            padding-left:0px;
            font-size:14px; 
            position: absolute;
        }
        .margin-tengah-judul-bawah {
            margin-top:80px;
            padding-left:0px;
            font-size:14px; 
            position: absolute;
        }
        .margin-tengah-judul-bawah2 {
            margin-top:115px;
            padding-left:0px;
            font-size:14px; 
            position: absolute;
        }
        .margin-tengah-judul-bawah3 {
            margin-top:100px;
            padding-left:0px;
            font-size:14px; 
        }
        .margin-tengah-judul-bawah4 {
            margin-top:100px;
            padding-left:0px;
            font-size:14px; 
            position: absolute;
        }
        .margin-judul-kapol {
            margin-top:30px;
            padding-left:420px;
            font-size:14px; 
        }
        .margin-judul-kapol2 {
            margin-top:5px;
            padding-left:440px;
            font-size:14px; 
        }
        .margin-nama-kapol {
            margin-top:70px;
            padding-left:550px;
            font-size:14px; 
            text-decoration: underline;
        }
        .margin-nama-kapol2 {
            margin-top:5px;
            padding-left:510px;
            font-size:14px; 
        }
        table thead {
            vertical-align: middle !important;
            text-align: center !important;
        }
        table tbody {
            vertical-align: top !important;
        }
        .checklist {
            vertical-align: middle !important;
        }
        table thead tr th {
            vertical-align: middle !important;
            text-align: center !important;
        }
        .table-ok {
            font-family: "Arial";
            font-size: 14px;
        }
        .table-no-border tr td {
            border: none !important;
        }
        .table-data {
            font-family: "Arial";
            color: #444;
            border-collapse: collapse;
            width: 100%;
            border: 2px solid #f2f5f7;
            font-size: 13px;
        }
        .table-data, th, td {
            margin-left: 0px;
            border: 1px solid black;
            padding: 5px 5px 5px 20px;
            margin-left: 0px;
            text-align: left;
        }
        .table-data ol {
            padding: 5px 5px 5px 20px;
        }
        button {
            width: 150px;
            height: 30px;
        }
        @media print {
            .samping {display:none;}
        }
        .button {
            padding-top: 850px;
            width: 100%;
            height: 50px;
            margin-top: -100px;
        }
        .left {
            float: left;
            display: block;
        }
        .right {
            margin-top: 30px;
            float: right;
            margin-right: 40%;
            display: block;
        }
        .text-center {
            text-align: center;
        }
        .text-right {
            text-align: right;
        }
    </style>
    <page size="A4">
        <div>
            <b>
                <img class="kop" src="<?php echo site_url(); ?>assets/tambahan/gambar/kop-pu_sda.png" width="680px">
                <div class="margin-judul-1"><b>CHECK LIST VERIFIKASI</b></div>
            </b>
        </div>
        <br><br>
        <div class="container">
            <table class="table-no-border">
                <tr>
                    <td style="vertical-align: top;">NAMA KEGIATAN</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><?php echo $resultData->perihal ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">NAMA POKMAS</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><?php echo $resultData->nama_kelompok ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Dsn/Ds/Kel/Kec/Kab/Kota</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><?php echo $resultData->desa ?> / <?php echo $resultData->kecamatan ?> / <?php echo $resultData->kabupaten ?></td>
                </tr>
                <?php if ($resultData->jenis_bantuan == 'Barang') { ?>
                    <tr>
                        <td style="vertical-align: top;">BARANG PROPOSAL</td>
                        <td style="vertical-align: top;">:</td>
                        <td style="vertical-align: top;"><?php echo nl2br($resultData->usulan_barang); ?></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">PAGU BARANG</td>
                        <td style="vertical-align: top;">:</td>
                        <td style="vertical-align: top;"><?php echo nl2br($resultData->barang); ?></td>
                    </tr>
                <?php } ?>
                <?php if ($resultData->jenis_bantuan == 'Uang') { ?>
                    <tr>
                        <td style="vertical-align: top;">NILAI PROPOSAL / PAGU DPA</td>
                        <td style="vertical-align: top;">:</td>
                        <td style="vertical-align: top;">Rp. <?php echo $resultData->usulan_nilai_anggaran; ?> / Rp. <?php echo $resultData->nilai_anggaran; ?></td>
                    </tr>
                <?php } ?>
            </table>
            <br><br>
            <table class="table-data" style="width: 100%;">
                <thead>
                    <tr>
                        <th rowspan="2" style="width: 2%;">NO</th>
                        <th rowspan="2" style="width: 45%;">URAIAN VERIFIKASI</th>
                        <th colspan="3">HASIL</th>
                        <th rowspan="2" style="width: 30%;">CATATAN</th>
                    </tr>
                    <tr>
                        <th style="width: 7%;">ADA</th>
                        <th style="width: 7%;">TIDAK</th>
                        <th style="width: 7%;">REVISI</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center"><span>1</span></td>
                        <td><span>Kop Surat dan Stempel</span></td>
                        <td class="checklist"><?php echo ($arrInput['check']['1'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['1'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['1'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['1']) ? $arrInput['catatan']['1'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td class="text-center"><span>2</span></td>
                        <td><span>Surat Permohonan</span></td>
                        <td class="checklist"><?php echo ($arrInput['check']['2'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['2'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['2'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['2']) ? $arrInput['catatan']['2'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td class="text-center"><span>3</span></td>
                        <td><span>Proposal</span></td>
                        <td class="checklist"><?php echo ($arrInput['check']['3'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['3'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['3'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['3']) ? $arrInput['catatan']['3'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td class="text-center" rowspan="5"><span>4</span></td>
                        <td>Pengesahan Kelembagaan POKMAS</td>
                        <td class="checklist"><?php echo ($arrInput['check']['4'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['4'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['4'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['4']) ? $arrInput['catatan']['4'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td><ol class="no-padding" type="a" start="1"><li>Surat Pengesahan penetapan SKPD Kab/Kota/Camat (Maksimal tanggal 20 Agustus 2019)</li></ol></td>
                        <td class="checklist"><?php echo ($arrInput['check']['5'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['5'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['5'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['5']) ? $arrInput['catatan']['5'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td><ol type="a" start="2"><li>Surat Keterangan Domisili (Desa)</li></ol></td>
                        <td class="checklist"><?php echo ($arrInput['check']['6'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['6'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['6'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['6']) ? $arrInput['catatan']['6'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td><ol type="a" start="3"><li>Memepunyai Kepengurusan yang jelas</li></ol></td>
                        <td class="checklist"><?php echo ($arrInput['check']['7'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['7'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['7'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['7']) ? $arrInput['catatan']['7'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td><ol type="a" start="4"><li>Berkedudukan di wilayah administrasi</li></ol></td>
                        <td class="checklist"><?php echo ($arrInput['check']['8'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['8'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['8'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['8']) ? $arrInput['catatan']['8'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td class="text-center" rowspan="5" style="padding-top: 5px;"><span>5</span></td>
                        <td>Surat Pernyataan yang ditandatangani ketua pokmas bermaterai Rp. 6000 dan diketahui pejabat setempat (Kades atau Lurah yang memuat)</td>
                        <td class="checklist"><?php echo ($arrInput['check']['9'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['9'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['9'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['9']) ? $arrInput['catatan']['9'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td><ol type="a" start="1"><li>Tidak menerima bantuan dari APBD Provinsi Jawa Timur 1 (satu) Tahun Anggaran sebelumnya</li></ol></td>
                        <td class="checklist"><?php echo ($arrInput['check']['10'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['10'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['10'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['10']) ? $arrInput['catatan']['10'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td><ol type="a" start="2"><li>Tidak menerima bantuan Hibah dan APBD Provinsi Jawa Timur secara terus menesur kecuali ditentukan lain oleh peraturan perundang-undangan</li></ol></td>
                        <td class="checklist"><?php echo ($arrInput['check']['11'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['11'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['11'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['11']) ? $arrInput['catatan']['11'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td><ol type="a" start="3"><li>Tidak menerima anggaran dari sumber anggaran lain (Anggaran ganda / double account) untuk Paket Pekerjaan tersebut</li></ol></td>
                        <td class="checklist"><?php echo ($arrInput['check']['12'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['12'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['12'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['12']) ? $arrInput['catatan']['12'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td><ol type="a" start="4"><li>Surat Pernyataan Kesiapan Lahan tidak bersengketa diketahui oleh Pejabat setempat (Kades/Lurah)</li></ol></td>
                        <td class="checklist"><?php echo ($arrInput['check']['13'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['13'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['13'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['13']) ? $arrInput['catatan']['13'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td class="text-center" rowspan="5" style="padding-top: 5px;"><span>6</span></td>
                        <td>Fotocopy KTP Struktur Kepengurusan Pokmas</td>
                        <td class="checklist"><?php echo ($arrInput['check']['14'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['14'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['14'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['14']) ? $arrInput['catatan']['14'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td><ol type="a" start="1"><li>Ketua</li></ol></td>
                        <td class="checklist"><?php echo ($arrInput['check']['15'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['15'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['15'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['15']) ? $arrInput['catatan']['15'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td><ol type="a" start="2"><li>Sekretaris</li></ol></td>
                        <td class="checklist"><?php echo ($arrInput['check']['16'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['16'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['16'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['16']) ? $arrInput['catatan']['16'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td><ol type="a" start="3"><li>bendahara</li></ol></td>
                        <td class="checklist"><?php echo ($arrInput['check']['17'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['17'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['17'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['17']) ? $arrInput['catatan']['17'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td><ol type="a" start="4"><li>Anggota</li></ol></td>
                        <td class="checklist"><?php echo ($arrInput['check']['18'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['18'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['18'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['18']) ? $arrInput['catatan']['18'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td class="text-center"><span>7</span></td>
                        <td>Denah Lokasi Pekerjaan</td>
                        <td class="checklist"><?php echo ($arrInput['check']['19'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['19'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['19'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['19']) ? $arrInput['catatan']['19'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td class="text-center"><span>8</span></td>
                        <td>Foto Lokasi Kegiatan ()</td>
                        <td class="checklist"><?php echo ($arrInput['check']['20'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['20'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['20'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['20']) ? $arrInput['catatan']['20'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td class="text-center"><span>9</span></td>
                        <td>Rencana Anggaran Biaya (RAB) yang ditanda tangani ketua Pokmas dan kepala Desa</td>
                        <td class="checklist"><?php echo ($arrInput['check']['21'] == 'ada') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['21'] == 'tidak') ? '&#10004;' : ''; ?></td>
                        <td class="checklist"><?php echo ($arrInput['check']['21'] == 'revisi') ? '&#10004;' : ''; ?></td>
                        <td><?php echo isset($arrInput['catatan']['21']) ? $arrInput['catatan']['21'] : ''; ?></td>
                    </tr>
                </tbody>
            </table>

            <table class="table-no-border">
                <tr>
                    <td width="150px;">Catatan Lain-lain</td>
                    <td>:</td>
                    <td><?php echo $res->catatan; ?></td>
                </tr>
            </table>


            <div class="gap1"></div>
            <table class="table-no-border" style="width: 100%;">
                <tr>
                    <td style="width: 30%;" class="text-center">
                        <table>

                        </table>
                    </td>
                    <td style="width: 25%;" class="text-center">
                        <table>

                        </table>
                    </td>
                    <td style="width: 45%;">
                        <table>
                            <tr>
                                <td>Surabaya, ......................... <?php echo date('Y'); ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table class="table-no-border" style="width: 100%;">
                <tr>
                    <td style="width: 30%; text-align: -webkit-center;">
                        <table>
                            <tr>
                                <td class="text-center">POKMAS <?php echo strtoupper($resultData->nama_kelompok); ?></td>
                            </tr>
                            <tr><td style="padding-bottom: 100px;"></td></tr>
                            <tr>
                                <td class="text-center"><u><?php echo strtoupper($resultData->nama_ketua); ?></u></td>
                            </tr>
                            <tr>
                                <td class="text-center">Ketua</td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 25%;" class="text-center">
                        <table>

                        </table>
                    </td>
                    <td style="width: 45%;" class="text-center">
                        <table>
                            <tr>
                                <td class="text-center">TIM EVALUASI</td>
                            </tr>
                            <tr>
                                <td class="text-center">DINAS PU SUMBER DAYA AIR</td>
                            </tr>
                            <tr>
                                <td class="text-center">PROVINSI JAWA TIMUR</td>
                            </tr>
                            <tr>
                                <td class="text-center">
                                    <ol>
                                        <li>.................</li>
                                        <li>.................</li>
                                        <li>.................</li>
                                        <li>.................</li>
                                    </ol>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </page>
    <div class="button">
        <ul class="right">
            <button class="samping" onClick="window.print();">Cetak Data</button>
            <button class="samping" onclick="window.top.close();">Kembali</button>
        </ul>
    </div>
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
</html>