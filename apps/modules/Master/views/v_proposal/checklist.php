<?php $this->load->view('_heading/_headerContent') ?>

<style>
    table thead {
        vertical-align: middle;
        text-align: center;
    }
    table tbody {
        vertical-align: top;
    }
    table tbody tr td span {
        padding: 5px;
    }
    table thead tr th {
        vertical-align: middle !important;
        text-align: center;
    }
    .table-bordered {
        border: 1px solid black !important;
    }
    .table-bordered tr th {
        border: 1px solid black !important;
    }
    .table-bordered tr td {
        border: 1px solid black !important;
    }
    .table-bordered tr td table tr td {
        border: none !important;
    }
    .text-center {
        text-align: center;
    }
    .text-right {
        text-align: right;
    }
    .font-data {
        font-family: Futura,Trebuchet MS,Arial,sans-serif; 
        color:red;
        font-size: 13px;
    }
    .kiri {
        margin-left: -10px;
    }
    .gap {
        margin-top: 30px;
    }
    #btn_loading {
        display: none;
    }
</style>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="nav-tabs-custom">
                    <div class="box-header with-border">
                        <h3 class="box-title">View Data Checklist</h3>
                    </div>
                    <form class="form-horizontal" id="form-update" method="POST">
                        <div class="box-body">
                            <table class="table table-striped">
                                <tr>
                                    <td style="vertical-align: top;">NAMA KEGIATAN</td>
                                    <td style="vertical-align: top;">:</td>
                                    <td style="vertical-align: top;"><?php echo $resultData->perihal ?></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;">NAMA POKMAS</td>
                                    <td style="vertical-align: top;">:</td>
                                    <td style="vertical-align: top;"><?php echo $resultData->nama_kelompok ?></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;">Dsn/Ds/Kel/Kec/Kab/Kota</td>
                                    <td style="vertical-align: top;">:</td>
                                    <td style="vertical-align: top;"><?php echo $resultData->desa ?> / <?php echo $resultData->kecamatan ?> / <?php echo $resultData->kabupaten ?></td>
                                </tr>
                                <?php if ($resultData->jenis_bantuan == 'Barang') { ?>
                                    <tr>
                                        <td style="vertical-align: top;">BARANG PROPOSAL</td>
                                        <td style="vertical-align: top;">:</td>
                                        <td style="vertical-align: top;"><?php echo nl2br($resultData->usulan_barang); ?></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">PAGU BARANG</td>
                                        <td style="vertical-align: top;">:</td>
                                        <td style="vertical-align: top;"><?php echo nl2br($resultData->barang); ?></td>
                                    </tr>
                                <?php } ?>
                                <?php if ($resultData->jenis_bantuan == 'Uang') { ?>
                                    <tr>
                                        <td style="vertical-align: top;">NILAI PROPOSAL / PAGU DPA</td>
                                        <td style="vertical-align: top;">:</td>
                                        <td style="vertical-align: top;">Rp. <?php echo $resultData->usulan_nilai_anggaran; ?> / Rp. <?php echo $resultData->nilai_anggaran; ?></td>
                                    </tr>
                                <?php } ?>
                            </table>
                            <br>
                            <div class="box-header">
                                <div class="col-md-4">
                                    <a href="<?php echo base_url() ?>print-checklist/<?php echo $resultData->id_proposal; ?>" target="_blank"><span tooltip="Cetak Data"><button type="button" class="btn btn-success"><i class="fa fa-print"></i> Cetak Data</button></a>
                                </div>
                            </div>
                            <table id="table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th rowspan="2" style="width: 4%;">#</th>
                                        <th rowspan="2" style="width: 50%;">URAIAN VERIFIKASI</th>
                                        <th colspan="3">HASIL</th>
                                        <th rowspan="2" style="width: 30%;">CATATAN</th>
                                    </tr>
                                    <tr>
                                        <th style="width: 7%;">ADA</th>
                                        <th style="width: 7%;">TIDAK</th>
                                        <th style="width: 7%;">REVISI</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td class="text-center"><input type="checkbox" id="check_ada_all"/></td>
                                        <td class="text-center"><input type="checkbox" id="check_tidak_all"/></td>
                                        <td class="text-center"><input type="checkbox" id="check_revisi_all"/></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"><span>1</span></td>
                                        <td><span>Kop Surat dan Stempel</span></td>
                                        <td class="text-center"><input type="radio" name="check1" value="ada" <?php echo ($arrInput['check']['1'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check1" value="tidak" <?php echo ($arrInput['check']['1'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check1" value="revisi" <?php echo ($arrInput['check']['1'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan1"><?php echo isset($arrInput['catatan']['1']) ? $arrInput['catatan']['1'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"><span>2</span></td>
                                        <td><span>Surat Permohonan</span></td>
                                        <td class="text-center"><input type="radio" name="check2" value="ada" <?php echo ($arrInput['check']['2'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check2" value="tidak" <?php echo ($arrInput['check']['2'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check2" value="revisi" <?php echo ($arrInput['check']['2'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan2"><?php echo isset($arrInput['catatan']['2']) ? $arrInput['catatan']['2'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"><span>3</span></td>
                                        <td><span>Proposal</span></td>
                                        <td class="text-center"><input type="radio" name="check3" value="ada" <?php echo ($arrInput['check']['3'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check3" value="tidak" <?php echo ($arrInput['check']['3'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check3" value="revisi" <?php echo ($arrInput['check']['3'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan3"><?php echo isset($arrInput['catatan']['3']) ? $arrInput['catatan']['3'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" rowspan="5" style="padding-top: 5px;"><span>4</span></td>
                                        <td><table class="no-border"><tr><td>Pengesahan Kelembagaan POKMAS</td></tr></table></td>
                                        <td class="text-center"><input type="radio" name="check4" value="ada" <?php echo ($arrInput['check']['4'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check4" value="tidak" <?php echo ($arrInput['check']['4'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check4" value="revisi" <?php echo ($arrInput['check']['4'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan4"><?php echo isset($arrInput['catatan']['4']) ? $arrInput['catatan']['4'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td><table><tr><td>a. Surat Pengesahan penetapan SKPD Kab/Kota/Camat (Maksimal tanggal 20 Agustus 2019)</td></tr></table></td>
                                        <td class="text-center"><input type="radio" name="check5" value="ada" <?php echo ($arrInput['check']['5'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check5" value="tidak" <?php echo ($arrInput['check']['5'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check5" value="revisi" <?php echo ($arrInput['check']['5'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan5"><?php echo isset($arrInput['catatan']['5']) ? $arrInput['catatan']['5'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td><table><tr><td>b. Surat Keterangan Domisili (Desa)</td></tr></table></td>
                                        <td class="text-center"><input type="radio" name="check6" value="ada" <?php echo ($arrInput['check']['6'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check6" value="tidak" <?php echo ($arrInput['check']['6'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check6" value="revisi" <?php echo ($arrInput['check']['6'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan6"><?php echo isset($arrInput['catatan']['6']) ? $arrInput['catatan']['6'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td><table><tr><td>c. Memepunyai Kepengurusan yang jelas</td></tr></table></td>
                                        <td class="text-center"><input type="radio" name="check7" value="ada" <?php echo ($arrInput['check']['7'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check7" value="tidak" <?php echo ($arrInput['check']['7'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check7" value="revisi" <?php echo ($arrInput['check']['7'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan7"><?php echo isset($arrInput['catatan']['7']) ? $arrInput['catatan']['7'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td><table><tr><td>d. Berkedudukan di wilayah administrasi</td></tr></table></td>
                                        <td class="text-center"><input type="radio" name="check8" value="ada" <?php echo ($arrInput['check']['8'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check8" value="tidak" <?php echo ($arrInput['check']['8'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check8" value="revisi" <?php echo ($arrInput['check']['8'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan8"><?php echo isset($arrInput['catatan']['8']) ? $arrInput['catatan']['8'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" rowspan="5" style="padding-top: 5px;"><span>5</span></td>
                                        <td><table><tr><td>Surat Pernyataan yang ditandatangani ketua pokmas bermaterai Rp. 6000 dan diketahui pejabat setempat (Kades atau Lurah yang memuat)</td></tr></table></td>
                                        <td class="text-center"><input type="radio" name="check9" value="ada" <?php echo ($arrInput['check']['9'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check9" value="tidak" <?php echo ($arrInput['check']['9'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check9" value="revisi" <?php echo ($arrInput['check']['9'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan9"><?php echo isset($arrInput['catatan']['9']) ? $arrInput['catatan']['9'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td><table><tr><td>a. Tidak menerima bantuan dari APBD Provinsi Jawa Timur 1 (satu) Tahun Anggaran sebelumnya</td></tr></table></td>
                                        <td class="text-center"><input type="radio" name="check10" value="ada" <?php echo ($arrInput['check']['10'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check10" value="tidak" <?php echo ($arrInput['check']['10'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check10" value="revisi" <?php echo ($arrInput['check']['10'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan10"><?php echo isset($arrInput['catatan']['10']) ? $arrInput['catatan']['10'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td><table><tr><td>b. Tidak menerima bantuan Hibah dan APBD Provinsi Jawa Timur secara terus menesur kecuali ditentukan lain oleh peraturan perundang-undangan</td></tr></table></td>
                                        <td class="text-center"><input type="radio" name="check11" value="ada" <?php echo ($arrInput['check']['11'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check11" value="tidak" <?php echo ($arrInput['check']['11'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check11" value="revisi" <?php echo ($arrInput['check']['11'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan11"><?php echo isset($arrInput['catatan']['11']) ? $arrInput['catatan']['11'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td><table><tr><td>c. Tidak menerima anggaran dari sumber anggaran lain (Anggaran ganda / double account) untuk Paket Pekerjaan tersebut</td></tr></table></td>
                                        <td class="text-center"><input type="radio" name="check12" value="ada" <?php echo ($arrInput['check']['12'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check12" value="tidak" <?php echo ($arrInput['check']['12'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check12" value="revisi" <?php echo ($arrInput['check']['12'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan12"><?php echo isset($arrInput['catatan']['12']) ? $arrInput['catatan']['12'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td><table><tr><td>d. Surat Pernyataan Kesiapan Lahan tidak bersengketa diketahui oleh Pejabat setempat (Kades/Lurah)</td></tr></table></td>
                                        <td class="text-center"><input type="radio" name="check13" value="ada" <?php echo ($arrInput['check']['13'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check13" value="tidak" <?php echo ($arrInput['check']['13'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check13" value="revisi" <?php echo ($arrInput['check']['13'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan13"><?php echo isset($arrInput['catatan']['13']) ? $arrInput['catatan']['13'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" rowspan="5" style="padding-top: 5px;"><span>6</span></td>
                                        <td><table><tr><td>Fotocopy KTP Struktur Kepengurusan Pokmas</td></tr></table></td>
                                        <td class="text-center"><input type="radio" name="check14" value="ada" <?php echo ($arrInput['check']['14'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check14" value="tidak" <?php echo ($arrInput['check']['14'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check14" value="revisi" <?php echo ($arrInput['check']['14'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan14"><?php echo isset($arrInput['catatan']['14']) ? $arrInput['catatan']['14'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td><span>a. Ketua</span></td>
                                        <td class="text-center"><input type="radio" name="check15" value="ada" <?php echo ($arrInput['check']['15'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check15" value="tidak" <?php echo ($arrInput['check']['15'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check15" value="revisi" <?php echo ($arrInput['check']['15'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan15"><?php echo isset($arrInput['catatan']['15']) ? $arrInput['catatan']['15'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td><span>b. Sekretaris</span></td>
                                        <td class="text-center"><input type="radio" name="check16" value="ada" <?php echo ($arrInput['check']['16'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check16" value="tidak" <?php echo ($arrInput['check']['16'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check16" value="revisi" <?php echo ($arrInput['check']['16'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan16"><?php echo isset($arrInput['catatan']['16']) ? $arrInput['catatan']['16'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td><span>c. bendahara</span></td>
                                        <td class="text-center"><input type="radio" name="check17" value="ada" <?php echo ($arrInput['check']['17'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check17" value="tidak" <?php echo ($arrInput['check']['17'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check17" value="revisi" <?php echo ($arrInput['check']['17'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan17"><?php echo isset($arrInput['catatan']['17']) ? $arrInput['catatan']['17'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td><span>d. Anggota</span></td>
                                        <td class="text-center"><input type="radio" name="check18" value="ada" <?php echo ($arrInput['check']['18'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check18" value="tidak" <?php echo ($arrInput['check']['18'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check18" value="revisi" <?php echo ($arrInput['check']['18'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan18"><?php echo isset($arrInput['catatan']['18']) ? $arrInput['catatan']['18'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"><span>7</span></td>
                                        <td><span>Denah Lokasi Pekerjaan</span></td>
                                        <td class="text-center"><input type="radio" name="check19" value="ada" <?php echo ($arrInput['check']['19'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check19" value="tidak" <?php echo ($arrInput['check']['19'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check19" value="revisi" <?php echo ($arrInput['check']['19'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan19"><?php echo isset($arrInput['catatan']['19']) ? $arrInput['catatan']['19'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"><span>8</span></td>
                                        <td><span>Foto Lokasi Kegiatan ()</span></td>
                                        <td class="text-center"><input type="radio" id="ada" name="check20" value="ada" <?php echo ($arrInput['check']['20'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" id="tidak" name="check20" value="tidak" <?php echo ($arrInput['check']['20'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" id="revisi" name="check20" value="revisi" <?php echo ($arrInput['check']['20'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan20"><?php echo isset($arrInput['catatan']['20']) ? $arrInput['catatan']['20'] : ''; ?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center"><span>9</span></td>
                                        <td><table><tr><td>Rencana Anggaran Biaya (RAB) yang ditanda tangani ketua Pokmas dan kepala Desa</td></tr></table></td>
                                        <td class="text-center"><input type="radio" name="check21" value="ada" <?php echo ($arrInput['check']['21'] == 'ada') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check21" value="tidak" <?php echo ($arrInput['check']['21'] == 'tidak') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><input type="radio" name="check21" value="revisi" <?php echo ($arrInput['check']['21'] == 'revisi') ? 'checked' : ''; ?>/></td>
                                        <td class="text-center"><textarea name="catatan21"><?php echo isset($arrInput['catatan']['21']) ? $arrInput['catatan']['21'] : ''; ?></textarea></td>
                                    </tr>
                                </tbody>
                            </table>

                            <table class="table table-striped">
                                <tr>
                                    <td width="150px;">Catatan Lain-lain</td>
                                    <td><textarea name="catatan" cols="50"><?php echo $res->catatan; ?></textarea></td>
                                </tr>
                            </table>


                        </div>


                        <div class="box-footer">
                            <div id="buka"> 
                                <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                                <a class="klik ajaxify" href="<?php echo site_url('master-proposal'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                            </div>
                            <div id="btn_loading">
                                <button name="submit" type="submit" class="btn btn-primary btn-flat animated-gradient">&nbsp;Tunggu..</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-header">
            <div class="error-content">
                <h3 class="box-title"><i class="fa fa-warning text-yellow"></i> Sebelum cetak laporan checklist pastikan semua data sudah terisi</h3>
            </div> 
        </div>
    </div>
</section>  

<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-update').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url('update-checklist') . '/' . $resultData->id_proposal; ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?php echo base_url("master-proposal"); ?>'", 450);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    $(function () {
        if (window.localStorage) {
            if (!localStorage.getItem('firstLoad')) {
                localStorage['firstLoad'] = true;
                window.location.reload();
            } else
                localStorage.removeItem('firstLoad');
        }
        $('#check_ada_all').click(function () {
            $('#check_tidak_all').prop("checked", false);
            $('#check_revisi_all').prop("checked", false);
            if ($('#check_ada_all').is(':checked')) {
                $('[value="ada"]').prop("checked", true);
            } else {
                $('[value="ada"]').prop("checked", false);
            }
        });
        $('#check_tidak_all').click(function () {
            $('#check_ada_all').prop("checked", false);
            $('#check_revisi_all').prop("checked", false);
            if ($('#check_tidak_all').is(':checked')) {
                $('[value="tidak"]').prop("checked", true);
            } else {
                $('[value="tidak"]').prop("checked", false);
            }
        });
        $('#check_revisi_all').click(function () {
            $('#check_ada_all').prop("checked", false);
            $('#check_tidak_all').prop("checked", false);
            if ($('#check_revisi_all').is(':checked')) {
                $('[value="revisi"]').prop("checked", true);
            } else {
                $('[value="revisi"]').prop("checked", false);
            }
        });
        $('[type="radio"]').click(function () {
            $('#check_ada_all').prop("checked", false);
            $('#check_tidak_all').prop("checked", false);
            $('#check_revisi_all').prop("checked", false);
        });
    });
</script>