<?php $this->load->view('_heading/_headerContent') ?>

<style>
    .font-data {
        font-family: Futura,Trebuchet MS,Arial,sans-serif; 
        color:red;
        font-size: 13px;
    }
    .kiri {
        margin-left: -10px;
    }
    .gap {
        margin-top: 30px;
    }
    #btn_loading {
        display: none;
    }
</style>

<section class="content">
    <div class="row">
        <form enctype="multipart/form-data" id="form-ubah" method="POST">
            <div class="form-horizontal">
                <div class="col-md-7">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">View Data Pengajuan</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Kode Proposal</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><b><?php echo $resultData->kode_proposal; ?></b></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama Pokmas</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->nama_kelompok; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama Ketua</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->nama_ketua; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Kota / Kabupaten</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->kabupaten; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Kecamatan</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->kecamatan; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Kelurahan / Desa</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->desa; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">No Surat</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->no_surat; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Perihal Proposal</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->perihal; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Berkas File Pengajuan</label>
                                <div class="col-sm-7" style="margin-top: 8px;">
                                    <p class="form-control-static">
                                        <a href="<?php echo base_url() . $resultData->file_upload ?>" target="blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-file-pdf" aria-hidden="true"></i> <?php echo $resultData->nama_file_upload; ?></font></b></a>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Pengajuan</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo date('d-m-Y', strtotime($resultData->tanggal)); ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Catatan</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="catatan"><?php echo $resultData->catatan; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label"></label>
                                <div class="col-sm-9">
                                    <p style='color: red; font-size: 14px;'><b>* wajib di isi jika proposal ada catatan / revisi  </b></p>
                                </div>
                            </div>
                            <?php if ($resultData->jenis_bantuan == 'Barang') { ?>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Usulan Barang Proposal</label>
                                    <div class="col-sm-7">
                                        <p class="form-control-static"><?php echo nl2br($resultData->usulan_barang); ?></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Barang Setelah Evaluasi</label>
                                    <div class="col-sm-7">
                                        <p class="form-control-static"><?php echo nl2br($resultData->barang); ?></p>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($resultData->jenis_bantuan == 'Uang') { ?>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Usulan Anggaran Proposal</label>
                                    <div class="col-sm-7">
                                        <p class="form-control-static">Rp. <?php echo $resultData->usulan_nilai_anggaran; ?></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Nilai Anggaran Setelah Evaluasi</label>
                                    <div class="col-sm-7">
                                        <p class="form-control-static">Rp. <?php echo $resultData->nilai_anggaran; ?></p>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nomor Berita Acara</label>
                                <div class="col-sm-7">
                                    <input type="text" name="no_ba" class="form-control" id="no_ba" value="<?php echo $resultData->no_ba; ?>">
                                </div>
                            </div>
                            <div class="gap"></div>    
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Status</label>
                                <div class="col-sm-4">
                                    <select name="status" id="status" class="form-control select-status" aria-describedby="sizing-addon2">
                                        <?php foreach ($status as $status) { ?>
                                            <option value="<?php echo $status->nama; ?>" <?php echo ($status->nama == $resultData->status) ? "selected" : ""; ?>>
                                                <?php echo $status->nama; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div> 
                            <div class="gap"></div> 
                            <div class="menu-show">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Status Survei</label>
                                    <div class="col-sm-4">
                                        <select name="status_survei" class="form-control select-survei" id="status_survei">
                                            <option></option>
                                            <?php foreach ($survei as $data) { ?>
                                                <option value="<?php echo $data->nama ?>" <?php echo ($status->nama == $resultData->survei) ? "selected" : ""; ?>>
                                                    <?php echo $data->nama; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div> 
                            </div> 
                            <?php if ($resultData->survei == 'Survey') { ?>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Status Survey</label>
                                    <div class="col-sm-7">
                                        <p class="form-control-static"><?php echo $resultData->survei; ?></p>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="box-footer">
                                <div id="buka">
                                    <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                                    <a class="klik ajaxify" href="<?php echo site_url('master-proposal'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                                </div>
                                <div id="btn_loading">
                                    <button name="submit" type="submit" class="btn btn-primary btn-flat animated-gradient">&nbsp;Tunggu..</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cetak Laporan</h3>
                    </div>
                    <div class="box-body box-profile">
                        <div class="row">
                            <div class="form-group col-md-5">
                                <a href="<?php echo base_url('print-ba') . '/' . $resultData->id_proposal; ?>" target="_blank"><button type="button" class="btn btn-success"><i class="fa fa-print"></i> Cetak Berita Acara</button></a>
                            </div>
                            <div class="col-md-3">
                                <a href="<?php echo base_url('print-sketsa-survey') . '/' . $resultData->id_proposal; ?>" target="_blank"><button type="button" class="btn btn-success"><i class="fa fa-print"></i> Cetak Sketsa Survey</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="box">
                    <div class="box-header">
                        <div class="error-content">
                            <h3 class="box-title"><i class="fa fa-warning text-yellow"></i> Sebelum cetak laporan pastikan semua data form sudah terisi</h3>
                        </div> 
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

<script type="text/javascript">
    var status_survei = '<?php echo $resultData->survei; ?>';

    $(document).ready(function () {
        survei();
        $('.menu-show2').fadeOut("slow");
        $('.menu-show').fadeIn("slow");
    });

    //Proses Controller logic ajax
    $('#form-ubah').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url('update-proposal') . '/' . $resultData->id_proposal; ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-ubah").reset();
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?php echo base_url("master-proposal"); ?>'", 450);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    function survei() {
        if (status_survei.length == 0) {
            if ($('#status').val() == 'Verifikasi') {
                $('.menu-show2').fadeOut("slow");
                $('.menu-show').fadeIn("slow");
            } else {
                $('.menu-show').fadeOut("slow");
                $('.menu-show2').fadeOut("slow");
            }
        }
    }

    // untuk select2 ajak pilih department
    $(function () {
        $(".select-status").select2({
            placeholder: " -- Pilih Status -- "
        });
        $(".select-survei").select2({
            placeholder: " -- Pilih Status -- "
        });
        $('.menu-show').hide();
        $('#status').change(function () {
            survei();
        });
    });
</script>