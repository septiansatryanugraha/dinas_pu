<?php $this->load->view('_heading/_headerContent') ?>

<style>
    #btn_loading {
        display: none;
    }
</style>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom" id="newContain">
                    <form class="form-horizontal" id="form-tambah" method="POST">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Kota / Kabupaten</label>
                                <div class="col-sm-3">
                                    <select name="id_kabupaten" class="form-control select-kabupaten" id="id_kabupaten">
                                        <option></option>
                                        <?php foreach ($kabupaten as $data) { ?>
                                            <option value="<?php echo $data->id; ?>">
                                                <?php echo $data->kabupaten; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div id="loadingImg">
                                <img src="<?php echo base_url() . 'assets/' ?>tambahan/gambar/loading-bubble.gif">
                                <p class="font-loading">Proccessing Data</p>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Kecamatan</label>
                                <div class="col-sm-3">
                                    <select name="id_kecamatan" class="form-control select-kecamatan" id="id_kecamatan">
                                        <option></option>
                                    </select>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Kelurahan / Desa</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="desa" placeholder="Nama Kelurahan / Desa" id="desa" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div id="buka"> 
                                <button name="simpan" id="simpan" type="button" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                                <a class="klik ajaxify" href="<?php echo base_url('master-desa'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                            </div>
                            <div id="btn_loading">
                                <button name="simpan" id="simpan" type="button" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    //Proses Controller logic ajax
    $("#simpan").click(function () {
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            var data = $("#newContain>form").serialize();
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url("save-desa"); ?>',
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-tambah").reset();
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?php echo base_url("add-desa"); ?>'", 450);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    $(function () {
        $(".select-kabupaten").select2({
            placeholder: " -- Pilih Kota / Kabupaten -- "
        });
        $(".select-kecamatan").select2({
            placeholder: " -- Pilih Kecamatan -- "
        });
    });

    $(function () {
        $("#loadingImg").hide();
        $("#loadingImg2").hide();

        $.ajaxSetup({
            type: "POST",
            url: "<?php echo base_url('load-data-foreign') ?>",
            cache: false,
        });

        $("#id_kabupaten").change(function () {
            var value = $(this).val();
            console.log(value);
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg").fadeIn();
                    },
                    data: {modul: 'kecamatan', id_kabupaten: value},
                    success: function (respond) {
                        $("#id_kecamatan").html(respond);
                        $("#loadingImg").fadeOut();
                        console.log(respond);
                    }
                })
            }
        });
    });
</script>