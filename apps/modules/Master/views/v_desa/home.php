<?php $this->load->view('_heading/_headerContent') ?>

<section class="content">
    <div class="box">
        <div class="box-header">
            <div class="col-md-4" style="margin-left: 0px; margin-bottom: 10px;">
                <?php if ($accessAdd > 0) { ?>
                    <a class="klik ajaxify" href="<?php echo site_url('add-desa'); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-plus-sign"></i> Add Data</button></a>
                <?php } ?>
            </div>
            <br><br><br>
            <div class="search-form" style="">
                <div class="form-group ">
                    <label class="control-label">Filter</label>
                </div>
                <div class="form-group ">
                    <label class="col-sm-2 control-label">Kota / Kabupaten</label>
                    <div class="col-sm-3">
                        <select name="id_kabupaten" id="id_kabupaten" class="form-control select-kabupaten" aria-describedby="sizing-addon2">
                            <option value="all">Semua Kota / Kabupaten</option>
                            <?php foreach ($kab as $data) { ?>
                                <option value="<?php echo $data->id; ?>"><?php echo $data->kabupaten; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div class="form-group ">
                    <label class="col-sm-2 control-label">Kecamatan</label>
                    <div class="col-sm-3">
                        <select name="id_kecamatan" id="id_kecamatan" class="form-control select-kecamatan" aria-describedby="sizing-addon2">
                            <option value="all">Semua Kecamatan</option>
                        </select>
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div class="box-footer">
                    <button name="button_filter" id="button_filter" style="margin-top: 13px" type="button" class="btn btn-success btn-flat"><i class="fa fa-refresh"></i> Tampilkan</button>
                </div>
                <div class="box-footer"><br></div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <div class="overflow-scroll">
                    <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Kota / Kabupaten</th>
                                <th>Kecamatan</th>
                                <th>Desa / Kelurahan</th>
                                <th style="width: 200px;">Dibuat</th>
                                <th style="width: 200px;">Diubah</th>
                                <th style="width: 45px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    //untuk load data table ajax  
    var save_method; //for save method string
    var table;

    $(document).ready(function () {
        reloadTable();
    });

    function reloadTable() {
        var id_kabupaten = $("#id_kabupaten").val();
        var id_kecamatan = $("#id_kecamatan").val();

        table = $('#table').DataTable({
            "processing": true, //Feature control the processing indicator.
            "aLengthMenu": [[75, 100, 150, -1], [75, 100, 150, "All"]],
            "bSort": false,
            "pageLength": 75,
            "order": [], //Initial no order.
            oLanguage: {
                "sProcessing": "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "Data tidak ada di server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('ajax-desa') ?>",
                "type": "POST",
                data: {id_kabupaten: id_kabupaten, id_kecamatan: id_kecamatan},
            },
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
            ],
            "initComplete": function (settings, json) {
                $('.row').css('margin-right', '0px');
                $('.row').css('margin-left', '0px');
            },
        });
    }

    $('#search-button').click(function () {
        $('.search-form').toggle();
        return false;
    });

    $("#button_filter").click(function () {
        table.destroy();
        reloadTable();
    });

    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    $(document).on("click", ".hapus-desa", function () {
        var id = $(this).attr("data-id");

        swal({
            title: "Hapus Data?",
            text: "Yakin anda akan menghapus data ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Hapus",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: "POST",
                url: "<?php echo base_url('delete-desa'); ?>",
                data: "id=" + id,
                success: function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == true) {
                        $("tr[data-id='" + id + "']").fadeOut("fast", function () {
                            $(this).remove();
                        });
                        hapus_berhasil();
                        setTimeout("window.location='<?php echo base_url("master-desa"); ?>'", 450);
                    } else {
                        swal("Warning", result.pesan, "warning");
                    }
                    // hapus_berhasil();
                    reload_table();
                }
            });
        });
    });

    $(function () {
        $(".select-kabupaten").select2({
            placeholder: " -- Pilih Kota / Kabupaten -- "
        });
        $(".select-kecamatan").select2({
            placeholder: " -- Pilih Kecamatan -- "
        });

        $("#loadingImg").hide();
        $("#loadingImg2").hide();

        $.ajaxSetup({
            type: "POST",
            url: "<?php echo base_url('load-data-foreign') ?>",
            cache: false,
        });

        $("#id_kabupaten").change(function () {
            var value = $(this).val();
            if (value > 0 || value != 'all') {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg").fadeIn();
                    },
                    data: {modul: 'kecamatan', id_kabupaten: value},
                    success: function (respond) {
                        $("#id_kecamatan").html('<option value="all">Semua Kecamatan ' + $("#id_kabupaten option:selected").html() + '</option>' + respond);
                        $("#loadingImg").fadeOut();
                    }
                })
            } else {
                $("#id_kecamatan").html('<option value="all">Semua Kecamatan</option>');
            }
        });
    });
</script>






