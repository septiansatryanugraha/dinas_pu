<?php $this->load->view('_heading/_headerContent') ?>

<style>
    .font-data {
        font-family: Futura,Trebuchet MS,Arial,sans-serif; 
        color:red;
        font-size: 13px;
    }
    .kiri {
        margin-left: -10px;
    }
    .gap {
        margin-top: 30px;
    }
    .select-survei {
        width: 160px;
    }
    #btn_loading {
        display: none;
    }
</style>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="row">
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="nav-tabs-custom">
                    <form class="form-horizontal" id="form-ubah" method="POST">
                        <input type="hidden" name="last_update_by" value="<?php echo $userdata->nama; ?>">
                        <input type="hidden" name="id_user" value="<?php echo $resultData->id_user; ?>">
                        <input type="hidden" class="form-control" name="id_proposal" value="<?php echo $resultData->id_proposal; ?>">
                        <input type="hidden" class="form-control" name="kode_proposal" value="<?php echo $resultData->kode_proposal; ?>">
                        <input type="hidden" class="form-control" name="nama_pokmas" value="<?php echo $resultData->kelompok; ?>">
                        <div class="box-header with-border">
                            <h3 class="box-title">View Data Pengajuan</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Kode Proposal</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><b><?php echo $resultData->kode_proposal; ?></b></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama Pokmas</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->nama_kelompok; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama Ketua</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->nama_ketua; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Kota / Kabupaten</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->kabupaten; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Kecamatan</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->kecamatan; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Kelurahan / Desa</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->desa; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">No Surat</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->no_surat; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Perihal Proposal</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->perihal; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Berkas File Pengajuan</label>
                                <div class="col-sm-7" style="margin-top: 8px;">
                                    <a href="<?php echo base_url() . $resultData->file_upload ?>" target="blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-file-pdf" aria-hidden="true"></i> <?php echo $resultData->nama_file_upload; ?></font></b></a></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Pengajuan</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo date('d-m-Y', strtotime($resultData->tanggal)); ?></p>
                                </div>
                            </div>

                            <?php if ($resultData->jenis_bantuan == 'Uang') { ?>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Usulan Anggaran Proposal</label>
                                    <div class="col-sm-7">
                                        <p class="form-control-static">Rp. <?php echo $resultData->usulan_nilai_anggaran; ?></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Nilai Anggaran Setelah Evaluasi</label>
                                    <div class="col-sm-7">
                                        <p class="form-control-static">Rp. <?php echo $resultData->nilai_anggaran; ?></p>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($resultData->jenis_bantuan == 'Barang') { ?>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Usulan Barang Proposal</label>
                                    <div class="col-sm-7">
                                        <p class="form-control-static"><?php echo nl2br($resultData->usulan_barang); ?></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Barang Setelah Evaluasi</label>
                                    <div class="col-sm-7">
                                        <p class="form-control-static"><?php echo nl2br($resultData->barang); ?></p>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="gap"></div>    
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Status</label>
                                <div class="col-sm-3">
                                    <select name="status" id="status" class="form-control select-status">
                                        <option></option>
                                        <?php foreach ($status as $data) { ?>
                                            <option value="<?php echo $data->nama ?>"<?php echo ($data->nama == $resultData->status) ? "selected" : ""; ?>>
                                                <?php echo $data->nama; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>  
                            <div class="gap"></div>
                            <div class="box-footer">
                                <div id="buka"> 
                                    <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                                    <a class="klik ajaxify" href="<?php echo site_url('master-proposal-rekom'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                                </div>
                                <div id="btn_loading">
                                    <button name="submit" type="submit" class="btn btn-primary btn-flat animated-gradient">&nbsp;Tunggu..</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>  

<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-ubah').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url('update-proposal-rekom') . '/' . $resultData->id_proposal; ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?php echo base_url("master-proposal-rekom"); ?>'", 450);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    // untuk select2 ajak pilih department
    $(function () {
        $(".select-status").select2({
            placeholder: " -- Pilih Status -- "
        });
    });
</script>