<?php $this->load->view('_heading/_headerContent') ?>

<section class="content">
    <div class="box">
        <div class="box-header">
            <div class="col-md-2">
                <a class="klik ajaxify" href="<?php echo site_url('master-proposal-pengajuan'); ?>"><button class="btn btn-success" ><i class="fas fa-arrow-left"></i> Kembali</button></a>
            </div>
            <br><br>
            <div class="box-body">		
                <table id="tableku" class=" table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Kode Proposal</th>
                            <th>Keterangan</th>
                            <th>Tanggal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($histori as $key => $data) { ?>
                            <tr>
                                <td><?php echo $key + 1; ?></td>
                                <td><?php echo $data->kode_proposal ?></td>
                                <td><?php echo $data->keterangan ?></td>
                                <td><?php echo '<b>' . date('d-m-Y', strtotime($data->created_date)) . '</b>' ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box -->
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        $('#tableku').DataTable();
    });
</script>