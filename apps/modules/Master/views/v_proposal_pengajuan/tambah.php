<?php $this->load->view('_heading/_headerContent') ?>

<style>
    #loadingImg {
        margin-top:5%; 
        margin-left:25%; 
        position: fixed;  
        z-index: 9999; 
    }
    #slider {
        margin-left: 1%;
    }
    #btn_loading {
        display: none;
    }
</style>

<section class="content">
    <div class="loading2"></div>
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom" id="newContain">
                    <form enctype="multipart/form-data" id="form-tambah" method="POST">
                        <div class="box-body">
                            <div id="loadingImg">
                                <img src="<?php echo base_url() . 'assets/' ?>tambahan/gambar/loaders.gif">
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Kota / Kabupaten</label>
                                    <select name="id_kabupaten" class="form-control select-kabupaten" id="id_kabupaten">
                                        <option></option>
                                        <?php foreach ($kabupaten as $data) { ?>
                                            <option value="<?php echo $data->id; ?>">
                                                <?php echo $data->kabupaten; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Kecamatan</label>
                                    <select name="id_kecamatan" class="form-control select-kecamatan" id="id_kecamatan">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Kelurahan / Desa</label>
                                    <select name="id_desa" class="form-control select-desa" id="id_desa">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Pokmas</label>
                                    <select name="id_pokmas" class="form-control select-pokmas" id="id_pokmas">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Ketua Pokmas</label>
                                    <input type="text" name="ketua" class="form-control" id="ketua" readonly="readOnly">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">No urut Pokmas di DPA</label>
                                    <input type="text" id="no_dpa" name="no_dpa" class="form-control" >
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">No Surat Proposal</label>
                                    <input type="text" id="no_surat" name="no_surat" class="form-control" >
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="exampleInputEmail1">tanggal Proposal</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input type="text" name="tanggal" id="tanggal" class="form-control datepicker">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">No Agenda Biro</label>
                                    <input type="text" name="no_biro" class="form-control" id="no_biro">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="exampleInputEmail1">Tanggal Disposisi</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input type="text" name="tanggal_disposisi" id="tanggal_disposisi" class="form-control datepicker">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-7">
                                    <label for="exampleInputEmail1">Perihal Proposal</label>
                                    <textarea class="form-control" id="perihal" name="perihal"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-5">
                                    <label for="exampleInputEmail1">Berkas File</label>
                                    <input type="file" name="file_upload" id="file_upload"  onchange="return validBerkas()"/>
                                </div>

                                <div class="form-group col-md-2" style="margin-top: 20px;">
                                    <small class="label pull-center bg-red">format .pdf</small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="exampleInputEmail1">Jenis Bantuan</label>
                                    <select name="jenis_bantuan" class="form-control select-jenis-bantuan" id="jenis_bantuan">
                                        <option></option>
                                        <option value="Uang">Uang</option>
                                        <option value="Barang">Barang</option>
                                    </select>
                                </div>
                            </div>
                            <?php if (isset($jenisHibah)) { ?>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label for="exampleInputEmail1">Jenis Hibah</label>
                                        <select name="jenis_hibah" class="form-control select-hibah" id="jenis_hibah">
                                            <option></option>
                                            <?php foreach ($jenisHibah as $data) { ?>
                                                <option value="<?php echo $data; ?>">
                                                    <?php echo $data; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="row div_barang" style="display: none;">
                                <div class="form-group col-md-7">
                                    <label for="exampleInputEmail1">Usulan Barang</label>
                                    <textarea name="usulan_barang" class="form-control" rows="3" id="usulan_barang"></textarea>
                                </div>
                            </div>
                            <div class="row div_barang" style="display: none;">
                                <div class="form-group col-md-7">
                                    <label for="exampleInputEmail1">Pagu Barang</label>
                                    <textarea name="barang" class="form-control" rows="3" id="barang"></textarea>
                                </div>
                            </div>
                            <div class="row div_uang" style="display: none;">
                                <div class="form-group col-md-7">
                                    <label for="exampleInputEmail1">Nilai Usulan Proposal</label>
                                    <input type="text" name="usulan_nilai_anggaran" class="form-control" id="currency1">
                                </div>
                            </div>
                            <div class="row div_uang" style="display: none;">
                                <div class="form-group col-md-7">
                                    <label for="exampleInputEmail1">Pagu DPA</label>
                                    <input type="text" name="nilai_anggaran" class="form-control" id="currency2" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div id="buka"> 
                                <button name="simpan" id="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                                <a class="klik ajaxify" href="<?php echo base_url('master-proposal-pengajuan'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                            </div>
                            <div id="btn_loading">
                                <button name="simpan" id="simpan" type="submit" class="btn btn-primary btn-flat animated-gradient"><i class="fa fa-save"></i> Tunggu..</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-tambah').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url("save-pengajuan-proposal"); ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-tambah").reset();
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?php echo base_url("add-pengajuan-proposal"); ?>'", 450);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    $(function () {
        $(".select-kabupaten").select2({
            placeholder: " -- Pilih Kota / Kabupaten -- "
        });
        $(".select-kecamatan").select2({
            placeholder: " -- Pilih Kecamatan -- "
        });
        $(".select-desa").select2({
            placeholder: " -- Pilih Kelurahan / Desa -- "
        });
        $(".select-pokmas").select2({
            placeholder: " -- Pilih Kelompok Masyarakat -- "
        });
        $(".select-jenis-bantuan").select2({
            placeholder: " -- Pilih Jenis Bantuan -- "
        });
        $(".select-hibah").select2({
            placeholder: " -- Pilih Jenis Hibah -- "
        });

        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        })

        $("#loadingImg").hide();
        $("#loadingImg2").hide();

        $.ajaxSetup({
            type: "POST",
            url: "<?php echo base_url('load-data-foreign') ?>",
            cache: false,
        });

        $("#id_kabupaten").change(function () {
            var value = $(this).val();
            console.log(value);
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg").fadeIn();
                    },
                    data: {modul: 'kecamatan', id_kabupaten: value},
                    success: function (respond) {
                        $("#id_kecamatan").html(respond);
                        $("#id_desa").html('<option></option>');
                        $("#id_pokmas").html('<option></option>');
                        $("#ketua").val("");
                        $("#loadingImg").fadeOut();
                        console.log(respond);
                    }
                })
            }
        });

        $("#id_kecamatan").change(function () {
            var value = $(this).val();
            console.log(value);
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg").fadeIn();
                    },
                    data: {modul: 'desa', id_kecamatan: value},
                    success: function (respond) {
                        $("#id_desa").html(respond);
                        $("#id_pokmas").html('<option></option>');
                        $("#ketua").val("");
                        $("#loadingImg").fadeOut();
                        console.log(respond);
                    }
                })
            }
        });

        $("#id_desa").change(function () {
            var value = $(this).val();
            console.log(value);
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg").fadeIn();
                    },
                    data: {modul: 'kelompok', id_desa: value},
                    success: function (respond) {
                        $("#id_pokmas").html(respond);
                        $("#ketua").val("");
                        $("#loadingImg").fadeOut();
                        console.log(respond);
                    }
                })
            }
        });

        $("#id_pokmas").change(function () {
            var value = $(this).val();
            console.log(value);
            var nama_ketua = this.options[this.selectedIndex].getAttribute("pokmas_nama_ketua_" + value)
            console.log(nama_ketua);
            $("#ketua").val(nama_ketua);
        });

        $("#jenis_bantuan").change(function () {
            jenis_bantuan();
        });
    });

    function jenis_bantuan() {
        var value = $("#jenis_bantuan").val();
        if (value == 'Uang') {
            $(".div_barang").css('display', 'none');
            $(".div_uang").css('display', 'block');
        }
        if (value == 'Barang') {
            $(".div_uang").css('display', 'none');
            $(".div_barang").css('display', 'block');
        }
    }

    function validBerkas() {
        var fileInput = document.getElementById("file_upload").value;
        if (fileInput != '') {
            var checkfile = fileInput.toLowerCase();
            // validasi ekstensi file
            if (!checkfile.match(/(\.pdf)$/)) {
                // swal("Peringatan", "File harus format .docx", "warning");
                toastr.error('File harus format .pdf', 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                document.getElementById("file_upload").value = '';
                return false;
            }
            var ukuran = document.getElementById("file_upload");
            // validasi ukuran size file
            if (ukuran.files[0].size > 8007200) {
                // swal("Peringatan", "File harus maksimal 1MB", "warning");
                toastr.error('File harus maksimal 8 MB', 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                ukuran.value = '';
                return false;
            }
            return true;
        }
    }

    /* Str Replacement untuk ke indo */
    var rupiah = document.getElementById('currency1');
    rupiah.addEventListener('keyup', function (e) {
        rupiah.value = formatRupiah(this.value);
    });

    var rupiah2 = document.getElementById('currency2');
    rupiah2.addEventListener('keyup', function (e) {
        rupiah2.value = formatRupiah2(this.value);
    });

    /* Fungsi */
    function formatRupiah(bilangan, prefix) {
        var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    /* Fungsi */
    function formatRupiah2(bilangan, prefix) {
        var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
</script>