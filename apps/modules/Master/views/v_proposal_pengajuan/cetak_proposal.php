<html>
    <style>
        body {
            /*background: rgb(204,204,204);*/ 
            font-family: "Bookman Old Style";
        }
        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            /* box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);*/

        }
        page[size="A4"] {  
            width: 21cm;
            height: 29.7cm; 
        }
        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
            }
        }
        .kop {
            margin-left: 110px;
            margin-top: 30px;
        }	
        .qrcode {
            margin-left: 220px;
            margin-top: 30px;
        }
        #box1 {
            width:656px;
            margin-left:70px;
        }
        .margin-judul-1 {
            padding-top:10px;
            font-size: 16px;
            margin-left:47%;
        }
        .margin-judul-2 {
            margin-top:10px;
            font-size: 16px;
            margin-left:44.3%;
        }
        .margin-judul-4 {
            margin-top:30px;
            font-size: 14px;
            margin-left:5%;
        }
        .margin-judul-3 {
            margin-top:10px;
            font-size: 16px
        }
        .frame_potong {
            min-height: 150px;
            max-width: 580px;
            border: 2px solid rgba(0, 0, 0, 0.3);
            background: #f2f2f2;
            padding: 20px 20px;
        }
        .container {
            max-width: 100%;
            margin: 0 auto;
            padding-left: 15%;
            padding-top: 1%;
            width: 100%;
        }
        .no_sertifikat {
            padding-top:20px;
            font-size: 12px
        }
        .text1 {
            margin-top:25px;
            font-size: 14px
        }
        .text2 {
            margin-top:-30px;
            margin-left: 470px;
            font-size: 14px;
            margin-bottom:40px;
        }
        .text3 {
            margin-top:-17px;
            margin-left: 600px;
            font-size: 14px
        }
        .text4 {
            margin-top:10px;
            font-size: 14px;
            line-height:1.5em;
        }
        .text5 {
            font-size: 14px;
            margin-left: 450px;
        }
        .text6 {
            font-size: 15px;
            margin-top: -20px;
            margin-left: 210px;
            position: absolute;
        }
        .text6-tgl {
            font-size: 14px;
            margin-top: 7px;
            margin-left: 50%;
            position: absolute;
        }
        .kotak {
            width: 4.3em;
            height: 6.3em;
            margin-top: 60px;
            margin-left:250px;
            position: absolute;
            z-index: 7;
            border: solid 1px black;
        }
        .kotak p {
            padding-top: 30px;
            padding-left: 15px;
        }
        .gap1 {
            margin-top: 30px;
        }
        .spasi {
            margin-top: 40px;
        }
        .margin-nama {
            margin-top:27px;
            padding-left:325px;
            text-transform: uppercase;
            font-size:18px;
        }
        .margin-tgl {
            margin-top:10px;
            padding-left:325px;
            text-transform: uppercase;
            font-size:18px;
        }
        .margin-judul-kapol {
            margin-top:30px;
            padding-left:420px;
            font-size:14px; 
        }
        .margin-judul-kapol2 {
            margin-top:5px;
            padding-left:440px;
            font-size:14px; 
        }
        .margin-nama-kapol {
            margin-top:70px;
            padding-left:550px;
            font-size:14px; 
            text-decoration: underline;
        }
        .margin-nama-kapol2 {
            margin-top:5px;
            padding-left:510px;
            font-size:14px; 
        }
        .table-ok {
            font-family: "Bookman Old Style";
            font-size: 14px;
        }
        .table-ok, th, td {
            padding: 10px 0px 0px 50px;
            margin-left: -100px;
            text-align: left;
        }
        button {
            width: 150px;
            height: 30px;
        }
        @media print {
            .samping {display:none;}
        }
        .button {
            width: 100%;
            height: 50px;
        }
        .left {
            float: left;

            display: block;
        }
        .right {
            margin-top: 10px;
            float: right;
            margin-right: 35%;
            display: block;
        }
    </style>
    <page size="A4">
        <img class="kop" src="<?php echo site_url(); ?>assets/tambahan/gambar/kop-pu_sda.png" width="680px">
        <div class="margin-judul-1"><b>DATA REGISTRASI</b></div>
        <div class="margin-judul-2"><b>PENGAJUAN PROPOSAL
            </b></div>
        <br><br>
        <div class="container">
            <table class="table-ok">
                <tr>
                    <td style="vertical-align: top;">Kode Pokmas</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><b><?php echo $resultData->kode_pokmas; ?></b></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Nama Kelompok</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->nama_kelompok; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Nama Ketua</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->nama_ketua; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Kota / Kabupaten</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->kabupaten; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Kecamatan</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->kecamatan; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Kelurahan / Desa</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->desa; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Alamat</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->alamat; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">No Telpon</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"></td>
                </tr>
            </table>
            <hr style="height:2px; border-width:0; color:gray; background-color:gray; margin-left: -100px;">
            <table class="table-ok">
                <tr>
                    <td style="vertical-align: top;">Kode Proposal</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><b><?php echo $resultData->kode_proposal; ?></b></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">No Surat</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->no_surat; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Perihal</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->perihal; ?></td>
                </tr>
                <?php if ($resultData->jenis_bantuan == 'Uang') { ?>
                    <tr>
                        <td style="vertical-align: top;">Nilai Usulan Proposal </td><td style="vertical-align: top;">:</td><td style="vertical-align: top;">Rp. <?php echo $resultData->usulan_nilai_anggaran; ?></td>
                    </tr>
                <?php } ?>
                <?php if ($resultData->jenis_bantuan == 'Barang') { ?>
                    <tr>
                        <td style="vertical-align: top;">Usulan Barang </td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo nl2br($resultData->usulan_barang); ?></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td style="vertical-align: top;">Tanggal Proposal</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo date_indo(date('Y-m-d', strtotime($resultData->tanggal))) ?></td>
                </tr>
            </table>
            <div class="gap1"></div>
            <div class="frame_potong" style="margin-top: 20px;
                 border: 4px dashed #b4b9be;">
                <img class="qrcode" src='../upload/qrcode/<?php echo $resultData->qrcode; ?>' width="150px">
                <div class="spasi"></div>
                <p class="text6">Silahkan Scan QR Code</p>
            </div>
        </div>
    </page>
    <div class="button">
        <ul class="right">
            <button class="samping" onClick="window.print();">Cetak Data</button>
            <button class="samping" onclick="window.top.close();">Kembali</button>
        </ul>
    </div>
</html>