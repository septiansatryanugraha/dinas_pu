 
<style>
    #btn_loading {
        display: none;
    }
</style>

<?php $this->load->view('_heading/_headerContent') ?>
<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom" id="newContain">
                    <form class="form-horizontal" id="form-ubah" method="POST">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Kota / Kabupaten</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="kabupaten" placeholder="Kabupaten" id="kabupaten" aria-describedby="sizing-addon2" value="<?php echo $resultData->kabupaten; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div id="buka"> 
                                <button name="simpan" id="simpan" type="button" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                                <a class="klik ajaxify" href="<?php echo site_url('kabupaten'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                            </div>
                            <div id="btn_loading">
                                <button name="simpan" id="simpan" type="button" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>  

<script type="text/javascript">
    //Proses Controller logic ajax
    $("#simpan").click(function () {
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            var data = $("#newContain>form").serialize();
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url("update-kabupaten") . '/' . $resultData->id; ?>',
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-ubah").reset();
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?php echo base_url("kabupaten"); ?>'", 450);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });
</script>