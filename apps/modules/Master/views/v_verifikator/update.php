<?php $this->load->view('_heading/_headerContent') ?>

<style>
    #btn_loading {
        display: none;
    }

    .field-icon {
        float: left;
        margin-left: 93%;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }
</style>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom" id="newContain">
                    <form class="form-horizontal" id="form-ubah" method="POST">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Bidang</label>
                                <div class="col-sm-3">
                                    <select name="grup_id" class="form-control select-grup" id="grup_id">
                                        <option></option>
                                        <?php foreach ($grup as $data) { ?>
                                            <option value="<?php echo $data->grup_id; ?>" <?php echo ($data->grup_id == $resultData->grup_id) ? "selected" : ""; ?>>
                                                <?php echo $data->nama_grup; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Nama Tim</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="nama" placeholder="Nama" id="nama" aria-describedby="sizing-addon2" value="<?php echo $resultData->nama; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Nama Ketua Tim</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="ketua" placeholder="Nama Ketua Tim" id="ketua" aria-describedby="sizing-addon2" value="<?php echo $resultData->ketua; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Alamat</label>
                                <div class="col-sm-4">
                                    <textarea id="alamat" name="alamat" class="form-control" placeholder="Alamat"><?php echo $resultData->alamat; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Telepon</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="telp" placeholder="Telepon" id="telp" aria-describedby="sizing-addon2" value="<?php echo $resultData->telp; ?>" onkeypress="return hanyaAngka(event)" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Kabupaten</label>
                                <div class="col-sm-3">
                                    <select name="id_kabupaten" class="form-control select-kabupaten" id="id_kabupaten">
                                        <option></option>
                                        <?php foreach ($kabupaten as $data) { ?>
                                            <option value="<?php echo $data->id; ?>" <?php echo ($data->id == $resultData->id_kabupaten) ? "selected" : ""; ?>>
                                                <?php echo $data->kabupaten; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" placeholder="username" name="username" id="username" aria-describedby="sizing-addon2" value="<?php echo $resultData->username; ?>">
                                </div>
                            </div> 
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-4">
                                    <input type="password" class="form-control" placeholder="password" id="password-field" name="password" aria-describedby="sizing-addon2" value="<?php echo base64_decode($resultData->password); ?>"><span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                </div>
                            </div> 
                        </div>
                        <div class="box-footer">
                            <div id="buka"> 
                                <button name="simpan" id="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                                <a class="klik ajaxify" href="<?php echo base_url('verifikator'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                            </div>
                            <div id="btn_loading">
                                <button name="simpan" id="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-ubah').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url("update-verifikator") . '/' . $resultData->id; ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-ubah").reset();
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?php echo base_url("verifikator"); ?>'", 450);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    $(function () {
        $(".select-grup").select2({
            placeholder: " -- Pilih Sub Bidang -- "
        });

        $(".select-kabupaten").select2({
            placeholder: " -- Pilih Kabupaten -- "
        });
    });

    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    // untuk show hide password
    $(".toggle-password").click(function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>