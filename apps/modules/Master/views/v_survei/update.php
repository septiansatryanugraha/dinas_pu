<?php $this->load->view('_heading/_headerContent') ?>

<style>
    #loadingImg {
        margin-top:5%; 
        margin-left:25%; 
        position: fixed;  
        z-index: 9999; 
    }
    #qrcode {
        margin-left: 30%;
        margin-bottom: 20px;
    }
    #slider {
        margin-left: 1%;
    }
    #btn_loading {
        display: none;
        width: 150%;
    }
    #btn_loading2 { 
        display: none; 
    }
</style>

<section class="content">
    <div class="row">
        <form enctype="multipart/form-data" id="form-ubah" method="POST">
            <div class="col-md-7">
                <div class="box box-primary" id="newContain">
                    <div class="box-header with-border">
                        <h3 class="box-title"><b>Data Survey Lapangan</b></h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="exampleInputEmail1">Kota / Kabupaten</label>
                                <input type="text" name="kabupaten" class="form-control" id="kabupaten" value="<?php echo $resultData->kabupaten; ?>" readOnly>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="exampleInputEmail1">Kecamatan</label>
                                <input type="text" name="kecamatan" class="form-control" id="kecamatan" value="<?php echo $resultData->kecamatan; ?>" readOnly>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="exampleInputEmail1">Kelurahan / Desa</label>
                                <input type="text" name="desa" class="form-control" id="desa" value="<?php echo $resultData->desa; ?>" readOnly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="exampleInputEmail1">Pokmas</label>
                                <input type="text" name="kelompok" class="form-control" id="kelompok" value="<?php echo $resultData->nama_kelompok; ?>" readOnly>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="exampleInputEmail1">Ketua Pokmas</label>
                                <input type="text" name="ketua" class="form-control" id="ketua" value="<?php echo $resultData->nama_ketua; ?>" readOnly>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-7">
                                <label for="exampleInputEmail1">Jenis Kegiatan</label>
                                <textarea class="form-control" id="jenis_kegiatan" name="jenis_kegiatan"><?php echo $resultData->jenis_kegiatan; ?></textarea>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="exampleInputEmail1">tanggal Survei</label>
                                <div class="input-group date">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <input type="text" name="tgl_survei" id="tgl_survei" class="form-control datepicker" value="<?php echo date('d-m-Y', strtotime($resultData->tgl_survei)); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-7">
                                <label for="exampleInputEmail1">Lokasi Pekerjaan</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <input type="text" class="form-control" name="latitude" placeholder="Lintang Selatan (LS)" value="<?php echo $resultData->latitude; ?>">
                                <br><font color="red"><a href="https://www.latlong.net/" target="blank">cek latitude & longitude</a></font>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="longitude" placeholder="Bujur Timur (BT)"value="<?php echo $resultData->longitude; ?>" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-7">
                                <label for="exampleInputEmail1">Alamat</label>
                                <textarea class="form-control" name='alamat'><?php echo $resultData->alamat; ?></textarea>
                            </div>
                            <?php if ($resultData->jenis_bantuan == 'Uang') { ?>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1">Jumlah Anggaran</label>
                                    <input type="text" name="jumlah_anggaran" class="form-control" id="currency1" value="<?php echo $resultData->jumlah_anggaran; ?>">
                                </div>
                            <?php } ?>
                        </div>
                        <?php if ($resultData->jenis_bantuan == 'Barang') { ?>
                            <div class="row">
                                <div class="form-group col-md-7">
                                    <label for="exampleInputEmail1">Barang</label>
                                    <textarea class="form-control" name='barang' id='barang'><?php echo $resultData->barang; ?></textarea>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="exampleInputEmail1">Catatan</label>
                                <textarea id="summernote" name="catatan"><?php echo $resultData->catatan; ?></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="inputEmail3 control-label">Status Survei</label>
                                <select name="survei" class="form-control select-survei" id="status_survei">
                                    <option></option>
                                    <?php foreach ($survei as $data) { ?>
                                        <option value="<?php echo $data->nama ?>" <?php echo ($data->nama == $resultData->survei) ? "selected" : ""; ?>>
                                            <?php echo $data->nama; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                                <p style='color: red; font-size: 14px;'><b>* wajib di isi setelah selesai mengisi form survey  </b></p>
                            </div> 
                        </div>
                    </div>
                    <div class="box-footer">
                        <div id="buka">
                            <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                            <a class="klik ajaxify" href="<?php echo site_url('master-survei'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                        </div>
                        <div id="btn_loading">
                            <button name="submit" type="submit" class="btn btn-primary btn-flat animated-gradient">&nbsp;Tunggu..</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Hasil Survei</h3>
                    </div>
                    <div class="box-body box-profile">
                        <div class="row">
                            <div class="form-group col-md-9">
                                <label>Upload PDF</label>
                                <input type="file" name="file_upload[]" id="file_hasil_survei"/>
                                <?php if (isset($historiFileHasilSurvei[0])) { ?>
                                    <a href="<?php echo base_url() . $historiFileHasilSurvei{0}->file_upload; ?>" target="_blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-file-pdf" aria-hidden="true"></i> <?php echo $historiFileHasilSurvei{0}->nama_file_upload; ?></font></b></a>
                                <?php } ?>
                            </div>
                            <div class="form-group col-md-2" style="margin-top: 20px;">
                                <small class="label pull-center bg-red">format .pdf</small>
                            </div>
                        </div>
                    </div>
                    <?php if (count($historiFileHasilSurvei) > 0) { ?>
                        <div class="box-body box-profile">
                            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="50%">
                                <tr>  
                                    <th>History File Upload</th>
                                    <th style="text-align: center;">Download</th>
                                </tr>
                                <?php foreach ($historiFileHasilSurvei as $data) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() . $data->file_upload ?>" target="blank"><b><font face="verdana" size="2" color="red"><i class="fa fa-download" aria-hidden="true"></i> <?php echo $data->nama_file_upload; ?></font></b></a><br>tanggal upload : <?php echo date_indo(date('Y-m-d', strtotime($data->tanggal))) ?><br><small class="label pull-center bg-green"><?php echo $data->status ?></small></td>
                                        <td style="text-align: center;"><a href="<?php echo base_url() . $data->file_upload ?>" target="_blank"><button class="btn btn-success"><i class="fa fa-download" aria-hidden="true"></i></button></a></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } ?>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Sketsa Survei</h3>
                    </div>
                    <div class="box-body box-profile">
                        <div class="row">
                            <div class="form-group col-md-9">
                                <label>Upload PDF</label>
                                <input type="file" name="file_upload[]" id="file_sketsa_survei"/>
                                <?php if (isset($historiSketsaSurvei[0])) { ?>
                                    <a href="<?php echo base_url() . $historiSketsaSurvei{0}->file_upload; ?>" target="_blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-file-pdf" aria-hidden="true"></i> <?php echo $historiSketsaSurvei{0}->nama_file_upload; ?></font></b></a>
                                <?php } ?>
                            </div>
                            <div class="form-group col-md-2" style="margin-top: 20px;">
                                <small class="label pull-center bg-red">format .pdf</small>
                            </div>
                        </div>
                    </div>
                    <?php if (count($historiSketsaSurvei) > 0) { ?>
                        <div class="box-body box-profile">
                            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="50%">
                                <tr>  
                                    <th>History File Upload</th>
                                </tr>
                                <?php foreach ($historiSketsaSurvei as $data) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() . $data->file_upload ?>" target="blank"><b><font face="verdana" size="2" color="red"><i class="fa fa-download" aria-hidden="true"></i> <?php echo $data->nama_file_upload; ?></font></b></a><br>tanggal upload : <?php echo date_indo(date('Y-m-d', strtotime($data->tanggal))) ?><br><small class="label pull-center bg-green"><?php echo $data->status ?></small></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } ?>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Berita Acara</h3>
                    </div>
                    <div class="box-body box-profile">
                        <div class="row">
                            <div class="form-group col-md-9">
                                <label>Upload PDF</label>
                                <input type="file" name="file_upload[]" id="file_berita_acara"/>
                                <?php if (isset($historiFileBeritaAcara[0])) { ?>
                                    <a href="<?php echo base_url() . $historiFileBeritaAcara{0}->file_upload; ?>" target="_blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-file-pdf" aria-hidden="true"></i> <?php echo $historiFileBeritaAcara{0}->nama_file_upload; ?></font></b></a>
                                <?php } ?>
                            </div>
                            <div class="form-group col-md-2" style="margin-top: 20px;">
                                <small class="label pull-center bg-red">format .pdf</small>
                            </div>
                        </div>
                    </div>
                    <?php if (count($historiFileBeritaAcara) > 0) { ?>
                        <div class="box-body box-profile">
                            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="50%">
                                <tr>  
                                    <th>History File Upload</th>
                                </tr>
                                <?php foreach ($historiFileBeritaAcara as $data) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() . $data->file_upload ?>" target="blank"><b><font face="verdana" size="2" color="red"><i class="fa fa-download" aria-hidden="true"></i> <?php echo $data->nama_file_upload; ?></font></b></a><br>tanggal upload : <?php echo date_indo(date('Y-m-d', strtotime($data->tanggal))) ?><br><small class="label pull-center bg-green"><?php echo $data->status ?></small></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } ?>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Checklist Survei</h3>
                    </div>
                    <div class="box-body box-profile">
                        <div class="row">
                            <div class="form-group col-md-9">
                                <label>Upload PDF</label>
                                <input type="file" name="file_upload[]" id="file_checklist"/>
                                <?php if (isset($historiFileChecklist[0])) { ?>
                                    <a href="<?php echo base_url() . $historiFileChecklist{0}->file_upload; ?>" target="blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-file-pdf" aria-hidden="true"></i> <?php echo $historiFileChecklist{0}->nama_file_upload; ?></font></b></a>
                                <?php } ?>
                            </div>
                            <div class="form-group col-md-2" style="margin-top: 20px;">
                                <small class="label pull-center bg-red">format .pdf</small>
                            </div>
                        </div>
                    </div>
                    <?php if (count($historiFileChecklist) > 0) { ?>
                        <div class="box-body box-profile">
                            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="50%">
                                <tr>  
                                    <th>History File Upload</th>
                                </tr>
                                <?php foreach ($historiFileChecklist as $data) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() . $data->file_upload ?>" target="blank"><b><font face="verdana" size="2" color="red"><i class="fa fa-download" aria-hidden="true"></i> <?php echo $data->nama_file_upload; ?></font></b></a><br>tanggal upload : <?php echo date_indo(date('Y-m-d', strtotime($data->tanggal))) ?><br><small class="label pull-center bg-green"><?php echo $data->status ?></small></td>

                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } ?>
                </div>



                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cetak Laporan</h3>
                    </div>
                    <div class="box-body box-profile">
                        <div class="row">
                            <div class="col-md-2">
                                <a href="<?php echo base_url('print-survei') . '/' . $resultData->id_survey; ?>" target="_blank"><button type="button" class="btn btn-success"><i class="fa fa-print"></i> Survei</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="col-md-7">
        </div>

        <!-- Profile Image -->
        <div class="col-md-5">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Gambar Survey</h3>  
                </div>
                <div class="box-body box-profile">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah</button><br><br>
                    <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <tr>  
                            <th>Gambar</th>
                            <th>Action</th>
                        </tr>
                        <?php foreach ($gambar_survei as $data) { ?>
                            <tr>
                                <td class='align-middle text-center'><img class='thumbnail' src ="<?php echo base_url($data->gambar); ?>" width='100px'></td>
                                <td class='align-middle text-center'><a href="<?php echo base_url("delete-gambar-survei") . '/' . $data->id_gambar; ?>" onClick="return confirm('Anda yakin ingin menghapus gambar ini?')" title='Hapus'><button class='btn btn-danger'><i class='fa fa-trash'></i> Hapus</button></a></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>  
    </div>







    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Tutup"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Import Data</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" enctype="multipart/form-data" id="form-upload" method="POST" >
                        <div class="modal-body">
                            <input type="hidden" name="id_proposal" value="<?php echo $resultData->id_proposal; ?>">
                            <input type="hidden" name="id_survey" value="<?php echo $resultData->id_survey; ?>">
                            <div class="input-group form-group">
                                <span class="input-group-addon" id="sizing-addon2">
                                    <i class="glyphicon glyphicon-file"></i>
                                </span>
                                <input type="file" class="form-control" name="gambar[]" multiple aria-describedby="sizing-addon2" id="gambar" onchange="return fileValidation2()">
                            </div>    
                        </div>
                        <div class="modal-footer">
                            <div id='btn_loading2'>
                                <button type='button' class='btn btn-success' disabled><i class='fa fa-spinner fa-spin'></i> &nbsp;Tunggu..</button>
                            </div>
                            <div id="buka2">
                                <button class="btn btn-success" type="submit" name="button"><i class="fa fa-upload" aria-hidden="true"></i>&nbsp; Upload</button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"> Batal</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>  

<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-ubah').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url('update-survei') . '/' . $resultData->id_survey; ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-ubah").reset();
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?php echo base_url("master-survei"); ?>'", 450);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    $(document).ready(function () {
        $('#summernote').summernote({
            height: 200,
        });
    });

    $(function () {
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })

        $(".select-survei").select2({
            placeholder: " -- Pilih Status -- "
        });
    });

    function fileValidation2() {
        var fileInput = document.getElementById("gambar").value;
        if (fileInput != '')
        {
            var checkfile = fileInput.toLowerCase();
            if (!checkfile.match(/(\.jpg|\.jpeg|\.png|\.gif)$/)) { // validasi ekstensi file
                window.location.reload();
                toastr.error('sorry, enter the image with the format .jpeg |.jpg |.png | only.', 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                return false;
            }
            var ukuran = document.getElementById("gambar");
            if (ukuran.files[0].size > 9007200)  // validasi ukuran size file
            {
                toastr.error('Maximal File 9 MB', 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                window.location.reload();
                document.getElementsByClassName('uploadFile').value = '';
                return false;
            }
            return true;
        }
    }

    //Proses Controller logic ajax
    $('#form-upload').submit(function (e) {
        var error = 0;
        var message = "";

        var data = $(this).serialize();

        var gambar = $("#gambar").val();
        var gambar = gambar.trim();

        if (error == 0) {
            if (gambar.length == 0) {
                error++;
                message = "Gambar wajib di isi.";
            }
        }

        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka2").hide();
                    $("#btn_loading2").show();
                },
                url: '<?php echo base_url('upload-gambar-survei') . '/' . $resultData->id_survey; ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-upload").reset();
                    $("#buka2").show();
                    $("#btn_loading2").hide();
                    $("#myModal").hide();
                    $("#myModal").modal('hide');
                    save_berhasil();
                    setTimeout("window.location='<?php echo base_url("edit-survei") . '/' . $resultData->id_survey; ?>'", 450);
                } else {
                    $("#buka2").show();
                    $("#btn_loading2").hide();
                    $("#myModal").hide();
                    $("#myModal").modal('hide');
                    swal("Warning", result.pesan, "warning");
                }
            })
            e.preventDefault();
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
        }
    });

    /* Str Replacement untuk ke indo */
    var rupiah = document.getElementById('currency1');
    rupiah.addEventListener('keyup', function (e) {
        rupiah.value = formatRupiah(this.value);
    });

    /* Fungsi */
    function formatRupiah(bilangan, prefix) {
        var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
</script>