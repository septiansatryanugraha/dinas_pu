<html>
    <style>
        body {
            /*background: rgb(204,204,204);*/ 
            font-family: "Arial";
        }
        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            /* box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);*/
        }
        page[size="A4"] {  
            width: 21cm;
            height: 29.7cm; 
        }
        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
            }
        }
        td {
            padding-left: 10px;
        }
        .thumbnail {
            margin: 5px;
            border : 3px grey solid;
        }
        .jarak-gambar {
            padding-left: 10px;
        }
        .kop {
            margin-left: 110px;
            margin-top: 30px;
        }	
        .qrcode {
            margin-left: 220px;
            margin-top: 30px;
        }
        #box1 {
            width:656px;
            margin-left:70px;
        }
        .margin-judul-1 {
            padding-top:10px;
            font-size: 16px;
            margin-left:27%;
        }
        .margin-judul-2 {
            margin-top:10px;
            font-size: 16px;
            margin-left:42%;
        }
        .margin-judul-4 {
            margin-top:30px;
            font-size: 14px;
            margin-left:5%;
        }
        .margin-judul-3 {
            margin-top:10px;
            font-size: 16px
        }
        .frame_potong {
            min-height: 150px;
            max-width: 580px;
            border: 2px solid rgba(0, 0, 0, 0.3);
            background: #f2f2f2;
            padding: 20px 20px;
        }
        .container {
            max-width: 100%;
            margin: 0 auto;
            padding-left: 15%;
            padding-top: 1%;
            width: 100%;
        }
        .no_sertifikat {
            padding-top:20px;
            font-size: 12px
        }
        .text1 {
            margin-top:25px;
            font-size: 14px
        }
        .text2 {
            margin-top:-30px;
            margin-left: 470px;
            font-size: 14px;
            margin-bottom:40px;
        }
        .text3 {
            margin-top:-17px;
            margin-left: 600px;
            font-size: 14px
        }
        .text4 {
            margin-top:10px;
            font-size: 14px;
            line-height:1.5em;
        }
        .text5 {
            font-size: 14px;
            margin-left: 450px;
        }
        .text6 {
            font-size: 15px;
            margin-top: -20px;
            margin-left: 210px;
            position: absolute;
        }
        .text6-tgl {
            font-size: 14px;
            margin-top: 7px;
            margin-left: 50%;
            position: absolute;
        }
        .kotak {
            width: 4.3em;
            height: 6.3em;
            margin-top: 60px;
            margin-left:250px;
            position: absolute;
            z-index: 7;
            border: solid 1px black;
        }
        .kotak p {
            padding-top: 30px;
            padding-left: 15px;
        }
        .gap1 {
            margin-top: 30px;
        }
        .spasi {
            margin-top: 40px;
        }
        .margin-nama {
            margin-top:27px;
            padding-left:325px;
            text-transform: uppercase;
            font-size:18px;
        }
        .margin-tgl {
            margin-top:10px;
            padding-left:325px;
            text-transform: uppercase;
            font-size:18px;
        }
        .margin-judul-kapol {
            margin-top:30px;
            padding-left:420px;
            font-size:14px; 
        }
        .margin-judul-kapol2 {
            margin-top:5px;
            padding-left:440px;
            font-size:14px; 
        }
        .margin-nama-kapol {
            margin-top:70px;
            padding-left:550px;
            font-size:14px; 
            text-decoration: underline;
        }
        .margin-nama-kapol2 {
            margin-top:5px;
            padding-left:510px;
            font-size:14px; 
        }
        .table-ok  {
            font-family: "Arial";
            font-size: 14px;
        }
        .table-ok, th, td {
            padding: 10px 0px 0px 50px;
            margin-left: -100px;
            text-align: left;

        }
        button {
            width: 150px;
            height: 30px;
        }
        @media print {
            .samping {display:none;}
        }
        .button {
            width: 100%;
            height: 50px;
        }
        .left {
            float: left;

            display: block;
        }
        .right {
            margin-top: -200px;
            float: right;
            margin-right: 25%;
            display: block;
        }

        .blok-image
        {
            margin-bottom: 26%;
        }
    </style>
    <page size="A4">
        <div>
            <b>
                <img class="kop" src="<?php echo site_url(); ?>assets/tambahan/gambar/kop-pu_sda.png" width="680px">
                <div class="margin-judul-1"><b>LAPORAN KEGIATAN SURVEY LAPANGAN KEGIATAN HIBAH</b></div>
            </b>
        </div>
        <br><br>
        <div class="container">
            <table class="table-ok">
                <tr>
                    <td style="vertical-align: top;">Nama Kelompok</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->nama_kelompok; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Nama Ketua</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->nama_ketua; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Kota / Kabupaten</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->kabupaten; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Kecamatan</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->kecamatan; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Desa</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->desa; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Alamat</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->alamat; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Bujur Timur (BT)</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->longitude; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Lintang Selatan (LS)</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->latitude; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Jenis Kegiatan</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->jenis_kegiatan; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Tanggal Survey</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo date_indo(date('Y-m-d', strtotime($resultData->tgl_survei))) ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Lama Pelaksanaan</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->lama_pelaksanaan; ?></td>
                </tr>
                <?php if ($resultData->jenis_bantuan == 'Uang') { ?>
                    <tr>
                        <td style="vertical-align: top;">Jumlah Anggaran</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;">Rp. <?php echo $resultData->jumlah_anggaran; ?></td>
                    </tr>
                <?php } ?>
                <?php if ($resultData->jenis_bantuan == 'Barang') { ?>
                    <tr>
                        <td style="vertical-align: top;">Barang</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo nl2br($resultData->barang); ?></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td style="vertical-align: top;">Catatan</td><td style="vertical-align: top;">:</td><td style="vertical-align: top;"><?php echo $resultData->catatan; ?></td>
                </tr>
            </table>
            <div class="gap1"></div>
            <div class="margin-judul-1"><b>FOTO KEGIATAN SURVEY</b></div>
            <div class="gap1"></div>
            <div class="blok-image">
                <?php
                foreach ($gambar_survei as $data) {
                    echo "<tr>
          <td class='align-middle text-center'><img class='thumbnail' src ='" . base_url($data->gambar) . "' height='120px'></td>
          </tr>";
                }

                ?>
            </div>
            <div class="gap1"></div>
        </div>
        <div class="button">
            <ul class="right">
                <button class="samping" onClick="window.print();">Cetak Data</button>
                <button class="samping" onclick="window.top.close();">Kembali</button>
            </ul>
        </div>
    </page>

    <script>
        function goBack() {
            window.history.back();
        }
    </script>
</html>