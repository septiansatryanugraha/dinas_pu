<?php $this->load->view('_heading/_headerContent') ?>

<style>
    .font-data{
        font-family: Futura,Trebuchet MS,Arial,sans-serif; 
        color:red;
        font-size: 13px;
    }
    .kiri {
        margin-left: -10px;
    }
    .gap {
        margin-top: 30px;
    }
</style>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="row">
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="nav-tabs-custom">
                    <form class="form-horizontal" id="form-ubah" method="POST">
                        <div class="box-header with-border">
                            <h3 class="box-title">View Data Pengajuan</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Kode Proposal</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->kode_proposal; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">No Biro</label>
                                <div class="col-sm-5">
                                    <p class="form-control-static"><?php echo $resultData->no_biro; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama Kelompok</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->nama_kelompok; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama Ketua</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->nama_ketua; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Desa</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->desa; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Kecamatan</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->kecamatan; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Kabupaten</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resultData->kabupaten; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Status</label>
                                <div class="col-sm-5">
                                    <select name="status" class="form-control selek-status" id="status_dokumen">
                                        <?php foreach ($status as $data) { ?>
                                            <option></option>
                                            <option value="<?php echo $data->nama ?>"<?php
                                            if ($data->nama == $resultData->status) {
                                                echo"selected='selected'";
                                            }

                                            ?>><?php echo $data->nama; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"></label>
                                <div class="col-sm-5">
                                    <input type="checkbox" id="status_dpa" name="status_dpa" value="1">&nbsp; Masuk DPA ?
                                </div>
                            </div>  
                        </div>
                        <div class="box-footer">
                            <div id="buka"> 
                                <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                                <a class="klik ajaxify" href="<?php echo site_url('master-proposal-penetapan'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                            </div>
                            <div id="btn_loading">
                                <button name="submit" type="submit" class="btn btn-primary btn-flat animated-gradient">&nbsp;Tunggu..</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>  

<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-ubah').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url('update-proposal-penetapan') . '/' . $resultData->id_proposal; ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?php echo base_url('master-proposal-penetapan'); ?>'", 450);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    // untuk select2 ajak pilih department
    $(function () {
        $(".selek-status").select2({
            placeholder: " -- Pilih Status -- "
        });
    });
</script>