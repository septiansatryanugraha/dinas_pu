<html>
    <style>
        body {
            font-family: 'Arial', sans-serif;
        }
        page {
            font-family: 'Arial', sans-serif;
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
        }
        page[size="A4"] {  
            width: 21cm;
        }
        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
            }
        }
        td {
            padding-left: 10px;
        }
        .kop {
            width: 100%;
            margin-top: 10px;
            margin-left: 60px;
        }
        .container {
            max-width: 88%;
            margin: 0 auto;
            padding-left: 0%;
            padding-top: 1%;
        }
        .table-ok {
            font-family: 'Arial', sans-serif;
            font-size: 14px;
        }
        .table-data {
            font-family: 'Arial', sans-serif;
            color: #444;
            border-collapse: collapse;
            width: 100%;
            border: 2px solid #f2f5f7;
            font-size: 15px;
        }
        .table-data, th, td {
            margin-top: 10px;
            margin-left: 0px;
            padding: 7px 5px 5px 20px;
            margin-left: 0px;
            text-align: left;
        }
        button {
            width: 150px;
            height: 30px;
        }
        @media print {
            .samping {display:none;}
        }
        .button {
            width: 100%;
            position: relative;
        }
        .right{
            float: right;
            margin-right: 40%;
            display: block;
        }
        .box .judul-kiri {
            width:65%;
            height:40px;
            border :1px solid black;
            float:left;
            margin-bottom: 2px;
            font-size: 13px;
        }
        .judul-kiri .tulisan {
            font-family: Arial, sans-serif;
            margin-top: 10px;
        }
        .judul-kanan .tulisan {
            font-family: Arial, sans-serif;
            margin-top: 9px;
        }
        .box .judul-kanan {
            width:30%;
            height:40px;
            margin-left: 10px;
            padding: -30px;
            border :1px solid black;
            float:left;
            margin-bottom: 2px;
            font-size: 12px;
        }
        .box .kiri {
            width:65%;
            padding-bottom: 20px;
            border :1px solid black;
            float:left;
        }
        .box .kanan {
            width:30%;
            height:220px;
            margin-left: 10px;
            padding: -30px;
            border :1px solid black;
            float:left;
            margin-bottom: 2px;
            object-fit: cover;
        }
        .gambar2 {
            width: 82%;
            height: 200px;
            object-fit: cover;
        }
        ol {
            margin: 0;
        }
        table {
            width: 100%;
        }
    </style>
    <page size="A4">
        <div>
            <img class="kop" src="<?php echo site_url(); ?>assets/tambahan/gambar/kop-pu_sda.png" width="680px">
            <br><br>
            <div class="container">
                <table class="table-ok">
                    <tr>
                        <td style="vertical-align: top; width: 110px;">Nomor SKGUB</td>
                        <td style="vertical-align: top; width: 10px;">:</td>
                        <td style="vertical-align: top;"><?php echo isset($resultNphd['sk_gub']) ? $resultNphd['sk_gub'] : ""; ?></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">Tanggal SKGUB</td>
                        <td style="vertical-align: top;">:</td>
                        <td style="vertical-align: top;"><?php echo isset($resultNphd['tgl_sk_gub']) ? $resultNphd['tgl_sk_gub'] : ""; ?></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">Nomor NPHD</td>
                        <td style="vertical-align: top;">:</td>
                        <td style="vertical-align: top;"><?php echo isset($resultNphd['no_nphd']) ? $resultNphd['no_nphd'] : ""; ?></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">Tanggal NPHD</td>
                        <td style="vertical-align: top;">:</td>
                        <td style="vertical-align: top;"><?php echo isset($resultNphd['tgl_nphd']) ? $resultNphd['tgl_nphd'] : ""; ?></td>
                    </tr>
                </table>
                <br>
                <div class="box">
                    <div class="judul-kiri"><center><div class="tulisan">PENERIMA DATA BANTUAN</div></center></div>
                    <div class="judul-kanan"><center><div class="tulisan">FOTO PENERIMA NPHD</div></center></div>
                    <div class="kiri">
                        <table style="border-style: none;" class="table-data">
                            <tr>
                                <td style="vertical-align: top; width: 180px;">Nama Pokmas</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;"><?php echo $resultData->nama_kelompok; ?></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; width: 180px;">Nama</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;"><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">NIK</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;"><?php echo isset($resultNphd['no_ktp']) ? $resultNphd['no_ktp'] : ""; ?></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Alamat Rumah</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;">Desa <?php echo $resultData->desa; ?> Kec. <?php echo $resultData->kecamatan; ?> Kab. <?php echo $resultData->kabupaten; ?></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Nomor Telepon Rumah / HP</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;"><?php echo isset($resultNphd['telp']) ? $resultNphd['telp'] : ""; ?></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Jabatan</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;">KETUA</td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Alamat Lembaga</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;"><?php echo isset($resultNphd['alamat_rumah']) ? $resultNphd['alamat_rumah'] : ""; ?></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Perihal</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;"><?php echo $resultData->perihal; ?></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Nominal Bantuan</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;">Rp. <?php echo $resultData->nilai_anggaran; ?> ( <?php echo $terbilang; ?> )</td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Pemilik Rekening</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;"><?php echo isset($resultNphd['nama_rek']) ? $resultNphd['nama_rek'] : ""; ?></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Cabang Bank</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;"><?php echo isset($resultNphd['cabang']) ? $resultNphd['cabang'] : ""; ?></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Nomor Rekening</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;"><?php echo isset($resultNphd['no_rek']) ? $resultNphd['no_rek'] : ""; ?></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Status</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;"><b>TELAH MENERIMA DANA BANTUAN</b></td>
                            </tr>
                        </table>
                    </div>
                    <div class="kanan">
                        <table class="table-ok">
                            <tr>  
                                <?php if ($resultNphd['file_img_pokmas_1'] != null) { ?>
                                <img class="img-thumbnail gambar2" style="margin-left: 17px;margin-top: 10px;" src="<?php echo base_url($resultNphd['file_img_pokmas_1']) ?>">
                            <?php } else { ?>
                                <img class="img-thumbnail" style="margin-left: 8px;margin-top: 6px;" src="<?php echo base_url(); ?>assets/tambahan/gambar/no-image.jpg" width="190px"/>
                            <?php } ?>
                            </tr>
                        </table>
                    </div>
                    <div class="judul-kanan"><center><div class="tulisan">FOTO PENERIMA NPHD</center></div>
                    <div class="kanan"><table class="table-ok">
                            <tr>  
                                <?php if ($resultNphd['file_img_pokmas_2'] != null) { ?>
                                <img class="img-thumbnail gambar2" style="margin-left: 17px;margin-top: 10px;" src="<?php echo base_url($resultNphd['file_img_pokmas_2']) ?>">
                            <?php } else { ?>
                                <img class="img-thumbnail " style="margin-left: 8px;margin-top: 6px;" src="<?php echo base_url(); ?>assets/tambahan/gambar/no-image.jpg" width="190px"/>
                            <?php } ?>
                            </tr>
                        </table></div>
                    <div class="gap1"></div>
                </div>
            </div>
        </div>
    </page>
    <div class="button">
        <ul class="right">
            <button class="samping" onClick="window.print();">Cetak Data</button>
            <button class="samping" onclick="window.top.close();">Kembali</button>
        </ul>
    </div>
    <script>
        function goBack() {
            window.history.back();
        }
        document.addEventListener('contextmenu', function (e) {
            e.preventDefault();
        });
        document.onkeydown = function (e) {
            if (event.keyCode == 123) {
                return false;
            }
            if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
                return false;
            }
            if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
                return false;
            }
            if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
                return false;
            }
            if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
                return false;
            }
        }
    </script>
</html>