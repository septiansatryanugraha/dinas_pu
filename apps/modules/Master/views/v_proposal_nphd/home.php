<?php $this->load->view('_heading/_headerContent') ?>

<section class="content">
    <div class="box">
        <div class="box-header">
            <div class="form-group ">
                <div class="col-md-4" style="margin-left: 0px; margin-bottom: 10px;">
                    <a class="klik ajaxify" href="<?php echo base_url('export-nphd'); ?>"><button class="btn btn-success" ><i class="fa fa-download"></i> Export Excel NPHD</button></a>
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="search-form" style="">
                <div class="form-group ">
                    <label class="control-label">Filter</label>
                </div>
                <div class="form-group ">
                    <label class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-3">
                        <select name="status" id="status" class="form-control select-status" aria-describedby="sizing-addon2">
                            <option value="all">Semua Status</option>
                            <?php foreach ($status as $data) { ?>
                                <option value="<?php echo $data->nama; ?>">
                                    <?php echo $data->nama; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div class="form-group ">
                    <label class="col-sm-2 control-label">Tanggal Last Update</label>
                    <div class="col-sm-6">
                        <input type="text" name="tanggal_awal" id="tanggal_awal" class="form-control datepicker" style="width: 30%; display: inline-block;" value="<?php echo date('01-m-Y'); ?>" readonly="">
                        <span>s/d</span>
                        <input type="text" name="tanggal_akhir" id="tanggal_akhir" class="form-control datepicker" style="width: 30%; display: inline-block;" value="<?php echo date('t-m-Y'); ?>" readonly="">
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div class="form-group">  
                    <label class="col-sm-2 control-label"></label>       
                    <div class="col-sm-6">
                        <label class="checkbox-inline">
                            <input type="checkbox" id="all_date" name="all_date" checked/>Semua Tanggal
                        </label>
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div class="box-footer">
                    <button name="button_filter" id="button_filter" style="margin-top: 13px" type="button" class="btn btn-success btn-flat"><i class="fa fa-refresh"></i> Tampilkan</button>
                </div>
                <div class="box-footer"><br></div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <div class="overflow-scroll">
                    <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th style="width: 18%;">Proposal</th>
                                <th>Pokmas</th>
                                <th>Kota / Kabupaten</th>
                                <th>Status</th>
                                <th style="width: 100px;">Dibuat</th>
                                <th style="width: 100px;">Diubah</th>
                                <th style="width: 45px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    // untuk datepicker
    $(function () {
        // untuk datepicker
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        });

        $(".select-status").select2({
            placeholder: " -- Pilih Status -- "
        });
    });

    //untuk load data table ajax  
    var save_method; //for save method string
    var table;

    $(document).ready(function () {
        reloadTable();
    });

    function reloadTable() {
        var status = $("#status").val();
        var tanggal_awal = $("#tanggal_awal").val();
        var tanggal_akhir = $("#tanggal_akhir").val();
        var all_date = 0;
        if (document.getElementById("all_date").checked == true) {
            all_date = 1;
        }

        table = $('#table').DataTable({
            "aLengthMenu": [[50, 75, 100, 150, -1], [50, 75, 100, 150, "All"]],
            "bSort": false,
            "pageLength": 50,
            "processing": true, //Feature control the processing indicator.
            "order": [], //Initial no order.
            oLanguage: {
                "sProcessing": "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "Data tidak ada di server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url('ajax-proposal-nphd') ?>",
                "type": "POST",
                data: {status: status, tanggal_awal: tanggal_awal, tanggal_akhir: tanggal_akhir, all_date: all_date},
            },
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
            ],
            "initComplete": function (settings, json) {
                $('.row').css('margin-right', '0px');
                $('.row').css('margin-left', '0px');
            },
        });
    }

    $('#search-button').click(function () {
        $('.search-form').toggle();
        return false;
    });

    $("#button_filter").click(function () {
        table.destroy();
        reloadTable();
    });
</script>