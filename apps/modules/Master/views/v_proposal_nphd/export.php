<?php $this->load->view('_heading/_headerContent') ?>

<section class="content">
    <div class="box">
        <div class="box-body">
            <div class="box-header">
                <div class="search-form" style="">
                    <form class="form-horizontal" id="form-export" method="post" enctype="multipart/form-data" role="form" action="<?php echo base_url('export-data-nphd'); ?>">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                <h4 class="modal-title">Export Data</h4>
                            </label>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Status Penerimaan LPJ</label>
                            <div class="col-sm-3">
                                <select name="status_penerimaan" class="form-control select-status" id="status_penerimaan">
                                    <option value="all">Semua Status</option>
                                    <option value="Belum">Belum</option>
                                    <option value="Sudah">Sudah</option>
                                </select>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-2 control-label">Dari Awal Tahun</label>
                            <div class="col-sm-3">
                                <select id="start_year" class="form-control select-year" name="start_year">
                                    <?php for ($i = 2010; $i <= 2050; $i++) { ?>
                                        <option value="<?php echo $i; ?>" <?php if ($i == $_SESSION['year']) echo 'selected'; ?> ><?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-2 control-label">Hingga Tanggal</label>
                            <div class="col-sm-3">
                                <input type="text" name="tanggal_akhir" id="tanggal_akhir" class="form-control datepicker" value="<?php echo date('t-m-Y'); ?>" readonly="">
                            </div>
                            <div style="clear:both"></div>
                        </div>
                        <div class="box-footer">
                            <div id="buka"> 
                                <button type="button" name="button_filter" id="button_filter" class="btn btn-success btn-flat"><i class="fa fa-refresh"></i> Show</button>
                                <button type="submit" class="btn btn-success btn-danger"><i class="fa fa-print"></i> Print</button>
                                <a class="klik ajaxify" href="<?php echo base_url('master-proposal-nphd'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                            </div>
                        </div>
                        <div class="box-footer"><br></div>
                    </form>
                </div>
            </div>
            <div class="table-responsive">
                <div class="overflow-scroll">
                    <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th><b>NO</b></th>
                                <th><b>Kode Proposal</b></th>
                                <th><b>Nama Pokmas</b></th>
                                <th><b>Kabupaten</b></th>
                                <th><b>Kecamatan</b></th>
                                <th><b>Desa / Kel</b></th>
                                <th><b>Status Penerimaan</b></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        });
        $(".select-status").select2({
            placeholder: " -- Pilih Status -- "
        });
        $(".select-year").select2({
            placeholder: " -- Pilih Tahun -- "
        });
    });

    var save_method; //for save method string
    var table;

    $(document).ready(function () {
        reloadTable();
    });

    function reloadTable() {
        var status_penerimaan = $("#status_penerimaan").val();
        var start_year = $("#start_year").val();
        var tanggal_akhir = $("#tanggal_akhir").val();
        table = $('#table').DataTable({
            "processing": true, //Feature control the processing indicator.
            "aLengthMenu": [[10, 50, 75, 100, 150, -1], [10, 50, 75, 100, 150, "All"]],
            "bSort": false,
            "searching": false,
            "lengthChange": false,
            "pageLength": 10,
            "order": [], //Initial no order.
            oLanguage: {
                "sProcessing": "<img src='<?= base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
                "sInfoPostFix": "",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?= base_url('ajax-export-data-nphd') ?>",
                "type": "POST",
                data: {status_penerimaan: status_penerimaan, start_year: start_year, tanggal_akhir: tanggal_akhir},
            },
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
            ],
            "initComplete": function (settings, json) {
                $('.row').css('margin-right', '0px');
                $('.row').css('margin-left', '0px');
            },
        });
    }

    $("#button_filter").click(function () {
        table.destroy();
        reloadTable();
    });

    function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
    }

    $('#form-export').submit(function (e) {
        var data = new FormData(this);
        var error = 0;
        var message = "";
        if (error == 0) {
            var start_year = $("#start_year").val();
            if (start_year.length == 0) {
                error++;
                message = "Awal Tahun wajib di isi.";
            }
        }
        if (error == 0) {
            var tanggal_akhir = $("#tanggal_akhir").val();
            if (tanggal_akhir.length == 0) {
                error++;
                message = "Tanggal wajib di isi.";
            }
        }
        if (error == 0) {
            return true;
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });
</script>