<html>
    <style>
        body {
            width: 100%;
            font-family: 'Arial', sans-serif;
            font-size: 11pt !important;
        }
        .container {
            margin-left: 20px;
            margin-right: 20px;
        }
        table {
            width: 100%;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        .valign-top {
            vertical-align: top;
        }
        .text-center {
            text-align: center;
        }
    </style>
    <body>
        <img src="<?php echo base_url(); ?>assets/tambahan/gambar/kop-pu_sda.png" width="750px">
        <div class="container">
            <table><tr><td align="right">Kode Pos 60235</td></tr></table>
            <table>
                <tr>
                    <td class="valign-top" style="width: 50%;">
                        <table>
                            <tr><td colspan="3">&nbsp;</td></tr>
                            <tr><td colspan="3">&nbsp;</td></tr>
                            <tr><td colspan="3">&nbsp;</td></tr>
                            <tr>
                                <td class="valign-top">Nomor</td>
                                <td class="valign-top">:</td>
                                <td class="valign-top"><?php echo isset($resultNphd['no_surat_ke_pokmas']) ? $resultNphd['no_surat_ke_pokmas'] : ""; ?></td>
                            </tr>
                            <tr>
                                <td class="valign-top">Sifat</td>
                                <td class="valign-top">:</td>
                                <td class="valign-top">Segera</td>
                            </tr>
                            <tr>
                                <td class="valign-top">Lampiran</td>
                                <td class="valign-top">:</td>
                                <td class="valign-top">1 (satu) berkas</td>
                            </tr>
                            <tr>
                                <td class="valign-top">Perihal</td>
                                <td class="valign-top">:</td>
                                <td class="valign-top">Undangan Penandatanganan NPHD</td>
                            </tr>
                        </table>
                    </td>
                    <td class="valign-top" style="padding-left: 50px;"  style="width: 48%;">
                        <table>
                            <tr><td colspan="2">Surabaya, <?php echo $tglSurat; ?></td></tr>
                            <tr><td colspan="2">&nbsp;</td></tr>
                            <tr>
                                <td class="valign-top" colspan="2">Kepada :</td>
                            </tr>
                            <tr>
                                <td class="valign-top">Yth.Sdr.</td>
                                <td class="valign-top">Ketua <?php echo $pokmas->nama_kelompok; ?></td>
                            </tr>
                            <tr>
                                <td class="valign-top"></td>
                                <td class="valign-top" style="padding-left: -55px;"><?php echo isset($resultNphd['alamat_pokmas']) ? $resultNphd['alamat_pokmas'] : $pokmas->alamat; ?></td>
                            </tr>
                            <tr>
                                <td class="valign-top" ></td>
                                <td class="valign-top" style="padding-left: -55px;">di</td>
                            </tr>
                            <tr>
                                <td class="valign-top"></td>
                                <td class="valign-top" style="padding-left: -55px;"><span style="padding-left: 20px;">&nbsp;</span><?php echo ucwords(strtolower($resultData->kabupaten)); ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <p class="pasal">
                <span style="padding-left: 50px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Sehubungan dengan surat Saudara tanggal <?php echo $tglProposal; ?>, nomor <?php echo $resultData->no_surat; ?>, perihal tersebut pada pokok surat dan Keputusan Gubernur Jawa Timur Tentang Penerima Hibah Uang, maka Pemerintah Provinsi Jawa Timur memberikan Hibah sebesar Rp. <?php echo $resultData->nilai_anggaran; ?>,- (<?php echo $terbilang; ?>).<br>
                <span style="padding-left: 50px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Berkenaan dengan hal tersebut, diharap agar Saudara hadir dalam rangka penandatanganan Naskah Perjanjian Hibah Daerah (NPHD) dan Pakta Integritas dengan melengkapi persyaratan sebagai berikut :
            </p>
            <ol>
                <li>Rencana penggunaan dana Hibah sebesar Rp. <?php echo $resultData->nilai_anggaran; ?>,- (<?php echo $terbilang; ?>)</li>
                <li>Copy rekening Bank Jatim atas nama <?php echo isset($resultNphd['nama_rek']) ? $resultNphd['nama_rek'] : ""; ?>.</li>
                <li>Kwitansi asli bermaterai Rp. 10.000,- atas nama Ketua <?php echo $pokmas->nama_kelompok; ?> Senilai Rp. <?php echo $resultData->nilai_anggaran; ?>,- (<?php echo $terbilang; ?>) dan distempel.</li>
            </ol>
            <p class="pasal">
                <span style="padding-left: 50px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Selanjutnya diingatkan bahwa Saudara bertanggung jawab penuh atas penggunaan Hibah yang telah diterima dan berkewajiban menyerahkan laporan pertanggungjawaban kepada Gubernur Jawa Timur melalui Dinas Pekerjaan Umum Sumber Daya Air Provinsi Jawa Timur selambat-lambatnya 3 (tiga) bulan setelah diterimanya dana bantuan.<br>
                <span style="padding-left: 50px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Demikian untuk menjadi maklum dan terima kasih.
            </p>
            <table style="page-break-inside: avoid;">
                <tr>
                    <td class="valign-top" style="width: 40%;"></td>
                    <td class="valign-top" style="width: 10%;"></td>
                    <td class="valign-top" style="width: 50%;">
                        <table>
                            <tr><td class="text-center"><?php echo $getManagementSystem['pu_head_sign_1']; ?></td></tr>
                            <tr><td class="text-center"><?php echo $getManagementSystem['pu_head_sign_2']; ?></td></tr>
                            <tr><td class="text-center"><?php echo $getManagementSystem['pu_head_sign_3']; ?></td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center"><u><b><?php echo $getManagementSystem['pu_nama_ketua']; ?></b></u></td></tr>
                            <tr><td class="text-center"><?php echo $getManagementSystem['pu_jabatan_ketua']; ?></td></tr>
                            <tr><td class="text-center">NIP. <?php echo $getManagementSystem['pu_nip']; ?></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>