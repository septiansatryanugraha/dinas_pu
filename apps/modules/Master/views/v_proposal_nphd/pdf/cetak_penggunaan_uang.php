<html>
    <style>
        body {
            width: 100%;
            font-family: 'Bookman Old Style', sans-serif;
            font-size: 11pt;
        }
        .container {
            margin-left: 20px;
            margin-right: 20px;
        }
        table {
            width: 100%;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        .valign-top {
            vertical-align: top;
        }
    </style>
    <body>
        <div class="container">
            <h2 style="text-align: center;">SURAT PERNYATAAN TANGGUNG JAWAB MUTLAK<br>PENGGUNAAN HIBAH BERUPA UANG</h2><hr/>
            <p class="pasal">Yang bertanda tangan dibawah ini :</p>
            <table class="pasal" style="padding-left: 50px;">
                <tr>
                    <td class="valign-top">Nama</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">No KTP</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['no_ktp']) ? $resultNphd['no_ktp'] : ""; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">Alamat Rumah</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['alamat_rumah']) ? $resultNphd['alamat_rumah'] : ""; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">Jabatan Dalam Organisasi</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top">Ketua</td>
                </tr>
                <tr>
                    <td class="valign-top">Alamat Lembaga</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['alamat_rumah']) ? $resultNphd['alamat_rumah'] : ""; ?></td>
                </tr>
            </table>
            <p class="pasal">Yang bertindak untuk dan atas nama <?php echo $pokmas->nama_kelompok; ?></p>
            <p class="pasal">Menyatakan bertanggung jawab atas penggunaan dana hibah yang telah diterima sesuai Naskah Perjanjian Hibah Daerah tanggal <?php echo $tglNphd; ?> Nomor <?php echo isset($resultNphd['no_nphd']) ? $resultNphd['no_nphd'] : ""; ?> dan membuktikan penggunaan dana tersebut dengan laporan pertanggungjawaban yang kami sampaikan paling lambat 3 (tiga) bulan setelah dana diterima sesuai dengan peruntukkannya.</p>
            <p class="pasal">Membuat laporan pertanggungjawaban (SPJ) penggunaan dana bantuan beserta bukti-bukti yang sah tiga bulan setelah dana diterima, serta menyimpan laporan realisasi fisik dan penggunaan dana hibah serta bukti-bukti asli lainnya yang sah sesuai dengan RAB.</p>
            <p class="pasal">Demikian Surat Pernyataan Tanggung Jawab ini dibuat dengan sebenarnya.</p>
            <table style="page-break-inside: avoid;">
                <tr>
                    <td class="valign-top" style="width: 50%;"></td>
                    <td class="valign-top" style="width: 50%;">
                        <table>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">Surabaya, <?php echo $tglNphd; ?></td></tr>
                            <tr><td style="text-align: center">Ketua <?php echo $pokmas->nama_kelompok; ?></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center; font-size: 12px;">MATERAI</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><u><b><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></b></u></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>