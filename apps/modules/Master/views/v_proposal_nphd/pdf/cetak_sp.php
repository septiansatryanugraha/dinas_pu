<html>
    <style>
        body {
            width: 100%;
            font-family: 'Bookman Old Style', sans-serif;
            font-size: 11pt;
        }
        .container {
            margin-left: 20px;
            margin-right: 20px;
        }
        table {
            width: 100%;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        .valign-top {
            vertical-align: top;
        }
    </style>
    <body>
        <div class="container">
            <h2 style="text-align: center;">SURAT PERNYATAAN</h2>
            <br>
            <p class="pasal">Pada hari ini <?php echo $dayNow; ?>, tanggal <?php echo $dateNow; ?>, bulan <?php echo $monthNow; ?>, tahun <?php echo $yearNow; ?> bertempat di <?php echo $getManagementSystem['pu_head_sign_3']; ?> yang bertanda tangan dibawah ini :</p>
            <table style="margin-left: 50px; margin-right: 50px;">
                <tr>
                    <td class="valign-top" style="width: 220px;">Nama</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top">.....................................</td>
                </tr>
                <tr>
                    <td class="valign-top">NIK</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top">.....................................</td>
                </tr>
                <tr>
                    <td class="valign-top">Alamat Rumah</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top">.....................................</td>
                </tr>
                <tr>
                    <td class="valign-top">Nomor Telepon Rumah/HP</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top">.....................................</td>
                </tr>
                <tr>
                    <td class="valign-top">Jabatan</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top">.....................................</td>
                </tr>
            </table>
            <p class="pasal">Dengan ini menyatakan bahwa:</p>
            <table style="margin-left: 50px; margin-right: 50px;">
                <tr>
                    <td class="valign-top" style="width: 220px;">Nama</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo $resultData->nama_kelompok; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">NIK</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['no_ktp']) ? $resultNphd['no_ktp'] : ""; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">Alamat Rumah</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['alamat_rumah']) ? $resultNphd['alamat_rumah'] : ""; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">Nomor Telepon Rumah/HP</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['telp']) ? $resultNphd['telp'] : ""; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">Jabatan</td>
                    <td class="valign-top">:</td>
                    <td class="pasal" class="valign-top">KETUA</td>
                </tr>
            </table>
            <p class="pasal">adalah memiliki kewenangan bertindak untuk dan atas nama <b><?php echo $resultData->nama_kelompok; ?></b> dalam keseluruhan mekanisme pemberian Hibah Daerah berupa uang yang bersumber dari Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur sesuai ketentuan yang berlaku pada <b>Peraturan Gubernur No 44 Tahun 2021 Tentang Tata Cara Penganggaran, Pelaksanaan dan Penatausahaan, Pelaporan dan Pertanggungjawaban serta Monitoring dan Evaluasi Hibah dan Bantuan Sosial</b>.</p>
            <p class="pasal">Demikian Surat Pernyataan ini ditandatangani di atas meterai yang bernilai cukup dalam keadaan sadar dan tanpa paksaan dari pihak manapun untuk dipergunakan sebagaimana mestinya.</p>
            <table style="page-break-inside: avoid;">
                <tr>
                    <td class="valign-top" style="width: 50%;">
                        <table>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">................... , ................</td></tr>
                            <tr><td style="text-align: center">Yang Membuat Pernyataan,</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><hr></td></tr>
                        </table>
                    </td>
                    <td class="valign-top" style="width: 50%;"></td>
                </tr>
            </table>
        </div>
    </body>
</html>