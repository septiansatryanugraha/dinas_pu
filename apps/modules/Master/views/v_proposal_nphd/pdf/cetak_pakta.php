<html>
    <style>
        body {
            width: 100%;
            font-family: 'Bookman Old Style', sans-serif;
            font-size: 11pt;
        }
        .container {
            margin-left: 20px;
            margin-right: 20px;
        }
        table {
            width: 100%;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        .valign-top {
            vertical-align: top;
        }
    </style>
    <body>
        <div class="container">
            <h2 style="text-align: center;">PAKTA INTEGRITAS HIBAH</h2>
            <br>
            <p class="pasal">Yang bertanda tangan dibawah ini :</p>
            <table class="pasal" style="padding-left: 50px;">
                <tr>
                    <td class="valign-top">Nama</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">No KTP</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['no_ktp']) ? $resultNphd['no_ktp'] : ""; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">Alamat Rumah</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['alamat_rumah']) ? $resultNphd['alamat_rumah'] : ""; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">Jabatan Dalam Organisasi</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top">Ketua</td>
                </tr>`
                <tr>
                    <td class="valign-top">Alamat Lembaga</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['alamat_rumah']) ? $resultNphd['alamat_rumah'] : ""; ?></td>
                </tr>
            </table>
            <p class="pasal">yang bertindak untuk dan atas nama <?php echo $pokmas->nama_kelompok; ?></p>
            <p class="pasal">Dengan ini saya menyatakan dengan sebenarnya :</p>
            <ol class="pasal">
                <li>Usulan/Proposal/RAB kegiatan <?php echo $resultData->perihal; ?> yang diajukan kepada Gubernur Jawa Timur untuk mendapatkan hibah, akan kami laksanakan sesuai dengan rencana kegiatan sebagaimana tertuang dalam usulan/proposal dimaksud;</li>
                <li>Hibah dari Pemerintah Provinsi Jawa Timur sebesar Rp. <?php echo $resultData->nilai_anggaran; ?>,- (<?php echo $terbilang; ?>) akan kami gunakan sesuai dengan Rencana Anggaran Biaya (RAB), sebagaimana terlampir dalam proposal;</li>
                <li>Dalam realisasinya, kami berjanji akan melaksanakan tugas/pekerjaan secara profesional dengan menggunakan sumberdaya secara optimal untuk memberikan hasil kerja terbaik;</li>
                <li>Pakta Integritas ini berlaku sejak tanggal ditandatangani dan berakhir sampai dengan laporan pertanggungjawaban diterima oleh Pemerintah Provinsi Jawa Timur selama tidak terjadi penyimpangan;</li>
                <li>Apabila melanggar hal-hal yang telah dinyatakan dalam PAKTA INTEGRITAS ini, saya bersedia dikenakan sanksi moral, sanksi administrasi serta dituntut ganti rugi dan pidana sesuai dengan ketentuan peraturan perundang-undangan yang berlaku.</li>
            </ol>
            <table style="page-break-inside: avoid;">
                <tr>
                    <td class="valign-top" style="width: 50%;"></td>
                    <td class="valign-top" style="width: 50%;">
                        <table>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><?php echo ucwords(strtolower(trim(str_replace('KABUPATEN', '', $resultData->kabupaten)))) . ', ' . $tglNphd; ?></td></tr>
                            <tr><td style="text-align: center">Ketua <?php echo $pokmas->nama_kelompok; ?></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center; font-size: 12px;">MATERAI</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><u><b><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></b></u></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>