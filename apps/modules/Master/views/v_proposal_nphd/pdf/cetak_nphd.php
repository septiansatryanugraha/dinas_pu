<html>
    <style>
        body {
            width: 100%;
            font-family: 'Arial', sans-serif;
            font-size: 11pt;
            line-height: 1.5;
        }
        .container {
            margin-left: 20px;
            margin-right: 20px;
        }
        table {
            width: 100%;
        }
        .pasal {
            text-align: justify; 
            /*text-justify: inter-word;*/
        }
        .valign-top {
            vertical-align: top;
        }
        .text-center {
            text-align: center;
        }
        .page-break-inside-avoid {
            page-break-inside: avoid;
        }
    </style>
    <body>
        <img src="<?php echo base_url(); ?>assets/tambahan/gambar/kop-pu_sda.png" width="750px">
        <div class="container">
            <table><tr><td align="right">Kode Pos 60235</td></tr></table>
            <h2 class="text-center">
                NASKAH PERJANJIAN HIBAH DAERAH ( NPHD )<br>
                DALAM BENTUK <?php echo strtoupper($resultData->jenis_bantuan); ?>
            </h2>
            <h3 class="text-center" style="padding-top: -15px;">Nomor : <?php echo isset($resultNphd['no_nphd']) ? $resultNphd['no_nphd'] : ""; ?></h3>
            <br>
            <p class="pasal">Pada hari ini <?php echo $dayNphd; ?>, tanggal <?php echo $dateNphd; ?>, bulan <?php echo $monthNphd; ?>, tahun <?php echo $yearNphd; ?> bertempat di <?php echo $getManagementSystem['pu_head_sign_3']; ?> yang bertanda tangan dibawah ini :</p>
            <table class="pasal" style="padding-left: 40px;">
                <tr>
                    <td class="valign-top" style="width: 20px;">I.</td>
                    <td class="valign-top" style="width: 190px;">Nama</td>
                    <td class="valign-top" style="width: 20px;">:</td>
                    <td class="valign-top"><?php echo $getManagementSystem['pu_nama_ketua']; ?></td>
                </tr>
                <tr>
                    <td class="valign-top"></td>
                    <td class="valign-top">NIP</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo $getManagementSystem['pu_nip']; ?></td>
                </tr>
                <tr>
                    <td class="valign-top"></td>
                    <td class="valign-top">Jabatan</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo ucwords(strtolower($getManagementSystem['pu_head_sign_1'])); ?></td>
                </tr>
                <tr>
                    <td class="valign-top"></td>
                    <td class="valign-top">Instansi</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top">DINAS <?php echo $getManagementSystem['pu_head_sign_2']; ?> <?php echo $getManagementSystem['pu_head_sign_3']; ?></td>
                </tr>
                <tr>
                    <td class="valign-top"></td>
                    <td class="valign-top">Alamat</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo $getManagementSystem['pu_alamat']; ?></td>
                </tr>
            </table>
            <p class="pasal">Dalam hal ini bertindak untuk dan atas nama Pemerintah Provinsi Jawa Timur sebagai Pemberi Hibah, yang selanjutnya disebut <b>PIHAK KESATU</b></p>
            <table class="pasal" style="padding-left: 40px;">
                <tr>
                    <td class="valign-top" style="width: 20px;">II.</td>
                    <td class="valign-top" style="width: 190px;">Nama</td>
                    <td class="valign-top" style="width: 20px;">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></td>
                </tr>
                <tr>
                    <td class="valign-top"></td>
                    <td class="valign-top">NIK</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['no_ktp']) ? $resultNphd['no_ktp'] : ""; ?></td>
                </tr>
                <tr>
                    <td class="valign-top"></td>
                    <td class="valign-top">Alamat Rumah</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['alamat_pokmas']) ? $resultNphd['alamat_pokmas'] : $pokmas->alamat; ?></td>
                </tr>
                <tr>
                    <td class="valign-top"></td>
                    <td class="valign-top">Nomor Telepon Rumah/HP</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['telp']) ? $resultNphd['telp'] : ""; ?></td>
                </tr>
                <tr>
                    <td class="valign-top"></td>
                    <td class="valign-top">Jabatan</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top">KETUA</td>
                </tr>
                <tr>
                    <td class="valign-top"></td>
                    <td class="valign-top">Alamat Lembaga</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['alamat_pokmas']) ? $resultNphd['alamat_pokmas'] : $pokmas->alamat; ?></td>
                </tr>
            </table>
            <p class="pasal">Dalam hal ini bertindak untuk dan atas nama, KETUA <?php echo $resultData->nama_kelompok; ?> sebagai Penerima Hibah yang selanjutnya disebut <b>PIHAK KEDUA</b></p>
            <?php if ($resultData->jenis_bantuan == 'Barang') { ?>
                <p class="pasal"><b>PIHAK KESATU</b> sepakat untuk memberikan Hibah Daerah berupa Barang kepada <b>PIHAK KEDUA</b> yang bersumber dari Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur dengan ketentuan sebagaimana tercantum dalam Pasal-Pasal tersebut di bawah ini:</p>
                <!--PASAL 1-->
                <div class="page-break-inside-avoid">
                    <h4 class="text-center">Pasal 1<br>JUMLAH DAN TUJUAN HIBAH</h4>
                    <table>
                        <tr>
                            <td class="valign-top" style="width: 30px;">(1)</td>
                            <td class="valign-top pasal"><p class="pasal"><b>PIHAK KESATU</b> memberikan Hibah Daerah kepada PIHAK KEDUA, berupa barang ........................... sebanyak ........................... unit.</p></td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td class="valign-top" style="width: 30px;">(2)</td>
                        <td class="valign-top pasal"><p class="pasal">Hibah sebagaimana dimaksud pada ayat (1) dipergunakan untuk ...........................  dengan rincian sebagaimana tertuang dalam Rencana Anggaran Belanja/Rencana Kerja Anggaran menjadi Lampiran NPHD.</p></td>
                    </tr>
                    <tr>
                        <td class="valign-top">(3)</td>
                        <td class="valign-top pasal"><p class="pasal">Rencana Anggaran Belanja/Rencana Kerja Anggaran menjadi Lampiran NPHD sebagaimana dimaksud pada ayat (2) telah dialokasikan dalam Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur serta dituangkan dalam Keputusan Gubernur Jawa Timur tentang Penerima Hibah Berupa Barang.</p></td>
                    </tr>
                </table>
                <br>
                <!--PASAL 2-->
                <div class="page-break-inside-avoid">
                    <h4 class="text-center">Pasal 2<br>PENYERAHAN HIBAH DAERAH</h4>
                    <table>
                        <tr>
                            <td class="valign-top" style="width: 30px;">(1)</td>
                            <td class="valign-top pasal"><p class="pasal"><b>PIHAK KESATU</b> menyerahkan Hibah Daerah berupa barang kepada PIHAK KEDUA dalam kondisi/keadaan baik sekaligus sebanyak ………….. unit sesuai alokasi yang ditetapkan dalam Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur serta dituangkan dalam Keputusan Gubernur Jawa Timur tentang Penerima Hibah Berupa Barang.</p></td>
                        </tr>
                    </table>                    
                </div>
                <table>
                    <tr>
                        <td class="valign-top" style="width: 30px;">(2)</td>
                        <td class="valign-top">
                            <p class="pasal">Penyerahan Hibah Daerah berupa barang sebagaimana dimaksud pada ayat (1) disertai dengan Berita Acara Serah Terima Barang yang dilampiri:</p>
                        </td>
                    </tr>
                </table>
                <ol type="a" class="pasal" style="padding-left: 70px; padding-top: -10px;">
                    <li>NPHD;</li>
                    <li>Rencana Anggaran Biaya sesuai dengan yang dialokasikan dalam Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur serta dituangkan dalam Keputusan Gubernur Jawa Timur tentang Penerima Hibah Berupa Barang;</li>
                    <li>Foto copy rekening Penerima Hibah;</li>
                    <li>Pakta Integritas; dan</li>
                    <li>Foto copy Kartu Identitas pihak yang bertindak untuk dan atas nama <b>PIHAK KEDUA</b> dalam NPHD</li>
                </ol>
                <table style="padding-top: -10px;">
                    <tr>
                        <td class="valign-top" style="width: 30px;">(3)</td>
                        <td class="valign-top pasal"><p class="pasal">Penyerahan Hibah Daerah berupa barang sebagaimana dimaksud pada ayat (1) dilakukan setelah penandatanganan NPHD yang dişertai dengan Berita Acara Serah Terima Barang sebagaimana dimaksud pada ayat (2).</p></td>
                    </tr>
                </table>
                <br>
                <!--PASAL 3-->
                <div class="page-break-inside-avoid">
                    <h4 class="text-center">Pasal 3<br>KEWAJIBAN PIHAK KEDUA</h4>
                    <p class="pasal"><b>PIHAK KEDUA</b> berkewajiban:</p>
                </div>
                <ol type="a" class="pasal" style="padding-top: -25px;">
                    <li>Melaksanakan dan bertanggungjawab formal dan material atas penggunaan Hibah Daerah berupa barang sesuai dengan rincian sebagaimana tertuang dalam Rencana Anggaran Belanja/Rencana Kerja Anggaran yang menjadi Lampiran NPHD dengan mendasar pada alokasi dalam Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur serta dituangkan dalam Keputusan Gubernur Jawa Timur tentang Penerima Hibah Berupa Barang sejak ditandatanganinya NPHD dan/atau sejak diterbitkan Surat Perintah Pencairan Dana (SP2D);</li>
                    <li>Menandatangani Surat Pernyataan Tanggung Jawab Mutlak dan menjamin keabsahan dokumen yang dipersyaratkan dalam permohonan hibah.</li>
                    <li>Membuat dan menyampaikan laporan penggunaan Hibah Daerah berupa barang kepada <b>PIHAK KESATU</b> paling lambat 10 (sepuluh) hari setelah Hibah Daerah berupa barang diserahkan dari <b>PIHAK KESATU</b> kepada <b>PIHAK KEDUA</b>.</li>
                </ol>
                <br>
                <!--PASAL 4-->
                <div class="page-break-inside-avoid">
                    <h4 class="text-center">Pasal 4<br>HAK DAN KEWAJIBAN PIHAK KESATU</h4>
                    <table>
                        <tr>
                            <td class="valign-top" style="width: 30px;">(1)</td>
                            <td class="valign-top pasal"><p class="pasal"><b>PIHAK KESATU</b> berhak untuk tidak melakukan penyerahan Hibah Daerah berupa barang apabila <b>PIHAK KEDUA</b> tidak atau belum memenuhi persyaratan yang ditetapkan dan menyampaikan pemberitahuan kepada <b>PIHAK KEDUA</b>.</p></td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td class="valign-top" style="width: 30px;">(2)</td>
                        <td class="valign-top pasal"><p class="pasal"><b>PIHAK KESATU</b> berkewajiban melaksanakan monitoring dan evaluasi kelengkapan dokumen administrasi pertanggungjawaban atas penyerahan Hibah Daerah berupa barang sesuai ketentuan peraturan perundang-undangan yang berlaku.</p></td>
                    </tr>
                </table>
                <br>
                <!--PASAL 5-->
                <div class="page-break-inside-avoid">
                    <h4 class="text-center">Pasal 5<br>LAIN-LAIN</h4>
                    <table>
                        <tr>
                            <td class="valign-top" style="width: 30px;">(1)</td>
                            <td class="valign-top pasal"><p class="pasal">Perjanjian Hibah Daerah ini mulai berlaku sejak tanggal ditandatangani dan berakhir sampai dengan dilakukan kegiatan monitoring dan evaluasi.</p></td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td class="valign-top" style="width: 30px;">(2)</td>
                        <td class="valign-top pasal"><p class="pasal">NPHD ini dibuat dalam rangkap 3 (tiga) dengan rangkap kesatu dan kedua masing-masing bermaterai cukup dan mempunyai kekuatan hukum sama.</p></td>
                    </tr>
                </table>
            <?php } else if ($resultData->jenis_bantuan == 'Uang') { ?>
                <!--PASAL 1-->
                <div class="page-break-inside-avoid">
                    <h4 class="text-center">Pasal 1<br>JUMLAH DAN TUJUAN HIBAH</h4>
                    <table>
                        <tr>
                            <td class="valign-top" style="width: 30px;">(1)</td>
                            <td class="valign-top pasal"><p class="pasal"><b>PIHAK KESATU</b> memberikan Hibah Daerah kepada <b>PIHAK KEDUA</b> berupa uang sebesar Rp. <?php echo $resultData->nilai_anggaran; ?> (<?php echo $terbilang; ?>) yang bersumber dari Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur.</p></td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td class="valign-top" style="width: 30px;">(2)</td>
                        <td class="valign-top pasal"><p class="pasal">Hibah sebagaimana dimaksud pada ayat (1) dipergunakan untuk <?php echo $resultData->perihal; ?> dengan rincian sebagaimana tertuang dalam Rencana Anggaran Belanja/Rencana Kerja Anggaran menjadi Lampiran NPHD.</p></td>
                    </tr>
                    <tr>
                        <td class="valign-top">(3)</td>
                        <td class="valign-top pasal"><p class="pasal">Rencana Anggaran Belanja/Rencana Kerja Anggaran menjadi Lampiran NPHD sebagaimana dimaksud pada ayat (2) telah dialokasikan dalam Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur serta dituangkan dalam Keputusan Gubernur Jawa Timur tentang Penerima Hibah Berupa Uang.</p></td>
                    </tr>
                </table>
                <br>
                <!--PASAL 2-->
                <div class="page-break-inside-avoid">
                    <h4 class="text-center">Pasal 2<br>PENCAIRAN DANA HIBAH DAERAH</h4>
                    <table>
                        <tr>
                            <td class="valign-top" style="width: 30px;">(1)</td>
                            <td class="valign-top pasal"><p class="pasal">Pencairan dana Hibah Daerah berupa uang sebagaimana sebagaimana dimaksud dalam Pasal 1, dilakukan sekaligus sebesar Rp <?php echo $resultData->nilai_anggaran; ?> (<?php echo $terbilang; ?>) sesuai alokasi yang ditetapkan dalam Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur serta dituangkan dalam Keputusan Gubernur Jawa Timur tentang Penerima Hibah Berupa Uang.</p></td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td class="valign-top" style="width: 30px;">(2)</td>
                        <td class="valign-top pasal"><p class="pasal">Untuk pencairan dana Hibah Daerah berupa uang sebagaimana dimaksud pada ayat (1), <b>PIHAK KEDUA</b> mengajukan permohonan kepada <b>PIHAK KESATU</b> dengan dilampiri :</p></td>
                    </tr>
                </table>
                <ol type="a" class="pasal" style="padding-left: 70px; padding-top: -10px;">
                    <li>NPHD;</li>
                    <li>Rencana Anggaran Biaya sesuai dengan yang dialokasikan dalam Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur serta dituangkan dalam Keputusan Gubernur Jawa Timur tentang Penerima Hibah Berupa Uang;</li>
                    <li>Foto copy rekening Penerima Hibah; Bank Jatim <b><?php echo isset($resultNphd['cabang']) ? $resultNphd['cabang'] : ""; ?></b> Nomor Rekening <b><?php echo isset($resultNphd['no_rek']) ? $resultNphd['no_rek'] : ""; ?></b></li>
                    <li>Pakta Integritas; dan</li>
                    <li>Foto copy Kartu Identitas pihak yang bertindak untuk dan atas nama <b>PIHAK KEDUA</b> dalam NPHD</li>
                </ol>
                <br>
                <!--PASAL 2-->
                <div class="page-break-inside-avoid">
                    <h4 class="text-center" style="padding-top: -15px;">Pasal 3<br>KEWAJIBAN PIHAK KEDUA</h4>
                    <p class="pasal"><b>PIHAK KEDUA</b> berkewajiban:</p>
                </div>
                <ol type="a" class="pasal" style="padding-top: -25px;">
                    <li>Melaksanakan dan bertanggungjawab formal dan material atas pelaksanaan program dan kegiatan yang didanai dari hibah dengan jangka waktu pelaksanaan 3 (Tiga) bulan sesuai dengan rincian sebagaimana tertuang dalam Rencana Anggaran Belanja/Rencana Kerja Anggaran yang menjadi Lampiran NPHD dengan mendasar pada alokasi dalam Anggaran Pendapatan Belanja Daerah Provinsi Jawa Timur serta dituangkan dalam Keputusan Gubernur Jawa Timur tentang Penerima Hibah Berupa Uang sejak ditandatanganinya NPHD dan/atau sejak diterbitkan Surat Perintah Pencairan Dana (SP2D);</li>
                    <li>Melaksanakan pengadaan barang dan jasa sesuai dengan ketentuan perundang-undangan yang berlaku;</li>
                    <li>Menyimpan bukti-bukti transaksi terkait dengan program dan kegiatan yang bersumber dari dana hibah Daerah;</li>
                    <li>Membuat dan menyampaikan laporan penggunaan dana hibah Daerah kepada <b>PIHAK KESATU</b> paling lambat 10 (sepuluh) hari kerja setelah kegiatan yang bersumber dari hibah selesai dilaksanakan dengan melampirkan bukti pertanggungjawaban yang merupakan tanggung jawab mutlak <b>PIHAK KEDUA</b>;</li>
                    <li>Untuk kegiatan yang pencairan dana hibahnya dilakukan menjelang akhir tahun anggaran, pembuatan dan penyampaian laporan penggunaan dana hibah Daerah tidak melebihi tanggal 10 (sepuluh) Bulan Januari tahun anggaran berikutnya;</li>
                    <li>Menandatangani Surat Pernyataan Tanggung Jawab Mutlak dan menjamin keabsahan dokumen yang dipersyaratkan dalam permohonan hibah; dan</li>
                    <li>Menyetorkan kembali sisa dana hibah Daerah yang tidak dapat direalisasikan ke Rekening Kas Daerah Propinsi Jawa Timur Nomor: 0011000477 pada PT Bank Jatim dengan menggunakan Surat Tanda Setoran (STS) paling lambat 5 (lima) hari setelah laporan pertanggungjawaban disampaikan.</li>
                </ol>
                <br>
                <div class="page-break-inside-avoid">
                    <h4 class="text-center" style="padding-top: -15px;">Pasal 4<br>HAK DAN KEWAJIBAN PIHAK KESATU</h4>
                    <table>
                        <tr>
                            <td class="valign-top" style="width: 30px;">(1)</td>
                            <td class="valign-top pasal"><p class="pasal"><b>PIHAK KESATU</b> berhak untuk tidak melakukan pencairan atau menunda pencairan dana hibah apabila <b>PIHAK KEDUA</b> tidak atau belum memenuhi persyaratan yang ditetapkan dan menyampaikan pemberitahuan kepada <b>PIHAK KEDUA</b>.</p></td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td class="valign-top" style="width: 30px;">(2)</td>
                        <td class="valign-top pasal"><p class="pasal"><b>PIHAK KESATU</b> berkewajiban melaksanakan monitoring dan evaluasi kelengkapan dokumen administrasi pertanggungjawaban atas penggunaan dana hibah sesuai ketentuan peraturan perundang-undangan yang berlaku.</p></td>
                    </tr>
                </table>
                <br>
                <div class="page-break-inside-avoid">
                    <h4 class="text-center">Pasal 5<br>ADDENDUM</h4>
                    <table>
                        <tr>
                            <td class="valign-top" style="width: 30px;">(1)</td>
                            <td class="valign-top pasal"><p class="pasal">Dalam hal terjadi perubahan terhadap jumlah alokasi dana hibah yang bersumber dari Anggaran Pendapatan dan Belanjaan Daerah Provinsi Jawa Timur, <b>PARA PIHAK</b> melakukan addendum terhadap NPHD ini berkaitan dengan:</p></td>
                        </tr>
                    </table>                    
                </div>
                <ol type='a' class="pasal" style="padding-left: 70px; padding-top: -10px; padding-bottom: -10px;">
                    <li>Jumlah Hibah Daerah berupa uang yang diberikan <b>PIHAK KESATU</b> kepada <b>PIHAK KEDUA</b> sebagaimana diatur dalam Pasal 1 ayat (1) NPHD ini:</li>
                    <li>Perubahan Rencana Anggaran Belanja/Rencana Anggaran Kerja sebagai Lampiran Addendum NPHD ini dengan menyesuaikan jumlah Hibah Daerah berupa uang yang diberikan <b>PIHAK KESATU</b> kepada <b>PIHAK KEDUA</b> dengan berpedoman pada Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur yang dituangkan dalam Keputusan Gubernur Jawa Timur tentang Penerima Hibah Berupa Uang.</li>
                </ol>
                <table>
                    <tr>
                        <td class="valign-top" style="width: 30px;">(2)</td>
                        <td class="valign-top pasal"><p class="pasal">Terhadap tujuan penggunaan dana Hibah Daerah sebagaimana diatur dalam Pasal 1 ayat (2) NPHD ini tidak dapat dilakukan perubahan.</p></td>
                    </tr>
                    <tr>
                        <td class="valign-top">(3)</td>
                        <td class="valign-top pasal"><p class="pasal">Addendum terhadap NPHD sebagaimana dimaksud pada ayat (1) merupakan bagian yang tidak terpisahkan dari NPHD ini.</p></td>
                    </tr>
                </table>
                <br>
                <div class="page-break-inside-avoid">
                    <h4 class="text-center">Pasal 6<br>LAIN-LAIN</h4>
                    <table>
                        <tr>
                            <td class="valign-top" style="width: 30px;">(1)</td>
                            <td class="valign-top pasal"><p class="pasal">Perjanjian Hibah Daerah ini mulai berlaku sejak tanggal ditandatangani dan berakhir sampai dengan dilakukan kegiatan monitoring dan evaluasi.</p></td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td class="valign-top" style="width: 30px;">(2)</td>
                        <td class="valign-top pasal"><p class="pasal">NPHD ini dibuat dalam rangkap 3 (tiga) dengan rangkap kesatu dan kedua masing-masing bermaterai cukup dan mempunyai kekuatan hukum sama.</p></td>
                    </tr>
                </table>
            <?php } ?>
            <div class="page-break-inside-avoid">
                <table>
                    <?php if ($resultData->jenis_bantuan == 'Barang') { ?>
                        <tr>
                            <td class="valign-top" style="width: 30px;">(3)</td>
                            <td class="valign-top pasal"><p class="pasal">Hal-hal lain yang belum tercantum dalam NPHD ini dapat diatur lebih lanjut dalam Addendum NPHD sepanjang tidak bertentangan dengan ketentuan peraturan perundang-undangan yang berlaku dan berkedudukan sebagai bagian yang tidak terpisahkan dari NPHD ini.</p></td>
                        </tr>
                    <?php } else if ($resultData->jenis_bantuan == 'Uang') { ?>
                        <tr>
                            <td class="valign-top" style="width: 30px;">(3)</td>
                            <td class="valign-top pasal"><p class="pasal">Hal-hal lain yang belum tercantum dalam NPHD ini dapat diatur lebih lanjut dalam Addendum NPHD sepanjang tidak bertentangan dengan ketentuan peraturan perundang-undangan yang berlaku dan berkedudukan sebagai bagian yang tidak terpisahkan dari NPHD ini.</p></td>
                        </tr>
                    <?php } ?>
                </table>
                <br>
                <br>
                <table>
                    <tr>
                        <td class="valign-top text-center" style="width: 40%;">
                            <table>
                                <tr><td class="text-center"><b>PIHAK KEDUA</b></td></tr>
                                <tr><td class="text-center">&nbsp;</td></tr>
                                <tr><td class="text-center">&nbsp;</td></tr>
                                <tr><td class="text-center">Ketua <?php echo $pokmas->nama_kelompok; ?></td></tr>
                                <tr><td class="text-center">&nbsp;</td></tr>
                                <tr><td class="text-center">&nbsp;</td></tr>
                                <tr><td class="text-center">&nbsp;</td></tr>
                                <tr><td class="text-center">&nbsp;</td></tr>
                                <tr><td class="text-center">&nbsp;</td></tr>
                                <tr><td class="text-center">&nbsp;</td></tr>
                                <tr><td class="text-center">&nbsp;</td></tr>
                                <tr><td class="text-center">&nbsp;</td></tr>
                                <tr><td class="text-center"><u><b><?php echo $resultNphd['nama_ketua']; ?></b></u></td></tr>
                            </table>
                        </td>
                        <td class="valign-top text-center" style="width: 10%;"></td>
                        <td class="valign-top text-center" style="width: 50%;">
                            <table>
                                <tr><td class="text-center"><b>PIHAK KESATU</b></td></tr>
                                <tr><td class="text-center">&nbsp;</td></tr>
                                <tr><td class="text-center"><?php echo $getManagementSystem['pu_head_sign_1']; ?></td></tr>
                                <tr><td class="text-center"><?php echo $getManagementSystem['pu_head_sign_2']; ?></td></tr>
                                <tr><td class="text-center"><?php echo $getManagementSystem['pu_head_sign_3']; ?></td></tr>
                                <tr><td class="text-center">&nbsp;</td></tr>
                                <tr><td class="text-center">&nbsp;</td></tr>
                                <tr><td class="text-center">&nbsp;</td></tr>
                                <tr><td class="text-center">&nbsp;</td></tr>
                                <tr><td class="text-center">&nbsp;</td></tr>
                                <tr><td class="text-center">&nbsp;</td></tr>
                                <tr><td class="text-center">&nbsp;</td></tr>
                                <tr><td class="text-center"><u><b><?php echo $getManagementSystem['pu_nama_ketua']; ?></b></u></td></tr>
                                <tr><td class="text-center"><?php echo $getManagementSystem['pu_jabatan_ketua']; ?></td></tr>
                                <tr><td class="text-center">NIP. <?php echo $getManagementSystem['pu_nip']; ?></td></tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>