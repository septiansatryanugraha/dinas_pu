<html>
    <style>
        body {
            width: 100%;
            font-family: 'Bookman Old Style', sans-serif;
            font-size: 11pt;
        }
        .container {
            margin-left: 20px;
            margin-right: 20px;
        }
        table {
            width: 100%;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        .valign-top {
            vertical-align: top;
        }
    </style>
    <body>
        <div class="container">
            <h2 style="text-align: center;">SURAT PERNYATAAN<br><u>KETIDAKSANGGUPAN MELAKSANAKAN PEKERJAAN</u></h2>
            <br>
            <p class="pasal">Yang bertanda tangan dibawah ini :</p>
            <table class="pasal" style="padding-left: 50px;">
                <tr>
                    <td class="valign-top" style="width: 250px;">Nama</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">No KTP</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['no_ktp']) ? $resultNphd['no_ktp'] : ""; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">Alamat Rumah</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['alamat_rumah']) ? $resultNphd['alamat_rumah'] : ""; ?></td>
                </tr>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td class="valign-top">Nama Pokmas</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo $pokmas->nama_kelompok; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">Jabatan</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top">Ketua</td>
                </tr>
                <tr>
                    <td class="valign-top">Alamat Pokmas</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['alamat_rumah']) ? $resultNphd['alamat_rumah'] : ""; ?></td>
                </tr>
            </table>
            <p class="pasal">Berdasarkan Surat Keputusan Gubernur Jawa Timur tanggal <?php echo $tglSkGub; ?>, nomor <?php echo isset($resultNphd['sk_gub']) ? $resultNphd['sk_gub'] : ""; ?> perihal hibah untuk <?php echo $resultData->perihal; ?>.</p>
            <p class="pasal">Maka dengan ini saya menyatakan  :</p>
            <p class="pasal">Bahwa  <?php echo $pokmas->nama_kelompok; ?>  tidak sanggup untuk melaksanakan pekerjaan sesuai dengan item, volume dan kualitas pekerjaan sebagaimana yang tercantum dalam proposal.</p>
            <p class="pasal">Demikian Surat Pernyataan ini kami buat dengan pernuh rasa tanggung jawab dalam keadaan sadar serta sehat jasmani dan rohani, tidak berdasarkan paksaan dari pihak manapun dan dapat dijadikan sebagai bukti hukum di pengadilan apabila terjadi permasalahan di kemudian hari.</p>
            <table style="page-break-inside: avoid;">
                <tr>
                    <td class="valign-top" style="width: 50%;"></td>
                    <td class="valign-top" style="width: 50%;">
                        <table>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">Ketua <?php echo $pokmas->nama_kelompok; ?></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><u><b><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></b></u></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>