<html>
    <style>
        body {
            width: 100%;
            font-family: 'Bookman Old Style', sans-serif;
            font-size: 11pt;
        }
        .container {
            margin-left: 20px;
            margin-right: 20px;
        }
        table {
            width: 100%;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        .valign-top {
            vertical-align: top;
        }
        .text-center {
            text-align: center;
        }
    </style>
    <body>
        <div class="container">
            <h2 class="text-center"> K W I T A N S I </h2>
            <br>
            <table>
                <tr>
                    <td class="valign-top" style="width: 170px;"><b>Terima Dari</b></td>
                    <td class="valign-top"><b>:</b></td>
                    <td class="valign-top"><b>GUBERNUR JAWA TIMUR</b></td>
                </tr>
                <tr>
                    <td class="valign-top"><b>Terbilang</b></td>
                    <td class="valign-top"><b>:</b></td>
                    <td class="valign-top" style="border: 3px solid #000;"><b><?php echo $terbilang; ?></b></td>
                </tr>
                <tr>
                    <td class="valign-top"><b>Untuk Pembayaran</b></td>
                    <td class="valign-top"><b>:</b></td>
                    <td class="valign-top" class="pasal"><b>Bantuan Hibah kepada <?php echo $pokmas->nama_kelompok; ?> dalam rangka <?php echo $resultData->perihal; ?> di <?php echo isset($resultNphd['alamat_pokmas']) ? $resultNphd['alamat_pokmas'] : $pokmas->alamat; ?></b></td>
                </tr>
                <tr>
                    <td class="valign-top"><b>Jumlah</b></td>
                    <td class="valign-top"><b>:</b></td>
                    <td class="valign-top"><span style="padding-right: 100px; border: 3px solid #000;"><b>Rp. <?php echo $resultData->nilai_anggaran; ?>,-</b></span></td>
                </tr>
            </table>
            <p>&nbsp;</p>
            <table class="pasal" style="page-break-inside: avoid;">
                <tr>
                    <td class="valign-top" style="width: 35%;">
                        <table>
                            <tr><td class="text-center">Setuju dibayar</td></tr>
                            <tr><td class="text-center"><b><?php echo $getManagementSystem['jabatan_kepala']; ?></b></td></tr>
                            <tr><td class="text-center"><b>Dinas PU. Sumber Daya Air Provinsi Jawa Timur</b></td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center"><u><b><?php echo $getManagementSystem['nama_kepala']; ?></b></u></td></tr>
                            <tr><td class="text-center">NIP. <?php echo $getManagementSystem['nip_kepala']; ?></td></tr>
                        </table>
                    </td>
                    <td class="valign-top" style="width: 35%;">
                        <table>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">Lunas Bayar</td></tr>
                            <tr><td> Tgl.........................................</td></tr>
                            <tr><td class="text-center"><b><?php echo $getManagementSystem['jabatan_bendahara']; ?></b></td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center"><u><b><?php echo $getManagementSystem['nama_bendahara']; ?></b></u></td></tr>
                            <tr><td class="text-center">NIP. <?php echo $getManagementSystem['nip_bendahara']; ?></td></tr>
                        </table>
                    </td>
                    <td class="valign-top" style="width: 30%;">
                        <table>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td>Surabaya, <?php echo $tglNphd; ?></td></tr>
                            <tr><td class="text-center">Yang Menerima</td></tr>
                            <tr><td class="text-center"><b><?php echo $pokmas->nama_kelompok; ?><br>KETUA</b></td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center"><u><b><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></b></u></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>