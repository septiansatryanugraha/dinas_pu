<html>
    <style>
        body {
            width: 100%;
            font-family: 'Bookman Old Style', sans-serif;
            font-size: 10pt;
        }
        .container {
            margin-left: 20px;
            margin-right: 20px;
        }
        table {
            width: 100%;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        .valign-top {
            vertical-align: top;
        }
        .text-center {
            text-align: center;
        }
        .text-left {
            text-align: left;
        }
    </style>
    <body>
        <div class="container">
            <h2 style="text-align: center;">TANDA TERIMA<br><u>NASKAH PERJANJIAN HIBAH DAERAH</u></h2>
            <p>&nbsp;</p>
            <table class="pasal">
                <tr>
                    <td class="valign-top" style="width: 150px;">Nama Kegiatan</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><b><?php echo $resultData->perihal; ?></b></td>
                </tr>
                <tr>
                    <td class="valign-top">Tahun Anggaran</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo (strlen($resultNphd['tgl_nphd']) > 0) ? date("Y", strtotime($resultNphd['tgl_nphd'])) : '-'; ?></td>
                </tr>
            </table>
            <p>&nbsp;</p>
            <table class="table-terima" border="1">
                <tr>
                    <th class="valign-top"><b>NO.</b></th>
                    <th class="valign-top"><b>Tanggal dan No. Surat</b></th>
                    <th class="valign-top"><b>Kepada</b></th>
                    <th class="valign-top"><b>Keterangan</b></th>
                </tr>
                <tr>
                    <td class="valign-top">1.</td>
                    <td class="valign-top"><?php echo $tglNphd; ?> / <?php echo isset($resultNphd['no_nphd']) ? $resultNphd['no_nphd'] : ""; ?></td>
                    <td class="valign-top">Ketua <?php echo $pokmas->nama_kelompok; ?></td>
                    <td></td>
                </tr>
            </table>
            <table style="page-break-inside: avoid;">
                <tr>
                    <td class="valign-top" style="width: 40%;">
                        <table>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-left">Telp. / HP</td></tr>
                            <tr><td class="text-left"><?php echo isset($resultNphd['telp']) ? (strlen($resultNphd['telp']) > 0) ? $resultNphd['telp'] : "……………………………………" : "……………………………………"; ?></td></tr>
                        </table>
                    </td>
                    <td class="valign-top" style="width: 20%; text-align: center;"></td>
                    <td class="valign-top" style="width: 40%;">
                        <table>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">Surabaya, <?php echo $tglNphd; ?></td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">Yang Menerima</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center"><u><b><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></b></u></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>