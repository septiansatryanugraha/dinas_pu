<html>
    <style>
        body {
            width: 100%;
            font-family: 'Arial', sans-serif;
            font-size: 11pt;
        }
        .container {
            margin-left: 20px;
            margin-right: 20px;
        }
        table {
            width: 100%;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        .valign-top {
            vertical-align: top;
        }
        .text-center {
            text-align: center;
        }
    </style>
    <body>
        <div>
            <img src="<?php echo base_url(); ?>assets/tambahan/gambar/kop-pu_sda.png" width="680px">
            <br><br>
            <div class="container">
                <table>
                    <tr>
                        <td class="valign-top" style="width: 150px;">Nomor SKGUB</td>
                        <td class="valign-top" style="width: 50px;">:</td>
                        <td class="valign-top"><?php echo isset($resultNphd['sk_gub']) ? $resultNphd['sk_gub'] : ""; ?></td>
                    </tr>
                    <tr>
                        <td class="valign-top">Tanggal SKGUB</td>
                        <td class="valign-top">:</td>
                        <td class="valign-top"><?php echo isset($resultNphd['tgl_sk_gub']) ? $resultNphd['tgl_sk_gub'] : ""; ?></td>
                    </tr>
                    <tr>
                        <td class="valign-top">Nomor NPHD</td>
                        <td class="valign-top">:</td>
                        <td class="valign-top"><?php echo isset($resultNphd['no_nphd']) ? $resultNphd['no_nphd'] : ""; ?></td>
                    </tr>
                    <tr>
                        <td class="valign-top">Tanggal NPHD</td>
                        <td class="valign-top">:</td>
                        <td class="valign-top"><?php echo isset($resultNphd['tgl_nphd']) ? $resultNphd['tgl_nphd'] : ""; ?></td>
                    </tr>
                </table>
                <br>
                <table>
                    <tr>
                        <td class="valign-top" style="width: 65%">
                            <table border="1">
                                <tr>
                                    <td class="text-center" style="font-size: 13pt;">PENERIMA DATA BANTUAN</td>
                                </tr>
                            </table>
                            <table border="1">
                                <tr>
                                    <td class="valign-top" style="height: 650px;">
                                        <table style="line-height: 1.7;">
                                            <tr>
                                                <td class="valign-top" style="width: 180px;">Nama Pokmas</td>
                                                <td class="valign-top">:</td>
                                                <td class="valign-top"><?php echo $resultData->nama_kelompok; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="valign-top" style="width: 180px;">Nama</td>
                                                <td class="valign-top">:</td>
                                                <td class="valign-top"><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="valign-top">NIK</td>
                                                <td class="valign-top">:</td>
                                                <td class="valign-top"><?php echo isset($resultNphd['no_ktp']) ? $resultNphd['no_ktp'] : ""; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="valign-top">Alamat Rumah</td>
                                                <td class="valign-top">:</td>
                                                <td class="valign-top">Desa <?php echo $resultData->desa; ?> Kec. <?php echo $resultData->kecamatan; ?> Kab. <?php echo $resultData->kabupaten; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="valign-top">Nomor Telepon Rumah / HP</td>
                                                <td class="valign-top">:</td>
                                                <td class="valign-top"><?php echo isset($resultNphd['telp']) ? $resultNphd['telp'] : ""; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="valign-top">Jabatan</td>
                                                <td class="valign-top">:</td>
                                                <td class="valign-top">KETUA</td>
                                            </tr>
                                            <tr>
                                                <td class="valign-top">Alamat Lembaga</td>
                                                <td class="valign-top">:</td>
                                                <td class="valign-top"><?php echo isset($resultNphd['alamat_rumah']) ? $resultNphd['alamat_rumah'] : ""; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="valign-top">Perihal</td>
                                                <td class="valign-top">:</td>
                                                <td class="valign-top"><?php echo $resultData->perihal; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="valign-top">Nominal Bantuan</td>
                                                <td class="valign-top">:</td>
                                                <td class="valign-top">Rp. <?php echo $resultData->nilai_anggaran; ?> ( <?php echo $terbilang; ?> )</td>
                                            </tr>
                                            <tr>
                                                <td class="valign-top">Pemilik Rekening</td>
                                                <td class="valign-top">:</td>
                                                <td class="valign-top"><?php echo isset($resultNphd['nama_rek']) ? $resultNphd['nama_rek'] : ""; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="valign-top">Cabang Bank</td>
                                                <td class="valign-top">:</td>
                                                <td class="valign-top"><?php echo isset($resultNphd['cabang']) ? $resultNphd['cabang'] : ""; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="valign-top">Nomor Rekening</td>
                                                <td class="valign-top">:</td>
                                                <td class="valign-top"><?php echo isset($resultNphd['no_rek']) ? $resultNphd['no_rek'] : ""; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="valign-top">Status</td>
                                                <td class="valign-top">:</td>
                                                <td class="valign-top"><b>TELAH MENERIMA DANA BANTUAN</b></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="valign-top" style="width: 30%">
                            <table border="1">
                                <tr>
                                    <td class="text-center" style="font-size: 13pt;">FOTO PENERIMA NPHD</td>
                                </tr>
                            </table>
                            <table border="1">
                                <tr>
                                    <td class="text-center">
                                        <?php if ($resultNphd['file_img_pokmas_1'] != null) { ?>
                                            <img class="img-thumbnail gambar2" style="margin-top: 10px; margin-bottom: 10px; width: 200px;" src="<?php echo base_url($resultNphd['file_img_pokmas_1']) ?>">
                                        <?php } else { ?>
                                            <img class="img-thumbnail" style="margin-top: 10px; margin-bottom: 10px; width: 200px;" src="<?php echo base_url(); ?>assets/tambahan/gambar/no-image.jpg" width="190px"/>
                                        <?php } ?>
                                    </td>
                                </tr>
                            </table>
                            <table border="1">
                                <tr>
                                    <td class="text-center" style="font-size: 13pt;">FOTO PENERIMA NPHD</td>
                                </tr>
                            </table>
                            <table border="1">
                                <tr>
                                    <td class="text-center">
                                        <?php if ($resultNphd['file_img_pokmas_2'] != null) { ?>
                                            <img class="img-thumbnail gambar2" style="margin-top: 10px; margin-bottom: 10px; width: 200px;" src="<?php echo base_url($resultNphd['file_img_pokmas_2']) ?>">
                                        <?php } else { ?>
                                            <img class="img-thumbnail " style="margin-top: 10px; margin-bottom: 10px; width: 200px;" src="<?php echo base_url(); ?>assets/tambahan/gambar/no-image.jpg" width="190px"/>
                                        <?php } ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>