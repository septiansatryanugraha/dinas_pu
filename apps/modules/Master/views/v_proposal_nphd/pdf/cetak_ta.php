<html>
    <style>
        body {
            width: 100%;
            font-family: 'Bookman Old Style', sans-serif;
            font-size: 10pt;
        }
        .container {
            margin-left: 20px;
            margin-right: 20px;
        }
        table {
            width: 100%;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        .valign-top {
            vertical-align: top;
        }
    </style>
    <body>
        <div class="container">
            <h2 style="text-align: center;">SURAT PERNYATAAN<br>TIDAK MENERIMA HIBAH DARI APBD PROVINSI<br>PADA TAHUN ANGGARAN SEBELUMNYA</h2>
            <p>&nbsp;</p>
            <p class="pasal">Yang bertanda tangan dibawah ini :</p>
            <table class="pasal" style="padding-left: 50px;">
                <tr>
                    <td class="valign-top" style="width: 250px;">Nama</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">No KTP</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['no_ktp']) ? $resultNphd['no_ktp'] : ""; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">Alamat Rumah</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['alamat_pokmas']) ? $resultNphd['alamat_pokmas'] : $pokmas->alamat; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">Nama Badan/Lembaga/Ormas</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo $pokmas->nama_kelompok; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">Jabatan Dalam Organisasi</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top">Ketua</td>
                </tr>
                <tr>
                    <td class="valign-top">Alamat Lembaga</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['alamat_pokmas']) ? $resultNphd['alamat_pokmas'] : $pokmas->alamat; ?></td>
                </tr>
            </table>
            <p class="pasal">Menyatakan dengan sesungguhnya bahwa <?php echo $pokmas->nama_kelompok; ?> sebagaimana tersebut diatas tidak pernah menerima hibah dari Pemerintah Provinsi Jawa Timur pada Tahun Anggaran <?php echo isset($resultNphd['tgl_sk_gub']) ? date("Y", strtotime($resultNphd['tgl_sk_gub'] . "-1 year")) : "sebelumnya"; ?>.</p>
            <p class="pasal">Demikian surat pernyataan ini dibuat dengan sebenarnya.</p>
            <table style="page-break-inside: avoid;">
                <tr>
                    <td class="valign-top" style="width: 50%;"></td>
                    <td class="valign-top" style="width: 50%;">
                        <table>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">Surabaya, <?php echo $tglNphd; ?></td></tr>
                            <tr><td style="text-align: center">Ketua <?php echo $pokmas->nama_kelompok; ?></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center; font-size: 12px;">MATERAI</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><u><b><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></b></u></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>