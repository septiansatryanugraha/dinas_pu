<html>
    <style>
        body {
            width: 100%;
            font-family: 'Bookman Old Style', sans-serif;
            font-size: 11pt;
        }
        .container {
            margin-left: 20px;
            margin-right: 20px;
        }
        table {
            width: 100%;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        .valign-top {
            vertical-align: top;
        }
        .text-center {
            text-align: center;
        }
    </style>
    <body>
        <div class="container">
            <h2 class="text-center">SURAT PERNYATAAN<br>TIDAK MENERIMA ANGGARAN DARI SUMBER LAIN</h2><hr/>
            <br>
            <p class="pasal">Yang bertanda tangan dibawah ini :</p>
            <table class="pasal" style="padding-left: 50px;">
                <tr>
                    <td class="valign-top" style="width: 150px;">Nama</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">Alamat</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['alamat_rumah']) ? $resultNphd['alamat_rumah'] : ""; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">Jabatan</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top">Ketua</td>
                </tr>
            </table>
            <p class="pasal">Dengan ini menyatakan dengan sebenar-benarnya bahwa Kelompok Masyarakat (Pokmas) yang tercantum sebagai berikut :</p>
            <table class="pasal" style="padding-left: 50px;">
                <tr>
                    <td class="valign-top" style="width: 150px;">Nama Pokmas</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo $pokmas->nama_kelompok; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">Alamat Pokmas</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['alamat_rumah']) ? $resultNphd['alamat_rumah'] : ""; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">Kegiatan</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo $resultData->perihal; ?></td>
                </tr>
                <tr>
                    <td class="valign-top">Lokasi Kegiatan</td>
                    <td class="valign-top">:</td>
                    <td class="valign-top"><?php echo isset($resultNphd['alamat_pokmas']) ? $resultNphd['alamat_pokmas'] : $pokmas->alamat; ?></td>
                </tr>
            </table>
            <p class="pasal">Tidak menerima anggaran dari sumber anggaran lain (anggaran ganda/double account) untuk paket pekerjaan tersebut.</p>
            <p class="pasal">Demikian surat pernyataan ini dibuat dengan sebenarnya.</p>
            <table style="page-break-inside: avoid;">
                <tr>
                    <td class="valign-top" style="width: 50%;"></td>
                    <td class="valign-top" style="width: 50%;">
                        <table>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">Surabaya, <?php echo $tglNphd; ?></td></tr>
                            <tr><td class="text-center">Ketua <?php echo $pokmas->nama_kelompok; ?></td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center" style="font-size: 12px;">MATERAI</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center">&nbsp;</td></tr>
                            <tr><td class="text-center"><u><b><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></b></u></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>