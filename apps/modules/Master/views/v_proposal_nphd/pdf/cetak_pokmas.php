<html>
    <style>
        body {
            width: 100%;
            font-family: 'Bookman Old Style', sans-serif;
            font-size: 11pt;
        }
        .container {
            margin-left: 20px;
            margin-right: 20px;
        }
        table {
            width: 100%;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        .valign-top {
            vertical-align: top;
        }
    </style>
    <body>
        <div class="container">
            <?php
            $Replace = $resultData->perihal;
            $ReplaceText = str_replace('Permohonan', 'Bantuan', $Replace);

            ?>
            <h2 style="text-align: center;"><?php echo $pokmas->nama_kelompok; ?><br><?php echo isset($resultNphd['alamat_pokmas']) ? $resultNphd['alamat_pokmas'] : $pokmas->alamat; ?></h2><hr/>
            <!-- <br><?php echo ucwords(strtolower(trim($resultData->kabupaten))); ?> -->
            <table class="pasal">
                <tr>
                    <td class="valign-top" style="width: 60%;">
                        <table>
                            <tr><td colspan="3">&nbsp;</td></tr>
                            <tr><td colspan="3">&nbsp;</td></tr>
                            <tr>
                                <td class="valign-top">Nomor</td>
                                <td class="valign-top">:</td>
                                <td class="valign-top"><?php echo $resultData->no_surat; ?></td>
                            </tr>
                            <tr>
                                <td class="valign-top">Lampiran</td>
                                <td class="valign-top">:</td>
                                <td class="valign-top">1 (satu) berkas</td>
                            </tr>
                            <tr>
                                <td class="valign-top">Perihal</td>
                                <td class="valign-top">:</td>
                                <td class="valign-top">Bantuan Hibah Uang</td>
                            </tr>
                        </table>
                    </td>
                    <td class="valign-top" style="width: 40%;">
                        <table>
                            <tr><td colspan="2">&nbsp;</td></tr>
                            <tr>
                                <td class="valign-top" colspan="2">Kepada :</td>
                            </tr>
                            <tr>
                                <td class="valign-top">Yth.</td>
                                <td class="valign-top">Ibu Gubernur Jawa Timur</td>
                            </tr>
                            <tr>
                                <td class="valign-top"></td>
                                <td class="valign-top">Jl. Pahlawan 110</td>
                            </tr>
                            <tr>
                                <td class="valign-top"></td>
                                <td class="valign-top">di</td>
                            </tr>
                            <tr>
                                <td class="valign-top"></td>
                                <td class="valign-top"><span style="padding-left: 20px;">&nbsp;</span>SURABAYA</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <p class="pasal">
                <span style="padding-left: 50px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Sehubungan dengan Surat Gubernur Jawa Timur tanggal <?php echo $tglSurat; ?> Nomor : <?php echo isset($resultNphd['no_surat_ke_pokmas']) ? $resultNphd['no_surat_ke_pokmas'] : ""; ?>, maka kami sampaikan terima kasih yang sebesar-besarnya kepada Ibu yang berkenan menyetujui permohonan hibah sebesar Rp. <?php echo $resultData->nilai_anggaran; ?>,-  untuk kegiatan sesuai usulan kami dalam proposal .<br>
                <span style="padding-left: 50px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Berkenan dengan hal tersebut, bersama ini kami sampaikan kelengkapan persyaratan sebagai berikut :
            </p>
            <ol>
                <li>Rencana penggunaan dana (RAB) sebagai bahan pengendalian;</li>
                <li>Copy rekening Bank Jatim atas nama <?php echo isset($resultNphd['nama_rek']) ? $resultNphd['nama_rek'] : ""; ?>.</li>
                <li>Kwitansi asli rangkap 3 (tiga), lembar kesatu bermaterai Rp. 10.000,- yang telah ditandatangani dan distempel;</li>
                <li>Pakta Integritas bermaterai Rp. 10.000,- yang telah ditandatangani dan distempel;</li>
                <li>Naskah Perjanjian Hibah Daerah (NPHD) bermaterai Rp. 10.000,- yang telah ditandatangani dan distempel.</li>
            </ol>
            <p class="pasal">
                <span style="padding-left: 50px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Adapun rencana penggunaan dana (RAB) kami lampirkan bersama surat ini, sedangkan laporan dan pertanggungjawaban realisasi penggunaan dananya akan kami sampaikan setelah diterimanya bantuan ini, kepada Gubernur Jawa Timur melalui Dinas Pekerjaan Umum Sumber Daya Air Provinsi Jawa Timur, Jalan Gayung Kebonsari No. 169 Surabaya selambat - lambatnya 3 (tiga) bulan setelah diterimanya dana bantuan.<br>
                <span style="padding-left: 50px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>Demikian untuk menjadi maklum dan terima kasih atas perkenannya.
            </p>
            <table style="page-break-inside: avoid;">
                <tr>
                    <td class="valign-top" style="width: 50%;"></td>
                    <td class="valign-top" style="width: 50%; text-align: center;">
                        <table>
                            <tr><td style="text-align: center">Surabaya, <?php echo $tglSuratPokmas; ?></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">Ketua <?php echo $pokmas->nama_kelompok; ?></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><u><b><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></b></u></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>