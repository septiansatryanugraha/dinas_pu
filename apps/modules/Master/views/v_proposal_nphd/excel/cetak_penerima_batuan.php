<style>
    .text-center {
        text-align: center;
    }
    .vertical-top {
        vertical-align: top;
    }
    .table-header {
        text-align: center;
        vertical-align: middle;
    }
    .body-money {
        text-align: right;
        vertical-align: top;
    }
</style>
<table>
    <tr><td class="text-center" colspan="21"><b>DAFTAR PENERIMA BANTUAN SOSIAL/BANTUAN KEUANGAN/SUBSIDI</b></td></tr>
    <tr><td class="text-center" colspan="21"><b>YANG DIEVALUASI OLEH DINAS PEKERJAAN UMUM SUMBER DAYA AIR PROVINSI JAWA TIMUR</b></td></tr>
    <tr><td class="text-center" colspan="21"><b>TAHUN ANGGARAN <?= $start_year ?> SAMPAI DENGAN <?= strtoupper($tanggal_akhir_indo) ?> </b></td></tr>
</table>
<table border="1">
    <thead>
        <tr>
            <th class="table-header" rowspan="2"><b>NO</b></th>
            <th class="table-header" rowspan="2"><b>KODE PROPOSAL</b></th>
            <th class="table-header" rowspan="2"><b>PENERIMA</b></th>
            <th class="table-header" colspan="3"><b>ALAMAT</b></th>
            <th class="table-header" rowspan="2"><b>PERUNTUKAN</b></th>
            <th class="table-header" rowspan="2"><b>NILAI BANTUAN</b></th>
            <th class="table-header" colspan="2"><b>SK GUBERNUR</b></th>
            <th class="table-header" colspan="2"><b>NPHD</b></th>
            <th class="table-header" colspan="3"><b>SP2D</b></th>
            <th class="table-header" colspan="5"><b>Realisasi Laporan Pertanggungjawaban</b></th>
            <th class="table-header" rowspan="2"><b>Pengembalian Dana Bantuan</b></th>
        </tr>
        <tr>
            <th class="table-header"><b>DESA/KEL</b></th>
            <th class="table-header"><b>KECAMATAN</b></th>
            <th class="table-header"><b>KABUPATEN</b></th>
            <th class="table-header"><b>No.</b></th>
            <th class="table-header"><b>Tanggal</b></th>
            <th class="table-header"><b>No.</b></th>
            <th class="table-header"><b>Tanggal</b></th>
            <th class="table-header"><b>No.</b></th>
            <th class="table-header"><b>Tanggal</b></th>
            <th class="table-header"><b>Nilai</b></th>
            <th class="table-header"><b>Sudah/Belum</b></th>
            <th class="table-header"><b>Tanggal LPJ</b></th>
            <th class="table-header"><b>Nomor LPJ</b></th>
            <th class="table-header"><b>Tanggal LPJ Diterima</b></th>
            <th class="table-header"><b>Nilai yang di-LPJ-kan</b></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($results as $key => $value) { ?>
            <?php $nphd = json_decode($value['nphd'], TRUE); ?>
            <tr>
                <td class="vertical-top"><?= ($key + 1) ?></td>
                <td class="vertical-top"><?= $value['kode_proposal'] ?></td>
                <td class="vertical-top"><?= $value['nama_kelompok'] ?></td>
                <td class="vertical-top"><?= $value['desa'] ?></td>
                <td class="vertical-top"><?= $value['kecamatan'] ?></td>
                <td class="vertical-top"><?= $value['kabupaten'] ?></td>
                <td class="vertical-top"><?= $value['perihal'] ?></td>
                <td class="body-money"><?= $value['nilai_anggaran'] ?></td>
                <td class="vertical-top"><?= $nphd['sk_gub'] ?></td>
                <td class="vertical-top"><?= $nphd['tgl_sk_gub'] ?></td>
                <td class="vertical-top"><?= $nphd['no_nphd'] ?></td>
                <td class="vertical-top"><?= $nphd['tgl_nphd'] ?></td>
                <td class="vertical-top"><?= $nphd['sk_sp2d'] ?></td>
                <td class="vertical-top"><?= $nphd['tgl_sp2d'] ?></td>
                <td class="body-money"><?= $nphd['nominal_sp2d'] ?></td>
                <td class="vertical-top"><?= $nphd['status_penerimaan'] ?></td>
                <td class="vertical-top"><?= $nphd['tgl_lpj'] ?></td>
                <td class="vertical-top"><?= $nphd['no_lpj'] ?></td>
                <td class="vertical-top"><?= $nphd['tgl_lpj_diterima'] ?></td>
                <td class="body-money"><?= $nphd['nominal_lpj'] ?></td>
                <td></td>
            </tr>
        <?php } ?>
    </tbody>
</table>