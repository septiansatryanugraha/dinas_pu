<?php $this->load->view('_heading/_headerContent') ?>

<style>
    .font-data{
        font-family: Futura,Trebuchet MS,Arial,sans-serif; 
        color:red;
        font-size: 13px;
    }
    .kiri {
        margin-left: -10px;
    }
    .gap {
        margin-top: 30px;
    }
</style>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="row">
        <form class="form-horizontal" enctype="multipart/form-data" id="form-ubah" method="POST">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="nav-tabs-custom">
                        <div class="box-header with-border">
                            <h3 class="box-title">LPJ</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">No LPJ</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="no_lpj" id="no_lpj" placeholder="Nomor LPJ" value="<?php echo isset($resultNphd['no_lpj']) ? $resultNphd['no_lpj'] : ""; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Tanggal LPJ</label>
                                <div class="col-sm-5">
                                    <input type="text" style="background: #FFF;" name="tgl_lpj" id="tgl_lpj" class="form-control datepicker" value="<?php echo isset($resultNphd['tgl_lpj']) ? (strlen($resultNphd['tgl_lpj']) > 0) ? date('d-m-Y', strtotime($resultNphd['tgl_lpj'])) : "" : ""; ?>" readOnly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Tanggal LPJ Diterima</label>
                                <div class="col-sm-5">
                                    <input type="text" style="background: #FFF;" name="tgl_lpj_diterima" id="tgl_lpj_diterima" class="form-control datepicker" value="<?php echo isset($resultNphd['tgl_lpj_diterima']) ? (strlen($resultNphd['tgl_lpj_diterima']) > 0) ? date('d-m-Y', strtotime($resultNphd['tgl_lpj_diterima'])) : "" : ""; ?>" readOnly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nilai / Nominal</label>
                                <div class="col-sm-5">
                                    <input type="text" name="nominal_lpj" id="nominal_lpj" class="form-control format_currency" value="<?php echo isset($resultNphd['nominal_lpj']) ? $resultNphd['nominal_lpj'] : ""; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Status penerimaan</label>
                                <div class="col-sm-5">
                                    <select name="status_penerimaan" class="form-control selek-status" id="status_penerimaan">
                                        <option></option>
                                        <option value="Belum"<?php echo ($resultNphd['status_penerimaan'] == "Belum") ? "selected='selected'" : ""; ?>>Belum</option>
                                        <option value="Sudah"<?php echo ($resultNphd['status_penerimaan'] == "Sudah") ? "selected='selected'" : ""; ?>>Sudah</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-7">
                                <label>Upload LPJ</label>
                                <input type="file" class="upload-pdf" name="file_upload[]" aria-describedby="sizing-addon2" id="file_lpj">
                                <?php if (isset($resultNphd['nama_file_lpj'])) { ?>
                                    <a href="<?php echo base_url() . $resultNphd['file_lpj']; ?>" target="_blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-file-pdf" aria-hidden="true"></i> <?php echo $resultNphd['nama_file_lpj']; ?></font></b></a>
                                <?php } ?>
                            </div>
                            <div class="form-group col-md-2" style="margin-top: 20px;">
                                <small class="label pull-center bg-red">format pdf</small>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div id="buka"> 
                                <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                                <a class="klik ajaxify" href="<?php echo site_url('master-proposal-nphd'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                            </div>
                            <div id="btn_loading">
                                <button name="submit" type="submit" class="btn btn-primary btn-flat animated-gradient">&nbsp;Tunggu..</button>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="nav-tabs-custom">
                        <div class="box-header with-border">
                            <h3 class="box-title">SP2D</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">SK SP2D</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="sk_sp2d" id="sk_sp2d" placeholder="SP2D" value="<?php echo isset($resultNphd['sk_sp2d']) ? $resultNphd['sk_sp2d'] : ""; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Tanggal SP2D</label>
                                <div class="col-sm-5">
                                    <input type="text" style="background: #FFF;" name="tgl_sp2d" id="tgl_sp2d" class="form-control datepicker" value="<?php echo isset($resultNphd['tgl_sp2d']) ? (strlen($resultNphd['tgl_sp2d']) > 0) ? date('d-m-Y', strtotime($resultNphd['tgl_sp2d'])) : "" : ""; ?>" readOnly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nilai / Nominal</label>
                                <div class="col-sm-5">
                                    <input type="text" name="nominal_sp2d" class="form-control format_currency" id="nominal_sp2d" value="<?php echo isset($resultNphd['nominal_sp2d']) ? $resultNphd['nominal_sp2d'] : ""; ?>">
                                </div>
                            </div>
                            <div class="form-group col-md-7">
                                <label>Upload SP2D</label>
                                <input type="file" class="upload-pdf" name="file_upload[]" aria-describedby="sizing-addon2" id="file_sp2d">
                                <?php if (isset($resultNphd['nama_file_sp2d'])) { ?>
                                    <a href="<?php echo base_url() . $resultNphd['file_sp2d']; ?>" target="_blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-file-pdf" aria-hidden="true"></i> <?php echo $resultNphd['nama_file_sp2d']; ?></font></b></a>
                                <?php } ?>
                            </div>
                            <div class="form-group col-md-2" style="margin-top: 20px;">
                                <small class="label pull-center bg-red">format pdf</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>  

<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-ubah').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url('process-lpj-proposal-nphd') . '/' . $resultData->id_proposal; ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?php echo base_url('master-proposal-nphd'); ?>'", 450);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    $(function () {
        $(".selek-status").select2({
            placeholder: " -- Pilih Status -- "
        });
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        });
        fileValidationPdf(9007200, '9 M');
    });
</script>