<?php $this->load->view('_heading/_headerContent') ?>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <div class="box">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom" id="newContain">
                    <div class="modal-header">
                        <h4 class="modal-title">Import Data</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="form-import" method="post" enctype="multipart/form-data" role="form">
                            <div class="form-group">
                                <label class="col-sm-1 control-label">Grup</label>
                                <div class="col-sm-3">
                                    <select name="id_grup" class="form-control select-grup" id="id_grup">
                                        <option></option>
                                        <?php foreach ($selectGrup as $data) { ?>
                                            <option value="<?php echo $data->grup_id; ?>">
                                                <?php echo $data->nama_grup; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-1 control-label">File</label>
                                <div class="col-sm-7">
                                    <input type="file" class="form-control" name="excel" id="excel" aria-describedby="sizing-addon2">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div id='btn_loading'>
                                    <button type='button' class='btn btn-success' disabled><i class='fa fa-spinner fa-spin'></i> &nbsp;Tunggu..</button>
                                </div>
                                <div id="buka">
                                    <button class="btn btn-success" type="submit" name="simpan" id="simpan"><i class="fa fa-upload" aria-hidden="true"></i>&nbsp; Upload</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    //Proses Controller logic ajax
    $("#form-import").submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url('import-data-nphd'); ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-import").reset();
                    $("#buka").show();
                    $("#btn_loading").hide();
                    $("#myModal").hide();
                    $("#myModal").modal('hide');
                    swal("Success", result.pesan, "success");
                    reload_table();

                } else if (result.status == 'awas') {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    $("#myModal").hide();
                    $("#myModal").modal('hide');
                    swal("Warning", result.pesan, "warning");
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    $("#myModal").hide();
                    $("#myModal").modal('hide');
                    swal("Warning", result.pesan, "warning");
                }
            });
        });
    });

    $(function () {
        $(".select-grup").select2({
            placeholder: " -- Pilih Grup -- "
        });
    });
</script>