<html>
    <style>
        body {
            font-family: 'Bookman Old Style', sans-serif;
        }
        page {
            font-family: 'Bookman Old Style', sans-serif;
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
        }
        page[size="A4"] {  
            width: 21cm;
        }
        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
            }
        }
        .container {
            width: 100%;
            margin-top: 30px;
            margin-left: 70px;
        }
        button {
            width: 150px;
            height: 30px;
        }
        @media print {
            .samping {display:none;}
        }
        .button {
            width: 100%;
            position: relative;
        }
        .right {
            float: right;
            margin-right: 35%;
            display: block;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        ol {
            margin: 0;
        }
        table {
            width: 100%;
        }
    </style>

    <page size="A4">
        <div class="container">
            <h2 style="text-align: center;">PAKTA INTEGRITAS HIBAH</h2>
            <p>&nbsp;</p>
            <p class="pasal">Yang bertanda tangan dibawah ini :</p>
            <table class="pasal" style="padding-left: 50px;">
                <tr>
                    <td style="vertical-align: top;">Nama</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">No KTP</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><?php echo isset($resultNphd['no_ktp']) ? $resultNphd['no_ktp'] : ""; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Alamat Rumah</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><?php echo isset($resultNphd['alamat_rumah']) ? $resultNphd['alamat_rumah'] : ""; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Jabatan Dalam Organisasi</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;">Ketua</td>
                </tr>`
                <tr>
                    <td style="vertical-align: top;">Alamat Lembaga</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><?php echo isset($resultNphd['alamat_rumah']) ? $resultNphd['alamat_rumah'] : ""; ?></td>
                </tr>
            </table>
            <p class="pasal">yang bertindak untuk dan atas nama <?php echo $pokmas->nama_kelompok; ?></p>
            <p class="pasal">Dengan ini saya menyatakan dengan sebenarnya :</p>
            <ol class="pasal">
                <li>Usulan/Proposal/RAB kegiatan <?php echo $resultData->perihal; ?> yang diajukan kepada Gubernur Jawa Timur untuk mendapatkan hibah, akan kami laksanakan sesuai dengan rencana kegiatan sebagaimana tertuang dalam usulan/proposal dimaksud;</li>
                <li>Hibah dari Pemerintah Provinsi Jawa Timur sebesar Rp. <?php echo $resultData->nilai_anggaran; ?>,- (<?php echo $terbilang; ?>) akan kami gunakan sesuai dengan Rencana Anggaran Biaya (RAB), sebagaimana terlampir dalam proposal;</li>
                <li>Dalam realisasinya, kami berjanji akan melaksanakan tugas/pekerjaan secara profesional dengan menggunakan sumberdaya secara optimal untuk memberikan hasil kerja terbaik;</li>
                <li>Pakta Integritas ini berlaku sejak tanggal ditandatangani dan berakhir sampai dengan laporan pertanggungjawaban diterima oleh Pemerintah Provinsi Jawa Timur selama tidak terjadi penyimpangan;</li>
                <li>Apabila melanggar hal-hal yang telah dinyatakan dalam PAKTA INTEGRITAS ini, saya bersedia dikenakan sanksi moral, sanksi administrasi serta dituntut ganti rugi dan pidana sesuai dengan ketentuan peraturan perundang-undangan yang berlaku.</li>
            </ol>
            <table style="page-break-inside: avoid;">
                <tr>
                    <td style="width: 50%; vertical-align: top;"></td>
                    <td style="width: 50%; vertical-align: top;">
                        <table>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><?php echo ucwords(strtolower(trim(str_replace('KABUPATEN', '', $resultData->kabupaten)))) . ', ' . $tglNphd; ?></td></tr>
                            <tr><td style="text-align: center">Ketua <?php echo $pokmas->nama_kelompok; ?></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center; font-size: 12px;">MATERAI</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><u><b><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></b></u></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </page>
    <div class="button">
        <ul class="right">
            <button class="samping" onClick="window.print();">Cetak Data</button>
            <button class="samping" onclick="window.top.close();">Kembali</button>
        </ul>
    </div>
</html>
<script>
    document.addEventListener('contextmenu', function (e) {
        e.preventDefault();
    });
    document.onkeydown = function (e) {
        if (event.keyCode == 123) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
            return false;
        }
    }
</script>
