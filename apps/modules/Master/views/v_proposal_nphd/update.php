<?php $this->load->view('_heading/_headerContent') ?>

<style>
    .font-data{
        font-family: Futura,Trebuchet MS,Arial,sans-serif; 
        color:red;
        font-size: 13px;
    }
    .kiri {
        margin-left: -10px;
    }
    .gap {
        margin-top: 30px;
    }
</style>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="row">
        <form class="form-horizontal" enctype="multipart/form-data" id="form-ubah" method="POST">
            <div class="col-md-7">
                <div class="box box-primary">
                    <div class="nav-tabs-custom">
                        <div class="box-header with-border">
                            <h3 class="box-title">Gubernur</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">SK Gub</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="sk_gub" placeholder="Surat keputusan Gubernur" value="<?php echo isset($resultNphd['sk_gub']) ? $resultNphd['sk_gub'] : ""; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Tanggal SK Gub</label>
                                <div class="col-sm-5">
                                    <input type="text" style="background: #FFF;" name="tgl_sk_gub" id="tgl_sk_gub" class="form-control datepicker" value="<?php echo isset($resultNphd['tgl_sk_gub']) ? (strlen($resultNphd['tgl_sk_gub']) > 0) ? date('d-m-Y', strtotime($resultNphd['tgl_sk_gub'])) : "" : ""; ?>" readOnly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="nav-tabs-custom">
                        <div class="box-header with-border">
                            <h3 class="box-title">Kelompok Masyarakat</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="nama_ketua" placeholder="Nama Ketua" value="<?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">No KTP</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="no_ktp" id="no_ktp" placeholder="Nomor KTP" value="<?php echo isset($resultNphd['no_ktp']) ? $resultNphd['no_ktp'] : $resultData->no_ktp; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label"></label>
                                <div class="col-sm-5">
                                    <a href="#" onclick="changeKTP();" >Ubah No KTP</a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Alamat Rumah</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="alamat_rumah" placeholder="Alamat Rumah" value="<?php echo isset($resultNphd['alamat_rumah']) ? $resultNphd['alamat_rumah'] : ""; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama POKMAS</label>
                                <div class="col-sm-5">
                                    <p class="form-control-static"><?php echo $resultData->nama_kelompok; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Jabatan dalam POKMAS</label>
                                <div class="col-sm-5">
                                    <p class="form-control-static">Ketua</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Alamat POKMAS</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="alamat_pokmas" placeholder="Alamat Pokmas" value="<?php echo isset($resultNphd['alamat_pokmas']) ? $resultNphd['alamat_pokmas'] : $pokmas->alamat; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Asal POKMAS</label>
                                <div class="col-sm-5">
                                    <p class="form-control-static"><?php echo $resultData->kabupaten; ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Kode Pos</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="kode_pos" placeholder="Kode Pos" value="<?php echo isset($resultNphd['kode_pos']) ? $resultNphd['kode_pos'] : ""; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">No Telp/HP</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="telp" placeholder="No Telepon/Handphone" value="<?php echo isset($resultNphd['telp']) ? $resultNphd['telp'] : ""; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nomor Surat POKMAS</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="no_surat" placeholder="No Surat" value="<?php echo $resultData->no_surat; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Surat</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo date('d F Y', strtotime($resultData->tanggal)); ?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Perihal</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="perihal" placeholder="Perihal Proposal" value="<?php echo $resultData->perihal; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Waktu Pelaksanaan</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="waktu_pelaksanaan" placeholder="Waktu Pelaksanaan" value="<?php echo isset($resultNphd['waktu_pelaksanaan']) ? $resultNphd['waktu_pelaksanaan'] : ""; ?>">
                                </div>
                            </div>
                            <?php if ($resultData->jenis_bantuan == 'Uang') { ?>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Uang Hibah</label>
                                    <div class="col-sm-7">
                                        <p class="form-control-static">Rp. <?php echo $resultData->nilai_anggaran; ?></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">
                                        <p class="form-control-static"><?php echo $terbilang; ?></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Pemilik Rekening</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" name="nama_rek" placeholder="Atas Nama Pemilik Rekening Bank" value="<?php echo isset($resultNphd['nama_rek']) ? $resultNphd['nama_rek'] : $pokmas->nama_kelompok; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Bank Jatim</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" name="cabang" placeholder="Cabang Bank Jatim" value="<?php echo isset($resultNphd['cabang']) ? $resultNphd['cabang'] : ""; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Rekening</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" name="no_rek" maxlength="11" placeholder="Nomor Rekening Bank" value="<?php echo isset($resultNphd['no_rek']) ? $resultNphd['no_rek'] : ""; ?>">
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($resultData->jenis_bantuan == 'Barang') { ?>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Barang</label>
                                    <div class="col-sm-7">
                                        <p class="form-control-static"><?php echo nl2br($resultData->barang); ?></p>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="box-footer">
                            <div id="buka"> 
                                <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                                <a class="klik ajaxify" href="<?php echo base_url('master-proposal-nphd'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                            </div>
                            <div id="btn_loading">
                                <button name="submit" type="submit" class="btn btn-primary btn-flat animated-gradient">&nbsp;Tunggu..</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="box box-primary">
                    <div class="nav-tabs-custom">
                        <div class="box-header with-border">
                            <h3 class="box-title">NASKAH PERJANJIAN HIBAH DAERAH</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nomor NPHD</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="no_nphd" placeholder="Nomor NPHD" value="<?php echo isset($resultNphd['no_nphd']) ? $resultNphd['no_nphd'] : ""; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Tanggal NPHD</label>
                                <div class="col-sm-5">
                                    <input type="text" style="background: #FFF;" name="tgl_nphd" id="tgl_nphd" class="form-control datepicker" value="<?php echo isset($resultNphd['tgl_nphd']) ? (strlen($resultNphd['tgl_nphd']) > 0) ? date('d-m-Y', strtotime($resultNphd['tgl_nphd'])) : "" : ""; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nomor Surat ke POKMAS</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="no_surat_ke_pokmas" placeholder="Nomor Surat ke POKMAS" value="<?php echo isset($resultNphd['no_surat_ke_pokmas']) ? $resultNphd['no_surat_ke_pokmas'] : ""; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Surat</label>
                                <div class="col-sm-5">
                                    <input type="text" style="background: #FFF;" name="tgl_surat" id="tgl_surat" class="form-control datepicker" value="<?php echo isset($resultNphd['tgl_surat']) ? (strlen($resultNphd['tgl_surat']) > 0) ? date('d-m-Y', strtotime($resultNphd['tgl_surat'])) : "" : ""; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Surat POKMAS</label>
                                <div class="col-sm-5">
                                    <input type="text" style="background: #FFF;" name="tgl_surat_pokmas" id="tgl_surat_pokmas" class="form-control datepicker" value="<?php echo isset($resultNphd['tgl_surat_pokmas']) ? (strlen($resultNphd['tgl_surat_pokmas']) > 0) ? date('d-m-Y', strtotime($resultNphd['tgl_surat_pokmas'])) : "" : ""; ?>" >
                                </div>
                            </div>
                            <?php if ($resultData->status != 'NPHD') { ?>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Status</label>
                                    <div class="col-sm-5">
                                        <select name="status" class="form-control selek-status" id="status_dokumen">
                                            <?php foreach ($status as $data) { ?>
                                                <option></option>
                                                <option value="<?php echo $data->nama ?>"<?php
                                                if ($data->nama == $resultData->status) {
                                                    echo"selected='selected'";
                                                }

                                                ?>><?php echo $data->nama; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Status</label>
                                    <div class="col-sm-7">
                                        <p class="form-control-static"><?php echo $resultData->status; ?></p>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Peringatan</label>
                                <div class="col-sm-7">
                                    <textarea name="peringatan" id="peringatan" class="form-control"><?php echo isset($resultNphd['peringatan']) ? $resultNphd['peringatan'] : ""; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pokmas</h3>
                    </div>
                    <div class="box-body box-profile">
                        <div class="row" style="margin-left: 0px;">
                            <div class="form-group col-md-9">
                                <label>Upload Foto 1</label>
                                <input type="file" class="upload-image" name="file_upload[]" aria-describedby="sizing-addon2" id="img_pokmas_1" onchange="return fileValidation2()">
                                <?php if (isset($resultNphd['nama_file_img_pokmas_1'])) { ?>
                                    <a href="<?php echo base_url() . $resultNphd['file_img_pokmas_1']; ?>" target="_blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-picture-o" aria-hidden="true"></i> <?php echo $resultNphd['nama_file_img_pokmas_1']; ?></font></b></a>
                                <?php } ?>
                            </div>
                            <div class="form-group col-md-2" style="margin-top: 20px;">
                                <small class="label pull-center bg-red">format jpg/jpeg/png</small>
                                <small class="label pull-center bg-red">maksimal upload 5 MB</small>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 0px;">
                            <div class="form-group col-md-9">
                                <label>Upload Foto 2</label>
                                <input type="file" class="upload-image" name="file_upload[]" aria-describedby="sizing-addon2" id="img_pokmas_2" onchange="return fileValidation2()">
                                <?php if (isset($resultNphd['nama_file_img_pokmas_2'])) { ?>
                                    <a href="<?php echo base_url() . $resultNphd['file_img_pokmas_2']; ?>" target="_blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-picture-o" aria-hidden="true"></i> <?php echo $resultNphd['nama_file_img_pokmas_2']; ?></font></b></a>
                                <?php } ?>
                            </div>
                            <div class="form-group col-md-2" style="margin-top: 20px;">
                                <small class="label pull-center bg-red">format jpg/jpeg/png</small>
                                <small class="label pull-center bg-red">maksimal upload 5 MB</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Rekening</h3>
                    </div>
                    <div class="box-body box-profile">
                        <div class="row" style="margin-left: 0px;">
                            <div class="form-group col-md-9">
                                <label>Upload Scan</label>
                                <input type="file" class="upload-image" name="file_upload[]" aria-describedby="sizing-addon2" id="img_rek" onchange="return fileValidation2()">
                                <?php if (isset($resultNphd['nama_file_img_rek'])) { ?>
                                    <a href="<?php echo base_url() . $resultNphd['file_img_rek']; ?>" target="_blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-file-pdf" aria-hidden="true"></i> <?php echo $resultNphd['nama_file_img_rek']; ?></font></b></a>
                                <?php } ?>
                            </div>
                            <div class="form-group col-md-2" style="margin-top: 20px;">
                                <small class="label pull-center bg-red">format jpg/jpeg/png</small>
                                <small class="label pull-center bg-red">maksimal upload 5 MB</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">KTP</h3>
                    </div>
                    <div class="box-body box-profile">
                        <div class="row" style="margin-left: 0px;">
                            <div class="form-group col-md-9">
                                <label>Upload Scan</label>
                                <input type="file" class="upload-image" name="file_upload[]" aria-describedby="sizing-addon2" id="img_ktp" onchange="return fileValidation2()">
                                <?php if (isset($resultNphd['nama_file_img_ktp'])) { ?>
                                    <a href="<?php echo base_url() . $resultNphd['file_img_ktp']; ?>" target="_blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-file-pdf" aria-hidden="true"></i> <?php echo $resultNphd['nama_file_img_ktp']; ?></font></b></a>
                                <?php } ?>
                            </div>
                            <div class="form-group col-md-2" style="margin-top: 20px;">
                                <small class="label pull-center bg-red">format jpg/jpeg/png</small>
                                <small class="label pull-center bg-red">maksimal upload 5 MB</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <?php if (count($historiFile) > 0) { ?>
                        <div class="box-header with-border">
                            <h3 class="box-title">Pengajuan File</h3>
                        </div>
                        <div class="box-body box-profile">
                            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="50%">
                                <tr>  
                                    <th>History File Upload</th>
                                </tr>
                                <?php foreach ($historiFile as $data) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() . $data->file_upload ?>" target="blank"><b><font face="verdana" size="2" color="red"><i class="fa fa-download" aria-hidden="true"></i> <?php echo $data->nama_file_upload; ?></font></b></a><br>tanggal upload : <?php echo date_indo(date('Y-m-d', strtotime($data->created_date))) ?><br><small class="label pull-center bg-green"><?php echo $data->status ?></small></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } ?>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Berita Acara</h3>
                    </div>
                    <?php if (count($historiFileBeritaAcara) > 0) { ?>
                        <div class="box-body box-profile">
                            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="50%">
                                <tr>  
                                    <th>History File Upload</th>
                                </tr>
                                <?php foreach ($historiFileBeritaAcara as $data) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() . $data->file_upload ?>" target="blank"><b><font face="verdana" size="2" color="red"><i class="fa fa-download" aria-hidden="true"></i> <?php echo $data->nama_file_upload; ?></font></b></a><br>tanggal upload : <?php echo date_indo(date('Y-m-d', strtotime($data->created_date))) ?><br><small class="label pull-center bg-green"><?php echo $data->status ?></small></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } else { ?>
                        <div class="box-body box-profile">
                            Belum ada file Berita Acara
                        </div> 
                    <?php } ?>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Checklist Survei</h3>
                    </div>
                    <?php if (count($historiFileChecklist) > 0) { ?>
                        <div class="box-body box-profile">
                            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="50%">
                                <tr>  
                                    <th>History File Upload</th>
                                </tr>
                                <?php foreach ($historiFileChecklist as $data) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() . $data->file_upload ?>" target="blank"><b><font face="verdana" size="2" color="red"><i class="fa fa-download" aria-hidden="true"></i> <?php echo $data->nama_file_upload; ?></font></b></a><br>tanggal upload : <?php echo date_indo(date('Y-m-d', strtotime($data->created_date))) ?><br><small class="label pull-center bg-green"><?php echo $data->status ?></small></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } else { ?>
                        <div class="box-body box-profile">
                            Belum ada file Checklist Survei
                        </div> 
                    <?php } ?>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Hasil Survei</h3>
                    </div>
                    <?php if (count($historiFileHasilSurvei) > 0) { ?>
                        <div class="box-body box-profile">
                            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="50%">
                                <tr>  
                                    <th>History File Upload</th>
                                </tr>
                                <?php foreach ($historiFileHasilSurvei as $data) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() . $data->file_upload ?>" target="blank"><b><font face="verdana" size="2" color="red"><i class="fa fa-download" aria-hidden="true"></i> <?php echo $data->nama_file_upload; ?></font></b></a><br>tanggal upload : <?php echo date_indo(date('Y-m-d', strtotime($data->created_date))) ?><br><small class="label pull-center bg-green"><?php echo $data->status ?></small></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } else { ?>
                        <div class="box-body box-profile">
                            Belum ada file Hasil Survei
                        </div> 
                    <?php } ?>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Sketsa Survei</h3>
                    </div>
                    <?php if (count($historiSketsaSurvei) > 0) { ?>
                        <div class="box-body box-profile">
                            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="50%">
                                <tr>  
                                    <th>History File Upload</th>
                                </tr>
                                <?php foreach ($historiSketsaSurvei as $data) { ?>
                                    <tr>
                                        <td><a href="<?php echo base_url() . $data->file_upload ?>" target="blank"><b><font face="verdana" size="2" color="red"><i class="fa fa-download" aria-hidden="true"></i> <?php echo $data->nama_file_upload; ?></font></b></a><br>tanggal upload : <?php echo date_indo(date('Y-m-d', strtotime($data->created_date))) ?><br><small class="label pull-center bg-green"><?php echo $data->status ?></small></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } else { ?>
                        <div class="box-body box-profile">
                            Belum ada file Sketsa Survei
                        </div> 
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
</section>  
<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-ubah').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url('update-proposal-nphd') . '/' . $resultData->id_proposal; ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?php echo base_url('master-proposal-nphd'); ?>'", 450);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    // untuk select2 ajak pilih department
    $(function () {
        $(".selek-status").select2({
            placeholder: " -- Pilih Status -- "
        });
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        })
    });

    function changeKTP() {
        $('#no_ktp').removeAttr('readOnly', 'readOnly');
    }

    function fileValidation2() {
        var fileInput = document.getElementsByClassName("upload-image")[0].value;
        if (fileInput != '')
        {
            var checkfile = fileInput.toLowerCase();
            if (!checkfile.match(/(\.jpg|\.jpeg|\.png|\.gif)$/)) { // validasi ekstensi file
                window.location.reload();
                toastr.error('sorry, enter the image with the format .jpeg |.jpg |.png | only.', 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                alert("sorry, enter the image with the format .jpeg |.jpg |.png | only.");
                return false;
            }
            var ukuran = document.getElementsByClassName("upload-image")[0];
            if (ukuran.files[0].size > 5007200)  // validasi ukuran size file
            {
                toastr.error('Maximal File 5 MB', 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                window.location.reload();
                document.getElementsByClassName('uploadFile').value = '';
                alert("Maximal File 9 MB");
                return false;
            }
            return true;
        }
    }
</script>