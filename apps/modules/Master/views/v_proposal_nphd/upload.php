<?php $this->load->view('_heading/_headerContent') ?>

<style>
    .font-data{
        font-family: Futura,Trebuchet MS,Arial,sans-serif; 
        color:red;
        font-size: 13px;
    }
    .kiri {
        margin-left: -10px;
    }
    .gap {
        margin-top: 30px;
    }
</style>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="row">
        <form class="form-horizontal" enctype="multipart/form-data" id="form-ubah" method="POST">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="nav-tabs-custom">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Berkas File</h3>
                            </div>
                            <div class="box-body box-profile">
                                <div class="row" style="margin-left: 0px;">
                                    <div class="form-group col-md-5">
                                        <label>Upload Scan Proposal</label>
                                        <input type="file" class="upload-pdf" name="file_upload[]" aria-describedby="sizing-addon2" id="file_undangan" onchange="return fileValidationPdf()">
                                        <?php if (isset($resultNphd['nama_file_undangan'])) { ?>
                                            <a href="<?php echo base_url() . $resultNphd['file_undangan']; ?>" target="_blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-file-pdf" aria-hidden="true"></i> <?php echo $resultNphd['nama_file_undangan']; ?></font></b></a>
                                        <?php } ?>
                                    </div>
                                    <div class="form-group col-md-2" style="margin-top: 20px;">
                                        <small class="label pull-center bg-red">format pdf</small>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body box-profile">
                                <div class="row" style="margin-left: 0px;">
                                    <div class="form-group col-md-5">
                                        <label>Upload Dokumen NPHD</label>
                                        <input type="file" class="upload-pdf" name="file_upload[]" aria-describedby="sizing-addon2" id="file_nphd" onchange="return fileValidationPdf()">
                                        <?php if (isset($resultNphd['nama_file_nphd'])) { ?>
                                            <a href="<?php echo base_url() . $resultNphd['file_nphd']; ?>" target="_blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-file-pdf" aria-hidden="true"></i> <?php echo $resultNphd['nama_file_nphd']; ?></font></b></a>
                                        <?php } ?>
                                    </div>
                                    <div class="form-group col-md-2" style="margin-top: 20px;">
                                        <small class="label pull-center bg-red">format pdf</small>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body box-profile">
                                <div class="row" style="margin-left: 0px;">
                                    <div class="form-group col-md-5">
                                        <label>Upload Buku Rekening ( Buku Rekening & KTP Discan dalam satu lembar )</label>
                                        <input type="file" class="upload-pdf" name="file_upload[]" aria-describedby="sizing-addon2" id="file_pokmas" onchange="return fileValidationPdf()">
                                        <?php if (isset($resultNphd['nama_file_pokmas'])) { ?>
                                            <a href="<?php echo base_url() . $resultNphd['file_pokmas']; ?>" target="_blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-file-pdf" aria-hidden="true"></i> <?php echo $resultNphd['nama_file_pokmas']; ?></font></b></a>
                                        <?php } ?>
                                    </div>
                                    <div class="form-group col-md-2" style="margin-top: 20px;">
                                        <small class="label pull-center bg-red">format pdf</small>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body box-profile">
                                <div class="row" style="margin-left: 0px;">
                                    <div class="form-group col-md-5">
                                        <label>Upload SKGUB ( Lembar 1 dan Lampiran Nama Pokmas NPHD di tandai warna kuning )</label>
                                        <input type="file" class="upload-pdf" name="file_upload[]" aria-describedby="sizing-addon2" id="file_kwitansi" onchange="return fileValidationPdf()">
                                        <?php if (isset($resultNphd['nama_file_kwitansi'])) { ?>
                                            <a href="<?php echo base_url() . $resultNphd['file_kwitansi']; ?>" target="_blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-file-pdf" aria-hidden="true"></i> <?php echo $resultNphd['nama_file_kwitansi']; ?></font></b></a>
                                        <?php } ?>
                                    </div>
                                    <div class="form-group col-md-2" style="margin-top: 20px;">
                                        <small class="label pull-center bg-red">format pdf</small>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body box-profile">
                                <div class="row" style="margin-left: 0px;">
                                    <div class="form-group col-md-5">
                                        <label>Upload PAKTA Integritas</label>
                                        <input type="file" class="upload-pdf" name="file_upload[]" aria-describedby="sizing-addon2" id="file_pakta" onchange="return fileValidationPdf()">
                                        <?php if (isset($resultNphd['nama_file_pakta'])) { ?>
                                            <a href="<?php echo base_url() . $resultNphd['file_pakta']; ?>" target="_blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-file-pdf" aria-hidden="true"></i> <?php echo $resultNphd['nama_file_pakta']; ?></font></b></a>
                                        <?php } ?>
                                    </div>
                                    <div class="form-group col-md-2" style="margin-top: 20px;">
                                        <small class="label pull-center bg-red">format pdf</small>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body box-profile">
                                <div class="row" style="margin-left: 0px;">
                                    <div class="form-group col-md-5">
                                        <label>Upload Berkas Kelengkapan SPJ ( Kwitansi, RAB Penetapan, SK Pembentukan Pokmas, SK Pengesahan Camat,KTP Ketua Pokmas, SPTJM )</label>
                                        <input type="file" class="upload-pdf" name="file_upload[]" aria-describedby="sizing-addon2" id="file_penggunaan_uang" onchange="return fileValidationPdf()">
                                        <?php if (isset($resultNphd['nama_file_penggunaan_uang'])) { ?>
                                            <a href="<?php echo base_url() . $resultNphd['file_penggunaan_uang']; ?>" target="_blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-file-pdf" aria-hidden="true"></i> <?php echo $resultNphd['nama_file_penggunaan_uang']; ?></font></b></a>
                                        <?php } ?>
                                    </div>
                                    <div class="form-group col-md-2" style="margin-top: 20px;">
                                        <small class="label pull-center bg-red">format pdf</small>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body box-profile">
                                <div class="row" style="margin-left: 0px;">
                                    <div class="form-group col-md-5">
                                        <label>Berkas Pendukung ( Surat undangan, Surat Pernyataan,dll )</label>
                                        <input type="file" class="upload-pdf" name="file_upload[]" aria-describedby="sizing-addon2" id="file_ta" onchange="return fileValidationPdf()">
                                        <?php if (isset($resultNphd['nama_file_ta'])) { ?>
                                            <a href="<?php echo base_url() . $resultNphd['file_ta']; ?>" target="_blank"><b><font face="verdana" size="2" color="red"><i class="nav-icon far fa-file-pdf" aria-hidden="true"></i> <?php echo $resultNphd['nama_file_ta']; ?></font></b></a>
                                        <?php } ?>
                                    </div>
                                    <div class="form-group col-md-2" style="margin-top: 20px;">
                                        <small class="label pull-center bg-red">format pdf</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div id="buka"> 
                                <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                                <a class="klik ajaxify" href="<?php echo site_url('master-proposal-nphd'); ?>"><button class="btn btn-warning btn-flat" ><i class="fa fa-arrow-left"></i> Back</button></a>
                            </div>
                            <div id="btn_loading">
                                <button name="submit" type="submit" class="btn btn-primary btn-flat animated-gradient">&nbsp;Tunggu..</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>  

<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-ubah').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        }, function () {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#buka").hide();
                    $("#btn_loading").show();
                },
                url: '<?php echo base_url('process-upload-proposal-nphd') . '/' . $resultData->id_proposal; ?>',
                type: "post",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    save_berhasil();
                    setTimeout("window.location='<?php echo base_url('master-proposal-nphd'); ?>'", 450);
                } else {
                    $("#buka").show();
                    $("#btn_loading").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
        });
    });

    function fileValidationPdf() {
        var fileInput = document.getElementsByClassName("upload-pdf")[0].value;
        if (fileInput != '')
        {
            var checkfile = fileInput.toLowerCase();
            if (!checkfile.match(/(\.pdf)$/)) { // validasi ekstensi file
                window.location.reload();
                toastr.error('sorry, enter the file with the format pdf only only.', 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                alert("sorry, enter the file with the format pdf only.");
                return false;
            }
            var ukuran = document.getElementsByClassName("upload-pdf")[0];
            if (ukuran.files[0].size > 9007200)  // validasi ukuran size file
            {
                toastr.error('Maximal File 9 MB', 'Warning', {timeOut: 5000}, toastr.options = {
                    "closeButton": true});
                window.location.reload();
                document.getElementsByClassName('uploadFile').value = '';
                alert("Maximal File 9 MB");
                return false;
            }
            return true;
        }
    }
</script>