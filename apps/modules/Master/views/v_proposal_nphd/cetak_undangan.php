<html>
    <style>
        body {
            font-family: 'Arial', sans-serif;
        }
        page {
            font-family: 'Arial', sans-serif;
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
        }
        page[size="A4"] {  
            width: 21cm;
        }
        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
            }
        }
        .kop {
            width: 100%;
            margin-top: 10px;
            margin-left: 60px;
        }
        .container {
            width: 100%;
            margin-top: 20px;
            margin-left: 70px;
        }
        button {
            width: 150px;
            height: 30px;
        }
        @media print {
            .samping {display:none;}
        }
        .button {
            width: 100%;
            position: relative;
        }
        .right {
            float: right;
            margin-right: 35%;
            display: block;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        ol {
            margin: 0;
        }
        table {
            width: 100%;
        }
    </style>
    <page size="A4">
        <img class="kop" src="<?php echo site_url(); ?>assets/tambahan/gambar/kop-pu_sda.png" width="680px">
        <div style="float: right; margin-right: -50px;">Kode Pos 60235</div>
        <div class="container">
            <table>
                <tr>
                    <td style="width: 50%; vertical-align: top;">
                        <table>
                            <tr><td colspan="3">&nbsp;</td></tr>
                            <tr><td colspan="3">&nbsp;</td></tr>
                            <tr><td colspan="3">&nbsp;</td></tr>
                            <tr>
                                <td style="vertical-align: top">Nomor</td>
                                <td style="vertical-align: top">:</td>
                                <td style="vertical-align: top"><?php echo isset($resultNphd['no_surat_ke_pokmas']) ? $resultNphd['no_surat_ke_pokmas'] : ""; ?></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">Sifat</td>
                                <td style="vertical-align: top">:</td>
                                <td style="vertical-align: top">Segera</td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">Lampiran</td>
                                <td style="vertical-align: top">:</td>
                                <td style="vertical-align: top">1 (satu) berkas</td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">Perihal</td>
                                <td style="vertical-align: top">:</td>
                                <td style="vertical-align: top">Undangan Penandatanganan NPHD</td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 50%; vertical-align: top;">
                        <table>
                            <tr><td colspan="2">Surabaya, <?php echo $tglSurat; ?></td></tr>
                            <tr><td colspan="2">&nbsp;</td></tr>
                            <tr>
                                <td style="vertical-align: top" colspan="2">Kepada :</td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">Yth.Sdr.</td>
                                <td style="vertical-align: top">Ketua <?php echo $pokmas->nama_kelompok; ?></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top"></td>
                                <td style="vertical-align: top"><?php echo isset($resultNphd['alamat_pokmas']) ? $resultNphd['alamat_pokmas'] : $pokmas->alamat; ?></td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top"></td>
                                <td style="vertical-align: top">di</td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top"></td>
                                <td style="vertical-align: top"><span style="padding-left: 20px;">&nbsp;</span><?php echo ucwords(strtolower(trim(str_replace('KABUPATEN', '', $resultData->kabupaten)))); ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br>
            <p class="pasal"><span style="padding-left: 50px;">&nbsp;</span>Sehubungan dengan surat Saudara tanggal <?php echo $tglProposal; ?>, nomor <?php echo $resultData->no_surat; ?>, perihal tersebut pada pokok surat dan Keputusan Gubernur Jawa Timur Tentang Penerima Hibah Uang, maka Pemerintah Provinsi Jawa Timur memberikan Hibah sebesar Rp. <?php echo $resultData->nilai_anggaran; ?>,- (<?php echo $terbilang; ?>).</p>
            <p class="pasal"><span style="padding-left: 50px;">&nbsp;</span>Berkenaan dengan hal tersebut, diharap agar Saudara hadir dalam rangka penandatanganan Naskah Perjanjian Hibah Daerah (NPHD) dan Pakta Integritas dengan melengkapi persyaratan sebagai berikut :</p>
            <ol>
                <li>Rencana penggunaan dana Hibah sebesar Rp. <?php echo $resultData->nilai_anggaran; ?>,- (<?php echo $terbilang; ?>)</li>
                <li>Copy rekening Bank Jatim atas nama <?php echo isset($resultNphd['nama_rek']) ? $resultNphd['nama_rek'] : ""; ?>.</li>
                <li>Kwitansi asli bermaterai Rp. 10.000,- atas nama Ketua <?php echo $pokmas->nama_kelompok; ?> Senilai Rp. <?php echo $resultData->nilai_anggaran; ?>,- (<?php echo $terbilang; ?>) dan distempel.</li>
            </ol>
            <p class="pasal"><span style="padding-left: 50px;">&nbsp;</span>Selanjutnya diingatkan bahwa Saudara bertanggung jawab penuh atas penggunaan Hibah yang telah diterima dan berkewajiban menyerahkan laporan pertanggungjawaban kepada Gubernur Jawa Timur melalui Dinas Pekerjaan Umum Sumber Daya Air Provinsi Jawa Timur selambat-lambatnya 3 (tiga) bulan setelah diterimanya dana bantuan.</p>
            <p class="pasal"><span style="padding-left: 50px;">&nbsp;</span>Demikian untuk menjadi maklum dan terima kasih.</p>
            <br>
            <table style="page-break-inside: avoid;">
                <tr>
                    <td style="width: 40%; vertical-align: top;"></td>
                    <td style="width: 10%; vertical-align: top;"></td>
                    <td style="width: 50%; vertical-align: top;">
                        <table>
                            <tr><td style="text-align: center"><?php echo $getManagementSystem['pu_head_sign_1']; ?></td></tr>
                            <tr><td style="text-align: center"><?php echo $getManagementSystem['pu_head_sign_2']; ?></td></tr>
                            <tr><td style="text-align: center"><?php echo $getManagementSystem['pu_head_sign_3']; ?></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><u><b><?php echo $getManagementSystem['pu_nama_ketua']; ?></b></u></td></tr>
                            <tr><td style="text-align: center"><?php echo $getManagementSystem['pu_jabatan_ketua']; ?></td></tr>
                            <tr><td style="text-align: center">NIP. <?php echo $getManagementSystem['pu_nip']; ?></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </page>
    <div class="button">
        <ul class="right">
            <button class="samping" onClick="window.print();">Cetak Data</button>
            <button class="samping" onclick="window.top.close();">Kembali</button>
        </ul>
    </div>
</html>
<script>
    document.addEventListener('contextmenu', function (e) {
        e.preventDefault();
    });
    document.onkeydown = function (e) {
        if (event.keyCode == 123) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
            return false;
        }
    }
</script>