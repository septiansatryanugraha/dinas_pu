<html>
    <style>
        body {
            font-family: 'Bookman Old Style', sans-serif;
        }
        page {
            font-family: 'Bookman Old Style', sans-serif;
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
        }
        page[size="A4"] {  
            width: 21cm;
        }
        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
            }
        }
        .container {
            width: 100%;
            margin-top: 30px;
            margin-left: 70px;
        }
        button {
            width: 150px;
            height: 30px;
        }
        @media print {
            .samping {display:none;}
        }
        .button {
            width: 100%;
            position: relative;
        }
        .right {
            float: right;
            margin-right: 35%;
            display: block;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        ol {
            margin: 0;
        }
        table {
            width: 100%;
        }
    </style>
    <page size="A4">
        <div class="container">
            <h2 style="text-align: center;">SURAT PERNYATAAN<br>TIDAK MENERIMA ANGGARAN DARI SUMBER LAIN</h2><hr/>
            <p>&nbsp;</p>
            <p class="pasal">Yang bertanda tangan dibawah ini :</p>
            <table class="pasal" style="padding-left: 50px;">
                <tr>
                    <td style="vertical-align: top; width: 150px;">Nama</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Alamat</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><?php echo isset($resultNphd['alamat_rumah']) ? $resultNphd['alamat_rumah'] : ""; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Jabatan</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;">Ketua</td>
                </tr>
            </table>
            <p class="pasal">Dengan ini menyatakan dengan sebenar-benarnya bahwa Kelompok Masyarakat (Pokmas) yang tercantum sebagai berikut :</p>
            <table class="pasal" style="padding-left: 50px;">
                <tr>
                    <td style="vertical-align: top; width: 150px;">Nama Pokmas</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><?php echo $pokmas->nama_kelompok; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Alamat Pokmas</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><?php echo isset($resultNphd['alamat_rumah']) ? $resultNphd['alamat_rumah'] : ""; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Kegiatan</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><?php echo $resultData->perihal; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Lokasi Kegiatan</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><?php echo isset($resultNphd['alamat_pokmas']) ? $resultNphd['alamat_pokmas'] : $pokmas->alamat; ?></td>
                </tr>
            </table>
            <p class="pasal">Tidak menerima anggaran dari sumber anggaran lain (anggaran ganda/double account) untuk paket pekerjaan tersebut.</p>
            <p class="pasal">Demikian surat pernyataan ini dibuat dengan sebenarnya.</p>
            <table style="page-break-inside: avoid;">
                <tr>
                    <td style="width: 50%; vertical-align: top;"></td>
                    <td style="width: 50%; vertical-align: top;">
                        <table>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">Surabaya, <?php echo $tglNphd; ?></td></tr>
                            <tr><td style="text-align: center">Ketua <?php echo $pokmas->nama_kelompok; ?></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center; font-size: 12px;">MATERAI</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><u><b><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></b></u></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </page>
    <div class="button">
        <ul class="right">
            <button class="samping" onClick="window.print();">Cetak Data</button>
            <button class="samping" onclick="window.top.close();">Kembali</button>
        </ul>
    </div>
</html>
<script>
    document.addEventListener('contextmenu', function (e) {
        e.preventDefault();
    });
    document.onkeydown = function (e) {
        if (event.keyCode == 123) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
            return false;
        }
    }
</script>