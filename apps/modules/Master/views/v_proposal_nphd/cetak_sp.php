<html>
    <style>
        body {
            font-family: 'Bookman Old Style', sans-serif;
        }
        page {
            font-family: 'Bookman Old Style', sans-serif;
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
        }
        page[size="A4"] {  
            width: 21cm;
        }
        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
            }
        }
        .container {
            width: 100%;
            margin-top: 30px;
            margin-left: 70px;
        }
        button {
            width: 150px;
            height: 30px;
        }
        @media print {
            .samping {display:none;}
        }
        .button {
            width: 100%;
            position: relative;
        }
        .right {
            float: right;
            margin-right: 35%;
            display: block;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        ol {
            margin: 0;
        }
        table {
            width: 100%;
        }
    </style>
    <page size="A4">
        <div class="container">
            <h2 style="text-align: center;">SURAT PERNYATAAN</h2>
            <p>&nbsp;</p>
            <p class="pasal">Pada hari ini <?php echo $dayNow; ?>, tanggal <?php echo $dateNow; ?>, bulan <?php echo $monthNow; ?>, tahun <?php echo $yearNow; ?> bertempat di <?php echo $getManagementSystem['pu_head_sign_3']; ?> yang bertanda tangan dibawah ini :</p>
            <table style="margin-left: 50px; margin-right: 50px;">
                <tr>
                    <td style="vertical-align: top; width: 220px;">Nama</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;">.....................................</td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">NIK</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;">.....................................</td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Alamat Rumah</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;">.....................................</td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Nomor Telepon Rumah/HP</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;">.....................................</td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Jabatan</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;">.....................................</td>
                </tr>
            </table>
            <p class="pasal">Dengan ini menyatakan bahwa:</p>
            <table style="margin-left: 50px; margin-right: 50px;">
                <tr>
                    <td style="vertical-align: top; width: 220px;">Nama</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><?php echo $resultData->nama_kelompok; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">NIK</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><?php echo isset($resultNphd['no_ktp']) ? $resultNphd['no_ktp'] : ""; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Alamat Rumah</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><?php echo isset($resultNphd['alamat_rumah']) ? $resultNphd['alamat_rumah'] : ""; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Nomor Telepon Rumah/HP</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><?php echo isset($resultNphd['telp']) ? $resultNphd['telp'] : ""; ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Jabatan</td>
                    <td style="vertical-align: top;">:</td>
                    <td class="pasal" style="vertical-align: top;">KETUA</td>
                </tr>
            </table>
            <p class="pasal">adalah memiliki kewenangan bertindak untuk dan atas nama <b><?php echo $resultData->nama_kelompok; ?></b> dalam keseluruhan mekanisme pemberian Hibah Daerah berupa uang yang bersumber dari Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur sesuai ketentuan yang berlaku pada <b>Peraturan Gubernur No 44 Tahun 2021 Tentang Tata Cara Penganggaran, Pelaksanaan dan Penatausahaan, Pelaporan dan Pertanggungjawaban serta Monitoring dan Evaluasi Hibah dan Bantuan Sosial</b>.</p>
            <p class="pasal">Demikian Surat Pernyataan ini ditandatangani di atas meterai yang bernilai cukup dalam keadaan sadar dan tanpa paksaan dari pihak manapun untuk dipergunakan sebagaimana mestinya.</p>
            <table style="page-break-inside: avoid;">
                <tr>
                    <td style="width: 50%; vertical-align: top;">
                        <table>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">................... , ................</td></tr>
                            <tr><td style="text-align: center">Yang Membuat Pernyataan,</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><hr></td></tr>
                        </table>
                    </td>
                    <td style="width: 50%; vertical-align: top;"></td>
                </tr>
            </table>
        </div>
    </page>
    <div class="button">
        <ul class="right">
            <button class="samping" onClick="window.print();">Cetak Data</button>
            <button class="samping" onclick="window.top.close();">Kembali</button>
        </ul>
    </div>
</html>
<script>
    document.addEventListener('contextmenu', function (e) {
        e.preventDefault();
    });
    document.onkeydown = function (e) {
        if (event.keyCode == 123) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
            return false;
        }
    }
</script>