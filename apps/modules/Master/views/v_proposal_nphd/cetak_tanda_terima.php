<html>
    <style>
        body {
            font-family: 'Bookman Old Style', sans-serif;
        }
        page {
            font-family: 'Bookman Old Style', sans-serif;
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
        }
        page[size="A4"] {  
            width: 21cm;
        }
        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
            }
        }
        .container {
            width: 100%;
            margin-top: 30px;
            margin-left: 70px;
        }
        button {
            width: 150px;
            height: 30px;
        }
        @media print {
            .samping {display:none;}
        }
        .button {
            width: 100%;
            position: relative;
        }
        .right {
            float: right;
            margin-right: 35%;
            display: block;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        table.table-terima, table.table-terima th, table.table-terima td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        ol {
            margin: 0;
        }
        table {
            width: 100%;
        }
    </style>
    <page size="A4">
        <div class="container">
            <h2 style="text-align: center;">TANDA TERIMA<br><u>NASKAH PERJANJIAN HIBAH DAERAH</u></h2>
            <p>&nbsp;</p>
            <table class="pasal">
                <tr>
                    <td style="vertical-align: top; width: 150px;">Nama Kegiatan</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><b><?php echo $resultData->perihal; ?></b></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Tahun Anggaran</td>
                    <td style="vertical-align: top;">:</td>
                    <td style="vertical-align: top;"><?php echo (strlen($resultNphd['tgl_nphd']) > 0) ? date("Y", strtotime($resultNphd['tgl_nphd'])) : '-'; ?></td>
                </tr>
            </table>
            <p>&nbsp;</p>
            <table class="table-terima" border="1">
                <tr>
                    <th style="vertical-align: top;"><b>NO.</b></th>
                    <th style="vertical-align: top;"><b>Tanggal dan No. Surat</b></th>
                    <th style="vertical-align: top;"><b>Kepada</b></th>
                    <th style="vertical-align: top;"><b>Keterangan</b></th>
                </tr>
                <tr>
                    <td style="vertical-align: top;">1.</td>
                    <td style="vertical-align: top;"><?php echo $tglNphd; ?> / <?php echo isset($resultNphd['no_nphd']) ? $resultNphd['no_nphd'] : ""; ?></td>
                    <td style="vertical-align: top;">Ketua <?php echo $pokmas->nama_kelompok; ?></td>
                    <td></td>
                </tr>
            </table>
            <table style="page-break-inside: avoid;">
                <tr>
                    <td style="width: 40%; vertical-align: top;">
                        <table>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: left">Telp. / HP</td></tr>
                            <tr><td style="text-align: left"><?php echo isset($resultNphd['telp']) ? (strlen($resultNphd['telp']) > 0) ? $resultNphd['telp'] : "……………………………………" : "……………………………………"; ?></td></tr>
                        </table>
                    </td>
                    <td style="width: 20%; text-align: center; vertical-align: top;"></td>
                    <td style="width: 40%; vertical-align: top;">
                        <table>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">Surabaya, <?php echo $tglNphd; ?></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">Yang Menerima</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><u><b><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></b></u></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </page>
    <div class="button">
        <ul class="right">
            <button class="samping" onClick="window.print();">Cetak Data</button>
            <button class="samping" onclick="window.top.close();">Kembali</button>
        </ul>
    </div>
</html>
<script>
    document.addEventListener('contextmenu', function (e) {
        e.preventDefault();
    });
    document.onkeydown = function (e) {
        if (event.keyCode == 123) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
            return false;
        }
    }
</script>