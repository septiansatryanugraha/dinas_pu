<html>
    <style>
        body {
            font-family: 'Arial', sans-serif;
        }
        page {
            font-family: 'Arial', sans-serif;
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
        }
        page[size="A4"] {  
            width: 21cm;
        }
        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
            }
        }
        .kop {
            width: 100%;
            margin-top: 10px;
            margin-left: 60px;
        }
        .container {
            width: 100%;
            margin-top: 30px;
            margin-left: 70px;
        }
        button {
            width: 150px;
            height: 30px;
        }
        @media print {
            .samping {display:none;}
        }
        .button {
            width: 100%;
            position: relative;
        }
        .left {
            float: left;
            display: block;
        }
        .right {
            float: right;
            margin-right: 35%;
            display: block;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        ol {
            margin: 0;
        }
        table {
            width: 100%;
        }
    </style>
    <page size="A4">
        <img class="kop" src="<?php echo site_url(); ?>assets/tambahan/gambar/kop-pu_sda.png" width="680px">
        <div style="float: right; margin-right: -50px;">Kode Pos 60235 </div>
        <div class="container">
            <p>&nbsp;</p>
            <h2 style="text-align: center;">NASKAH PERJANJIAN HIBAH DAERAH ( NPHD )</h2>
            <h2 style="text-align: center;">DALAM BENTUK <?php echo strtoupper($resultData->jenis_bantuan); ?></h2>
            <h3 style="text-align: center;">Nomor : <?php echo isset($resultNphd['no_nphd']) ? $resultNphd['no_nphd'] : ""; ?></h3>
            <p>&nbsp;</p>
            <p class="pasal">Pada hari ini <?php echo $dayNphd; ?>, tanggal <?php echo $dateNphd; ?>, bulan <?php echo $monthNphd; ?>, tahun <?php echo $yearNphd; ?> bertempat di <?php echo $getManagementSystem['pu_head_sign_3']; ?> yang bertanda tangan dibawah ini :</p>
            <ol type='I'>
                <li style="padding: inherit;">
                    <table class="pasal">
                        <tr>
                            <td style="vertical-align: top; width: 220px;">Nama</td>
                            <td style="vertical-align: top;">:</td>
                            <td style="vertical-align: top;"><?php echo $getManagementSystem['pu_nama_ketua']; ?></td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">NIP</td>
                            <td style="vertical-align: top;">:</td>
                            <td style="vertical-align: top;"><?php echo $getManagementSystem['pu_nip']; ?></td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">Jabatan</td>
                            <td style="vertical-align: top;">:</td>
                            <td style="vertical-align: top;"><?php echo ucwords(strtolower($getManagementSystem['pu_head_sign_1'])); ?></td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">Instansi</td>
                            <td style="vertical-align: top;">:</td>
                            <td style="vertical-align: top;">DINAS <?php echo $getManagementSystem['pu_head_sign_2']; ?> <?php echo $getManagementSystem['pu_head_sign_3']; ?></td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">Alamat</td>
                            <td style="vertical-align: top;">:</td>
                            <td style="vertical-align: top;"><?php echo $getManagementSystem['pu_alamat']; ?></td>
                        </tr>
                        <tr><td colspan="3">&nbsp;</td></tr>
                        <tr><td colspan="3"><p class="pasal">Dalam hal ini bertindak untuk dan atas nama Pemerintah Provinsi Jawa Timur sebagai Pemberi Hibah, yang selanjutnya disebut <b>PIHAK KESATU</b></p></td></tr>
                        <tr><td colspan="3">&nbsp;</td></tr>
                    </table>
                </li>
                <li style="padding: inherit;">
                    <table class="pasal">
                        <tr>
                            <td style="vertical-align: top; width: 220px;">Nama</td>
                            <td style="vertical-align: top;">:</td>
                            <td style="vertical-align: top;"><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">NIK</td>
                            <td style="vertical-align: top;">:</td>
                            <td style="vertical-align: top;"><?php echo isset($resultNphd['no_ktp']) ? $resultNphd['no_ktp'] : ""; ?></td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">Alamat Rumah</td>
                            <td style="vertical-align: top;">:</td>
                            <td style="vertical-align: top;"><?php echo isset($resultNphd['alamat_pokmas']) ? $resultNphd['alamat_pokmas'] : $pokmas->alamat; ?></td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">Nomor Telepon Rumah/HP</td>
                            <td style="vertical-align: top;">:</td>
                            <td style="vertical-align: top;"><?php echo isset($resultNphd['telp']) ? $resultNphd['telp'] : ""; ?></td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">Jabatan</td>
                            <td style="vertical-align: top;">:</td>
                            <td style="vertical-align: top;">KETUA</td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">Alamat Lembaga</td>
                            <td style="vertical-align: top;">:</td>
                            <td style="vertical-align: top;"><?php echo isset($resultNphd['alamat_pokmas']) ? $resultNphd['alamat_pokmas'] : $pokmas->alamat; ?></td>
                        </tr>
                        <tr><td colspan="3">&nbsp;</td></tr>
                        <tr><td colspan="3"><p class="pasal">Dalam hal ini bertindak untuk dan atas nama, KETUA <?php echo $resultData->nama_kelompok; ?> sebagai Penerima Hibah yang selanjutnya disebut <b>PIHAK KEDUA</b></p></td></tr>
                        <tr><td colspan="3">&nbsp;</td></tr>
                    </table>
                </li>
            </ol>
            <?php if ($resultData->jenis_bantuan == 'Barang') { ?>
                <p class="pasal"><b>PIHAK KESATU</b> sepakat untuk memberikan Hibah Daerah berupa Barang kepada <b>PIHAK KEDUA</b> yang bersumber dari Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur dengan ketentuan sebagaimana tercantum dalam Pasal-Pasal tersebut di bawah ini:</p>
                <div style="page-break-inside: avoid;">
                    <h4 style="text-align: center;">Pasal 1</h4>
                    <h4 style="text-align: center;">JUMLAH DAN TUJUAN HIBAH</h4>
                    <table>
                        <tr>
                            <td style="width: 50px; vertical-align: top;">(1)</td>
                            <td style="vertical-align: top;"><p class="pasal"><b>PIHAK KESATU</b> memberikan Hibah Daerah kepada PIHAK KEDUA, berupa barang ........................... sebanyak ........................... unit.</p></td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td style="width: 50px; vertical-align: top;">(2)</td>
                        <td style="vertical-align: top;"><p class="pasal">Hibah sebagaimana dimaksud pada ayat (1) dipergunakan untuk ...........................  dengan rincian sebagaimana tertuang dalam Rencana Anggaran Belanja/Rencana Kerja Anggaran menjadi Lampiran NPHD.</p></td>
                    </tr>
                    <tr>
                        <td style="width: 50px; vertical-align: top;">(3)</td>
                        <td style="vertical-align: top;"><p class="pasal">Rencana Anggaran Belanja/Rencana Kerja Anggaran menjadi Lampiran NPHD sebagaimana dimaksud pada ayat (2) telah dialokasikan dalam Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur serta dituangkan dalam Keputusan Gubernur Jawa Timur tentang Penerima Hibah Berupa Barang.</p></td>
                    </tr>
                </table>
                <p>&nbsp;</p>
                <div style="page-break-inside: avoid;">
                    <h4 style="text-align: center;">Pasal 2</h4>
                    <h4 style="text-align: center;">PENYERAHAN HIBAH DAERAH</h4>
                    <table>
                        <tr>
                            <td style="width: 50px; vertical-align: top;">(1)</td>
                            <td style="vertical-align: top;"><p class="pasal"><b>PIHAK KESATU</b> menyerahkan Hibah Daerah berupa barang kepada PIHAK KEDUA dalam kondisi/keadaan baik sekaligus sebanyak ………….. unit sesuai alokasi yang ditetapkan dalam Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur serta dituangkan dalam Keputusan Gubernur Jawa Timur tentang Penerima Hibah Berupa Barang.</p></td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td style="width: 50px; vertical-align: top;">(2)</td>
                        <td style="vertical-align: top;">
                            <p class="pasal">Penyerahan Hibah Daerah berupa barang sebagaimana dimaksud pada ayat (1) disertai dengan Berita Acara Serah Terima Barang yang dilampiri:</p>
                            <ol type="a" class="pasal">
                                <li>NPHD;</li>
                                <li>Rencana Anggaran Biaya sesuai dengan yang dialokasikan dalam Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur serta dituangkan dalam Keputusan Gubernur Jawa Timur tentang Penerima Hibah Berupa Barang;</li>
                                <li>Foto copy rekening Penerima Hibah;</li>
                                <li>Pakta Integritas; dan</li>
                                <li>Foto copy Kartu Identitas pihak yang bertindak untuk dan atas nama <b>PIHAK KEDUA</b> dalam NPHD</li>
                            </ol>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50px; vertical-align: top;">(3)</td>
                        <td style="vertical-align: top;"><p class="pasal">Penyerahan Hibah Daerah berupa barang sebagaimana dimaksud pada ayat (1) dilakukan setelah penandatanganan NPHD yang dişertai dengan Berita Acara Serah Terima Barang sebagaimana dimaksud pada ayat (2).</p></td>
                    </tr>
                </table>
                <p>&nbsp;</p>
                <div style="page-break-inside: avoid;">
                    <h4 style="text-align: center;">Pasal 3</h4>
                    <h4 style="text-align: center;">KEWAJIBAN PIHAK KEDUA</h4>
                    <p class="pasal"><b>PIHAK KEDUA</b> berkewajiban:</p>
                    <ol type="a" align='justify'>
                        <li>Melaksanakan dan bertanggungjawab formal dan material atas penggunaan Hibah Daerah berupa barang sesuai dengan rincian sebagaimana tertuang dalam Rencana Anggaran Belanja/Rencana Kerja Anggaran yang menjadi Lampiran NPHD dengan mendasar pada alokasi dalam Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur serta dituangkan dalam Keputusan Gubernur Jawa Timur tentang Penerima Hibah Berupa Barang sejak ditandatanganinya NPHD dan/atau sejak diterbitkan Surat Perintah Pencairan Dana (SP2D);</li>
                    </ol>
                </div>
                <ol type="a" start="2" align='justify'>
                    <li>Menandatangani Surat Pernyataan Tanggung Jawab Mutlak dan menjamin keabsahan dokumen yang dipersyaratkan dalam permohonan hibah.</li>
                    <li>Membuat dan menyampaikan laporan penggunaan Hibah Daerah berupa barang kepada <b>PIHAK KESATU</b> paling lambat 10 (sepuluh) hari setelah Hibah Daerah berupa barang diserahkan dari <b>PIHAK KESATU</b> kepada <b>PIHAK KEDUA</b>.</li>
                </ol>
                <p>&nbsp;</p>
                <div style="page-break-inside: avoid;">
                    <h4 style="text-align: center;">Pasal 4</h4>
                    <h4 style="text-align: center;">HAK DAN KEWAJIBAN PIHAK KESATU</h4>
                    <table>
                        <tr>
                            <td style="width: 50px; vertical-align: top;">(1)</td>
                            <td style="vertical-align: top;"><p class="pasal"><b>PIHAK KESATU</b> berhak untuk tidak melakukan penyerahan Hibah Daerah berupa barang apabila <b>PIHAK KEDUA</b> tidak atau belum memenuhi persyaratan yang ditetapkan dan menyampaikan pemberitahuan kepada <b>PIHAK KEDUA</b>.</p></td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td style="width: 50px; vertical-align: top;">(2)</td>
                        <td style="vertical-align: top;"><p class="pasal"><b>PIHAK KESATU</b> berkewajiban melaksanakan monitoring dan evaluasi kelengkapan dokumen administrasi pertanggungjawaban atas penyerahan Hibah Daerah berupa barang sesuai ketentuan peraturan perundang-undangan yang berlaku.</p></td>
                    </tr>
                </table>
                <p>&nbsp;</p>
                <div style="page-break-inside: avoid;">
                    <h4 style="text-align: center;">Pasal 5</h4>
                    <h4 style="text-align: center;">LAIN-LAIN</h4>
                    <table>
                        <tr>
                            <td style="width: 50px; vertical-align: top;">(1)</td>
                            <td style="vertical-align: top;"><p class="pasal">Perjanjian Hibah Daerah ini mulai berlaku sejak tanggal ditandatangani dan berakhir sampai dengan dilakukan kegiatan monitoring dan evaluasi.</p></td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td style="width: 50px; vertical-align: top;">(2)</td>
                        <td style="vertical-align: top;"><p class="pasal">NPHD ini dibuat dalam rangkap 3 (tiga) dengan rangkap kesatu dan kedua masing-masing bermaterai cukup dan mempunyai kekuatan hukum sama.</p></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(3)</td>
                        <td style="vertical-align: top;"><p class="pasal">Hal-hal lain yang belum tercantum dalam NPHD ini dapat diatur lebih lanjut dalam Addendum NPHD sepanjang tidak bertentangan dengan ketentuan peraturan perundang-undangan yang berlaku dan berkedudukan sebagai bagian yang tidak terpisahkan dari NPHD ini.</p></td>
                    </tr>
                </table>
            <?php } else if ($resultData->jenis_bantuan == 'Uang') { ?>
                <div style="page-break-inside: avoid;">
                    <h4 style="text-align: center;">Pasal 1</h4>
                    <h4 style="text-align: center;">JUMLAH DAN TUJUAN HIBAH</h4>
                    <table>
                        <tr>
                            <td style="width: 50px; vertical-align: top;">(1)</td>
                            <td style="vertical-align: top;"><p class="pasal"><b>PIHAK KESATU</b> memberikan Hibah Daerah kepada <b>PIHAK KEDUA</b> berupa uang sebesar Rp. <?php echo $resultData->nilai_anggaran; ?> (<?php echo $terbilang; ?>) yang bersumber dari Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur.</p></td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td style="width: 50px; vertical-align: top;">(2)</td>
                        <td style="vertical-align: top;"><p class="pasal">Hibah sebagaimana dimaksud pada ayat (1) dipergunakan untuk <?php echo $resultData->perihal; ?> dengan rincian sebagaimana tertuang dalam Rencana Anggaran Belanja/Rencana Kerja Anggaran menjadi Lampiran NPHD.</p></td>
                    </tr>
                    <tr>
                        <td style="width: 50px; vertical-align: top;">(3)</td>
                        <td style="vertical-align: top;"><p class="pasal">Rencana Anggaran Belanja/Rencana Kerja Anggaran menjadi Lampiran NPHD sebagaimana dimaksud pada ayat (2) telah dialokasikan dalam Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur serta dituangkan dalam Keputusan Gubernur Jawa Timur tentang Penerima Hibah Berupa Uang.</p></td>
                    </tr>
                </table>
                <p>&nbsp;</p>
                <div style="page-break-inside: avoid;">
                    <h4 style="text-align: center;">Pasal 2</h4>
                    <h4 style="text-align: center;">PENCAIRAN DANA HIBAH DAERAH</h4>
                    <table>
                        <tr>
                            <td style="width: 50px; vertical-align: top;">(1)</td>
                            <td style="vertical-align: top;"><p class="pasal">Pencairan dana Hibah Daerah berupa uang sebagaimana sebagaimana dimaksud dalam Pasal 1, dilakukan sekaligus sebesar Rp <?php echo $resultData->nilai_anggaran; ?> (<?php echo $terbilang; ?>) sesuai alokasi yang ditetapkan dalam Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur serta dituangkan dalam Keputusan Gubernur Jawa Timur tentang Penerima Hibah Berupa Uang.</p></td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td style="width: 50px; vertical-align: top;"></td>
                        <td style="vertical-align: top;"><p class="pasal"></p></td>
                    </tr>
                    <tr>
                        <td style="width: 50px; vertical-align: top;">(2)</td>
                        <td style="vertical-align: top;">
                            <p class="pasal">Untuk pencairan dana Hibah Daerah berupa uang sebagaimana dimaksud pada ayat (1), <b>PIHAK KEDUA</b> mengajukan permohonan kepada <b>PIHAK KESATU</b> dengan dilampiri :</p>
                            <ol type="a" class="pasal">
                                <li>NPHD;</li>
                                <li>Rencana Anggaran Biaya sesuai dengan yang dialokasikan dalam Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur serta dituangkan dalam Keputusan Gubernur Jawa Timur tentang Penerima Hibah Berupa Uang;</li>
                                <li>Foto copy rekening Penerima Hibah; Bank Jatim <b><?php echo isset($resultNphd['cabang']) ? $resultNphd['cabang'] : ""; ?></b> Nomor Rekening <b><?php echo isset($resultNphd['no_rek']) ? $resultNphd['no_rek'] : ""; ?></b></li>
                                <li>Pakta Integritas; dan</li>
                                <li>Foto copy Kartu Identitas pihak yang bertindak untuk dan atas nama <b>PIHAK KEDUA</b> dalam NPHD</li>
                            </ol>
                        </td>
                    </tr>
                </table>
                <p>&nbsp;</p>
                <div style="page-break-inside: avoid;">
                    <h4 style="text-align: center;">Pasal 3</h4>
                    <h4 style="text-align: center;">KEWAJIBAN PIHAK KEDUA</h4>
                    <p class="pasal"><b>PIHAK KEDUA</b> berkewajiban:</p>
                    <ol type="a" align='justify'>
                        <li>Melaksanakan dan bertanggungjawab formal dan material atas pelaksanaan program dan kegiatan yang didanai dari hibah dengan jangka waktu pelaksanaan 3 (Tiga) bulan sesuai dengan rincian sebagaimana tertuang dalam Rencana Anggaran Belanja/Rencana Kerja Anggaran yang menjadi Lampiran NPHD dengan mendasar pada alokasi dalam Anggaran Pendapatan Belanja Daerah Provinsi Jawa Timur serta dituangkan dalam Keputusan Gubernur Jawa Timur tentang Penerima Hibah Berupa Uang sejak ditandatanganinya NPHD dan/atau sejak diterbitkan Surat Perintah Pencairan Dana (SP2D);</li>
                    </ol>
                </div>
                <ol type="a" start="2" align='justify'>
                    <li>Melaksanakan pengadaan barang dan jasa sesuai dengan ketentuan perundang-undangan yang berlaku;</li>
                    <li>Menyimpan bukti-bukti transaksi terkait dengan program dan kegiatan yang bersumber dari dana hibah Daerah;</li>
                    <li>Membuat dan menyampaikan laporan penggunaan dana hibah Daerah kepada <b>PIHAK KESATU</b> paling lambat 10 (sepuluh) hari kerja setelah kegiatan yang bersumber dari hibah selesai dilaksanakan dengan melampirkan bukti pertanggungjawaban yang merupakan tanggung jawab mutlak <b>PIHAK KEDUA</b>;</li>
                    <li>Untuk kegiatan yang pencairan dana hibahnya dilakukan menjelang akhir tahun anggaran, pembuatan dan penyampaian laporan penggunaan dana hibah Daerah tidak melebihi tanggal 10 (sepuluh) Bulan Januari tahun anggaran berikutnya;</li>
                    <li>Menandatangani Surat Pernyataan Tanggung Jawab Mutlak dan menjamin keabsahan dokumen yang dipersyaratkan dalam permohonan hibah; dan</li>
                    <li>Menyetorkan kembali sisa dana hibah Daerah yang tidak dapat direalisasikan ke Rekening Kas Daerah Propinsi Jawa Timur Nomor: 0011000477 pada PT Bank Jatim dengan menggunakan Surat Tanda Setoran (STS) paling lambat 5 (lima) hari setelah laporan pertanggungjawaban disampaikan.</li>
                </ol>
                <p>&nbsp;</p>
                <div style="page-break-inside: avoid;">
                    <h4 style="text-align: center;">Pasal 4</h4>
                    <h4 style="text-align: center;">HAK DAN KEWAJIBAN PIHAK KESATU</h4>
                    <table>
                        <tr>
                            <td style="width: 50px; vertical-align: top;">(1)</td>
                            <td style="vertical-align: top;"><p class="pasal"><b>PIHAK KESATU</b> berhak untuk tidak melakukan pencairan atau menunda pencairan dana hibah apabila <b>PIHAK KEDUA</b> tidak atau belum memenuhi persyaratan yang ditetapkan dan menyampaikan pemberitahuan kepada <b>PIHAK KEDUA</b>.</p></td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td style="width: 50px; vertical-align: top;">(2)</td>
                        <td style="vertical-align: top;"><p class="pasal"><b>PIHAK KESATU</b> berkewajiban melaksanakan monitoring dan evaluasi kelengkapan dokumen administrasi pertanggungjawaban atas penggunaan dana hibah sesuai ketentuan peraturan perundang-undangan yang berlaku.</p></td>
                    </tr>
                </table>
                <p>&nbsp;</p>
                <div style="page-break-inside: avoid;">
                    <h4 style="text-align: center;">Pasal 5</h4>
                    <h4 style="text-align: center;">ADDENDUM</h4>
                    <table>
                        <tr>
                            <td style="width: 50px; vertical-align: top;">(1)</td>
                            <td style="vertical-align: top;">
                                <p class="pasal">Dalam hal terjadi perubahan terhadap jumlah alokasi dana hibah yang bersumber dari Anggaran Pendapatan dan Belanjaan Daerah Provinsi Jawa Timur, <b>PARA PIHAK</b> melakukan addendum terhadap NPHD ini berkaitan dengan:</p>
                                <ol class="pasal" type='a'>
                                    <li>Jumlah Hibah Daerah berupa uang yang diberikan <b>PIHAK KESATU</b> kepada <b>PIHAK KEDUA</b> sebagaimana diatur dalam Pasal 1 ayat (1) NPHD ini:</li>
                                    <li>Perubahan Rencana Anggaran Belanja/Rencana Anggaran Kerja sebagai Lampiran Addendum NPHD ini dengan menyesuaikan jumlah Hibah Daerah berupa uang yang diberikan <b>PIHAK KESATU</b> kepada <b>PIHAK KEDUA</b> dengan berpedoman pada Anggaran Pendapatan dan Belanja Daerah Provinsi Jawa Timur yang dituangkan dalam Keputusan Gubernur Jawa Timur tentang Penerima Hibah Berupa Uang.</li>
                                </ol>
                            </td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td style="width: 50px; vertical-align: top;">(2)</td>
                        <td style="vertical-align: top;"><p class="pasal">Terhadap tujuan penggunaan dana Hibah Daerah sebagaimana diatur dalam Pasal 1 ayat (2) NPHD ini tidak dapat dilakukan perubahan.</p></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(3)</td>
                        <td style="vertical-align: top;"><p class="pasal">Addendum terhadap NPHD sebagaimana dimaksud pada ayat (1) merupakan bagian yang tidak terpisahkan dari NPHD ini.</p></td>
                    </tr>
                </table>
                <p>&nbsp;</p>
                <div style="page-break-inside: avoid;">
                    <h4 style="text-align: center;">Pasal 6</h4>
                    <h4 style="text-align: center;">LAIN-LAIN</h4>
                    <table>
                        <tr>
                            <td style="width: 50px; vertical-align: top;">(1)</td>
                            <td style="vertical-align: top;"><p class="pasal">Perjanjian Hibah Daerah ini mulai berlaku sejak tanggal ditandatangani dan berakhir sampai dengan dilakukan kegiatan monitoring dan evaluasi.</p></td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td style="width: 50px; vertical-align: top;">(2)</td>
                        <td style="vertical-align: top;"><p class="pasal">NPHD ini dibuat dalam rangkap 3 (tiga) dengan rangkap kesatu dan kedua masing-masing bermaterai cukup dan mempunyai kekuatan hukum sama.</p></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">(3)</td>
                        <td style="vertical-align: top;"><p class="pasal">Hal-hal lain yang belum tercantum dalam NPHD ini dapat diatur lebih lanjut dalam Addendum NPHD sepanjang tidak bertentangan dengan ketentuan peraturan perundang-undangan yang berlaku dan berkedudukan sebagai bagian yang tidak terpisahkan dari NPHD ini.</p></td>
                    </tr>
                </table>
            <?php } ?>
            <p>&nbsp;</p>
            <table style="page-break-inside: avoid;">
                <tr>
                    <td style="width: 40%; text-align: center; vertical-align: top;">
                        <table>
                            <tr><td style="text-align: center"><b>PIHAK KEDUA</b></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">Ketua <?php echo $pokmas->nama_kelompok; ?></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><u><b><?php echo $resultNphd['nama_ketua']; ?></b></u></td></tr>
                        </table>
                    </td>
                    <td style="width: 10%; text-align: center; vertical-align: top;"></td>
                    <td style="width: 50%; text-align: center; vertical-align: top;">
                        <table>
                            <tr><td style="text-align: center"><b>PIHAK KESATU</b></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><?php echo $getManagementSystem['pu_head_sign_1']; ?></td></tr>
                            <tr><td style="text-align: center"><?php echo $getManagementSystem['pu_head_sign_2']; ?></td></tr>
                            <tr><td style="text-align: center"><?php echo $getManagementSystem['pu_head_sign_3']; ?></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><u><b><?php echo $getManagementSystem['pu_nama_ketua']; ?></b></u></td></tr>
                            <tr><td style="text-align: center"><?php echo $getManagementSystem['pu_jabatan_ketua']; ?></td></tr>
                            <tr><td style="text-align: center">NIP. <?php echo $getManagementSystem['pu_nip']; ?></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </page>
    <div class="button">
        <ul class="right">
            <button class="samping" onClick="window.print();">Cetak Data</button>
            <button class="samping" onclick="window.top.close();">Kembali</button>
        </ul>
    </div>
</html>
<script>
    document.addEventListener('contextmenu', function (e) {
        e.preventDefault();
    });
    document.onkeydown = function (e) {
        if (event.keyCode == 123) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
            return false;
        }
    }
</script>