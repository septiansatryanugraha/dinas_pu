<html>
    <style>
        body {
            font-family: 'Bookman Old Style', sans-serif;
        }
        page {
            font-family: 'Bookman Old Style', sans-serif;
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
        }
        page[size="A4"] {  
            width: 21cm;
        }
        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
            }
        }
        .container {
            width: 100%;
            margin-top: 30px;
            margin-left: 70px;
        }
        button {
            width: 150px;
            height: 30px;
        }
        @media print {
            .samping {display:none;}
        }
        .button {
            width: 100%;
            position: relative;
        }
        .right {
            float: right;
            margin-right: 35%;
            display: block;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        ol {
            margin: 0;
        }
        table {
            width: 100%;
        }
    </style>
    <page size="A4">
        <div class="container">
            <i><u><h2 style="text-align: center;"> K W I T A N S I </h2></u></i>
            <p>&nbsp;</p>
            <table>
                <tr>
                    <td style="width: 170px; vertical-align: top;"><b>Terima Dari</b></td>
                    <td style="vertical-align: top;"><b>:</b></td>
                    <td style="vertical-align: top;"><b>GUBERNUR JAWA TIMUR</b></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;"><b>Terbilang</b></td>
                    <td style="vertical-align: top;"><b>:</b></td>
                    <td style="vertical-align: top; border: 3px solid #000;"><b><?php echo $terbilang; ?></b></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;"><b>Untuk Pembayaran</b></td>
                    <td style="vertical-align: top;"><b>:</b></td>
                    <td style="vertical-align: top;" class="pasal"><b>Bantuan Hibah kepada <?php echo $pokmas->nama_kelompok; ?> dalam rangka <?php echo $resultData->perihal; ?> di <?php echo isset($resultNphd['alamat_pokmas']) ? $resultNphd['alamat_pokmas'] : $pokmas->alamat; ?></b></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;"><b>Jumlah</b></td>
                    <td style="vertical-align: top;"><b>:</b></td>
                    <td style="vertical-align: top;"><span style="padding-right: 100px; border: 3px solid #000;"><b>Rp. <?php echo $resultData->nilai_anggaran; ?>,-</b></span></td>
                </tr>
            </table>
            <p>&nbsp;</p>
            <table class="pasal" style="page-break-inside: avoid;">
                <tr>
                    <td style="width: 35%; vertical-align: top;">
                        <table>
                            <tr><td style="text-align: center">Setuju dibayar</td></tr>
                            <tr><td style="text-align: center"><b><?php echo $getManagementSystem['jabatan_kepala']; ?></b></td></tr>
                            <tr><td style="text-align: center"><b>Dinas PU. Sumber Daya Air Provinsi Jawa Timur</b></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><u><b><?php echo $getManagementSystem['nama_kepala']; ?></b></u></td></tr>
                            <tr><td style="text-align: center">NIP. <?php echo $getManagementSystem['nip_kepala']; ?></td></tr>
                        </table>
                    </td>
                    <td style="width: 35%; vertical-align: top;">
                        <table>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">Lunas Bayar</td></tr>
                            <tr><td> Tgl.........................................</td></tr>
                            <tr><td style="text-align: center"><b><?php echo $getManagementSystem['jabatan_bendahara']; ?></b></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><u><b><?php echo $getManagementSystem['nama_bendahara']; ?></b></u></td></tr>
                            <tr><td style="text-align: center">NIP. <?php echo $getManagementSystem['nip_bendahara']; ?></td></tr>
                        </table>
                    </td>
                    <td style="width: 30%; vertical-align: top;">
                        <table>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td>Surabaya, <?php echo $tglNphd; ?></td></tr>
                            <tr><td style="text-align: center">Yang Menerima</td></tr>
                            <tr><td style="text-align: center"><b><?php echo $pokmas->nama_kelompok; ?><br>KETUA</b></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><u><b><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></b></u></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </page>
    <div class="button">
        <ul class="right">
            <button class="samping" onClick="window.print();">Cetak Data</button>
            <button class="samping" onclick="window.top.close();">Kembali</button>
        </ul>
    </div>
</html>
<script>
    document.addEventListener('contextmenu', function (e) {
        e.preventDefault();
    });
    document.onkeydown = function (e) {
        if (event.keyCode == 123) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
            return false;
        }
    }
</script>