<html>
    <style>
        body {
            font-family: 'Bookman Old Style', sans-serif;
        }
        page {
            font-family: 'Bookman Old Style', sans-serif;
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
        }
        page[size="A4"] {  
            width: 21cm;
        }
        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
            }
        }
        .container {
            width: 100%;
            margin-top: 30px;
            margin-left: 70px;
        }
        button {
            width: 150px;
            height: 30px;
        }
        @media print {
            .samping {display:none;}
        }
        .button {
            width: 100%;
            position: relative;
        }
        .right {
            float: right;
            margin-right: 35%;
            display: block;
        }
        .pasal {
            text-align: justify; 
            text-justify: inter-word;
        }
        ol {
            margin: 0;
        }
        table {
            width: 100%;
        }
    </style>
    <page size="A4">
        <div class="container">
            <?php
            $Replace = $resultData->perihal;
            $ReplaceText = str_replace('Permohonan', 'Bantuan', $Replace);

            ?>
            <h2 style="text-align: center;"><?php echo $pokmas->nama_kelompok; ?><br><?php echo isset($resultNphd['alamat_pokmas']) ? $resultNphd['alamat_pokmas'] : $pokmas->alamat; ?></h2><hr/>
            <!-- <br><?php echo ucwords(strtolower(trim($resultData->kabupaten))); ?> -->
            <table class="pasal">
                <tr>
                    <td style="width: 60%; vertical-align: top;">
                        <table>
                            <tr><td colspan="3">&nbsp;</td></tr>
                            <tr><td colspan="3">&nbsp;</td></tr>
                            <tr>
                                <td style="vertical-align: top">Nomor</td>
                                <td style="vertical-align: top">:</td>
                                <td style="vertical-align: top"><?php echo $resultData->no_surat; ?></td>
                                <!-- <td style="vertical-align: top"></td> -->
                            </tr>
                            <tr>
                                <td style="vertical-align: top">Lampiran</td>
                                <td style="vertical-align: top">:</td>
                                <td style="vertical-align: top">1 (satu) berkas</td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">Perihal</td>
                                <td style="vertical-align: top">:</td>
                                <td style="vertical-align: top">Bantuan Hibah Uang</td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 40%; vertical-align: top;">
                        <table>
                            <tr><td colspan="2">&nbsp;</td></tr>
                            <tr>
                                <td style="vertical-align: top" colspan="2">Kepada :</td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top">Yth.</td>
                                <td style="vertical-align: top">Ibu Gubernur Jawa Timur</td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top"></td>
                                <td style="vertical-align: top">Jl. Pahlawan 110</td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top"></td>
                                <td style="vertical-align: top">di</td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top"></td>
                                <td style="vertical-align: top"><span style="padding-left: 20px;">&nbsp;</span>SURABAYA</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <p>&nbsp;</p>
            <p class="pasal"><span style="padding-left: 50px;">&nbsp;</span>Sehubungan dengan Surat Gubernur Jawa Timur tanggal <?php echo $tglSurat; ?> Nomor : <?php echo isset($resultNphd['no_surat_ke_pokmas']) ? $resultNphd['no_surat_ke_pokmas'] : ""; ?>, maka kami sampaikan terima kasih yang sebesar-besarnya kepada Ibu yang berkenan menyetujui permohonan hibah sebesar Rp. <?php echo $resultData->nilai_anggaran; ?>,-  untuk kegiatan sesuai usulan kami dalam proposal .</p>
            <p class="pasal"><span style="padding-left: 50px;">&nbsp;</span>Berkenan dengan hal tersebut, bersama ini kami sampaikan kelengkapan persyaratan sebagai berikut :</p>
            <ol>
                <li>Rencana penggunaan dana (RAB) sebagai bahan pengendalian;</li>
                <li>Copy rekening Bank Jatim atas nama <?php echo isset($resultNphd['nama_rek']) ? $resultNphd['nama_rek'] : ""; ?>.</li>
                <li>Kwitansi asli rangkap 3 (tiga), lembar kesatu bermaterai Rp. 10.000,- yang telah ditandatangani dan distempel;</li>
                <li>Pakta Integritas bermaterai Rp. 10.000,- yang telah ditandatangani dan distempel;</li>
                <li>Naskah Perjanjian Hibah Daerah (NPHD) bermaterai Rp. 10.000,- yang telah ditandatangani dan distempel.</li>
            </ol>
            <p class="pasal"><span style="padding-left: 50px;">&nbsp;</span>Adapun rencana penggunaan dana (RAB) kami lampirkan bersama surat ini, sedangkan laporan dan pertanggungjawaban realisasi penggunaan dananya akan kami sampaikan setelah diterimanya bantuan ini, kepada Gubernur Jawa Timur melalui Dinas Pekerjaan Umum Sumber Daya Air Provinsi Jawa Timur, Jalan Gayung Kebonsari No. 169 Surabaya selambat - lambatnya 3 (tiga) bulan setelah diterimanya dana bantuan.</p>
            <p class="pasal"><span style="padding-left: 50px;">&nbsp;</span>Demikian untuk menjadi maklum dan terima kasih atas perkenannya.</p>
            <p>&nbsp;</p>
            <table style="page-break-inside: avoid;">
                <tr>
                    <td style="width: 50%; vertical-align: top;"></td>
                    <td style="width: 50%; vertical-align: top; text-align: center;">
                        <table>
                            <tr><td style="text-align: center">Surabaya, <?php echo $tglSuratPokmas; ?></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">Ketua <?php echo $pokmas->nama_kelompok; ?></td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center">&nbsp;</td></tr>
                            <tr><td style="text-align: center"><u><b><?php echo isset($resultNphd['nama_ketua']) ? $resultNphd['nama_ketua'] : $resultData->nama_ketua; ?></b></u></td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </page>
    <div class="button">
        <ul class="right">
            <button class="samping" onClick="window.print();">Cetak Data</button>
            <button class="samping" onclick="window.top.close();">Kembali</button>
        </ul>
    </div>
</html>
<script>
    document.addEventListener('contextmenu', function (e) {
        e.preventDefault();
    });
    document.onkeydown = function (e) {
        if (event.keyCode == 123) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
            return false;
        }
    }
</script>