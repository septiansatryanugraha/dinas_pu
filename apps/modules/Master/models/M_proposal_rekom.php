<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_proposal_rekom extends CI_Model
{
    const __tableName = 'tbl_proposal';
    const __tableId = 'id_proposal';

    private $idUser = null;
    private $year = null;


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->idUser = $this->session->userdata('id');
        $this->idGrupUser = $this->session->userdata('grup_id');
        $this->year = isset($_SESSION['year']) ? $_SESSION['year'] : date('Y');
    }

    function getData($isAjaxList = 0, $filter = array())
    {
        $status = $filter['status'];
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];
        $allDate = $filter['all_date'];

        $sql = "SELECT " . self::__tableName . ".*, 
                tbl_pokmas.nama_kelompok as nama_kelompok, 
                grup.nama_grup as nama_grup, 
                tbl_desa.desa as desa, 
                tbl_kecamatan.kecamatan as kecamatan, 
                tbl_kabupaten.kabupaten as kabupaten
                FROM " . self::__tableName . "
                LEFT JOIN tbl_pokmas ON tbl_pokmas.id_pokmas = " . self::__tableName . ".id_pokmas
                LEFT JOIN grup ON grup.grup_id = tbl_pokmas.id_grup
                LEFT JOIN tbl_desa ON tbl_desa.id = tbl_pokmas.id_desa
                LEFT JOIN tbl_kecamatan ON tbl_kecamatan.id = tbl_desa.id_kecamatan
                LEFT JOIN tbl_kabupaten ON tbl_kabupaten.id = tbl_kecamatan.id_kabupaten
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND " . self::__tableName . ".status";
        if (strlen($status) > 0 && $status != 'all') {
            $sql .= " = '{$status}'";
        } else {
            $sql .= " in (";
            foreach (self::selectStatus() as $key => $value) {
                if ($key > 0)
                    $sql .= ",";
                $sql .= "'{$value->nama}'";
            }
            $sql .= ")";
        }
        if ($allDate == 0) {
            if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
                $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
                $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
                $sql .= " AND " . self::__tableName . ".updated_date >= '{$tanggalAwal}'";
                $sql .= " AND " . self::__tableName . ".updated_date <= '{$tanggalAkhir}'";
            }
        }
        if ($this->idGrupUser > 2) {
            $sql .= " AND tbl_pokmas.id_grup = '{$this->idGrupUser}'";
        }
        if (strlen($this->year) > 0) {
            $sql .= " AND YEAR(" . self::__tableName . ".created_date) = '{$this->year}'";
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY " . self::__tableName . ".updated_date DESC";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT " . self::__tableName . ".*, 
                tbl_pokmas.nama_kelompok as nama_kelompok, 
                tbl_pokmas.nama_ketua as nama_ketua, 
                tbl_desa.desa as desa, 
                tbl_kecamatan.kecamatan as kecamatan, 
                tbl_kabupaten.kabupaten as kabupaten
                FROM " . self::__tableName . "
                LEFT JOIN tbl_pokmas ON tbl_pokmas.id_pokmas = " . self::__tableName . ".id_pokmas
                LEFT JOIN tbl_desa ON tbl_desa.id = tbl_pokmas.id_desa
                LEFT JOIN tbl_kecamatan ON tbl_kecamatan.id = tbl_desa.id_kecamatan
                LEFT JOIN tbl_kabupaten ON tbl_kabupaten.id = tbl_kecamatan.id_kabupaten
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND " . self::__tableName . "." . self::__tableId . " = '{$id}'";
        if ($this->idGrupUser > 2) {
            $sql .= " AND tbl_pokmas.id_grup = '{$this->idGrupUser}'";
        }
        if (strlen($this->year) > 0) {
            $sql .= " AND YEAR(" . self::__tableName . ".created_date) = '{$this->year}'";
        }
        $data = $this->db->query($sql);

        return $data->row();
    }

    function selectStatus()
    {
        $sql = " select * from status_grup WHERE nama in ('Verifikasi','Rekom Bidang','Tidak Rekom')";
        $data = $this->db->query($sql);

        return $data->result();
    }

    function getDataDetail($id)
    {
        $sql = " select * from " . self::__tableName . "_detail WHERE deleted_date IS NULL AND " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->result();
    }
}
