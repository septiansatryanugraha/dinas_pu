<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_survei extends CI_Model
{
    const __tableName = 'tbl_proposal';
    const __tableName2 = 'tbl_user';
    const __tableName3 = 'tbl_history_dokumen';
    const __tableName4 = 'tbl_survey';
    const __tableName5 = 'tbl_gambar_survey';
    const __tableName6 = 'tbl_checklis';
    const __tableId = 'id_proposal';
    const __tableId2 = 'id_survey';

    private $idUser = null;
    private $year = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->idUser = $this->session->userdata('id');
        $this->idGrupUser = $this->session->userdata('grup_id');
        $this->year = isset($_SESSION['year']) ? $_SESSION['year'] : date('Y');
    }

    function getData($isAjaxList = 0, $filter = array())
    {
        $survei = $filter['survei'];
        $statusFile = isset($filter['statusFile']) ? true : false;
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];
        $allDate = $filter['all_date'];

        $sql = "SELECT " . self::__tableName . ".*, 
                tbl_pokmas.nama_kelompok as nama_kelompok, 
                grup.nama_grup as nama_grup, 
                tbl_desa.desa as desa, 
                tbl_kecamatan.kecamatan as kecamatan, 
                tbl_kabupaten.kabupaten as kabupaten";
        if ($statusFile) {
            $sql .= "   ,IFNULL(hasil_survei.status_file, '<small class=\"label pull-center bg-red\">Berkas belum lengkap</small>') AS status_hasil_survei
                        ,IFNULL(sketsa_survei.status_file, '<small class=\"label pull-center bg-red\">Berkas belum lengkap</small>') AS status_sketsa_survei
                        ,IFNULL(berita_acara.status_file, '<small class=\"label pull-center bg-red\">Berkas belum lengkap</small>') AS status_berita_acara
                        ,IFNULL(checklist.status_file, '<small class=\"label pull-center bg-red\">Berkas belum lengkap</small>') AS status_checklist";
        }
        $sql .= "   FROM " . self::__tableName . "
                    LEFT JOIN tbl_pokmas ON tbl_pokmas.id_pokmas = " . self::__tableName . ".id_pokmas
                    LEFT JOIN grup ON grup.grup_id = tbl_pokmas.id_grup
                    LEFT JOIN tbl_desa ON tbl_desa.id = tbl_pokmas.id_desa
                    LEFT JOIN tbl_kecamatan ON tbl_kecamatan.id = tbl_desa.id_kecamatan
                    LEFT JOIN tbl_kabupaten ON tbl_kabupaten.id = tbl_kecamatan.id_kabupaten";
        if ($statusFile) {
            $sql .= "   LEFT JOIN 
                        (
                            SELECT tbl_history_upload.ref_id AS ref_id
                            ,'<small class=\"label pull-center bg-blue\">Berkas sudah lengkap</small>' AS status_file 
                            FROM tbl_history_upload
                            WHERE tbl_history_upload.deleted_date IS NULL 
                            AND tbl_history_upload.ref_table = '" . self::__tableName . "'
                            AND tbl_history_upload.status LIKE '%hasil survei%' 
                            GROUP BY tbl_history_upload.ref_table,tbl_history_upload.ref_id
                        ) AS hasil_survei ON hasil_survei.ref_id = " . self::__tableName . "." . self::__tableId . "
                        LEFT JOIN 
                        (
                            SELECT tbl_history_upload.ref_id AS ref_id
                            ,'<small class=\"label pull-center bg-blue\">Berkas sudah lengkap</small>' AS status_file 
                            FROM tbl_history_upload
                            WHERE tbl_history_upload.deleted_date IS NULL 
                            AND tbl_history_upload.ref_table = '" . self::__tableName . "'
                            AND tbl_history_upload.status LIKE '%sketsa survei%' 
                            GROUP BY tbl_history_upload.ref_table,tbl_history_upload.ref_id
                        ) AS sketsa_survei ON sketsa_survei.ref_id = " . self::__tableName . "." . self::__tableId . "
                        LEFT JOIN 
                        (
                            SELECT tbl_history_upload.ref_id AS ref_id
                            ,'<small class=\"label pull-center bg-blue\">Berkas sudah lengkap</small>' AS status_file 
                            FROM tbl_history_upload
                            WHERE tbl_history_upload.deleted_date IS NULL 
                            AND tbl_history_upload.ref_table = '" . self::__tableName . "'
                            AND tbl_history_upload.status LIKE '%berita acara%' 
                            GROUP BY tbl_history_upload.ref_table,tbl_history_upload.ref_id
                        ) AS berita_acara ON berita_acara.ref_id = " . self::__tableName . "." . self::__tableId . "
                        LEFT JOIN 
                        (
                            SELECT tbl_history_upload.ref_id AS ref_id
                            ,'<small class=\"label pull-center bg-blue\">Berkas sudah lengkap</small>' AS status_file 
                            FROM tbl_history_upload
                            WHERE tbl_history_upload.deleted_date IS NULL 
                            AND tbl_history_upload.ref_table = '" . self::__tableName . "'
                            AND tbl_history_upload.status LIKE '%checklist%' 
                            GROUP BY tbl_history_upload.ref_table,tbl_history_upload.ref_id
                        ) AS checklist ON checklist.ref_id = " . self::__tableName . "." . self::__tableId . "";
        }
        $sql .= "   WHERE " . self::__tableName . ".deleted_date IS NULL
                    AND " . self::__tableName . ".survei";
        if (strlen($survei) > 0 && $survei != 'all') {
            $sql .= " = '{$survei}'";
        } else {
            $sql .= " in ('Survey','Selesai Survey')";
        }
        if ($allDate == 0) {
            if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
                $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
                $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
                $sql .= " AND " . self::__tableName . ".created_date >= '{$tanggalAwal}'";
                $sql .= " AND " . self::__tableName . ".created_date <= '{$tanggalAkhir}'";
            }
        }
        if ($this->idGrupUser > 2) {
            $sql .= " AND tbl_pokmas.id_grup = '{$this->idGrupUser}'";
        }
        if (strlen($this->year) > 0) {
            $sql .= " AND YEAR(" . self::__tableName . ".created_date) = '{$this->year}'";
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY " . self::__tableName . ".id_proposal DESC";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT " . self::__tableName4 . ".*, 
                tbl_proposal.jenis_bantuan as jenis_bantuan, 
                tbl_proposal.barang as barang_tbl_proposal, 
                tbl_proposal.usulan_barang as usulan_barang, 
                tbl_proposal.nilai_anggaran as nilai_anggaran_proposal, 
                tbl_proposal.usulan_nilai_anggaran as usulan_nilai_anggaran, 
                tbl_pokmas.nama_kelompok as nama_kelompok, 
                tbl_pokmas.nama_ketua as nama_ketua, 
                tbl_pokmas.alamat as alamat_pokmas, 
                IF(LENGTH(" . self::__tableName4 . ".latitude) > 0, " . self::__tableName4 . ".latitude, tbl_pokmas.latitude) as latitude, 
                IF(LENGTH(" . self::__tableName4 . ".longitude) > 0, " . self::__tableName4 . ".longitude, tbl_pokmas.longitude) as longitude, 
                tbl_desa.desa as desa, 
                tbl_kecamatan.kecamatan as kecamatan, 
                tbl_kabupaten.kabupaten as kabupaten
                FROM " . self::__tableName4 . "
                LEFT JOIN tbl_proposal ON tbl_proposal.id_proposal = " . self::__tableName4 . ".id_proposal
                LEFT JOIN tbl_pokmas ON tbl_pokmas.id_pokmas = tbl_proposal.id_pokmas
                LEFT JOIN tbl_desa ON tbl_desa.id = tbl_pokmas.id_desa
                LEFT JOIN tbl_kecamatan ON tbl_kecamatan.id = tbl_desa.id_kecamatan
                LEFT JOIN tbl_kabupaten ON tbl_kabupaten.id = tbl_kecamatan.id_kabupaten
                WHERE " . self::__tableName4 . ".deleted_date IS NULL
                AND " . self::__tableName4 . "." . self::__tableId . " = '{$id}'";
        if ($this->idGrupUser > 2) {
            $sql .= " AND tbl_pokmas.id_grup = '{$this->idGrupUser}'";
        }
        if (strlen($this->year) > 0) {
            $sql .= " AND YEAR(" . self::__tableName . ".created_date) = '{$this->year}'";
        }
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function hapus2($table, $where)
    {
        $res = $this->db->update($table, array('deleted_date' => date('Y-m-d H:i:s')), $where);
        return $res;
    }

    public function insert($data = array())
    {
        $insert = $this->db->insert_batch('tbl_gambar_survey', $data);
        return $insert;
    }

    function selectStatusSurvey()
    {
        $sql = " select * from status_grup WHERE nama in ('Selesai Survey')";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectByIdCheckList($id)
    {
        $sql = "SELECT * FROM " . self::__tableName6 . " WHERE deleted_date IS NULL AND " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function selectGbrSurvey($id)
    {
        $sql = "SELECT * FROM " . self::__tableName5 . " WHERE deleted_date IS NULL AND " . self::__tableId2 . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->result();
    }

    function getDataDetail($id)
    {
        $sql = " select * from " . self::__tableName . "_detail WHERE deleted_date IS NULL AND " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->result();
    }

    function getDataRow($idProposal)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE deleted_date IS NULL AND " . self::__tableId . " = '{$idProposal}'";
        $data = $this->db->query($sql);

        return $data->row();
    }
}
