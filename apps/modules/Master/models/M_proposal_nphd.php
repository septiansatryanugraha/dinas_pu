<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_proposal_nphd extends CI_Model
{
    const __tableName = 'tbl_proposal';
    const __tableId = 'id_proposal';

    private $idUser = null;
    private $year = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->idUser = $this->session->userdata('id');
        $this->idGrupUser = $this->session->userdata('grup_id');
        $this->year = isset($_SESSION['year']) ? $_SESSION['year'] : date('Y');
    }

    function getData($isAjaxList = 0, $filter = array())
    {
        $status = $filter['status'];
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];
        $allDate = $filter['all_date'];

        $sql = "SELECT " . self::__tableName . ".*, 
                tbl_pokmas.nama_kelompok as nama_kelompok, 
                tbl_pokmas.id_grup as id_grup, 
                grup.nama_grup as nama_grup, 
                tbl_desa.desa as desa, 
                tbl_kecamatan.kecamatan as kecamatan, 
                tbl_kabupaten.kabupaten as kabupaten
                FROM " . self::__tableName . "
                LEFT JOIN tbl_pokmas ON tbl_pokmas.id_pokmas = " . self::__tableName . ".id_pokmas
                LEFT JOIN grup ON grup.grup_id = tbl_pokmas.id_grup
                LEFT JOIN tbl_desa ON tbl_desa.id = tbl_pokmas.id_desa
                LEFT JOIN tbl_kecamatan ON tbl_kecamatan.id = tbl_desa.id_kecamatan
                LEFT JOIN tbl_kabupaten ON tbl_kabupaten.id = tbl_kecamatan.id_kabupaten
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND " . self::__tableName . ".status";
        if (strlen($status) > 0 && $status != 'all') {
            $sql .= " = '{$status}'";
        } else {
            $sql .= " in (";
            foreach (self::selectStatus() as $key => $value) {
                if ($key > 0)
                    $sql .= ",";
                $sql .= "'{$value->nama}'";
            }
            $sql .= ")";
        }
        if ($allDate == 0) {
            if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
                $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
                $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
                $sql .= " AND " . self::__tableName . ".updated_date >= '{$tanggalAwal}'";
                $sql .= " AND " . self::__tableName . ".updated_date <= '{$tanggalAkhir}'";
            }
        }
        if ($this->idGrupUser > 2) {
            $sql .= " AND tbl_pokmas.id_grup = '{$this->idGrupUser}'";
        }
        if (strlen($this->year) > 0) {
            $sql .= " AND YEAR(" . self::__tableName . ".created_date) = '{$this->year}'";
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY " . self::__tableName . ".updated_date DESC";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT " . self::__tableName . ".*, 
                tbl_pokmas.nama_kelompok as nama_kelompok, 
                tbl_pokmas.nama_ketua as nama_ketua, 
                tbl_pokmas.no_ktp as no_ktp, 
                tbl_pokmas.id_grup as id_grup,
                tbl_desa.desa as desa, 
                tbl_kecamatan.kecamatan as kecamatan, 
                tbl_kabupaten.kabupaten as kabupaten
                FROM " . self::__tableName . "
                LEFT JOIN tbl_pokmas ON tbl_pokmas.id_pokmas = " . self::__tableName . ".id_pokmas
                LEFT JOIN tbl_desa ON tbl_desa.id = tbl_pokmas.id_desa
                LEFT JOIN tbl_kecamatan ON tbl_kecamatan.id = tbl_desa.id_kecamatan
                LEFT JOIN tbl_kabupaten ON tbl_kabupaten.id = tbl_kecamatan.id_kabupaten
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND " . self::__tableName . "." . self::__tableId . " = '{$id}'";
        if ($this->idGrupUser > 2) {
            $sql .= " AND tbl_pokmas.id_grup = '{$this->idGrupUser}'";
        }
        if (strlen($this->year) > 0) {
            $sql .= " AND YEAR(" . self::__tableName . ".created_date) = '{$this->year}'";
        }
        $data = $this->db->query($sql);

        return $data->row();
    }

    function selectStatus()
    {
        $sql = " select * from status_grup WHERE nama in ('Penetapan','NPHD')";
        $data = $this->db->query($sql);

        return $data->result();
    }

    function selectBysurvey($id)
    {
        $sql = " select * from tbl_survey where id_proposal = $id";
        $data = $this->db->query($sql);
        return $data->row();
    }

    function getDataLpj($isAjaxList = 0, $filter = array())
    {
        $statusPenerimaan = $filter['status_penerimaan'];
        $startYear = $filter['start_year'];
        $tanggalAkhir = $filter['tanggal_akhir'];

        $sql = "SELECT " . self::__tableName . "." . self::__tableId . "
                FROM " . self::__tableName . "
                LEFT JOIN tbl_pokmas ON tbl_pokmas.id_pokmas = " . self::__tableName . ".id_pokmas
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND " . self::__tableName . ".nphd IS NOT NULL";
        if (strlen($startYear) > 0 && strlen($tanggalAkhir) > 0) {
            $tanggalAwal = $startYear . '-01-01 00:00:00';
            $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
            $sql .= " AND " . self::__tableName . ".created_date >= '{$tanggalAwal}'";
            $sql .= " AND " . self::__tableName . ".created_date <= '{$tanggalAkhir}'";
        }
        if ($this->idGrupUser > 2) {
            $sql .= " AND tbl_pokmas.id_grup = '{$this->idGrupUser}'";
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY " . self::__tableName . ".updated_date DESC";
        }
        $data = $this->db->query($sql)->result_array();
        $result = [];
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $proposal = (array) self::selectById($value['id_proposal']);
                if (strlen($proposal['nphd']) > 0) {
                    $resultNphd = json_decode($proposal['nphd'], TRUE);
                    if (isset($resultNphd['status_penerimaan']) && strlen($resultNphd['status_penerimaan']) > 0) {
                        if (strlen($statusPenerimaan) > 0) {
                            if ($statusPenerimaan == 'all') {
                                $result[] = $proposal;
                            } else {
                                if ($statusPenerimaan == $resultNphd['status_penerimaan']) {
                                    $result[] = $proposal;
                                }
                            }
                        } else {
                            $result[] = $proposal;
                        }
                    }
                }
            }
        }

        return $result;
    }
}
