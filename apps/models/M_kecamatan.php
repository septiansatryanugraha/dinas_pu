<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kecamatan extends CI_Model
{
    const __tableName = 'tbl_kecamatan';
    const __tableId = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getData($isAjaxList = 0, $filter = array())
    {
        $idKabupaten = $filter['id_kabupaten'];

        $sql = "SELECT " . self::__tableName . ".*, 
                tbl_kabupaten.kabupaten as kabupaten
                FROM " . self::__tableName . "
                LEFT JOIN tbl_kabupaten ON tbl_kabupaten.id = " . self::__tableName . ".id_kabupaten
                LEFT JOIN tbl_provinsi ON tbl_provinsi.id = tbl_kabupaten.id_provinsi
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND tbl_kabupaten.deleted_date IS NULL
                AND tbl_provinsi.deleted_date IS NULL";
        if (strlen($idKabupaten) > 0 && $idKabupaten != 'all') {
            $sql .= " AND " . self::__tableName . ".id_kabupaten = '{$idKabupaten}'";
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY " . self::__tableName . ".created_date DESC";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT " . self::__tableName . ".*,
                tbl_kabupaten.kabupaten as kabupaten
                FROM " . self::__tableName . "
                LEFT JOIN tbl_kabupaten ON tbl_kabupaten.id = " . self::__tableName . ".id_kabupaten
                LEFT JOIN tbl_provinsi ON tbl_provinsi.id = tbl_kabupaten.id_provinsi
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND tbl_kabupaten.deleted_date IS NULL
                AND tbl_provinsi.deleted_date IS NULL
                AND " . self::__tableName . "." . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function selectByExist($arrWhere = array(), $id = null)
    {
        $sql = "SELECT " . self::__tableName . ".*,
                tbl_kabupaten.kabupaten as kabupaten
                FROM " . self::__tableName . "
                LEFT JOIN tbl_kabupaten ON tbl_kabupaten.id = " . self::__tableName . ".id_kabupaten
                LEFT JOIN tbl_provinsi ON tbl_provinsi.id = tbl_kabupaten.id_provinsi
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND tbl_kabupaten.deleted_date IS NULL
                AND tbl_provinsi.deleted_date IS NULL";
        if (is_array($arrWhere)) {
            foreach ($arrWhere as $key => $value) {
                $sql .= " AND " . self::__tableName . "." . $key . " = '{$value}'";
            }
        }
        if ($id != null) {
            $sql .= " AND " . self::__tableName . "." . self::__tableId . " <> '{$id}'";
        }
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function checkForeign($id)
    {
        $isExist = false;
        if (!$isExist) {
            $qDesa = "SELECT * FROM tbl_desa WHERE deleted_date IS NULL AND id_kecamatan = '{$id}'";
            $resDesa = $this->db->query($qDesa)->row();
            if ($resDesa != null) {
                $isExist = true;
            }
        }

        return $isExist;
    }

    public function select($where = array())
    {
        $this->db->select(self::__tableName . '.*');
        $this->db->from(self::__tableName);
        $this->db->join('tbl_kabupaten', 'tbl_kabupaten.id = ' . self::__tableName . '.id_kabupaten', 'left');
        $this->db->join('tbl_provinsi', 'tbl_provinsi.id = tbl_kabupaten.id_provinsi', 'left');
        if (count($where) > 0) {
            foreach ($where as $key => $value) {
                $this->db->where(self::__tableName . '.' . $key, $value);
            }
        }
        $this->db->where(self::__tableName . '.deleted_date IS NULL');
        $this->db->where('tbl_kabupaten.deleted_date IS NULL');
        $this->db->where('tbl_provinsi.deleted_date IS NULL');
        $this->db->order_by(self::__tableName . '.kecamatan', 'ASC');
        $data = $this->db->get();

        return $data->result();
    }
}
