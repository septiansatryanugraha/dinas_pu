<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_grup extends CI_Model
{

    public function getData()
    {
        $sql = " select * from grup WHERE deleted_date IS NULL";
        if ($this->session->userdata('grup_id') != 1) {
            $sql .= " and grup_id <> '1'";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM grup WHERE deleted_date IS NULL AND grup_id = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function selectByName($name, $id = null)
    {
        $sql = "SELECT * FROM grup WHERE deleted_date IS NULL AND nama_grup = '{$name}'";
        if ($id != null) {
            $sql .= " AND grup_id <> '{$id}'";
        }
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function checkForeign($id)
    {
        $isExist = false;
        if (!$isExist) {
            $qAdmin = "SELECT * FROM admin WHERE deleted_date IS NULL AND grup_id = '{$id}'";
            $resAdmin = $this->db->query($qAdmin)->row();
            if ($resAdmin != null) {
                $isExist = true;
            }
        }
        if (!$isExist) {
            $qMenuAkses = "SELECT * FROM menu_akses WHERE deleted_date IS NULL AND grup_id = '{$id}'";
            $resMenuAkses = $this->db->query($qMenuAkses)->row();
            if ($resMenuAkses != null) {
                $isExist = true;
            }
        }

        return $isExist;
    }

    // VIEW USER PRIVILEGE
    public function akses_grup($loop)
    {
        if (!$loop)
            return FALSE;
        $query = "  select a.id_menuakses, b.id_menu as menus, a.id_menu, b.nama_menu, a.grup_id, b.menuparent, c.nama_grup, a.view, a.add, a.edit, a.del, b.menu_file
					from (select * from menu_akses where grup_id = '" . $loop . "') a
					right join (select a.*, b.nama_menu as menuparent from tbl_menu a left join tbl_menu b on a.parent = b.id_menu where a.deleted_date is null) b on a.id_menu = b.id_menu
					left join grup c on a.grup_id = c.grup_id
                    where c.deleted_date is null
					order by id_menu ASC";
        $data = $this->db->query($query);

        return $data->result();
    }

    public function select($where = array(), $whereIn = array())
    {
        $this->db->select('grup.*');
        $this->db->from('grup');
        if (count($where) > 0) {
            foreach ($where as $key => $value) {
                $this->db->where('grup.' . $key, $value);
            }
        }
        if (count($whereIn) > 0) {
            $this->db->where_in("grup.nama_grup", $whereIn);
        }
        $this->db->where('grup.deleted_date IS NULL');
        $this->db->order_by('grup.nama_grup', 'ASC');
        $data = $this->db->get();

        return $data->result();
    }
}
