<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pokmas extends CI_Model
{
    const __tableName = 'tbl_pokmas';
    const __tableId = 'id_pokmas';

    private $idUser = null;
    private $year = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->idUser = $this->session->userdata('id');
        $this->idGrupUser = $this->session->userdata('grup_id');
        $this->year = isset($_SESSION['year']) ? $_SESSION['year'] : date('Y');
    }

    function getData($isAjaxList = 0, $filter = array())
    {
        $status = $filter['status'];
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];
        $allDate = $filter['all_date'];

        $sql = "SELECT " . self::__tableName . ".*, 
                grup.nama_grup as nama_grup, 
                tbl_desa.desa as desa,
                tbl_kecamatan.kecamatan as kecamatan,
                tbl_kabupaten.kabupaten as kabupaten
                FROM " . self::__tableName . "
                LEFT JOIN grup ON grup.grup_id = " . self::__tableName . ".id_grup
                LEFT JOIN tbl_desa ON tbl_desa.id = " . self::__tableName . ".id_desa
                LEFT JOIN tbl_kecamatan ON tbl_kecamatan.id = tbl_desa.id_kecamatan
                LEFT JOIN tbl_kabupaten ON tbl_kabupaten.id = tbl_kecamatan.id_kabupaten
                WHERE " . self::__tableName . ".deleted_date IS NULL";
        if (strlen($status) > 0 && $status != 'all') {
            $sql .= " AND " . self::__tableName . ".status = '{$status}'";
        }
        if ($allDate == 0) {
            if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
                $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
                $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
                $sql .= "   AND " . self::__tableName . ".created_date >= '{$tanggalAwal}'
                            AND " . self::__tableName . ".created_date <= '{$tanggalAkhir}'";
            }
        }
        if ($this->idGrupUser > 2) {
            $sql .= " AND " . self::__tableName . ".id_grup = '{$this->idGrupUser}'";
        }
        if (strlen($this->year) > 0) {
            $sql .= " AND YEAR(" . self::__tableName . ".created_date) = '{$this->year}'";
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY " . self::__tableName . ".id_pokmas DESC";
        }

        $data = $this->db->query($sql);
        return $data->result();
    }

    public function select($where = array(), $whereIn = array())
    {
        $this->db->from(self::__tableName);
        $this->db->where(self::__tableName . ".deleted_date IS NULL");
        if ($this->idGrupUser > 2) {
            $this->db->where(self::__tableName . ".id_grup", $this->idGrupUser);
        }
        if (count($where) > 0) {
            foreach ($where as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        if (count($whereIn) > 0) {
            $this->db->where_in(self::__tableName . ".nama_kelompok", $whereIn);
        }
        $this->db->order_by('id_pokmas', 'ASC');
        $data = $this->db->get();

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT " . self::__tableName . ".* FROM " . self::__tableName . "
                WHERE " . self::__tableName . ".deleted_date IS NULL 
                AND " . self::__tableName . "." . self::__tableId . " = '{$id}'";
        if ($this->idGrupUser > 2) {
            $sql .= " AND " . self::__tableName . ".id_grup = '{$this->idGrupUser}'";
        }
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function selectByName($name)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE deleted_date IS NULL AND nama_kelompok = '{$name}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function selectByRandom($where = array(), $whereIn = array())
    {
        $this->db->from(self::__tableName);
        $this->db->where(self::__tableName . ".deleted_date IS NULL");
        if ($this->idGrupUser > 2) {
            $this->db->where(self::__tableName . ".id_grup", $this->idGrupUser);
        }
        if (count($where) > 0) {
            foreach ($where as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        if (count($whereIn) > 0) {
            $this->db->where_in(self::__tableName . ".nama_kelompok", $whereIn);
        }
        $this->db->order_by('id_pokmas', 'ASC');
        $data = $this->db->get();

        return $data->row();
    }

    public function checkForeign($id)
    {
        $isExist = false;
        if (!$isExist) {
            $qProposal = "SELECT * FROM tbl_proposal WHERE deleted_date IS NULL AND id_pokmas = '{$id}'";
            $resProposal = $this->db->query($qProposal)->row();
            if ($resProposal != null) {
                $isExist = true;
            }
        }

        return $isExist;
    }

    public function checkFiles($value)
    {
        $this->db->select('nama_kelompok');
        $this->db->from('tbl_pokmas');
        $this->db->where('deleted_date IS NULL');
        $this->db->like('nama_kelompok', $value);
        // $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}
