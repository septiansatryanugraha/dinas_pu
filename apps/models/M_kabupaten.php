<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kabupaten extends CI_Model
{
    const __tableName = 'tbl_kabupaten';
    const __tableId = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getData($isAjaxList = 0)
    {
        $sql = "SELECT " . self::__tableName . ".*
                FROM " . self::__tableName . "
                LEFT JOIN tbl_provinsi ON tbl_provinsi.id = " . self::__tableName . ".id_provinsi
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND tbl_provinsi.deleted_date IS NULL";
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY updated_date DESC";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT " . self::__tableName . ".*
                FROM " . self::__tableName . "
                LEFT JOIN tbl_provinsi ON tbl_provinsi.id = " . self::__tableName . ".id_provinsi
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND tbl_provinsi.deleted_date IS NULL
                AND " . self::__tableName . "." . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function selectByExist($arrWhere = array(), $id = null)
    {
        $sql = "SELECT " . self::__tableName . ".*,
                FROM " . self::__tableName . "
                LEFT JOIN tbl_provinsi ON tbl_provinsi.id = " . self::__tableName . ".id_provinsi
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND tbl_provinsi.deleted_date IS NULL";
        if (is_array($arrWhere)) {
            foreach ($arrWhere as $key => $value) {
                $sql .= " AND " . $key . " = '{$value}'";
            }
        }
        if ($id != null) {
            $sql .= " AND " . self::__tableId . " <> '{$id}'";
        }
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function checkForeign($id)
    {
        $isExist = false;
        if (!$isExist) {
            $qKecamatan = "SELECT * FROM tbl_kecamatan WHERE deleted_date IS NULL AND id_kabupaten = '{$id}'";
            $resKecamatan = $this->db->query($qKecamatan)->row();
            if ($resKecamatan != null) {
                $isExist = true;
            }
        }

        return $isExist;
    }

    public function select()
    {
        $this->db->select(self::__tableName . '.*');
        $this->db->from(self::__tableName);
        $this->db->join('tbl_provinsi', 'tbl_provinsi.id = ' . self::__tableName . '.id_provinsi', 'left');
        $this->db->where(self::__tableName . '.deleted_date IS NULL');
        $this->db->where('tbl_provinsi.deleted_date IS NULL');
        $this->db->order_by(self::__tableName . '.kabupaten', 'ASC');
        $data = $this->db->get();

        return $data->result();
    }
}
