<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_verifikator extends CI_Model
{
    const __tableName = 'tbl_verifikator';
    const __tableId = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getData($isAjaxList = 0, $filter = array())
    {
        $grupId = $filter['grup_id'];

        $sql = "SELECT " . self::__tableName . ".*, 
                grup.nama_grup as nama_grup
                FROM " . self::__tableName . "
                LEFT JOIN grup ON grup.grup_id= " . self::__tableName . ".grup_id
                WHERE " . self::__tableName . ".deleted_date IS NULL";
        if (strlen($grupId) > 0 && $grupId != 'all') {
            $sql .= " AND " . self::__tableName . ".grup_id = '{$grupId}'";
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY " . self::__tableName . ".created_date DESC";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT " . self::__tableName . ".*, 
                grup.nama_grup as nama_grup
                FROM " . self::__tableName . "
                LEFT JOIN grup ON grup.grup_id= " . self::__tableName . ".grup_id
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND " . self::__tableName . "." . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function selectByUsername($username, $id = null)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE deleted_date IS NULL AND username = '{$username}'";
        if ($id != null) {
            $sql .= " AND id <> '{$id}'";
        }
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function checkForeign($id)
    {
        $isExist = false;
        if (!$isExist) {
            $qDesa = "SELECT * FROM tbl_proposal WHERE deleted_date IS NULL AND id_verifikator = '{$id}'";
            $resDesa = $this->db->query($qDesa)->row();
            if ($resDesa != null) {
                $isExist = true;
            }
        }

        return $isExist;
    }
}
