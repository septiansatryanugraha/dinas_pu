<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_utilities extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function historiFile($refTable, $refId, $status, $limit = 0)
    {
        $sql = "SELECT * FROM tbl_history_upload
                WHERE deleted_date IS NULL 
                AND ref_table = '{$refTable}' 
                AND ref_id = '{$refId}'
                AND status LIKE '%{$status}%' 
                ORDER BY id_history_file DESC";
        if ($limit > 0) {
            $sql .= " LIMIT 1";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function getKelompokDpa($idPokmas = '')
    {
        $sql = "SELECT * 
                FROM tbl_history_pengajuan
                WHERE deleted_date IS NULL 
                AND id_pokmas = '{$idPokmas}'
                ORDER BY id_pokmas DESC 
                LIMIT 1";
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function getManagementSystem($idGrup = null)
    {
        $sql = "SELECT * 
                FROM management_system
                WHERE deleted_date IS NULL";
        if ($idGrup != null) {
            $sql .= " AND id_grup IS NULL OR id_grup = '{$idGrup}'";
        } else {
            $sql .= " AND id_grup IS NULL";
        }
        $data = $this->db->query($sql)->result_array();

        $arrData = [];
        foreach ($data as $key => $value) {
            $arrData[$value['option_name']] = $value['option_value'];
        }

        return $arrData;
    }
}
