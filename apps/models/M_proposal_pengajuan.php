<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_proposal_pengajuan extends CI_Model
{
    const __tableName = 'tbl_proposal';
    const __tableId = 'id_proposal';

    private $idUser = null;
    private $year = null;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->idUser = $this->session->userdata('id');
        $this->idGrupUser = $this->session->userdata('grup_id');
        $this->year = isset($_SESSION['year']) ? $_SESSION['year'] : date('Y');
    }

    function getData($isAjaxList = 0, $filter = array())
    {
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];
        $allDate = $filter['all_date'];

        $sql = "SELECT " . self::__tableName . ".*,
                tbl_pokmas.nama_kelompok as nama_kelompok, 
                grup.nama_grup as nama_grup, 
                tbl_desa.desa as desa,
                tbl_kecamatan.kecamatan as kecamatan,
                tbl_kabupaten.kabupaten as kabupaten
                FROM " . self::__tableName . "
                LEFT JOIN tbl_pokmas ON tbl_pokmas.id_pokmas = " . self::__tableName . ".id_pokmas
                LEFT JOIN grup ON grup.grup_id = tbl_pokmas.id_grup
                LEFT JOIN tbl_desa ON tbl_desa.id = tbl_pokmas.id_desa
                LEFT JOIN tbl_kecamatan ON tbl_kecamatan.id = tbl_desa.id_kecamatan
                LEFT JOIN tbl_kabupaten ON tbl_kabupaten.id = tbl_kecamatan.id_kabupaten
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND " . self::__tableName . ".status in ('Belum Verifikasi', 'Verifikasi', 'Tidak Rekom', 'Rekom Bidang', 'Direvisi')";
        if ($allDate == 0) {
            if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
                $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
                $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
                $sql .= " AND " . self::__tableName . ".updated_date >= '{$tanggalAwal}'";
                $sql .= " AND " . self::__tableName . ".updated_date <= '{$tanggalAkhir}'";
            }
        }
        if ($this->idGrupUser > 2) {
            $sql .= " AND tbl_pokmas.id_grup = '{$this->idGrupUser}'";
        }
        if (strlen($this->year) > 0) {
            $sql .= " AND YEAR(" . self::__tableName . ".created_date) = '{$this->year}'";
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY " . self::__tableName . ".updated_date DESC";
        }
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT " . self::__tableName . ".*,
                tbl_pokmas.nama_kelompok as nama_kelompok,
                tbl_pokmas.kode_pokmas as kode_pokmas,
                tbl_pokmas.nama_ketua as nama_ketua,
                tbl_pokmas.alamat as alamat,
                tbl_desa.id as id_desa,
                tbl_desa.desa as desa,
                tbl_kecamatan.id as id_kecamatan,
                tbl_kecamatan.kecamatan as kecamatan,
                tbl_kabupaten.id as id_kabupaten,
                tbl_kabupaten.kabupaten as kabupaten
                FROM " . self::__tableName . "
                LEFT JOIN tbl_pokmas ON tbl_pokmas.id_pokmas = " . self::__tableName . ".id_pokmas
                LEFT JOIN tbl_desa ON tbl_desa.id = tbl_pokmas.id_desa
                LEFT JOIN tbl_kecamatan ON tbl_kecamatan.id = tbl_desa.id_kecamatan
                LEFT JOIN tbl_kabupaten ON tbl_kabupaten.id = tbl_kecamatan.id_kabupaten
                WHERE " . self::__tableName . ".deleted_date IS NULL
                AND " . self::__tableName . "." . self::__tableId . " = '{$id}'";
        if ($this->idGrupUser > 2) {
            $sql .= " AND tbl_pokmas.id_grup = '{$this->idGrupUser}'";
        }
        if (strlen($this->year) > 0) {
            $sql .= " AND YEAR(" . self::__tableName . ".created_date) = '{$this->year}'";
        }
        $data = $this->db->query($sql);

        return $data->row();
    }

    public function select_histori($id)
    {
        $sql = "SELECT a.*, b.kode_proposal as kode_proposal
                FROM tbl_log as a
                LEFT JOIN tbl_proposal b ON b.id_proposal = a.id_proposal
                LEFT JOIN tbl_pokmas c ON c.id_pokmas = b.id_pokmas
                WHERE a.deleted_date IS NULL AND a.id_proposal = '{$id}'";
        if ($this->idGrupUser > 2) {
            $sql .= " AND c.id_grup = '{$this->idGrupUser}'";
        }
        if (strlen($this->year) > 0) {
            $sql .= " AND YEAR(a.created_date) = '{$this->year}'";
        }
        $sql .= " ORDER BY a.id_history DESC";
        $data = $this->db->query($sql);

        return $data->result();
    }

    public function checkForeign($id)
    {
        $isExist = false;
//        if (!$isExist) {
//            $qProposal = "SELECT * FROM tbl_proposal WHERE deleted_date IS NULL AND id_pokmas = '{$id}'";
//            $resProposal = $this->db->query($qProposal)->row();
//            if ($resProposal != null) {
//                $isExist = true;
//            }
//        }

        return $isExist;
    }
}
