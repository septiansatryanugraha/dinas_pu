$(document).ready(function(){    
  $('.ui.dropdown').dropdown();

  $('.ui.form').form({
      fields: {
        nik: {
          identifier: '_nik',
          rules: [
            {
              type   : 'empty',
              prompt : 'Silahkan Masukan NIK'
            },
          ]
        },
        email: {
          identifier  : '_email',
          rules: [
            {
              type   : 'empty',
              prompt : 'Silahkan masukan e-mail'
            },
            {
              type   : 'email',
              prompt : 'Masukan e-mail yang valid'
            }
          ]
        },
        password: {
          identifier  : '_password',
          rules: [
            {
              type   : 'empty',
              prompt : 'Silahkan masukan Password'
            },
            {
              type   : 'minLength[5]',
              prompt : 'Panjang Password minimal 5 karakter'
            }
          ]
        },
        recaptcha: {
          identifier  : 'g-recaptcha-response',
          rules: [
            {
              type   : 'empty',
              prompt : 'Silahkan masukan recaptcha'
            },
          ]
        },
        name: {
          identifier: '_nama_lengkap',
          rules: [
            {
              type   : 'empty',
              prompt : 'Silahkan masukan Nama Lengkap'
            },
            {
              type   : 'minLength[5]',
              prompt : 'Nama Lengkap minimal 5 karakter'
            },
            {
              type   : 'maxLength[35]',
              prompt : 'Nama Lengkap maksimal 35 karakter'
            }
          ]
        },
        gender: {
          identifier: 'kat_dinas',
          rules: [
            {
              type   : 'empty',
              prompt : 'Silahkan Pilih Kategori Dinas / Badan'
            }
          ]
        },
        jabatan: {
          identifier: '_jabatan',
          rules: [
            {
              type   : 'empty',
              prompt : 'Masukan Jabatan User.'
            }
          ]
        }
      }
    });

  $('.message .close').on('click', function() {
    $(this)
      .closest('.message')
      .transition('fade')
    ;
  });
  $('[data-toggle="modal"]').on('click', function(){
    $('.ui.modal.checkout').modal({
      blurring: true
    }).modal('show');
  });

});